# source this script to add path to library/binary path
#
# Author : O. Stezowski
#
if test $# -lt 2 ; then
   echo "Usage: source add2path.sh path [lib][bin][/lib][/bin][-silent]"
   echo " [lib][bin]   : add the given path as it is to library_path/path"
   echo " [/lib][/bin] : add first lib/bin to the given path and add them to library_path/path"
   return 
fi

if [ ! -d "$1" ] ; then
   echo "Error: Directory $1 does not exist"
   return
fi

#
# loop on all arguments to set up the options 
#
DO_LIB_PATH=0
DO_BIN_PATH=0
DO_SILENT=1
#
for VAR in "$@"  
do
	if [[ $VAR == "lib" ]] ; then
		ADD_TO_PATH_LIB="$1" ; DO_LIB_PATH=1
	fi
	if [[ $VAR == "/lib" ]] ; then
		ADD_TO_PATH_LIB="$1/lib" ; DO_LIB_PATH=2
	fi
	if [[ $VAR == "bin" ]] ; then
		ADD_TO_PATH_BIN="$1" ; DO_BIN_PATH=1
	fi
	if [[ $VAR == "/bin" ]] ; then
		ADD_TO_PATH_BIN="$1/bin" ; DO_BIN_PATH=2
	fi
    if [[ $VAR == "-silent" ]] ; then
        DO_SILENT=0
    fi
done

#
# Add to library path
#
if [ ${DO_LIB_PATH} -eq 0 ] ; then
    if [ ${DO_SILENT} -ne 0 ] ; then
        echo "Nothing to add to library_path"
    fi
else
    if [ ${DO_SILENT} -ne 0 ] ; then
        echo "Add to LIBRARY_PATH" $ADD_TO_PATH_LIB
    fi

   	# linux-like
	if [ -n "${LD_LIBRARY_PATH}" ] ; then
  		LD_LIBRARY_PATH=${ADD_TO_PATH_LIB}:${LD_LIBRARY_PATH} ; export LD_LIBRARY_PATH
	else
  		LD_LIBRARY_PATH=${ADD_TO_PATH_LIB}; export LD_LIBRARY_PATH
  	fi
  	# macosx
	if [ -n "${DYLD_LIBRARY_PATH}" ] ; then
  		DYLD_LIBRARY_PATH=${ADD_TO_PATH_LIB}:${DYLD_LIBRARY_PATH}; export DYLD_LIBRARY_PATH
	else
  		DYLD_LIBRARY_PATH=${ADD_TO_PATH_LIB}; export DYLD_LIBRARY_PATH 	
	fi  		
	#legacy HP-UX
	if [ -n "${SHLIB_PATH}" ] ; then
  		SHLIB_PATH=${ADD_TO_PATH_LIB}:${SHLIB_PATH}; export SHLIB_PATH 	
	else
  		SHLIB_PATH=${ADD_TO_PATH_LIB}; export SHLIB_PATH		
	fi  			
	# AIX
	if [ -n "${LIBPATH}" ] ; then
  		LIBPATH=${ADD_TO_PATH_LIB}:${LIBPATH}; export LIBPATH  
	else
  		LIBPATH=${ADD_TO_PATH_LIB}; export LIBPATH   		
	fi 	
fi

#
# Add to path
#
if [ ${DO_BIN_PATH} -eq 0 ] ; then
    if [ ${DO_SILENT} -ne 0 ] ; then
        echo "Nothing to add to path"
    fi
else
    if [ ${DO_SILENT} -ne 0 ] ; then
        echo "Add to PATH" $ADD_TO_PATH_BIN
    fi
	if [ -n "${PATH}" ] ; then
  		PATH=${ADD_TO_PATH_BIN}:${PATH}; export PATH
	else
  		PATH=${ADD_TO_PATH_BIN}; export PATH
  	fi   	
fi

