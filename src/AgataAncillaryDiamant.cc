#ifdef ANCIL
// this code was created by grzegorz jaworski -> tatrofil@slcj.uw.edu.pl
#include "AgataAncillaryDiamant.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Trap.hh"
#include "G4Tet.hh"
#include "G4SubtractionSolid.hh"
#include "G4Box.hh"

#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4PVPlacement.hh"
#include "globals.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

using namespace std;

AgataAncillaryDiamant::AgataAncillaryDiamant(G4String path,G4String nome)
{
  G4String iniPath     = path;
  
  dirName = nome;

  useAbsorbers = true;
  useDelrin    = true;
    
  CentresOfSolids[0]=G4ThreeVector(14.75,0,54.5);
  CentresOfSolids[1]=G4ThreeVector(0,14.75,54.5);
  CentresOfSolids[2]=G4ThreeVector(-14.75,0,54.5);
  CentresOfSolids[3]=G4ThreeVector(0,-14.75,54.5);
  CentresOfSolids[4]=G4ThreeVector(14.75,14.75,54.5);
  CentresOfSolids[5]=G4ThreeVector(-14.75,14.75,54.5);
  CentresOfSolids[6]=G4ThreeVector(-14.75,-14.75,54.5);
  CentresOfSolids[7]=G4ThreeVector(14.75,-14.75,54.5);
  CentresOfSolids[8]=G4ThreeVector(0.,0.,0.);
  CentresOfSolids[9]=G4ThreeVector(0.,0.,0.);
  CentresOfSolids[10]=G4ThreeVector(0.,0.,0.);
  CentresOfSolids[11]=G4ThreeVector(0.,0.,0.);
  CentresOfSolids[12]=G4ThreeVector(22.3341,22.3341,17.6137);
  CentresOfSolids[13]=G4ThreeVector(-22.3341,22.3341,17.6137);
  CentresOfSolids[14]=G4ThreeVector(-22.3341,-22.4184,17.6137);
  CentresOfSolids[15]=G4ThreeVector(22.3341,-22.4184,17.6137);
  CentresOfSolids[16]=G4ThreeVector(29.132,7.06425,19.1415);
  CentresOfSolids[17]=G4ThreeVector(7.06425,29.132,19.1415);
  CentresOfSolids[18]=G4ThreeVector(-7.06425,29.132,19.1415);
  CentresOfSolids[19]=G4ThreeVector(-29.132,7.06425,19.1415);
  CentresOfSolids[20]=G4ThreeVector(-29.132,-7.06425,19.1415);
  CentresOfSolids[21]=G4ThreeVector(-7.06425,-29.132,19.1415);
  CentresOfSolids[22]=G4ThreeVector(7.06425,-29.132,19.1415);
  CentresOfSolids[23]=G4ThreeVector(29.132,-7.06425,19.1415);
  CentresOfSolids[24]=G4ThreeVector(19.1415,7.06425,29.132);
  CentresOfSolids[25]=G4ThreeVector(7.06425,19.1415,29.132);
  CentresOfSolids[26]=G4ThreeVector(-7.06425,19.1415,29.132);
  CentresOfSolids[27]=G4ThreeVector(-19.1415,7.06425,29.132);
  CentresOfSolids[28]=G4ThreeVector(-19.1415,-7.06425,29.132);
  CentresOfSolids[29]=G4ThreeVector(-7.06425,-19.1415,29.132);
  CentresOfSolids[30]=G4ThreeVector(7.06425,-19.1415,29.132);
  CentresOfSolids[31]=G4ThreeVector(19.1415,-7.06425,29.132);
  CentresOfSolids[32]=G4ThreeVector(34.135,7.06425,7.06425);
  CentresOfSolids[33]=G4ThreeVector(29.132,19.1415,7.06425);
  CentresOfSolids[34]=G4ThreeVector(19.1415,29.132,7.06425);
  CentresOfSolids[35]=G4ThreeVector(7.06425,34.135,7.06425);
  CentresOfSolids[36]=G4ThreeVector(-7.06425,34.135,7.06425);
  CentresOfSolids[37]=G4ThreeVector(-19.1415,29.132,7.06425);
  CentresOfSolids[38]=G4ThreeVector(-29.132,19.1415,7.06425);
  CentresOfSolids[39]=G4ThreeVector(-34.135,7.06425,7.06425);
  CentresOfSolids[40]=G4ThreeVector(-34.135,-7.06425,7.06425);
  CentresOfSolids[41]=G4ThreeVector(-29.132,-19.1415,7.06425);
  CentresOfSolids[42]=G4ThreeVector(-19.1415,-29.132,7.06425);
  CentresOfSolids[43]=G4ThreeVector(-7.06425,-34.135,7.06425);
  CentresOfSolids[44]=G4ThreeVector(7.06425,-34.135,7.06425);
  CentresOfSolids[45]=G4ThreeVector(19.1415,-29.132,7.06425);
  CentresOfSolids[46]=G4ThreeVector(29.132,-19.1415,7.06425);
  CentresOfSolids[47]=G4ThreeVector(34.135,-7.06425,7.06425);
  CentresOfSolids[48]=G4ThreeVector(34.135,7.06425,-7.06425);
  CentresOfSolids[49]=G4ThreeVector(29.132,19.1415,-7.06425);
  CentresOfSolids[50]=G4ThreeVector(19.1415,29.132,-7.06425);
  CentresOfSolids[51]=G4ThreeVector(7.06425,34.135,-7.06425);
  CentresOfSolids[52]=G4ThreeVector(-7.06425,34.135,-7.06425);
  CentresOfSolids[53]=G4ThreeVector(-19.1415,29.132,-7.06425);
  CentresOfSolids[54]=G4ThreeVector(-29.132,19.1415,-7.06425);
  CentresOfSolids[55]=G4ThreeVector(-34.135,7.06425,-7.06425);
  CentresOfSolids[56]=G4ThreeVector(-34.135,-7.06425,-7.06425);
  CentresOfSolids[57]=G4ThreeVector(-29.132,-19.1415,-7.06425);
  CentresOfSolids[58]=G4ThreeVector(-19.1415,-29.132,-7.06425);
  CentresOfSolids[59]=G4ThreeVector(-7.06425,-34.135,-7.06425);
  CentresOfSolids[60]=G4ThreeVector(7.06425,-34.135,-7.06425);
  CentresOfSolids[61]=G4ThreeVector(19.1415,-29.132,-7.06425);
  CentresOfSolids[62]=G4ThreeVector(29.132,-19.1415,-7.06425);
  CentresOfSolids[63]=G4ThreeVector(34.135,-7.06425,-7.06425);
  CentresOfSolids[64]=G4ThreeVector(29.132,7.06425,-19.1415);
  CentresOfSolids[65]=G4ThreeVector(7.06425,29.132,-19.1415);
  CentresOfSolids[66]=G4ThreeVector(-7.06425,29.132,-19.1415);
  CentresOfSolids[67]=G4ThreeVector(-7.06425,-29.132,-19.1415);
  CentresOfSolids[68]=G4ThreeVector(7.06425,-29.132,-19.1415);
  CentresOfSolids[69]=G4ThreeVector(29.132,-7.06425,-19.1415);
  CentresOfSolids[70]=G4ThreeVector(19.1415,7.06425,-29.132);
  CentresOfSolids[71]=G4ThreeVector(7.06425,19.1415,-29.132);
  CentresOfSolids[72]=G4ThreeVector(-7.06425,19.1415,-29.132);
  CentresOfSolids[73]=G4ThreeVector(-7.06425,-19.1415,-29.132);
  CentresOfSolids[74]=G4ThreeVector(7.06425,-19.1415,-29.132);
  CentresOfSolids[75]=G4ThreeVector(19.1415,-7.06425,-29.132);
  

  G4double skos=35.26438968*deg;
  for(int i=0;i<76;i++)
    DiaRotMatrix[i].set(0,0,0);
  DiaRotMatrix[12].rotateY(180.*deg);DiaRotMatrix[12].rotateX(-90.*deg+skos);DiaRotMatrix[12].rotateZ( -45.*deg);
  DiaRotMatrix[13].rotateY(180.*deg);DiaRotMatrix[13].rotateX(-90.*deg+skos);DiaRotMatrix[13].rotateZ(+45.*deg);
  DiaRotMatrix[14].rotateY(180.*deg);DiaRotMatrix[14].rotateX(-90.*deg+skos);DiaRotMatrix[14].rotateZ(+135.*deg);
  DiaRotMatrix[15].rotateY(180.*deg);DiaRotMatrix[15].rotateX(-90.*deg+skos);DiaRotMatrix[15].rotateZ(-135.*deg);
  DiaRotMatrix[16].rotateY(90.*deg);DiaRotMatrix[16].rotateX(270.*deg);DiaRotMatrix[16].rotateY(135.*deg);
  DiaRotMatrix[17].rotateX(90.*deg);DiaRotMatrix[17].rotateY(270.*deg);DiaRotMatrix[17].rotateX(45.*deg);
  DiaRotMatrix[18].rotateX(90.*deg);DiaRotMatrix[18].rotateX(45.*deg);DiaRotMatrix[19].rotateY(90.*deg);
  DiaRotMatrix[19].rotateX(180.*deg);DiaRotMatrix[19].rotateY(45.*deg);DiaRotMatrix[20].rotateY(90.*deg);
  DiaRotMatrix[20].rotateX(90.*deg);DiaRotMatrix[20].rotateY(45.*deg);
  DiaRotMatrix[21].rotateX(90.*deg);DiaRotMatrix[21].rotateZ(180.*deg);DiaRotMatrix[21].rotateY(90.*deg);DiaRotMatrix[21].rotateX(-45.*deg);
  DiaRotMatrix[22].rotateX(90.*deg);DiaRotMatrix[22].rotateZ(180.*deg);DiaRotMatrix[22].rotateX(-45.*deg);
  DiaRotMatrix[23].rotateY(90.*deg);DiaRotMatrix[23].rotateY(135.*deg);
  DiaRotMatrix[24].rotateY(90.*deg);DiaRotMatrix[24].rotateX(180.*deg);DiaRotMatrix[24].rotateY(135.*deg);
  DiaRotMatrix[25].rotateX(90.*deg);DiaRotMatrix[25].rotateY(180.*deg);DiaRotMatrix[25].rotateX(45.*deg);
  DiaRotMatrix[26].rotateX(90.*deg);DiaRotMatrix[26].rotateY(90.*deg);DiaRotMatrix[26].rotateX(45.*deg);
  DiaRotMatrix[27].rotateY(90.*deg);DiaRotMatrix[27].rotateX(270.*deg);DiaRotMatrix[27].rotateY(45.*deg);
  DiaRotMatrix[28].rotateY(90.*deg);DiaRotMatrix[28].rotateY(45.*deg);
  DiaRotMatrix[29].rotateX(90.*deg);DiaRotMatrix[29].rotateZ(180.*deg);DiaRotMatrix[29].rotateY(180.*deg);DiaRotMatrix[29].rotateX(-45.*deg);
  DiaRotMatrix[30].rotateX(90.*deg);DiaRotMatrix[30].rotateZ(180.*deg);DiaRotMatrix[30].rotateY(270.*deg);DiaRotMatrix[30].rotateX(-45.*deg);
  DiaRotMatrix[31].rotateY(90.*deg);DiaRotMatrix[31].rotateX(90.*deg);DiaRotMatrix[31].rotateY(135.*deg);
  DiaRotMatrix[32].rotateY(270.*deg);DiaRotMatrix[32].rotateX(180.*deg);
  DiaRotMatrix[33].rotateX(90.*deg);DiaRotMatrix[33].rotateY(180.*deg);DiaRotMatrix[33].rotateZ(-45.*deg);
  DiaRotMatrix[34].rotateX(90.*deg);DiaRotMatrix[34].rotateY(90.*deg);DiaRotMatrix[34].rotateZ(-45.*deg);
  DiaRotMatrix[35].rotateX(90.*deg);DiaRotMatrix[35].rotateY(180.*deg);
  DiaRotMatrix[36].rotateX(90.*deg);DiaRotMatrix[36].rotateY(90.*deg);
  DiaRotMatrix[37].rotateX(90.*deg);DiaRotMatrix[37].rotateY(180.*deg);DiaRotMatrix[37].rotateZ(45.*deg);
  DiaRotMatrix[38].rotateX(90.*deg);DiaRotMatrix[38].rotateY(90.*deg);DiaRotMatrix[38].rotateZ(45.*deg);
  DiaRotMatrix[39].rotateY(90.*deg);DiaRotMatrix[39].rotateX(270.*deg);
  DiaRotMatrix[40].rotateY(90.*deg);
  DiaRotMatrix[41].rotateX(90.*deg);DiaRotMatrix[41].rotateY(180.*deg);DiaRotMatrix[41].rotateZ(135.*deg);
  DiaRotMatrix[42].rotateX(90.*deg);DiaRotMatrix[42].rotateY(90.*deg);DiaRotMatrix[42].rotateZ(135.*deg);
  DiaRotMatrix[43].rotateX(270.*deg);
  DiaRotMatrix[44].rotateX(270.*deg);DiaRotMatrix[44].rotateY(90.*deg);
  DiaRotMatrix[45].rotateX(90.*deg);DiaRotMatrix[45].rotateY(180.*deg);DiaRotMatrix[45].rotateZ(225.*deg);
  DiaRotMatrix[46].rotateX(90.*deg);DiaRotMatrix[46].rotateY(90.*deg);DiaRotMatrix[46].rotateZ(225.*deg);
  DiaRotMatrix[47].rotateY(270.*deg);DiaRotMatrix[47].rotateX(270.*deg);
  DiaRotMatrix[48].rotateY(270.*deg);DiaRotMatrix[48].rotateX(90.*deg);
  DiaRotMatrix[49].rotateX(90.*deg);DiaRotMatrix[49].rotateY(270.*deg);DiaRotMatrix[49].rotateZ(-45.*deg);
  DiaRotMatrix[50].rotateX(90.*deg);DiaRotMatrix[50].rotateZ(-45.*deg);
  DiaRotMatrix[51].rotateX(90.*deg);DiaRotMatrix[51].rotateY(270.*deg);
  DiaRotMatrix[52].rotateX(90.*deg);DiaRotMatrix[53].rotateX(90.*deg);
  DiaRotMatrix[53].rotateY(270.*deg);DiaRotMatrix[53].rotateZ(45.*deg);
  DiaRotMatrix[54].rotateX(90.*deg);DiaRotMatrix[54].rotateZ(45.*deg);
  DiaRotMatrix[55].rotateY(90.*deg);DiaRotMatrix[55].rotateX(180.*deg);
  DiaRotMatrix[56].rotateY(90.*deg);DiaRotMatrix[56].rotateX(90.*deg);
  DiaRotMatrix[57].rotateX(90.*deg);DiaRotMatrix[57].rotateY(270.*deg);DiaRotMatrix[57].rotateZ(135.*deg);
  DiaRotMatrix[58].rotateX(90.*deg);DiaRotMatrix[58].rotateZ(135.*deg);
  DiaRotMatrix[59].rotateX(270.*deg);DiaRotMatrix[59].rotateY(270.*deg);
  DiaRotMatrix[60].rotateX(270.*deg);DiaRotMatrix[60].rotateY(180.*deg);
  DiaRotMatrix[61].rotateX(90.*deg);DiaRotMatrix[61].rotateY(270.*deg);DiaRotMatrix[61].rotateZ(225.*deg);
  DiaRotMatrix[62].rotateX(90.*deg);DiaRotMatrix[62].rotateZ(225.*deg);
  DiaRotMatrix[63].rotateY(270.*deg);
  DiaRotMatrix[64].rotateY(-90.*deg);DiaRotMatrix[64].rotateX(180.*deg);DiaRotMatrix[64].rotateY(45.*deg);
  DiaRotMatrix[65].rotateX(-90.*deg);DiaRotMatrix[65].rotateY(180.*deg);DiaRotMatrix[65].rotateX(135.*deg);
  DiaRotMatrix[66].rotateX(-90.*deg);DiaRotMatrix[66].rotateY(270.*deg);DiaRotMatrix[66].rotateX(135.*deg);
  DiaRotMatrix[67].rotateX(-90.*deg);DiaRotMatrix[67].rotateX(45.*deg);
  DiaRotMatrix[68].rotateX(-90.*deg);DiaRotMatrix[68].rotateY(90.*deg);DiaRotMatrix[68].rotateX(45.*deg);
  DiaRotMatrix[69].rotateY(-90.*deg);DiaRotMatrix[69].rotateX(-90.*deg);DiaRotMatrix[69].rotateY(45.*deg);
  DiaRotMatrix[70].rotateY(-90.*deg);DiaRotMatrix[70].rotateX(90.*deg);DiaRotMatrix[70].rotateY(45.*deg);
  DiaRotMatrix[71].rotateX(-90.*deg);DiaRotMatrix[71].rotateY(90.*deg);DiaRotMatrix[71].rotateX(135.*deg);
  DiaRotMatrix[72].rotateX(-90.*deg);DiaRotMatrix[72].rotateX(135.*deg);
  DiaRotMatrix[73].rotateX(-90.*deg);DiaRotMatrix[73].rotateY(270.*deg);DiaRotMatrix[73].rotateX(45.*deg);
  DiaRotMatrix[74].rotateX(-90.*deg);DiaRotMatrix[74].rotateY(180.*deg);DiaRotMatrix[74].rotateX(45.*deg);
  DiaRotMatrix[75].rotateY(-90.*deg);DiaRotMatrix[75].rotateY(45.*deg);

  name[0]="dia-0-FW1";name[1]="dia-1-FW";name[2]="dia-2-FW3";name[3]="dia-3-FW4";name[4]="dia-4-FW5";
  name[5]="dia-5-FW6";name[6]="dia-6-FW7";name[7]="dia-7-FW8";name[8]="dia-8-DT1a";name[9]="dia-9-DT2a";
  name[10]="dia-10-DT3a";name[11]="dia-11-DT4a";name[12]="dia-12-DT1b";name[13]="dia-13-DT2b";name[14]="dia-14-DT3b";
  name[15]="dia-15-DT4b";name[16]="dia-16-A2";name[17]="dia-17-B1";name[18]="dia-18-B2";name[19]="dia-19-C1";
  name[20]="dia-20-C2";name[21]="dia-21-D1";name[22]="dia-22-D2";name[23]="dia-23-A1";name[24]="dia-24-A3";
  name[25]="dia-25-B4";name[26]="dia-26-B3";name[27]="dia-27-C4";name[28]="dia-28-C3";name[29]="dia-29-D4";
  name[30]="dia-30-D3";name[31]="dia-31-A4";name[32]="dia-32-LF2";name[33]="dia-33-KG1";name[34]="dia-34-KG2";
  name[35]="dia-35-JH1";name[36]="dia-36-JH2";name[37]="dia-37-IN1";name[38]="dia-38-IN2";name[39]="dia-39-HP1";
  name[40]="dia-40-HP2";name[41]="dia-41-GI1";name[42]="dia-42-GI2";name[43]="dia-43-FK1";name[44]="dia-44-FK2";
  name[45]="dia-45-EL1";name[46]="dia-46-EL2";name[47]="dia-47-LF1";name[48]="dia-48-LF3";name[49]="dia-49-KG4";
  name[50]="dia-50-KG3";name[51]="dia-51-JH4";name[52]="dia-52-JH3";name[53]="dia-53-IN4";name[54]="dia-54-IN3";
  name[55]="dia-55-HP4";name[56]="dia-56-HP3";name[57]="dia-57-GI4";name[58]="dia-58-GI3";name[59]="dia-59-FK4";
  name[60]="dia-60-FK3";name[61]="dia-61-EL4";name[62]="dia-62-EL3";name[63]="dia-63-LF4";name[64]="dia-64-E4";
  name[65]="dia-65-M3";name[66]="dia-66-M4";name[67]="dia-67-J3";name[68]="dia-68-J4";name[69]="dia-69-E3";
  name[70]="dia-70-E1";name[71]="dia-71-M2";name[72]="dia-72-M1";name[73]="dia-73-J2";name[74]="dia-74-J1";
  name[75]="dia-75-E2";name[76]="dia-76-T1";name[77]="dia-77-T2";name[78]="dia-78-T3";name[79]="dia-79-T4";


  FWFoilThickness            =  5. * um;//15
  AbsorbersOn45degThickness  =  5. * um;//10
  AbsorbersOn90degThickness  =  5. * um;//5
  AbsorbersOn135degThickness =  5. * um;//5
  
  FoilDistanceToCsI          =  1. * nm;
  DelrinDistanceToCsI        =  1. * nm;

  DelrinThickness            = 10. * mm;
  DelrinTrianglesThickness   = 14. * mm;

  nameCsI    = "CsI";
  matCsI     =  NULL;
  nameTa     = "Tantalum";
  matTa      =  NULL;
  nameDelrin = "Delrin";
  matDelrin  =  NULL;
  
  ancSD = NULL;

  ancName   = G4String("DIAMANT");
  ancOffset = 8000;

  numAncSd = 0;


  myMessenger = new AgataAncillaryDiamantMessenger(this,nome);
}

AgataAncillaryDiamant::~AgataAncillaryDiamant()
{
  delete myMessenger;
}


G4int AgataAncillaryDiamant::FindMaterials()
{
  G4Material* ptMaterial = G4Material::GetMaterial(nameCsI);
  if (ptMaterial)
    {
      matCsI = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matCsI->GetName() << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << nameCsI << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }   
  
  ptMaterial = G4Material::GetMaterial(nameTa);
  if (ptMaterial)
    {
      matTa = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matTa->GetName() << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << nameTa << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }   
  
  ptMaterial = G4Material::GetMaterial(nameDelrin);
  if (ptMaterial)
    {
      matDelrin = ptMaterial;
      G4cout << "\n----> The ancillary detector material is "
	     << matDelrin->GetName() << G4endl;
    }
  else 
    {
      G4cout << " Could not find the material " << nameDelrin << G4endl;
      G4cout << " Could not build the ancillary! " << G4endl;
      return 1;
    }   
  
  return 0;
}


void AgataAncillaryDiamant::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

void AgataAncillaryDiamant::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}

void AgataAncillaryDiamant::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    //    ancSD = new AgataSensitiveDetector( dirName, "/anc/Diamant", "AncCollection", offset, depth, menu );
    ancSD = new AgataSensitiveDetector( dirName, "/anc/all", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  } 
}

void AgataAncillaryDiamant::PlaceQuads()
{
  G4double OuterSquareWidth  =   14.500 * mm;
  G4double InnerSquareWidth  =   13.257 * mm;
  G4double SquareHight       =    3.000 * mm;

  G4double teta,phi;
  teta=atan(sqrt(2.)*(OuterSquareWidth-InnerSquareWidth)/SquareHight/2.);
  phi=45*deg;

  DiamantElement_sd = new G4Trap("Diamant Element", 0.5*SquareHight,teta,phi,
				 0.5*OuterSquareWidth,0.5*OuterSquareWidth,0.5*OuterSquareWidth,0.*deg,
				 0.5*InnerSquareWidth,0.5*InnerSquareWidth,0.5*InnerSquareWidth,0.*deg);
  DiamantElement_log =  new G4LogicalVolume(DiamantElement_sd, matCsI,"diamant_el_log");
  DiamantElement_log -> SetSensitiveDetector(ancSD);
  
  for(int i=16;i<64/*76*/;i++)
    DiamantElement_phys[i]=new G4PVPlacement(G4Transform3D(DiaRotMatrix[i],CentresOfSolids[i]),
					     name[i],DiamantElement_log,DiamantBox_phys,true,i);
}


void AgataAncillaryDiamant::PlaceBackwardTriangles()
{
  G4RotationMatrix rmZero;rmZero.set(0,0,0);
  bool tetbool = false;
  G4Tet* dia76 = new G4Tet("dia-76-T1",G4ThreeVector(4.90254,4.90254,-4.84646),
			   G4ThreeVector(35.309,14.803,-14.769),G4ThreeVector(14.803,14.803,-35.275),G4ThreeVector(14.803,35.309,-14.769),&tetbool);
  G4Tet* dia76_cut = new G4Tet("dia-76-T1",G4ThreeVector(4.90254,4.90254,-4.84646),
			       G4ThreeVector(32.467,13.625,-13.592),G4ThreeVector(13.625,13.625,-32.433),G4ThreeVector(13.625,32.467,-13.592),&tetbool);
  G4SubtractionSolid* final_dia_76 = new G4SubtractionSolid("final-dia-76",dia76,dia76_cut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* dia76_log = new G4LogicalVolume(final_dia_76,matCsI,"diamant_el_log");
  dia76_log->SetSensitiveDetector(ancSD);
  DiamantElement_phys[76] = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0,0,0)),"Dia-76-T1",dia76_log,DiamantBox_phys,true,76);
  
  G4Tet* dia77 = new G4Tet("dia-77-T2",G4ThreeVector(-4.90254,4.90254,-4.84646),
			   G4ThreeVector(-14.803,35.309,-14.769),G4ThreeVector(-14.803,14.803,-35.275),G4ThreeVector(-35.309,14.803,-14.769),&tetbool);
  G4Tet* dia77_cut = new G4Tet("dia-77-T2",G4ThreeVector(-4.90254,4.90254,-4.84646),
			       G4ThreeVector(-13.625,32.467,-13.592),G4ThreeVector(-13.625,13.625,-32.433),G4ThreeVector(-32.467,13.625,-13.592),&tetbool);
  G4SubtractionSolid* final_dia_77 = new G4SubtractionSolid("final-dia-77",dia77,dia77_cut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* dia77_log = new G4LogicalVolume(final_dia_77,matCsI,"diamant_el_log");
  dia77_log->SetSensitiveDetector(ancSD);
  DiamantElement_phys[77] = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0,0,0)),"Dia-77-T2",dia77_log,DiamantBox_phys,true,77);

  G4Tet* dia78 = new G4Tet("dia-78-T3",G4ThreeVector(-4.90254,-4.90254,-4.84646),
			   G4ThreeVector(-14.803,-14.803,-35.275),G4ThreeVector(-14.803,-35.309,-14.769),G4ThreeVector(-35.309,-14.803,-14.769),&tetbool);
  G4Tet* dia78_cut = new G4Tet("dia-78-T3",G4ThreeVector(-4.90254,-4.90254,-4.84646),
			       G4ThreeVector(-13.625,-13.625,-32.433),G4ThreeVector(-13.625,-32.467,-13.592),G4ThreeVector(-32.467,-13.625,-13.592),&tetbool);
  G4SubtractionSolid* final_dia_78 = new G4SubtractionSolid("final-dia-78",dia78,dia78_cut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* dia78_log = new G4LogicalVolume(final_dia_78,matCsI,"diamant_el_log");
  dia78_log->SetSensitiveDetector(ancSD);
  DiamantElement_phys[78] = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0,0,0)),"Dia-78-T3",dia78_log,DiamantBox_phys,true,78);

  G4Tet* dia79 = new G4Tet("dia-79-T4",G4ThreeVector(4.90254,-4.90254,-4.84646),
			   G4ThreeVector(14.803,-35.309,-14.769),G4ThreeVector(35.309,-14.803,-14.769),G4ThreeVector(14.803,-14.803,-35.275),&tetbool);
  G4Tet* dia79_cut = new G4Tet("dia-79-T4",G4ThreeVector(4.90254,-4.90254,-4.84646),
			       G4ThreeVector(13.625,-32.467,-13.592),G4ThreeVector(32.467,-13.625,-13.592),G4ThreeVector(13.625,-13.625,-32.433),&tetbool);
  G4SubtractionSolid* final_dia_79 = new G4SubtractionSolid("final-dia-79",dia79,dia79_cut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* dia79_log = new G4LogicalVolume(final_dia_79,matCsI,"diamant_el_log");
  dia79_log->SetSensitiveDetector(ancSD);
  DiamantElement_phys[79] = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0,0,0)),"Dia-79-T4",dia79_log,DiamantBox_phys,true,79);
}


void AgataAncillaryDiamant::PlaceForwardTriangles(){
  G4double OuterTrapA  =  29.            * mm;
  G4double OuterTrapB  =  18.8953        * mm;
  G4double InnerTrapA  =  26.6466        * mm;
  G4double InnerTrapB  =  17.3269        * mm;
  G4double TrapHight   =   3.            * mm;
  G4double OuterTrapH  =   8.749887652   * mm;
  G4double InnerTrapH  =   8.07024246594 * mm;
  G4double teta        =   6.46258683    * deg;
  G4double fi          = 270.            * deg;

  G4RotationMatrix rmZero;rmZero.set(0.,0.,0.);
  G4Trap* DiamantTrap_sd = new G4Trap("Diamant Element",0.5*TrapHight,teta,fi,0.5*OuterTrapH,0.5*OuterTrapB,0.5*OuterTrapA,0.*deg,
				      0.5*InnerTrapH,0.5*InnerTrapB,0.5*InnerTrapA,0.*deg);
  G4LogicalVolume* DiamantTrap_log =  new G4LogicalVolume(DiamantTrap_sd,matCsI,"diamant_el_log");DiamantTrap_log -> SetSensitiveDetector(ancSD);

  for(int i=12;i<16;i++)
    DiamantElement_phys[i]=new G4PVPlacement(G4Transform3D(DiaRotMatrix[i],CentresOfSolids[i]),name[i],
					     DiamantTrap_log,DiamantBox_phys,true,i);
  bool tetbool = false;

  //small triangles
  G4Tet* dia8 = new G4Tet("dia-8-DT1a",G4ThreeVector(0.783844,0.783844,1.46379),
			  G4ThreeVector(28.001,14.803,22.076),G4ThreeVector(14.803,28.001,22.076),G4ThreeVector(14.803,14.803,35.275),&tetbool);
  G4Tet* dia8_cut = new G4Tet("dia-8-DT1a",G4ThreeVector(0.783844,0.783844,1.46379),
			      G4ThreeVector(10.,30.,19.683),G4ThreeVector(30.,10.,19.683),G4ThreeVector(10.,10.,39.683),&tetbool);
  G4SubtractionSolid* final_dia_8 = new G4SubtractionSolid("final-dia-8",dia8,dia8_cut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* dia8_log = new G4LogicalVolume(final_dia_8,matCsI,"diamant_el_log");
  DiamantElement_phys[8] = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0,0,0)),"Dia-8-DT1a",dia8_log,DiamantBox_phys,true,8);
  G4Tet* dia9 = new G4Tet("dia-9-DT2a",G4ThreeVector(-0.783844,0.783844,1.45289),
			  G4ThreeVector(-28.001,14.803,22.076),G4ThreeVector(-14.803,28.001,22.076),G4ThreeVector(-14.803,14.803,35.275),&tetbool);
  G4Tet* dia9_cut = new G4Tet("dia-9-DT2a",G4ThreeVector(-0.783844,0.783844,1.45289),
			      G4ThreeVector(-30.,10.,19.683),G4ThreeVector(-10.,30.,19.683),G4ThreeVector(-10.,10.,39.683),&tetbool);
  G4SubtractionSolid* final_dia_9 = new G4SubtractionSolid("final-dia-9",dia9,dia9_cut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* dia9_log = new G4LogicalVolume(final_dia_9,matCsI,"diamant_el_log");
  DiamantElement_phys[9] = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0,0,0)),"Dia-9-DT2a",dia9_log,DiamantBox_phys,true,9);
  G4Tet* dia10 = new G4Tet("dia-10-DT3a",G4ThreeVector(-0.783844,-0.783844,1.46379),
			   G4ThreeVector(-14.803,-28.001,22.076),G4ThreeVector(-28.001,-14.803,22.076),G4ThreeVector(-14.803,-14.803,35.275),&tetbool);
  G4Tet* dia10_cut = new G4Tet("dia-10-DT3a",G4ThreeVector(-0.783844,-0.783844,1.46379),
			       G4ThreeVector(-10.,-30.,19.683),G4ThreeVector(-30.,-10.,19.683),G4ThreeVector(-10.,-10.,39.683),&tetbool);
  G4SubtractionSolid* final_dia_10 = new G4SubtractionSolid("final-dia-10",dia10,dia10_cut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* dia10_log = new G4LogicalVolume(final_dia_10,matCsI,"diamant_el_log");
  DiamantElement_phys[10] = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0,0,0)),"Dia-10-DT3a",dia10_log,DiamantBox_phys,true,10);
  G4Tet* dia11 = new G4Tet("dia-11-DT4a",G4ThreeVector(0.783844,-0.783844,1.46379),
			   G4ThreeVector(14.803,-28.001,22.076),G4ThreeVector(28.001,-14.803,22.076),G4ThreeVector(14.803,-14.803,35.275),&tetbool);
  G4Tet* dia11_cut = new G4Tet("dia-11-DT4a",G4ThreeVector(0.783844,-0.783844,1.46379),
			       G4ThreeVector(10.,-30.,19.683),G4ThreeVector(30.,-10.,19.683),G4ThreeVector(10.,-10.,39.683),&tetbool);
  G4SubtractionSolid* final_dia_11 = new G4SubtractionSolid("final-dia-11",dia11,dia11_cut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* dia11_log = new G4LogicalVolume(final_dia_11,matCsI,"diamant_el_log");
  DiamantElement_phys[11] = new G4PVPlacement(G4Transform3D(rmZero,G4ThreeVector(0,0,0)),"Dia-11-DT4a",dia11_log,DiamantBox_phys,true,11);
  dia8_log->SetSensitiveDetector(ancSD);dia9_log->SetSensitiveDetector(ancSD);dia10_log->SetSensitiveDetector(ancSD);dia11_log->SetSensitiveDetector(ancSD);
}

void AgataAncillaryDiamant::PlaceForwardWall(){
  G4Box* DiaFW_box = new G4Box("DiaFW",14.5*mm/2.,14.5*mm/2.,3.*mm/2.);
  G4LogicalVolume* DiaFW_log = new G4LogicalVolume(DiaFW_box,matCsI,"diaFW_log");
  DiaFW_log->SetSensitiveDetector(ancSD);
  //G4VPhysicalVolume* DiaFW[8];
  
  for(int i=0;i<8;i++)
    DiamantElement_phys[i] = new G4PVPlacement(G4Transform3D(DiaRotMatrix[i],CentresOfSolids[i]),name[i],
					       DiaFW_log,DiamantBox_phys,true,i);
}

void AgataAncillaryDiamant::PlaceFWAbsorbers(){
  G4Box* FWBigFoil = new G4Box("FW foil big",44.*mm/2.,44.*mm/2.,FWFoilThickness/2.);
  G4Box* FWCutFoil = new G4Box("FW foil cut",15.*mm/2.,15.*mm/2.,FWFoilThickness);
  G4SubtractionSolid* finalFWFoil = new G4SubtractionSolid("finalFWFoil",FWBigFoil,FWCutFoil,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* FWFoil_log = new G4LogicalVolume(finalFWFoil,matTa,"finalFWFoil_log");
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[0],G4ThreeVector(0.,0.,53.*mm-FWFoilThickness/2.-FoilDistanceToCsI)),
							      "FWFoil",FWFoil_log,DiamantBox_phys,true,0);
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));czerwony->SetVisibility(true);
  FWFoil_log->SetVisAttributes(czerwony);
}

void AgataAncillaryDiamant::PlaceQuadsAbsorbers(){
  G4Box* QuadsFoil45  = new G4Box("quads foil",26.764*mm/2.,26.764*mm/2.,AbsorbersOn45degThickness/2.);
  G4Box* QuadsFoil90  = new G4Box("quads foil",26.764*mm/2.,26.764*mm/2.,AbsorbersOn90degThickness/2.);
  G4Box* QuadsFoil135 = new G4Box("quads foil",26.764*mm/2.,26.764*mm/2.,AbsorbersOn135degThickness/2.);
  G4LogicalVolume* QuadsFoil45_log  = new G4LogicalVolume(QuadsFoil45, matTa,"QuadsFoil45");
  G4LogicalVolume* QuadsFoil90_log  = new G4LogicalVolume(QuadsFoil90, matTa,"QuadsFoil90");
  G4LogicalVolume* QuadsFoil135_log = new G4LogicalVolume(QuadsFoil135,matTa,"QuadsFoil135");
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));czerwony->SetVisibility(true);
  QuadsFoil45_log->SetVisAttributes(czerwony);QuadsFoil90_log->SetVisAttributes(czerwony);QuadsFoil135_log->SetVisAttributes(czerwony);
  //45deg
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[23],G4ThreeVector(23.0707,0,23.0707)),
							   "QuadFoil_A",QuadsFoil45_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[17],G4ThreeVector(0,23.0707,23.0707)),
							   "QuadFoil_B",QuadsFoil45_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[20],G4ThreeVector(-23.0707,0,23.0707)),
							   "QuadFoil_C",QuadsFoil45_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[22],G4ThreeVector(0,-23.0707,23.0707)),
							   "QuadFoil_C",QuadsFoil45_log,DiamantBox_phys,true,0);
  //90deg
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[63],G4ThreeVector(32.627,0.,0.)),
							     "QuadFoil_LF",QuadsFoil90_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[49],G4ThreeVector(23.0703,23.0703,0.)),
							     "QuadFoil_KG",QuadsFoil90_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[51],G4ThreeVector(0.,32.627,0.)),
							     "QuadFoil_JH",QuadsFoil90_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[54],G4ThreeVector(-23.0703,23.0703,0.)),
							     "QuadFoil_IN",QuadsFoil90_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[56],G4ThreeVector(-32.627,0.,0.)),
							     "QuadFoil_HP",QuadsFoil90_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[57],G4ThreeVector(-23.0703,-23.0703,0.)),
							     "QuadFoil_GI",QuadsFoil90_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[60],G4ThreeVector(0.,-32.627,0.)),
							     "QuadFoil_FK",QuadsFoil90_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[62],G4ThreeVector(23.0703,-23.0703,0.)),
							    "QuadFoil_EL",QuadsFoil90_log,DiamantBox_phys,true,0);
  //135deg
  // G4PVPlacement* QuadFoil_E;QuadFoil_E = new G4PVPlacement(G4Transform3D(DiaRotMatrix[69],G4ThreeVector(23.0707,0.,-23.0707)),
  // 							   "QuadFoil_E",QuadsFoil135_log,DiamantBox_phys,true,0);
  // G4PVPlacement* QuadFoil_M;QuadFoil_M = new G4PVPlacement(G4Transform3D(DiaRotMatrix[65],G4ThreeVector(0.,23.0707,-23.0707)),
  // 							   "QuadFoil_M",QuadsFoil135_log,DiamantBox_phys,true,0);
  // G4PVPlacement* QuadFoil_J;QuadFoil_J = new G4PVPlacement(G4Transform3D(DiaRotMatrix[68],G4ThreeVector(0.,-23.0707,-23.0707)),
  // 							   "QuadFoil_J",QuadsFoil135_log,DiamantBox_phys,true,0);
}

void AgataAncillaryDiamant::PlaceBackwardTrianglesAbsorbers(){
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));czerwony->SetVisibility(true);
  G4double skos      = 35.26438968 * deg;G4double hight     = 23.07582423;G4double width     = 26.6466;G4double sth_small =  0.0000001  * nm;
  G4Trap* DiaBackTriangAbs_sd = new G4Trap("Dia Back Triang Abs",0.5*AbsorbersOn135degThickness,0.,0.,
					   0.5*hight,sth_small,0.5*width,0.*deg,0.5*hight,sth_small,0.5*width,0.*deg);
  G4LogicalVolume* DiaBackTriangAbs_log =  new G4LogicalVolume(DiaBackTriangAbs_sd,matTa,"BackTrAbs_log");
  DiaBackTriangAbs_log->SetVisAttributes(czerwony);
  G4RotationMatrix dia76abs;dia76abs.rotateZ(180.*deg);dia76abs.rotateY(180.*deg);dia76abs.rotateX(-90.*deg+skos);dia76abs.rotateZ(+135.*deg);
  new G4PVPlacement(G4Transform3D(dia76abs,G4ThreeVector(18.3309,18.3309,-23.0079)),
								  "Dia76Abs_phys",DiaBackTriangAbs_log,DiamantBox_phys,true,0);
  G4RotationMatrix dia77abs;dia77abs.rotateZ(180.*deg);dia77abs.rotateY(180.*deg);dia77abs.rotateX(-90.*deg+skos);dia77abs.rotateZ(-135.*deg);
  new G4PVPlacement(G4Transform3D(dia77abs,G4ThreeVector(-18.3309,18.3309,-23.0079)),
								  "Dia77Abs_phys",DiaBackTriangAbs_log,DiamantBox_phys,true,0);
  G4RotationMatrix dia78abs;dia78abs.rotateZ(180.*deg);dia78abs.rotateY(180.*deg);dia78abs.rotateX(-90.*deg+skos);dia78abs.rotateZ( -45.*deg);
  new G4PVPlacement(G4Transform3D(dia78abs,G4ThreeVector(-18.3309,-18.3309,-23.0079)),
								  "Dia78Abs_phys",DiaBackTriangAbs_log,DiamantBox_phys,true,0);
  G4RotationMatrix dia79abs;dia79abs.rotateZ(180.*deg);dia79abs.rotateY(180.*deg);dia79abs.rotateX(-90.*deg+skos);dia79abs.rotateZ(+45.*deg);
  new G4PVPlacement(G4Transform3D(dia79abs,G4ThreeVector(18.3309,-18.3309,-23.0079)),
								  "Dia79Abs_phys",DiaBackTriangAbs_log,DiamantBox_phys,true,0);
}

void AgataAncillaryDiamant::PlaceForwardTrianglesAbsorbers(){
  G4VisAttributes* czerwony = new G4VisAttributes( G4Colour(255/255. ,100/255. ,100/255. ));czerwony->SetVisibility(true);
  G4double hight     = 23.07582423;G4double width     = 26.6466;G4double sth_small =  0.0000001  * nm;
  G4Trap* DiaForwTriangAbs_sd = new G4Trap("Dia Forw Triang Abs",0.5*AbsorbersOn45degThickness,0.,0.,
					   0.5*hight,sth_small,0.5*width,0.*deg,0.5*hight,sth_small,0.5*width,0.*deg);
  G4LogicalVolume* DiaForwTriangAbs_log =  new G4LogicalVolume(DiaForwTriangAbs_sd,matTa,"ForwTrAbs_log");
  DiaForwTriangAbs_log->SetVisAttributes(czerwony);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[12],G4ThreeVector(18.3309,18.3309,23.0079)),
								      "Dia8_12Abs_phys",DiaForwTriangAbs_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[13],G4ThreeVector(-18.3309,18.3309,23.0079)),
								      "Dia9_13Abs_phys",DiaForwTriangAbs_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[14],G4ThreeVector(-18.3309,-18.3309,23.0079)),
								       "Dia10_14Abs_phys",DiaForwTriangAbs_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[15],G4ThreeVector(18.3309,-18.3309,23.0079)),
								       "Dia11_15Abs_phys",DiaForwTriangAbs_log,DiamantBox_phys,true,0);
}


void AgataAncillaryDiamant::PlaceFWDelrin(){
  G4VisAttributes* zielony = new G4VisAttributes( G4Colour(100/255. ,100/255. ,0/255. ));//green
  zielony->SetVisibility(true);

  G4Box* FWDelrinBig = new G4Box("FW delrin foil big",44.*mm/2.,44.*mm/2.,DelrinThickness/2.);
  G4Box* FWDelrinCut = new G4Box("FW delrin foil cut",15.*mm/2.,15.*mm/2.,DelrinThickness);
  G4SubtractionSolid* finalFWDelrin = new G4SubtractionSolid("finalFWDelrin",FWDelrinBig,FWDelrinCut,0,G4ThreeVector(0,0,0));
  G4LogicalVolume* FWDelrin_log = new G4LogicalVolume(finalFWDelrin,matDelrin,"finalFWDelrin_log");
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[0],G4ThreeVector(0.,0.,56.*mm+DelrinThickness/2.+DelrinDistanceToCsI)),
								  "FWDelrin",FWDelrin_log,DiamantBox_phys,true,0);
  FWDelrin_log->SetVisAttributes(zielony);
}  
 
void AgataAncillaryDiamant::PlaceQuadsDelrin(){
  G4VisAttributes* zielony = new G4VisAttributes(G4Colour(100/255.,100/255.,0/255.));zielony->SetVisibility(true);
  
  G4Box* QuadsDelrin = new G4Box("quads delrin",29.25*mm/2.,29.25*mm/2.,DelrinThickness/2.);
  G4LogicalVolume* QuadsDelrin_log  = new G4LogicalVolume(QuadsDelrin, matDelrin,"QuadsDelrin");
  QuadsDelrin_log->SetVisAttributes(zielony);
  
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[23],G4ThreeVector(28.733,0.0,28.733)),
							       "QuadDelrin_A",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[17],G4ThreeVector(0.,28.733,28.733)),
							       "QuadDelrin_B",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[20],G4ThreeVector(-28.733,0.,28.733)),
							       "QuadDelrin_C",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[22],G4ThreeVector(0,-28.733,28.733)),
							       "QuadDelrin_C",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[63],G4ThreeVector(40.64,0.,0.)),
								 "QuadDelrin_LF",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[49],G4ThreeVector(28.733,28.733,0.)),
								 "QuadDelrin_KG",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[51],G4ThreeVector(0.,40.64,0.)),
								 "QuadDelrin_JH",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[54],G4ThreeVector(-28.733,28.733,0.)),
								 "QuadDelrin_IN",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[56],G4ThreeVector(-40.64,0.,0.)),
								 "QuadDelrin_HP",QuadsDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[57],
				  G4ThreeVector(-28.733,-28.733,0.)),
		    "QuadDelrin_GI",QuadsDelrin_log,
		    DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[60],
						  G4ThreeVector(0.,-40.64,0.)),
				    "QuadDelrin_FK",QuadsDelrin_log,
				    DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[62],
				  G4ThreeVector(28.733,-28.733,0.)),
		    "QuadDelrin_EL",QuadsDelrin_log,
		    DiamantBox_phys,true,0);
  // G4PVPlacement* QuadDelrin_E;
  // QuadDelrin_E =
  //   new G4PVPlacement(G4Transform3D(DiaRotMatrix[69],
  // 				    G4ThreeVector(28.733,0.,-28.733)),
  // 		      "QuadDelrin_E",QuadsDelrin_log,
  // 		      DiamantBox_phys,true,0);
  // G4PVPlacement* QuadDelrin_M;
  // QuadDelrin_M =
  //   new G4PVPlacement(G4Transform3D(DiaRotMatrix[65],
  // 				    G4ThreeVector(0.,28.733,-28.733)),
  // 		      "QuadDelrin_M",QuadsDelrin_log,
  // 		      DiamantBox_phys,true,0);
  // G4PVPlacement* QuadDelrin_J;
  // QuadDelrin_J =
  //   new G4PVPlacement(G4Transform3D(DiaRotMatrix[68],
  // 				    G4ThreeVector(0.,-28.733,-28.733)),
  // 		      "QuadDelrin_J",QuadsDelrin_log,
  // 		      DiamantBox_phys,true,0);
}

void AgataAncillaryDiamant::PlaceForwardTrianglesDelrin(){
  G4VisAttributes* zielony = new G4VisAttributes(G4Colour(100/255.,100/255.,0/255.));zielony->SetVisibility(true);
  G4double hight     = 25.11473671;G4double width     = 29.;G4double sth_small =  0.0000001  * nm;
  G4Trap* DiaForwTriangDelrin_sd = new G4Trap("Dia Forw Triang Delrin",0.5*DelrinTrianglesThickness,0.,0.,
					      0.5*hight,sth_small,0.5*width,0.*deg,0.5*hight,sth_small,0.5*width,0.*deg);
  G4LogicalVolume* DiaForwTriangDelrin_log =  new G4LogicalVolume(DiaForwTriangDelrin_sd,matDelrin,"ForwTrDelrin_log");
  DiaForwTriangDelrin_log->SetVisAttributes(zielony);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[12],G4ThreeVector(23.9998,23.9998,29.0923)),
									     "Dia8_12Delrin_Phys",DiaForwTriangDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[13],G4ThreeVector(-23.9998,23.9998,29.0923)),
									     "Dia9_13Delrin_Phys",DiaForwTriangDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[14],G4ThreeVector(-23.9998,-23.9998,29.0923)),
									     "Dia10_14Delrin_Phys",DiaForwTriangDelrin_log,DiamantBox_phys,true,0);
  new G4PVPlacement(G4Transform3D(DiaRotMatrix[15],G4ThreeVector(23.9998,-23.9998,29.0923)),
									     "Dia11_15Delrin_Phys",DiaForwTriangDelrin_log,DiamantBox_phys,true,0);
}

void AgataAncillaryDiamant::PlaceBackwardTrianglesDelrin(){
  G4VisAttributes* zielony = new G4VisAttributes(G4Colour(100/255.,100/255.,0/255.));zielony->SetVisibility(true);
  G4double skos      = 35.26438968 * deg;G4double hight     = 25.11473671;G4double width     = 29.;G4double sth_small =  0.0000001  * nm;
  G4Trap* DiaBackTriangDelrin_sd = new G4Trap("Dia Back Triang Abs",0.5*DelrinTrianglesThickness,0.,0.,
					      0.5*hight,sth_small,0.5*width,0.*deg,0.5*hight,sth_small,0.5*width,0.*deg);
  G4LogicalVolume* DiaBackTriangDelrin_log =  new G4LogicalVolume(DiaBackTriangDelrin_sd,matDelrin,"BackTrDelrin_log");
  DiaBackTriangDelrin_log->SetVisAttributes(zielony);
  G4RotationMatrix dia76delrin;dia76delrin.rotateZ(180.*deg);dia76delrin.rotateY(180.*deg);dia76delrin.rotateX(-90.*deg+skos);dia76delrin.rotateZ(+135.*deg);
  new G4PVPlacement(G4Transform3D(dia76delrin,G4ThreeVector(23.9998,23.9998,-29.0923)),
									"Dia76Delrin_phys",DiaBackTriangDelrin_log,DiamantBox_phys,true,0);
  G4RotationMatrix dia77delrin;dia77delrin.rotateZ(180.*deg);dia77delrin.rotateY(180.*deg);dia77delrin.rotateX(-90.*deg+skos);dia77delrin.rotateZ(-135.*deg);
  new G4PVPlacement(G4Transform3D(dia77delrin,G4ThreeVector(-23.9998,23.9998,-29.0923)),
									"Dia77Delrin_phys",DiaBackTriangDelrin_log,DiamantBox_phys,true,0);
  G4RotationMatrix dia78delrin;dia78delrin.rotateZ(180.*deg);dia78delrin.rotateY(180.*deg);dia78delrin.rotateX(-90.*deg+skos);dia78delrin.rotateZ( -45.*deg);
  new G4PVPlacement(G4Transform3D(dia78delrin,G4ThreeVector(-23.9998,-23.9998,-29.0923)),
									"Dia78Delrin_phys",DiaBackTriangDelrin_log,DiamantBox_phys,true,0);
  G4RotationMatrix dia79delrin;dia79delrin.rotateZ(180.*deg);dia79delrin.rotateY(180.*deg);dia79delrin.rotateX(-90.*deg+skos);dia79delrin.rotateZ(+45.*deg);
  new G4PVPlacement(G4Transform3D(dia79delrin,G4ThreeVector(23.9998,-23.9998,-29.0923)),
									"Dia79Delrin_phys",DiaBackTriangDelrin_log,DiamantBox_phys,true,0);
}



G4int AgataAncillaryDiamant::GetSegmentNumber(G4int, G4int, G4ThreeVector)
{
  //just to avoid warnings:
  return 0;
}

void AgataAncillaryDiamant::Placement()
{
  DiamantBox = new G4Box("diamantbox",100*mm,100*mm,100*mm);
  DiamantBox_log = new G4LogicalVolume(DiamantBox,
				       G4Material::GetMaterial("Vacuum"),
				       "diamantbox_log");
  DiamantBox_log->SetVisAttributes(G4VisAttributes::Invisible); 
  G4RotationMatrix* rm= new G4RotationMatrix();
  DiamantBox_phys = new G4PVPlacement(rm,G4ThreeVector(0,0,36*mm),
				      "DiamantBox_Phys",DiamantBox_log,
				      theDetector->HallPhys(),
				      false,0); 
  PlaceQuads();
  if(useAbsorbers)
    PlaceQuadsAbsorbers();
  if(useDelrin)
    PlaceQuadsDelrin();
  
  // PlaceBackwardTriangles();
  // if(useAbsorbers)
  //   PlaceBackwardTrianglesAbsorbers();
  // if(useDelrin)
  //   PlaceBackwardTrianglesDelrin();
  
  PlaceForwardTriangles();
  if(useAbsorbers)
    PlaceForwardTrianglesAbsorbers();
  if(useDelrin)
    PlaceForwardTrianglesDelrin();
  
  PlaceForwardWall();
  if(useAbsorbers)
    PlaceFWAbsorbers();
  if(useDelrin)
    PlaceFWDelrin();

  return;
}


void AgataAncillaryDiamant::ShowStatus()
{}

void AgataAncillaryDiamant::SetUseAbsorbers( G4bool value )
{
  useAbsorbers = value;
  if( useAbsorbers )
    G4cout << " ----> Absorbers in front of CsI Diamant detectors will be placed." << G4endl;
  else  
    G4cout << " ----> Absorbers wont be generated." << G4endl;
}

void AgataAncillaryDiamant::SetUseDelrin( G4bool value )
{
  useDelrin = value;
  if( useDelrin )
    G4cout << " ----> Delrin will be placed behind CsI Diamant detectors." << G4endl;
  else  
    G4cout << " ----> Delrin material wont be placed." << G4endl;
}


#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithADoubleAndUnit.hh"

AgataAncillaryDiamantMessenger::AgataAncillaryDiamantMessenger(AgataAncillaryDiamant* pTarget, G4String name)
  :myTarget(pTarget)
{ 
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/detector/ancillary/Diamant/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of DIAMANT construction.");  

  
  //absorbers
  commandName = directoryName + "enableAbsorbers";
  enableAbsorbers = new G4UIcmdWithABool(commandName, this);
  enableAbsorbers->SetGuidance("Creating Absorbers before CsI detectors.");
  enableAbsorbers->SetGuidance("Required parameters: none.");
  enableAbsorbers->SetParameterName("useAbsorbers",true);
  enableAbsorbers->SetDefaultValue(true);
  enableAbsorbers->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableAbsorbers";
  disableAbsorbers = new G4UIcmdWithABool(commandName, this);
  disableAbsorbers->SetGuidance("Absrobers wont be generated.");
  disableAbsorbers->SetGuidance("Required parameters: none.");
  disableAbsorbers->SetParameterName("useAbsorbers",true);
  disableAbsorbers->SetDefaultValue(false);
  disableAbsorbers->AvailableForStates(G4State_PreInit,G4State_Idle);

  //delrin
  commandName = directoryName + "enableDelrin";
  enableDelrin = new G4UIcmdWithABool(commandName, this);
  enableDelrin->SetGuidance("Creating Delrin behind CsI detectors.");
  enableDelrin->SetGuidance("Required parameters: none.");
  enableDelrin->SetParameterName("useDelrin",true);
  enableDelrin->SetDefaultValue(true);
  enableDelrin->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableDelrin";
  disableDelrin = new G4UIcmdWithABool(commandName, this);
  disableDelrin->SetGuidance("Delrin material wont be placed.");
  disableDelrin->SetGuidance("Required parameters: none.");
  disableDelrin->SetParameterName("useDelrin",true);
  disableDelrin->SetDefaultValue(false);
  disableDelrin->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataAncillaryDiamantMessenger::~AgataAncillaryDiamantMessenger()
{
  delete enableAbsorbers;
  delete disableAbsorbers;
  delete enableDelrin;
  delete disableDelrin;
}

void AgataAncillaryDiamantMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == enableAbsorbers ) 
    myTarget->SetUseAbsorbers( enableAbsorbers->GetNewBoolValue(newValue) );
  if( command == disableAbsorbers ) 
    myTarget->SetUseAbsorbers( disableAbsorbers->GetNewBoolValue(newValue) );

  if( command == enableDelrin ) 
    myTarget->SetUseDelrin( enableDelrin->GetNewBoolValue(newValue) );
  if( command == disableDelrin ) 
    myTarget->SetUseDelrin( disableDelrin->GetNewBoolValue(newValue) );
}

#endif
