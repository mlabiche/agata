#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"
#include "G4GeometryManager.hh"
#include "G4PhysicalVolumeStore.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4SolidStore.hh"
#include "G4UImanager.hh"

#include "G4GDMLParser.hh"


#ifdef ADCA
#include "AgataDetectorAdca.hh"
#endif

#ifdef GASP
#include "AgataDetectorGasp.hh"
#else
 #ifdef CLARA
 #include "AgataDetectorClara.hh"
 #else
  #ifdef POLAR
  #include "AgataDetectorPolar.hh"
  #include "AgataDetectorClover.hh"
  #else
   #ifdef EUCLIDES
   #include "AgataDetectorEuclides.hh"
   #else
    #include "AgataDetectorArray.hh"
    #ifndef DEIMOS
     #include "AgataDetectorShell.hh"
     #include "AgataDetectorSimple.hh"
     #include "AgataDetectorTest.hh"
    #endif
   #endif
  #endif
 #endif
#endif

#include "OrgamDetector.hh"
#include "GalileoDetector.hh"
#include "Nuball2Detector.hh"

#include "G4Material.hh"
#include "G4MaterialTable.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"
#include "G4VPhysicalVolume.hh"

#include "G4NistManager.hh"


using namespace std;

AgataDetectorConstruction::AgataDetectorConstruction( G4int gtype, G4int atype, G4String path, G4bool vol, G4String name, G4bool toscore )
{
  char aLine[128];
  printf(aLine, "1 %d", atype);
  MakeScoreSphere=toscore;
  this->InitData( gtype, G4String(aLine), path, vol, name ); 
}


AgataDetectorConstruction::AgataDetectorConstruction( G4int gtype, G4String atype, G4String path, G4bool vol, G4String name, G4bool toscore )
{
  MakeScoreSphere=toscore;
  this->InitData( gtype, atype, path, vol, name ); 
}

void AgataDetectorConstruction::InitData( G4int gtype, G4String atype, G4String path, G4bool vol, G4String name )
{
  geomType = gtype;
  ancType  = atype;
  volume   = vol;

  if( path.find( "./", 0 ) != string::npos ) {
    G4int position = path.find( "./", 0 );
    if( position == 0 )
      path.erase( position, 2 );
  }
  iniPath = path;

  calcOmega = false;

#ifdef GASP
  newGasp        = NULL;
#else
#ifdef CLARA
  newClara       = NULL;
#else  
#ifdef POLAR
  newPolar       = NULL;
  newClover      = NULL;
#else  
#ifdef EUCLIDES
  newEuclides    = NULL;
#else
#ifdef DEIMOS
  newArray       = NULL;
#else  
  newArray       = NULL;
  newShell       = NULL;
  newSimple      = NULL;
#endif
#endif
#endif
#endif  
#endif  
  
  theConstructed = NULL;
  hallPhys = 0;  
  geSD           = NULL;

  matWorld       = NULL;
  matTarget      = NULL;
  matProductionTarget = NULL;
  matChamber     = NULL;
  matFronting = NULL;
  matBacking = NULL;

  matWorldName   = "Vacuum";
  matTargetName  = "Vacuum";
  matProductionTargetName = "Vacuum";
  matFrontingName = "Vacuum";
  matBackingName = "Vacuum";
  matChamberName = "Vacuum";
  matDegraderName= "Vacuum";
  matSupportName = "Vacuum";
  
  outRadius      = 10.0*cm; 
  wallThickness  =  2.0*mm; 
  pipeRadius     =  1.8*cm; 
  
  thetaChamb     = 0.;
  phiChamb       = 0.;
  
  targetSize.set( 2.0 *cm, 2.0 *cm, 1.0 *mm ); // full lengths
  targetThickness = 1.0*mg/cm2; 
  targetPosition.set( 0., 0., 0.);
  productiontargetPosition.set(0,0,0);
  productiontargetThickness = 0*mg/cm2;
  productiontargetSize.set(0,0,0);
 

  degraderSize.set( 2.0 *cm, 2.0 *cm, 1.0 *mm ); // full lengths
  degraderThickness = 1.0*mg/cm2; 
  supportSize.set( 2.0 *cm, 2.0 *cm, 1.0 *mm ); // full lengths
  supportThickness = 1.0*mg/cm2; 
  degraderPosition.set( 0., 0., 0.);
  separation = 0.;
  tiltAngle  = 0.;
  normal2Target = G4ThreeVector(0., 0., 1.);
  nStepTarget = 10;
  nStepDegrader = 10;
  builtTarget   = false;
  builtDegrader = false;
  builtSupport  = false;
  buildSpectrometer = false;
  AddVAMOSDetection = false;
  SpecOpeningTheta=10.0*deg;
  SpecOpeningPhi=7.0*deg;
  builtProductionTarget = false;
  ancillaryOffset = 0;
  
  directoryName = name;
  
  myMessenger = new AgataDetectorConstructionMessenger(this, volume, name);
}


AgataDetectorConstruction::~AgataDetectorConstruction()
{
   delete  myMessenger;
   
   if( volume ) {
     delete [] numHits;
     for(G4int i=0; i<180; i++){
       delete []histoThetaCoincDetperDet[i];
     }
     delete []histoThetaCoincDetperDet;
   }
}



G4VPhysicalVolume* AgataDetectorConstruction::Construct()
{
  // Cleanup old geometry
  //
//  G4GeometryManager::GetInstance()->OpenGeometry();
//  G4PhysicalVolumeStore::GetInstance()->Clean();
//  G4LogicalVolumeStore::GetInstance()->Clean();
//  G4SolidStore::GetInstance()->Clean();
     G4GeometryManager::GetInstance()->OpenGeometry();
     G4PhysicalVolumeStore::GetInstance()->Clean();
     G4LogicalVolumeStore::GetInstance()->Clean();
     G4SolidStore::GetInstance()->Clean();

  DefineMaterials();

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !geSD ) {
    geSD = new AgataSensitiveDetector( directoryName, "/ge/all", "GeCollection" );
    SDman->AddNewDetector( geSD );
  }

  ConstructHall();

  if(geomType>=100) geomType-=100; //Remove offset to tell to create score geom
#ifdef ADCA
  newAdca = new AgataDetectorAdca( geomType, iniPath, directoryName );
  theConstructed = (AgataDetectorConstructed*)newAdca;
#endif
#ifdef GASP
  newGasp = new AgataDetectorGasp( geomType, iniPath, directoryName );
  theConstructed = (AgataDetectorConstructed*)newGasp;
#else
#ifdef CLARA
  newClara = new AgataDetectorClara(iniPath, directoryName );
  theConstructed = (AgataDetectorConstructed*)newClara;
#else
#ifdef EUCLIDES
  newEuclides = new AgataDetectorEuclides(iniPath, directoryName);
  theConstructed = (AgataDetectorConstructed*)newEuclides;
#else  
#ifdef POLAR
  switch( geomType )
  {
    case 0:
      newPolar = new AgataDetectorPolar(iniPath, directoryName );
      theConstructed = (AgataDetectorConstructed*)newPolar;
      break;
    case 1:
      newClover = new AgataDetectorClover(iniPath, directoryName );
      theConstructed = (AgataDetectorConstructed*)newClover;
      break;
  }      
#else
#ifdef DEIMOS
  newArray       = new AgataDetectorArray( ancType, iniPath, directoryName );
  theConstructed = (AgataDetectorConstructed*)newArray;
#else
  switch( geomType )
    {
    case 0:
      newArray       = new AgataDetectorArray( ancType, iniPath, directoryName );
      theConstructed = (AgataDetectorConstructed*)newArray;
      break;
    case 1:
      newShell       = new AgataDetectorShell(directoryName);
      theConstructed = (AgataDetectorConstructed*)newShell;
      break;  
    case 2:
      newSimple      = new AgataDetectorSimple(directoryName);
      theConstructed = (AgataDetectorConstructed*)newSimple;
      break;
    case 3:
      newTestHall = new AgataDetectorTest(ancType, iniPath,directoryName);
      theConstructed = (AgataDetectorConstructed*)newTestHall;
      break;
    case 4:
      newOrgam = new OrgamDetector( ancType, iniPath, directoryName);
      theConstructed = (AgataDetectorConstructed*)newOrgam;
      break;    
    case 5:
      newGalileo = new GalileoDetector( ancType, iniPath, directoryName);
      theConstructed = (AgataDetectorConstructed*)newGalileo;
      break;
    case 6:
      newNuball2 = new Nuball2Detector( ancType, iniPath, directoryName);
      theConstructed = (AgataDetectorConstructed*)newNuball2;
    default:
      break;  
    }
#endif
#endif
#endif
#endif
#endif

  theConstructed->Placement();


  if( volume )
    numHits = new G4int[theConstructed->GetMaxDetectorIndex()+1];

  return hallPhys;
  
}

//////////////////////////////////////////////////////////////////////
/// In this method, the materials used by "fancy" ancillary detectors
/// are defined. By default most of the materials are not loaded to 
/// save time at initialization. Notice that also the G4Elements already
/// defined in DefineMaterials() must be redefined here. This method is
/// used only if the ANCIL compilation flag is uncommented.
///////////////////////////////////////////////////////////////////////
void AgataDetectorConstruction::DefineAdditionalMaterials()
{
#ifdef ANCIL
  G4NistManager* nistManager = G4NistManager::Instance();
  G4double a, z, density;
  G4double fractionmass;
  G4String name, symbol;
  G4int    nelements, natoms, nprot, nnucl;
  G4int    ncomponents;

  std::vector<G4Element*>  myElements;    // save pointers here to avoid
  std::vector<G4Material*> myMaterials;   // warnings of unused variables

  G4Element* elO  = new G4Element(name="Oxigen",   symbol="O",  z=8.,  a= 15.9994 *g/mole);
  myElements.push_back(elO);

  G4Element* elH  = new G4Element(name="Hydrogen", symbol="H",  z=1.,  a= 1.007940*g/mole);
  myElements.push_back(elH);

  G4Element* elC  = new G4Element(name="Carbon",   symbol="C",  z=6.,  a= 12.00000*g/mole);
  myElements.push_back(elC);

  G4Element* elSi = new G4Element(name="Silicon",  symbol="Si", z=14., a= 28.0855*g/mole);
  myElements.push_back(elSi);

  G4Element* elN  = new G4Element(name="Nitrogen",symbol="N" , z= 7., a=14.01*g/mole);
  myElements.push_back(elN);

  G4Element* elMo  = new G4Element(name="Molybdenum",symbol="Mo" , z=42., a=95.94*g/mole);
  myElements.push_back(elMo);

  
  // Germanium isotopes
  G4Isotope* Ge70 = new G4Isotope(name="Ge70", nprot=32, nnucl=70, a=69.9242*g/mole);
  G4Isotope* Ge72 = new G4Isotope(name="Ge72", nprot=32, nnucl=72, a=71.9221*g/mole);
  G4Isotope* Ge73 = new G4Isotope(name="Ge73", nprot=32, nnucl=73, a=72.9235*g/mole);
  G4Isotope* Ge74 = new G4Isotope(name="Ge74", nprot=32, nnucl=74, a=73.9212*g/mole);
  G4Isotope* Ge76 = new G4Isotope(name="Ge76", nprot=32, nnucl=76, a=75.9214*g/mole);
  // germanium defined via its isotopes
  G4Element* elGe = new G4Element(name="Germanium",symbol="Ge", nelements=5);
  elGe->AddIsotope(Ge70, 0.2123);
  elGe->AddIsotope(Ge72, 0.2766);
  elGe->AddIsotope(Ge73, 0.0773);
  elGe->AddIsotope(Ge74, 0.3594);
  elGe->AddIsotope(Ge76, 0.0744);
  myElements.push_back(elGe);
  
  G4Element* elNa = new G4Element(name="Sodium",   symbol="Na", z=11., a= 22.98977*g/mole);
  myElements.push_back(elNa);

  G4Element* elFe = new G4Element(name="Iron",     symbol="Fe", z=26.,  a= 55.845*g/mole);
  myElements.push_back(elFe);
  
  G4Element* elI  = new G4Element(name="Iodine",   symbol="I" , z=53., a=126.90477*g/mole);
  myElements.push_back(elI);

  G4Element* elCs  = new G4Element(name="Cesium",   symbol="Cs" , z=55., a=132.90545*g/mole);
  myElements.push_back(elCs);

  G4Element* elPb = new G4Element(name="Lead",     symbol="Pb", z=82., a=207.2    *g/mole);
  myElements.push_back(elPb);

  G4Element* elBi = new G4Element(name="Bismuth",  symbol="Bi", z=83., a=208.98038*g/mole);
  myElements.push_back(elBi);
  
  G4Element* elLa  = new G4Element(name="Lantanum",  symbol="La", z=57., a=138.9055*g/mole);
  myElements.push_back(elLa);

  G4Element* elBr  = new G4Element(name="Bromine", symbol="Br", z=35., a=79.904*g/mole);
  myElements.push_back(elBr);

  G4Element* elBa  = new G4Element(name="Barium", symbol="Ba",  z=56., a=137.327*g/mole);
  myElements.push_back(elBa);

  G4Element* elF  = new G4Element(name="Fluorine", symbol="F",  z=9., a=18.9984032*g/mole);
  myElements.push_back(elF);

  G4Element* elCe  = new G4Element(name="Cerium", symbol="Ce",  z=58., a=140.115*g/mole);
  myElements.push_back(elBr);

  G4Element* elMg  = new G4Element(name="Magnesium",symbol="Mg", z=12., a=24.305*g/mole);
  myElements.push_back(elMg);

  G4Element* elK   = new G4Element(name="Potassium",symbol="K", z=19., a=39.0983*g/mole);
  myElements.push_back(elK);

  G4Element* elCa  = new G4Element(name="Calcium",symbol="Ca", z=20., a=40.078*g/mole);
  myElements.push_back(elCa);



  // hydrogen isotopes
  G4Isotope* H2 = new G4Isotope("H2", nprot=1., nnucl=2, a=2.014101*g/mole);
  G4Isotope* H1 = new G4Isotope("H1", nprot=1., nnucl=1, a=1.007940*g/mole);
  G4Element* elD = new G4Element("Deuterium",symbol="D",nelements=2);
  elD->AddIsotope(H2, 99.1228*perCent);  // % Abundance
  elD->AddIsotope(H1,  0.8772*perCent);  // % Abundance
  myElements.push_back(elD);

  G4Material* W  = new G4Material(name="Tungsten",  z=74., a=183.850  *g/mole, density=19.3*g/cm3);
  myMaterials.push_back(W);

  G4Material *Nd = new G4Material(name="Neodymium",z=60.,a=144.24*g/mole,density=6.8*g/cm3);
  myMaterials.push_back(Nd);
// Add EnD to BC-537 with D:C ratio 0.99. Get proper ratio of atoms if you
// say there are 99 enD atoms for every 100 Carbon atoms
// Use same definition for Carbon (natural) as given above.
  G4Material* BC537 = new G4Material("BC537", density = 0.954*g/cm3, nelements=2);
  BC537->AddElement(elC, natoms=100);    // Data Sheet - D:C = 0.99
  BC537->AddElement(elD, natoms= 99);
  myMaterials.push_back(BC537);
  
  G4Material* BC501A = new G4Material(name="BC501A", density=0.874*g/cm3, nelements=2);
  BC501A->AddElement(elC, 90.779*perCent); // Note here: 90.779% by weight
  BC501A->AddElement(elH,  9.221*perCent); // Note here:  9.221% by weight
  myMaterials.push_back(BC501A);
  
  G4Material* matD = new G4Material("matDeuter", density = 0.1624*g/cm3, nelements=1);
  matD->AddElement(elD, 100.*perCent);
  myMaterials.push_back(matD);
  
  
  // some crap!
  // ?  //gj 2010-05-20 defining BC501 for NORDBALL neutron dets
  // ?  G4Material* BC501 = new G4Material("BC537", density = 0.901*g/cm3, nelements=2);
  // ?  BC501->AddElement(elH, natoms=1287);
  // ?  BC501->AddElement(elC, natoms=1000);
  // ?  myMaterials.push_back(BC501);

  G4Material* BaF2 = new G4Material(name="BaF2", density=4.89*g/cm3, nelements=2);
  BaF2->AddElement(elBa, natoms=1);
  BaF2->AddElement(elF,  natoms=2);
  myMaterials.push_back(BaF2);

  G4Material* Upilex = new G4Material(name="Upilex", density=1.470*g/cm3, ncomponents=4);
  Upilex->AddElement(elH, natoms=6);
  Upilex->AddElement(elO, natoms=4);
  Upilex->AddElement(elC, natoms=16);
  Upilex->AddElement(elN, natoms=2);
  myMaterials.push_back(Upilex);
  
  G4Material* Quarz = new G4Material(name="Quarz", density=2.66*g/cm3, nelements=2);
  Quarz->AddElement(elSi, natoms=1);
  Quarz->AddElement(elO,  natoms=2);
  myMaterials.push_back(Quarz);

 
  G4Material* Epoxy = new G4Material(name="Epoxy", density=1.470*g/cm3, ncomponents=3);
  Epoxy->AddElement(elH, natoms=18);
  Epoxy->AddElement(elO, natoms= 4);
  Epoxy->AddElement(elC, natoms=21);
  myMaterials.push_back(Epoxy);
  
  G4Material* Cu = new G4Material(name="Copper", z=29., a=63.546*g/mole, density=8.920*g/cm3);
  myMaterials.push_back(Cu);
  
  G4Material* FR4 = new G4Material(name="FR4", density=1.670*g/cm3, ncomponents=3);
  FR4->AddMaterial(Quarz, fractionmass=70.*perCent);
  FR4->AddMaterial(Epoxy, fractionmass=22.*perCent);
  FR4->AddMaterial(Cu,    fractionmass= 8.*perCent);
  myMaterials.push_back(FR4);

  G4Material* Pb = new G4Material(name="Lead",      z=82., a=207.2    *g/mole, density=11.342 *g/cm3);
  myMaterials.push_back(Pb);
  
  G4Material* Fe = new G4Material(name="Iron",      z=26., a=55.845   *g/mole, density=7.874*g/cm3);
  myMaterials.push_back(Fe);
  
  G4Material* Steel = new G4Material(name="Steel", density=7.86*g/cm3, nelements=2);
  Steel->AddElement(elFe, fractionmass=98.5*perCent);
  Steel->AddElement(elC,  fractionmass= 1.5*perCent);
  myMaterials.push_back(Steel);

  G4Material* NaI = new G4Material(name="NaI", density=3.67*g/cm3, nelements=2);
  NaI->AddElement(elNa, natoms=1);
  NaI->AddElement(elI , natoms=1);
  myMaterials.push_back(NaI);

  G4Material* CsI = new G4Material(name="CsI", density=4.51*g/cm3, nelements=2);
  CsI->AddElement(elCs, natoms=1);
  CsI->AddElement(elI , natoms=1);
  myMaterials.push_back(CsI);

  G4Material* BGO = new G4Material(name="BGO", density=7.13*g/cm3, nelements=3);
  BGO->AddElement(elBi, natoms=4);
  BGO->AddElement(elGe, natoms=3);
  BGO->AddElement(elO , natoms=12);
  myMaterials.push_back(BGO);
  
  G4Material* plastic = new G4Material(name="Plastic", density=0.900*g/cm3, nelements=2);
  plastic->AddElement(elC, natoms=3);
  plastic->AddElement(elH, natoms=6);
  myMaterials.push_back(plastic);

  //NWall liquid scintillator
  G4Material* liqSci = new G4Material(name="liqSci", density=0.870*g/cm3, nelements=2);
  liqSci->AddElement(elC,natoms= 8);
  liqSci->AddElement(elH,natoms=10);
  myMaterials.push_back(liqSci);

  //material for RFD
  G4Material* Mylar = new G4Material("Mylar", density= 1.397*g/cm3, ncomponents=3);
  Mylar->AddElement(elC, natoms=10);
  Mylar->AddElement(elH, natoms= 8);
  Mylar->AddElement(elO, natoms= 4);
  myMaterials.push_back(Mylar);
  
  //materials for Diamant:
  G4Material* Ta = new G4Material(name="Tantalum", z=73.,a=178.945929*g/mole, density=16.6*g/cm3);
  myMaterials.push_back(Ta);
  
  G4Material* Delrin = new G4Material(name="Delrin", density=1.415*g/cm3, nelements=3);
  Delrin->AddElement(elC, natoms=1);
  Delrin->AddElement(elO, natoms=1);
  Delrin->AddElement(elH, natoms=2);
  myMaterials.push_back(Delrin);

  //materials for PrismaFP
  G4double temperature= 20*kelvin;
  G4double pressure= 800*pascal;  // 8 mBar
  G4Material* isoBut = new G4Material("isoButane", density = 0.675*kg/m3, nelements=2, kStateGas, temperature, pressure);  // 7-8 mBar
  isoBut->AddElement(elC, natoms=4);    
  isoBut->AddElement(elH, natoms= 10);
  myMaterials.push_back(isoBut);

  pressure=10000*pascal; // 100 mBar
  G4Material* Methane = new G4Material("Methane", density = 2.5*kg/m3, nelements=2, kStateGas, temperature, pressure);  // 100 mBar
  Methane->AddElement(elC, natoms=1);    
  Methane->AddElement(elH, natoms= 4);
  myMaterials.push_back(Methane);

  
  //materials for TRACE
  G4Material* kapton  = new G4Material(name="Kapton", density=1.42*g/cm3, nelements= 4);
  kapton->AddElement(elH, natoms=10);
  kapton->AddElement(elC, natoms=22);
  kapton->AddElement(elN, natoms=2);
  kapton->AddElement(elO, natoms=5);
  myMaterials.push_back(kapton);
  
  G4Material* PCB  = new G4Material(name="Vetronite", density=2.*g/cm3, nelements= 2);//vetro epossidico
  PCB->AddElement(elSi, natoms=1);
  PCB->AddElement(elO , natoms=2); 
  myMaterials.push_back(PCB);
  
  G4Material* LaBr3  = new G4Material(name="LaBr3", density=5.29*g/cm3, nelements= 3);
  LaBr3->AddElement(elLa, natoms= 7);
  LaBr3->AddElement(elBr, natoms=21); 
  LaBr3->AddElement(elCe, natoms= 1); 
  myMaterials.push_back(LaBr3);

  /// new materials for NArray 04/06/2008
  G4Material* Paraffin = new G4Material(name="Paraffin", density=0.9*g/cm3, ncomponents=2);
  Paraffin->AddElement(elH, natoms=52);
  Paraffin->AddElement(elC, natoms=25);
  myMaterials.push_back(Paraffin);

  G4Material* Polypropylene = new G4Material(name="Polypropylene", density=0.855*g/cm3, ncomponents=2);
  Polypropylene->AddElement(elH, natoms=6);
  Polypropylene->AddElement(elC, natoms=3);
  myMaterials.push_back(Polypropylene);

  // Additional material Diamond and FastPlastic for Lycca
  G4Material * Diamond = new G4Material("Diamond", z=6., a = 12.0*g/mole, density=3.5*g/cm3);
  myMaterials.push_back(Diamond);

  G4Material* FastPlastic = new G4Material(name="FastPlastic", density=1.032*g/cm3, nelements=2);
  FastPlastic->AddElement(elC, natoms=9);
  FastPlastic->AddElement(elH, natoms=10);
  myMaterials.push_back(FastPlastic);

  //absorbers for rising array
  G4Material* Tin = new G4Material(name="Tin", z=50., a= 118.71*g/mole, density=7.31*g/cm3);
  myMaterials.push_back(Tin);


  // materials for EXOGAM
  G4Material* Al = new G4Material(name="Aluminum", z=13., a=26.981538*g/mole, density=2.7*g/cm3);
  myMaterials.push_back(Al);

  // carbon as a material; gj:2011.02.10
  G4Material* matC = new G4Material(name="matCarbon", z=6., a=12.*g/mole, density=2.26*g/cm3);
  myMaterials.push_back(matC);

  //density as for liquid hydrogen
  //G4Material* matH = new G4Material(name="matHydrogen", z=1., a=1.007940*g/mole, density=0.071*g/cm3);
  //  G4Material* matH = new G4Material(name="matHydrogen", z=1., a=1.007940*g/mole, density=2.4485*0.1624*g/cm3);
  G4Material* matH = new G4Material(name="matHydrogen", z=1., a=1.007940*g/mole, density=1.2243*0.1624*g/cm3);
  myMaterials.push_back(matH);
    


  G4Element* elAl  = new G4Element(name="Aluminum",symbol="Al", z=13., a=26.981538*g/mole);
  myElements.push_back(elAl);

  G4Element* elW = new G4Element(name="Tungsten"  ,symbol="W", z=74., a=183.85*g/mole);
  myElements.push_back(elW);
  
  G4Element* elNi = new G4Element(name="Nickel"  ,symbol="Ni", z=28., a=58.6934*g/mole);
  myElements.push_back(elNi);

  G4Element* elCu  = new G4Element(name="Copper",symbol="Cu", z=29., a=63.546*g/mole);
  myElements.push_back(elCu);
  
  //Heavymet
  density = 18.*g/cm3;

  G4Material* Heavymet = new G4Material(name="Heavymet", density, ncomponents=3);
  Heavymet->AddElement(elW,  .80);
  Heavymet->AddElement(elNi, .13);
  Heavymet->AddElement(elCu, .07);
  myMaterials.push_back(Heavymet);

  G4Material* Mumetal = new G4Material(name="Mumetal", density=8.7*g/cm3, nelements=4);
  Mumetal->AddElement(elNi, fractionmass=77.*perCent);
  Mumetal->AddElement(elFe, fractionmass=16.*perCent);
  Mumetal->AddElement(elCu, fractionmass=5.*perCent);
  Mumetal->AddElement(elMo, fractionmass=2.*perCent);
  myMaterials.push_back(Mumetal);
 

  //for layer:
  G4Material* Zirconium90 = new G4Material(name="Zirconium90", z=40., a=91.224*g/mole, density=6.506*g/cm3);
  myMaterials.push_back(Zirconium90);

  G4Element* elZr = new G4Element(name="Zirconium",  symbol="Zr", z=40., a= 91.224*g/mole);
  myElements.push_back(elZr);

  G4Material* Silicon28 = new G4Material(name="Silicon28", z=28., a=28.0855*g/mole, density=2.329*g/cm3);
  myMaterials.push_back(Silicon28);

  //  Concrete  
  density = 2.3*g/cm3;
  G4Material* Concrete = new G4Material(name="Concrete", density, ncomponents=10);
  Concrete->AddElement(elH , .01);
  Concrete->AddElement(elC , .001);
  Concrete->AddElement(elO , .529107);
  Concrete->AddElement(elNa, .016);
  Concrete->AddElement(elMg, .002);
  Concrete->AddElement(elAl, .033872);
  Concrete->AddElement(elSi, .337021);
  Concrete->AddElement(elK , .013);
  Concrete->AddElement(elCa, .044);
  Concrete->AddElement(elFe, .014);
  myMaterials.push_back(Concrete);

  density = 7.1*g/cm3;
  std::vector<G4String> elLYSO={"Lu","Y","Si","O"};
  std::vector<G4int> nbatomsLYSO={9,10,5,25};
  nistManager->ConstructNewMaterial("LYSO",elLYSO,nbatomsLYSO,density);

#endif  
}

void AgataDetectorConstruction::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  outFileLMD << "GEOMETRY" << G4endl;
  theConstructed->WriteHeader(outFileLMD,unit);
  outFileLMD << "ENDGEOMETRY" << G4endl;
}

void AgataDetectorConstruction::ShowStatus()
{
  theConstructed->ShowStatus();
  ShowCommonStatus();
}

G4int AgataDetectorConstruction::GetCrystalType( G4int detNum )
{
  return theConstructed->GetCrystalType(detNum);
}

G4int AgataDetectorConstruction::GetSegmentNumber( G4int offset, G4int nGe, G4ThreeVector position )
{
  return theConstructed->GetSegmentNumber(offset, nGe, position);
}

G4int AgataDetectorConstruction::GetAncillaryOffset()
{
  ancillaryOffset += 1000;
  return ancillaryOffset;
}

/////////////////////////////////////////////////////////////////////
/// This method is mandatory to commit any change in the geometry
/////////////////////////////////////////////////////////////////////
void AgataDetectorConstruction::UpdateGeometry()
{
//  G4GeometryManager::GetInstance()->OpenGeometry();
//  G4PhysicalVolumeStore::GetInstance()->Clean();
//  G4LogicalVolumeStore::GetInstance()->Clean();
//  G4SolidStore::GetInstance()->Clean();
//  DefineMaterials();

  std::cout << std::endl; std::cout << "Updating geometry" << std::endl;

  ConstructHall();


  if(buildSpectrometer){
    std::cout << "------> Adding spectrometer....\n";
    //We add a spectrometer... or actually just a slice of it
    G4Box *specvol = new G4Box("specvol",tan(SpecOpeningTheta)*0.3*m,
			       tan(SpecOpeningPhi)*0.3*m,0.01*m);
    G4Material* ptMaterial = G4Material::GetMaterial("Vacuum");
    G4LogicalVolume *speclog = new G4LogicalVolume(specvol,ptMaterial,
						   "speclogic",0,0,0);
    G4VisAttributes* specVisAtt = new G4VisAttributes(G4Colour(0.0,1.0,1.0));
    specVisAtt->SetForceSolid(true);
    speclog->SetVisAttributes(specVisAtt);
    new G4PVPlacement(G4Transform3D(G4RotationMatrix(),
				    G4ThreeVector(0,0,.3*m)),
		      "specphys",speclog,hallPhys,0,0,0);
    if(AddVAMOSDetection){
      G4Material *ptMmylar = G4NistManager::Instance()->
	FindOrBuildMaterial("G4_MYLAR");
      G4VisAttributes* mylarVisAtt = new G4VisAttributes(G4Colour(0.0,.5,1.0));
      mylarVisAtt->SetForceSolid(true);
      G4Box *mylar1 = new G4Box("mylar1",tan(SpecOpeningTheta)*0.221*m,
				tan(SpecOpeningPhi)*0.221*m,0.00045*mm);
      G4LogicalVolume *logMylar1 = new G4LogicalVolume(mylar1,ptMmylar,
						       "mylar1logic",0,0,0);
      logMylar1->SetVisAttributes(mylarVisAtt);
      new G4PVPlacement(G4Transform3D(G4RotationMatrix(),
				      G4ThreeVector(0,0,.1*m)),
			"mylar1phys",logMylar1,hallPhys,0,0,0);
      G4Box *mylar2 = new G4Box("mylar2",tan(SpecOpeningTheta)*0.221*m,
				tan(SpecOpeningPhi)*0.221*m,0.00045*mm);
      G4LogicalVolume *logMylar2 = new G4LogicalVolume(mylar2,ptMmylar,
						       "mylar2logic",0,0,0);
      logMylar2->SetVisAttributes(mylarVisAtt);
      new G4PVPlacement(G4Transform3D(G4RotationMatrix(),
				      G4ThreeVector(0,0,.221*m)),
			"mylar2phys",logMylar2,hallPhys,0,0,0);
      G4Box *gas = new G4Box("gas",tan(SpecOpeningTheta)*0.221*m,
			     tan(SpecOpeningPhi)*0.221*m,59*mm);
      G4Material *ptMG = G4NistManager::Instance()->
	ConstructNewGasMaterial("lpButane","G4_BUTANE",
				120.*kelvin,5.7e-3*atmosphere);
      G4LogicalVolume *logGas = new G4LogicalVolume(gas,ptMG,"gaslogic",0,0,0);
      G4VisAttributes* GasVisAtt = new G4VisAttributes(G4Colour(0.0,.5,.1));
      GasVisAtt->SetForceSolid(true);
      logGas->SetVisAttributes(GasVisAtt);
      new G4PVPlacement(G4Transform3D(G4RotationMatrix(),
				      G4ThreeVector(0,0,.160*m)),
			"Gasphys",logGas,hallPhys,0,0,0);
    }
    
  }


  theConstructed->Placement();

  G4RunManager* runManager = G4RunManager::GetRunManager();
  runManager->DefineWorldVolume( hallPhys );
 
  //get the pointer to the User Interface manager 
  G4UImanager * UI = G4UImanager::GetUIpointer();
  // this is needed to apply radioactive decay to all
  // volumes, ie to those not created at program startup
  // NB: with present version of the physics list
  //     G4RadioactiveDecay is ALWAYS used  
  UI->ApplyCommand("/grdm/allVolumes");
  if(getenv("AGATAWRITEGDML")){
    G4GDMLParser parser;
    std::string filename("AGATA.gdml");
    parser.Write(filename,hallPhys);
  }
  ShowStatus();

  std::cout << std::endl; std::cout << "geometry updated" << std::endl;

}

#ifdef GASP

G4int AgataDetectorConstruction::GetNumberOfNeutron()
{
  return newGasp->GetNumberOfNeutron();
}

G4int AgataDetectorConstruction::GetMaxNeutronIndex()
{
  return newGasp->GetMaxNeutronIndex();
}

G4int AgataDetectorConstruction::GetNumberOfBgo()
{
  return newGasp->GetNumberOfBgo();
}

G4int AgataDetectorConstruction::GetMaxBgoIndex()
{
  return newGasp->GetMaxBgoIndex();
}

G4int AgataDetectorConstruction::GetNumberOfSi()
{
  return newGasp->GetNumberOfSi();
}

G4int AgataDetectorConstruction::GetMaxSiIndex()
{
  return newGasp->GetMaxSiIndex();
}

G4bool AgataDetectorConstruction::IsSiSegmented()
{
  return newGasp->IsSiSegmented();
}
#endif

#ifdef CLARA
G4int AgataDetectorConstruction::GetNumberOfMcp()
{
  return newClara->GetNumberOfMcp();
}

G4int AgataDetectorConstruction::GetMaxMcpIndex()
{
  return newClara->GetMaxMcpIndex();
}
#endif


#include "AgataDetectorConstruction.icc"

