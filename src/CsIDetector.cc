/***************************************************************************
                          CsIDetector.cc  -  description
                             -------------------
    begin                : Mon Oct 3 2005
    copyright            : (C) 2005 by Mike Taylor
    email                : mjt502@york.ac.uk
    
Modified by Pavel Golubev (pavel.golubev@nuclear.lu.se) 
LYCCA0 configuration implemented
***************************************************************************/
// Adapted for Agata code, October 2009 by P. Joshi, The University fo York.
// Modified segment arrangement for expt. s377, D. Bloor June 2011, University of York
#include "CsIDetector.h"
#include "G4Polyhedra.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4Material.hh"

#include "globals.hh"
#include "G4ios.hh"

CsIDetector::CsIDetector(G4double defTgtDetDist) :
  CsI_log(0), CsI_phys(0), constructed(false)
{
  detDist = defTgtDetDist + 0.07*m;
}

CsIDetector::~CsIDetector()
{
}

void CsIDetector::createDetector(G4VPhysicalVolume *lab_phy, G4Material* detMaterial)
{


  G4double z_values[]={0.0*mm, 13.0*mm, 20.0*mm};
  const G4double rOuter[]={9.75*mm,9.75*mm,5.0*mm};
  const G4double rInner[]={0*cm,0*cm,0*cm};
  
  G4double       cate_z_values[]={0.0*mm, 10.0*mm, 25.0*mm};
  const G4double cate_rOuter[]={27.0*mm,27.0*mm,9.0*mm};
  const G4double cate_rInner[]={0*cm,0*cm,0*cm};
  
  G4RotationMatrix *Ra;

  G4Polyhedra* CsI_box = new G4Polyhedra("CsI_box",0*deg, 360*deg,4,3,z_values,rInner,rOuter);
  G4Polyhedra* Cate_CsI_box = new G4Polyhedra("Cate_CsI_box",0*deg, 360*deg,4,3,cate_z_values,cate_rInner,cate_rOuter);

  CsI_log = new G4LogicalVolume(CsI_box, detMaterial, "CsI_log",0,0,0);
  Cate_CsI_log = new G4LogicalVolume(Cate_CsI_box, detMaterial, "Cate_CsI_log",0,0,0);

  G4VisAttributes* CsIVisATT
    = new G4VisAttributes(G4Colour(1.0,0.0,0.0));

  CsIVisATT->SetForceSolid(true);

  CsI_log->SetVisAttributes(CsIVisATT);
  Cate_CsI_log->SetVisAttributes(CsIVisATT);

  Ra = new G4RotationMatrix;
  //Ra->rotateX(45.0*deg);
  //Ra->rotateY(-90.0*deg);
  Ra->rotateZ(45.0*deg);


  G4double SupportDist1 = 1.0;
  // removed to get rid of warnings
  //  G4double SupportDist2 = 2.0;

  placement[1]=G4ThreeVector(-(62.25/2.0)-62.25-20,SupportDist1+62.25+20,detDist);
  placement[2]=G4ThreeVector(-(62.25/2.0)-62.25,SupportDist1+62.25+20,detDist);
  placement[3]=G4ThreeVector(-(62.25/2.0)-62.25+20,SupportDist1+62.25+20,detDist);
  placement[4]=G4ThreeVector(-(62.25/2.0)-62.25-20,SupportDist1+62.25,detDist);
  placement[5]=G4ThreeVector(-(62.25/2.0)-62.25,SupportDist1+62.25,detDist);
  placement[6]=G4ThreeVector(-(62.25/2.0)-62.25+20,SupportDist1+62.25,detDist);
  placement[7]=G4ThreeVector(-(62.25/2.0)-62.25-20,SupportDist1+62.25-20,detDist);
  placement[8]=G4ThreeVector(-(62.25/2.0)-62.25,SupportDist1+62.25-20,detDist);
  placement[9]=G4ThreeVector(-(62.25/2.0)-62.25+20,SupportDist1+62.25-20,detDist);

  placement[10]=G4ThreeVector(-(62.25/2)-20, SupportDist1+62.25+20,detDist);
  placement[11]=G4ThreeVector(-(62.25/2)   , SupportDist1+62.25+20,detDist);
  placement[12]=G4ThreeVector(-(62.25/2)+20, SupportDist1+62.25+20,detDist);
  placement[13]=G4ThreeVector(-(62.25/2)-20,  SupportDist1+62.25,detDist);
  placement[14]=G4ThreeVector(-(62.25/2)   ,  SupportDist1+62.25,detDist);
  placement[15]=G4ThreeVector(-(62.25/2)+20,  SupportDist1+62.25,detDist);
  placement[16]=G4ThreeVector(-(62.25/2)-20, SupportDist1+62.25-20,detDist);
  placement[17]=G4ThreeVector(-(62.25/2)   ,SupportDist1+62.25-20,detDist);
  placement[18]=G4ThreeVector(-(62.25/2)+20,SupportDist1+62.25-20,detDist);

  placement[19]=G4ThreeVector((62.25/2.0)-20,SupportDist1+62.25+20,detDist);
  placement[20]=G4ThreeVector((62.25/2.0),SupportDist1+62.25+20,detDist);
  placement[21]=G4ThreeVector((62.25/2.0)+20,SupportDist1+62.25+20,detDist);
  placement[22]=G4ThreeVector((62.25/2.0)-20,SupportDist1+62.25,detDist);
  placement[23]=G4ThreeVector((62.25/2.0),SupportDist1+62.25,detDist);
  placement[24]=G4ThreeVector((62.25/2.0)+20,SupportDist1+62.25,detDist);
  placement[25]=G4ThreeVector((62.25/2.0)-20,SupportDist1+62.25-20,detDist);
  placement[26]=G4ThreeVector((62.25/2.0),SupportDist1+62.25-20,detDist);
  placement[27]=G4ThreeVector((62.25/2.0)+20,SupportDist1+62.25-20,detDist);

  placement[28]=G4ThreeVector((62.25/2.0)+62.25-20,SupportDist1+62.25+20,detDist);
  placement[29]=G4ThreeVector((62.25/2.0)+62.25,SupportDist1+62.25+20,detDist);
  placement[30]=G4ThreeVector((62.25/2.0)+62.25+20,SupportDist1+62.25+20,detDist);
  placement[31]=G4ThreeVector((62.25/2.0)+62.25-20,SupportDist1+62.25,detDist);
  placement[32]=G4ThreeVector((62.25/2.0)+62.25,SupportDist1+62.25,detDist);
  placement[33]=G4ThreeVector((62.25/2.0)+62.25+20,SupportDist1+62.25,detDist);
  placement[34]=G4ThreeVector((62.25/2.0)+62.25-20,SupportDist1+62.25-20,detDist);
  placement[35]=G4ThreeVector((62.25/2.0)+62.25,SupportDist1+62.25-20,detDist);
  placement[36]=G4ThreeVector((62.25/2.0)+62.25+20,SupportDist1+62.25-20,detDist);

  placement[37]=G4ThreeVector(-(62.25/2.0)-62.25-20,20,detDist);
  placement[38]=G4ThreeVector(-(62.25/2.0)-62.25,20,detDist);
  placement[39]=G4ThreeVector(-(62.25/2.0)-62.25+20,20,detDist);
  placement[40]=G4ThreeVector(-(62.25/2.0)-62.25-20,0,detDist);
  placement[41]=G4ThreeVector(-(62.25/2.0)-62.25,0,detDist);
  placement[42]=G4ThreeVector(-(62.25/2.0)-62.25+20,0,detDist);
  placement[43]=G4ThreeVector(-(62.25/2.0)-62.25-20,-20,detDist);
  placement[44]=G4ThreeVector(-(62.25/2.0)-62.25,-20,detDist);
  placement[45]=G4ThreeVector(-(62.25/2.0)-62.25+20,-20,detDist);

  placement[46]=G4ThreeVector(-(62.25/2)-20, 20,detDist);
  placement[47]=G4ThreeVector(-(62.25/2)   , 20,detDist);
  placement[48]=G4ThreeVector(-(62.25/2)+20, 20,detDist);
  placement[49]=G4ThreeVector(-(62.25/2)-20,  0,detDist);
  placement[50]=G4ThreeVector(-(62.25/2)   ,  0,detDist);
  placement[51]=G4ThreeVector(-(62.25/2)+20,  0,detDist);
  placement[52]=G4ThreeVector(-(62.25/2)-20,-20,detDist);
  placement[53]=G4ThreeVector(-(62.25/2)   ,-20,detDist);
  placement[54]=G4ThreeVector(-(62.25/2)+20,-20,detDist);

  placement[55]=G4ThreeVector((62.25/2.0)-20,20,detDist);
  placement[56]=G4ThreeVector((62.25/2.0),20,detDist);
  placement[57]=G4ThreeVector((62.25/2.0)+20,20,detDist);
  placement[58]=G4ThreeVector((62.25/2.0)-20,0,detDist);
  placement[59]=G4ThreeVector((62.25/2.0),0,detDist);
  placement[60]=G4ThreeVector((62.25/2.0)+20,0,detDist);
  placement[61]=G4ThreeVector((62.25/2.0)-20,-20,detDist);
  placement[62]=G4ThreeVector((62.25/2.0),-20,detDist);
  placement[63]=G4ThreeVector((62.25/2.0)+20,-20,detDist);

  placement[64]=G4ThreeVector((62.25/2.0)+62.25-20,20,detDist);
  placement[65]=G4ThreeVector((62.25/2.0)+62.25,20,detDist);
  placement[66]=G4ThreeVector((62.25/2.0)+62.25+20,20,detDist);
  placement[67]=G4ThreeVector((62.25/2.0)+62.25-20,0,detDist);
  placement[68]=G4ThreeVector((62.25/2.0)+62.25,0,detDist);
  placement[69]=G4ThreeVector((62.25/2.0)+62.25+20,0,detDist);
  placement[70]=G4ThreeVector((62.25/2.0)+62.25-20,-20,detDist);
  placement[71]=G4ThreeVector((62.25/2.0)+62.25,-20,detDist);
  placement[72]=G4ThreeVector((62.25/2.0)+62.25+20,-20,detDist);


  
  placement[73]=G4ThreeVector(-(62.25/2.0)-62.25-20,-SupportDist1-62.25+20,detDist);
  placement[74]=G4ThreeVector(-(62.25/2.0)-62.25,-SupportDist1-62.25+20,detDist);
  placement[75]=G4ThreeVector(-(62.25/2.0)-62.25+20,-SupportDist1-62.25+20,detDist);
  placement[76]=G4ThreeVector(-(62.25/2.0)-62.25-20,-SupportDist1-62.25,detDist);
  placement[77]=G4ThreeVector(-(62.25/2.0)-62.25,-SupportDist1-62.25,detDist);
  placement[78]=G4ThreeVector(-(62.25/2.0)-62.25+20,-SupportDist1-62.25,detDist);
  placement[79]=G4ThreeVector(-(62.25/2.0)-62.25-20,-SupportDist1-62.25-20,detDist);
  placement[80]=G4ThreeVector(-(62.25/2.0)-62.25,-SupportDist1-62.25-20,detDist);
  placement[81]=G4ThreeVector(-(62.25/2.0)-62.25+20,-SupportDist1-62.25-20,detDist);

  placement[82]=G4ThreeVector(-(62.25/2)-20, -SupportDist1-62.25+20,detDist);
  placement[83]=G4ThreeVector(-(62.25/2)   , -SupportDist1-62.25+20,detDist);
  placement[84]=G4ThreeVector(-(62.25/2)+20, -SupportDist1-62.25+20,detDist);
  placement[85]=G4ThreeVector(-(62.25/2)-20,  -SupportDist1-62.25,detDist);
  placement[86]=G4ThreeVector(-(62.25/2)   ,  -SupportDist1-62.25,detDist);
  placement[87]=G4ThreeVector(-(62.25/2)+20,  -SupportDist1-62.25,detDist);
  placement[88]=G4ThreeVector(-(62.25/2)-20, -SupportDist1-62.25-20,detDist);
  placement[89]=G4ThreeVector(-(62.25/2)   ,-SupportDist1-62.25-20,detDist);
  placement[90]=G4ThreeVector(-(62.25/2)+20,-SupportDist1-62.25-20,detDist);

  placement[91]=G4ThreeVector((62.25/2.0)-20,-SupportDist1-62.25+20,detDist);
  placement[92]=G4ThreeVector((62.25/2.0),-SupportDist1-62.25+20,detDist);
  placement[93]=G4ThreeVector((62.25/2.0)+20,-SupportDist1-62.25+20,detDist);
  placement[94]=G4ThreeVector((62.25/2.0)-20,-SupportDist1-62.25,detDist);
  placement[95]=G4ThreeVector((62.25/2.0),-SupportDist1-62.25,detDist);
  placement[96]=G4ThreeVector((62.25/2.0)+20,-SupportDist1-62.25,detDist);
  placement[97]=G4ThreeVector((62.25/2.0)-20,-SupportDist1-62.25-20,detDist);
  placement[98]=G4ThreeVector((62.25/2.0),-SupportDist1-62.25-20,detDist);
  placement[99]=G4ThreeVector((62.25/2.0)+20,-SupportDist1-62.25-20,detDist);

  placement[100]=G4ThreeVector((62.25/2.0)+62.25-20,-SupportDist1-62.25+20,detDist);
  placement[101]=G4ThreeVector((62.25/2.0)+62.25,-SupportDist1-62.25+20,detDist);
  placement[102]=G4ThreeVector((62.25/2.0)+62.25+20,-SupportDist1-62.25+20,detDist);
  placement[103]=G4ThreeVector((62.25/2.0)+62.25-20,-SupportDist1-62.25,detDist);
  placement[104]=G4ThreeVector((62.25/2.0)+62.25,-SupportDist1-62.25,detDist);
  placement[105]=G4ThreeVector((62.25/2.0)+62.25+20,-SupportDist1-62.25,detDist);
  placement[106]=G4ThreeVector((62.25/2.0)+62.25-20,-SupportDist1-62.25-20,detDist);
  placement[107]=G4ThreeVector((62.25/2.0)+62.25,-SupportDist1-62.25-20,detDist);
  placement[108]=G4ThreeVector((62.25/2.0)+62.25+20,-SupportDist1-62.25-20,detDist);


  char phys_name[20];

  for(G4int i=1; i<109; i++)
    {
      sprintf(phys_name, "CsI_Segment%d", i);

      CsI_phys_cpy[i] = new  G4PVPlacement(Ra,
					   placement[i],
					   phys_name, CsI_log, lab_phy, false, i+800 );
      G4cout << "Lycca Placement for CsI Segment#" << i+800 <<"=" << placement[i] << G4endl;

    }
  
  /*for(G4int i=37; i<45; i++)
    {
    sprintf(phys_name, "Cate_CsI_Segment%d", i);

    CsI_phys_cpy[i] = new  G4PVPlacement(Ra,
    placement[i],
    phys_name, Cate_CsI_log, lab_phy, false, i+900 );
    G4cout << "Lycca Placement for CsI Segment#" << i+900 <<"=" << placement[i] << G4endl;

    }*/
  
}

