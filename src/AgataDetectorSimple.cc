#include "AgataDetectorSimple.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Polycone.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataDetectorSimple::AgataDetectorSimple(G4String name)
{
  matCryst       = NULL;
  matCrystName   = "Germanium";
  matCap         = NULL;
  matCapName     = "Aluminium";
  matVac         = NULL;
  matVacName     = "Vacuum";
  
  readOut   = false;

  cylRadius = 35.*mm;
  cylHeight = 30.*mm;
  capSpace  =  2.*mm;
  capThick  =  1.*mm;

  position = G4ThreeVector();
  
  nDets = 0;
  nClus = 0;
  iCMin = 0;
  iGMin = 0;

  myMessenger = new AgataDetectorSimpleMessenger(this,name);
}

AgataDetectorSimple::~AgataDetectorSimple()
{
  delete myMessenger;
}

G4int AgataDetectorSimple::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matCrystName);
  if (ptMaterial) {
    matCryst = ptMaterial;
    G4String nome = matCryst->GetName();
    G4cout << "\n ----> The detector material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matCrystName << G4endl;
    G4cout << " Could not build the detector! " << G4endl;
    return 1;
  }
  ptMaterial = G4Material::GetMaterial(matCapName);
  if (ptMaterial) {
    matCap = ptMaterial;
    G4String name = matCap->GetName();
    G4cout << "\n ----> The capsule material is "
          << name << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matCapName << G4endl;
    G4cout << " Could not build the detector! " << G4endl;
    return 1;
  }
  ptMaterial = G4Material::GetMaterial(matVacName);
  if (ptMaterial) {
    matVac = ptMaterial;
    G4String nume = matVac->GetName();
    G4cout << "\n ----> The vacuum material is "
          << nume << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matVacName << G4endl;
    G4cout << " Could not build the detector! " << G4endl;
    return 1;
  }
  return 0;  
}


void AgataDetectorSimple::Placement()
{
  if( FindMaterials() ) return;
  
  G4RunManager* runManager                = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();

  char sName[128];
  
  G4double zdet[2];
  G4double innR[2]; 
  G4double outR[2]; 
  
  // crystal
  zdet[0] = 0.;
  zdet[1] = cylHeight;
  
  innR[0] = 0.;
  innR[1] = 0.;
  
  outR[0] = cylRadius;
  outR[1] = cylRadius;
  
  sprintf(sName, "geCrystS%2.2d", 0);
  G4Polycone *pCryst = new G4Polycone(G4String(sName), 0.*deg, 360.*deg, 2, zdet, innR, outR );

  sprintf(sName, "geCrystL%2.2d", 0);
  G4LogicalVolume *pCrystL  = new G4LogicalVolume( pCryst, matCryst, G4String(sName), 0, 0, 0 );

  G4VisAttributes *pDetVA = new G4VisAttributes( G4Colour(1.0, 0.0, 0.0) );
  pCrystL->SetVisAttributes( pDetVA );

  // Sensitive Detector
  pCrystL->SetSensitiveDetector( theDetector->GeSD() );

  G4int depth = 2;
  theDetector->GeSD()->SetDepth(depth);


  // vacuum
  zdet[0] = -capSpace;
  zdet[1] = cylHeight + capSpace;
  
  outR[0] = cylRadius + capSpace;
  outR[1] = cylRadius + capSpace;
  
  sprintf(sName, "geVacS%2.2d", 0);
  G4Polycone *pVac = new G4Polycone(G4String(sName), 0.*deg, 360.*deg, 2, zdet, innR, outR );

  sprintf(sName, "geVacL%2.2d", 0);
  G4LogicalVolume *pVacL  = new G4LogicalVolume( pVac, matVac, G4String(sName), 0, 0, 0 );

  // capsule
  zdet[0] = -capSpace - capThick;
  zdet[1] = cylHeight + capSpace + capThick;
  
  outR[0] = cylRadius + capSpace + capThick;
  outR[1] = cylRadius + capSpace + capThick;
  
  sprintf(sName, "geCapS%2.2d", 0);
  G4Polycone *pCap = new G4Polycone(G4String(sName), 0.*deg, 360.*deg, 2, zdet, innR, outR );

  sprintf(sName, "geCapL%2.2d", 0);
  G4LogicalVolume *pCapL  = new G4LogicalVolume( pCap, matCap, G4String(sName), 0, 0, 0 );

  // placements
  G4RotationMatrix rm;
  rm.set(0,0,0);

  sprintf(sName, "geCrystP%2.2d", 0);
  new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pCrystL, G4String(sName), pVacL, false, 0 ); 

  sprintf(sName, "geVacP%2.2d", 0);
  new G4PVPlacement( G4Transform3D(rm, G4ThreeVector()), pVacL, G4String(sName), pCapL, false, 0 ); 
  
  sprintf(sName, "geDetP%3.3d", 0);
  new G4PVPlacement(G4Transform3D(rm, position), G4String(sName), pCapL, theDetector->HallPhys(), false, 0);

  nDets++;
  nClus++;                  
  G4cout << " ----> Placed a single germanium crystal" << G4endl;
}

void AgataDetectorSimple::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  char line[256];
  outFileLMD << "BOX3" << G4endl << "SUMMARY 1" << G4endl;
  sprintf(line, "%3d %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f %6.2f 1 1 1",
               0, cylRadius/unit, cylHeight/unit, capSpace/unit, capThick/unit, position.x()/unit, position.y()/unit, position.z()/unit); 
  
  outFileLMD << G4String(line) << G4endl;
}

void AgataDetectorSimple::ShowStatus()
{
  G4cout << G4endl;
  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);
  G4cout << " Placed a single germanium crystal" << G4endl;
  G4cout << " Cylinder size " << cylRadius/cm << " cm x " << cylHeight/cm << " cm" << G4endl;
  G4cout << " Capsule spacing " << capSpace/mm << " mm" << G4endl;
  G4cout << " Capsule thickness " << capThick/mm << " mm" << G4endl;
  G4cout << " Position " << position/cm << " cm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

/////////////////////////////////////////////////////////////////////////////////////////
///////////// methods for the messenger
/////////////////////////////////////////////////////////////////////////
void AgataDetectorSimple::SetRadius( G4double Radi )
{
  if( Radi < 0. ) {
    G4cout << " Warning! Keeping previous value (" << cylRadius/mm << " mm)" << G4endl;
  }
  else {
    cylRadius = Radi * mm;
    G4cout << " ----> Crystal radius is " << cylRadius/mm << " mm" << G4endl;
  }
}

void AgataDetectorSimple::SetHeight( G4double Radi )
{
  if( Radi < 0. ) {
    G4cout << " Warning! Keeping previous value (" << cylHeight/mm << " mm)" << G4endl;
  }
  else {
    cylHeight = Radi * mm;
    G4cout << " ----> Crystal height is " << cylHeight/mm << " mm" << G4endl;
  }
}

void AgataDetectorSimple::SetSpace( G4double Radi )
{
  if( Radi < 0. ) {
    G4cout << " Warning! Keeping previous value (" << capSpace/mm << " mm)" << G4endl;
  }
  else {
    capSpace = Radi * mm;
    G4cout << " ----> Crystal height is " << capSpace/mm << " mm" << G4endl;
  }
}

void AgataDetectorSimple::SetThick( G4double Radi )
{
  if( Radi < 0. ) {
    G4cout << " Warning! Keeping previous value (" << capThick/mm << " mm)" << G4endl;
  }
  else {
    capThick = Radi * mm;
    G4cout << " ----> Crystal height is " << capThick/mm << " mm" << G4endl;
  }
}

void AgataDetectorSimple::SetPosition( G4ThreeVector pos )
{
  position = pos*mm;
  G4cout << " ----> Single Ge will be placed at " << position/mm << " mm" << G4endl;
}  

////////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3Vector.hh"

AgataDetectorSimpleMessenger::AgataDetectorSimpleMessenger(AgataDetectorSimple* pTarget, G4String /*name*/)
:myTarget(pTarget)
{ 

  SetRadiusCmd = new G4UIcmdWithADouble("/Agata/detector/size",this);  
  SetRadiusCmd->SetGuidance("Define radius of the single Ge detector.");
  SetRadiusCmd->SetGuidance("Required parameters: 1 double (radius in mm)");
  SetRadiusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetHeightCmd = new G4UIcmdWithADouble("/Agata/detector/height",this);  
  SetHeightCmd->SetGuidance("Define height of the single Ge detector.");
  SetHeightCmd->SetGuidance("Required parameters: 1 double (height in mm)");
  SetHeightCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetSpaceCmd = new G4UIcmdWithADouble("/Agata/detector/space",this);  
  SetSpaceCmd->SetGuidance("Define capsule-crystal spacing.");
  SetSpaceCmd->SetGuidance("Required parameters: 1 double (spacing in mm)");
  SetSpaceCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetThickCmd = new G4UIcmdWithADouble("/Agata/detector/thick",this);  
  SetThickCmd->SetGuidance("Define capsule thickness.");
  SetThickCmd->SetGuidance("Required parameters: 1 double (thickness in mm)");
  SetThickCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  SetGePositionCmd = new G4UIcmdWith3Vector("/Agata/detector/positionGe",this);  
  SetGePositionCmd->SetGuidance("Define position of the single Ge detector.");
  SetGePositionCmd->SetGuidance("Required parameters: 3 double (x, y, z in mm).");
  SetGePositionCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}

AgataDetectorSimpleMessenger::~AgataDetectorSimpleMessenger()
{

  delete SetGePositionCmd;
  delete SetRadiusCmd;
  delete SetHeightCmd;
  delete SetSpaceCmd;
  delete SetThickCmd;
}

void AgataDetectorSimpleMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetGePositionCmd ) {
    myTarget->SetPosition(SetGePositionCmd->GetNew3VectorValue(newValue));
  }
  if( command == SetRadiusCmd ) {
    myTarget->SetRadius(SetRadiusCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetHeightCmd ) {
    myTarget->SetHeight(SetHeightCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetSpaceCmd ) {
    myTarget->SetSpace(SetSpaceCmd->GetNewDoubleValue(newValue));
  }
  if( command == SetThickCmd ) {
    myTarget->SetThick(SetThickCmd->GetNewDoubleValue(newValue));
  }
  
}
  

