#include "AgataDefaultGenerator.hh"

#include "AgataInternalEmission.hh"
#include "AgataExternalEmission.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4IonTable.hh"
#include "G4ParticleDefinition.hh"

AgataDefaultGenerator::AgataDefaultGenerator( G4String path, G4bool inter, G4bool hadr, G4bool polar, G4String name )
{
  if( path.find( "./", 0 ) != string::npos ) {
    G4int position = path.find( "./", 0 );
    if( position == 0 )
      path.erase( position, 2 );
  }
  iniPath = path;

  internal  = inter;
  hadrons   = hadr;
  usePola   = polar;
  
  G4int n_particle = 1;
  particleGun = new G4ParticleGun(n_particle);
  
  theInternalEmission = NULL;
  theExternalEmission = NULL;
  theEmission         = NULL;
  
  if( internal ) {
    theInternalEmission = new AgataInternalEmission( iniPath, hadrons, usePola, name );
    theEmission = (AgataEmission*)theInternalEmission;
  }  
  else {  
    theExternalEmission = new AgataExternalEmission( iniPath, hadrons, usePola, name );
    theEmission = (AgataEmission*)theExternalEmission;
  }  
}

AgataDefaultGenerator::~AgataDefaultGenerator()
{
  delete particleGun;
}

void AgataDefaultGenerator::BeginOfRun()
{
  theEmission->BeginOfRun();
}  

void AgataDefaultGenerator::EndOfRun()
{
  theEmission->EndOfRun();
}  

void AgataDefaultGenerator::BeginOfEvent()
{
  theEmission->BeginOfEvent();
}  

void AgataDefaultGenerator::EndOfEvent()
{
  theEmission->EndOfEvent();
}  

void AgataDefaultGenerator::GeneratePrimaries(G4Event* anEvent)
{
  G4String      emittedName;
  G4int         emittedAtomNum=0;
  G4int         emittedMassNum=0;
  G4ThreeVector emittedDirection;
  G4ThreeVector emitterPosition;
  G4ThreeVector emittedPolarization;
  G4double      emittedEnergy;
  G4double      emitterTime;
  // For AGATA-PRISMA
  G4double      emittedIonicCharge = 0.;
 
  //////////////////////////////////////////////////////////////////////////
  /// Asks the AgataEmission object what will be the next emitted particle
  /// (name, energy, direction, ...)
  //////////////////////////////////////////////////////////////////////////
  
  if( theEmission->IsAbortRun() ) {  
    particleGun->GeneratePrimaryVertex(anEvent);
    return;
  }
  
  theEmission->BeginOfEvent();
  theEmission->NextParticle();
  if( !theEmission->IsAbortRun() ) {
    emittedName      = theEmission->GetEmittedName();
    if( emittedName == "GenericIon" && !internal ) {
      emittedAtomNum = theExternalEmission->GetEmitterAtomNum();
      emittedMassNum = theExternalEmission->GetEmitterMassNum();
      // for AGATA-PRISMA
      emittedIonicCharge = theExternalEmission->GetEmitterIonicCharge();
    }
    emittedDirection = theEmission->GetEmittedDirection();
    emittedEnergy    = theEmission->GetEmittedEnergy();
    emitterPosition  = theEmission->GetEmitterPosition();
    emitterTime      = theEmission->GetCascadeTime(); 
    if( theEmission->IsPolarized() ) {
      emittedPolarization = theEmission->GetEmittedPolarization();
    } 
    else
      emittedPolarization = G4ThreeVector(); 
  }
  else {
    particleGun->GeneratePrimaryVertex(anEvent);
    return;
  }   
  
  
  ///////////////////////////////////////////////
  // finally, call particle gun 
  ///////////////////////////////////////////////

  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
#ifdef G4V10
  G4IonTable* ionTable = G4IonTable::GetIonTable();
#endif
  G4ParticleDefinition* emitted;
  
  if( emittedName == "GenericIon" )
#ifdef G4V10
    emitted = ionTable->GetIon( emittedAtomNum, emittedMassNum, 0. );
#else
    emitted = particleTable->GetIon( emittedAtomNum, emittedMassNum, 0. );
#endif
  else  
    emitted = particleTable->FindParticle(emittedName);

  particleGun->SetParticleDefinition       ( emitted );
  particleGun->SetParticleMomentumDirection( emittedDirection );
  particleGun->SetParticleEnergy           ( emittedEnergy );
  particleGun->SetParticlePosition         ( emitterPosition );
  // For AGATA-PRISMA
  particleGun->SetParticleCharge           ( emittedIonicCharge );
   
  particleGun->SetParticleTime( emitterTime );
  
  if( theEmission->IsPolarized() )
    particleGun->SetParticlePolarization   ( emittedPolarization );
    
  particleGun->GeneratePrimaryVertex(anEvent);

}

void AgataDefaultGenerator::PrintToFile( std::ofstream &outFileLMD, G4double unitL, G4double unitE )
{
  theEmission->PrintToFile( outFileLMD, unitL, unitE );
}

void AgataDefaultGenerator::GetStatus()
{
  theEmission->GetStatus();  
}

