#include "CSpec1D.hh"

#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>

#include "G4Types.hh"

using namespace std;


CSpec1D::CSpec1D(int nch, int nsp)
{
  if(nch < 1) nch = 1;
  if(nsp < 1) nsp = 1;
  nChan = nch;
  nSpec = nsp;
  pData = new int[nChan*nSpec];
  erase();
}

CSpec1D::~CSpec1D()
{
  if(pData) delete [] pData;
}

void CSpec1D::reNew(int nch, int nsp)
{
  if(pData)  delete [] pData;
  CSpec1D(nch, nsp);
}

void CSpec1D::erase()
{
  int nn = nChan*nSpec;
  for(int ii = 0; ii < nn; pData[ii++] = 0) {;}
}

void CSpec1D::list()
{
  for(int ii = 0; ii < nChan; ii += 10) {
    cout << std::setw(4) << ii << " ";
    for(int jj = 0; jj < 10; jj++) {
      if(ii+jj < nChan) cout << " " << pData[ii+jj];
    }
    cout << endl;
  }
}

void CSpec1D::write(const char * specname, int writeMessage)
{
  fstream fout;
  fout.open(specname, ios::out | ios::binary);
  if(!fout.is_open()) return;
  
  fout.write((const char *)pData, sizeof(int)*nChan*nSpec);
  fout.close();
  if(writeMessage)
    cout << "Spectrum " << specname << "(" << nChan << "*" << nSpec << ")" << endl;
}

void CSpec1D::writeA(const char * specname, int writeMessage)
{
  fstream fout;
  fout.open(specname, ios::out);
  if(!fout.is_open()) return;
  
  //fout.write((const char *)pData, sizeof(int)*nChan*nSpec);
  char dummy[128];
  
  for( int ii=0; ii<nChan*nSpec; ii++ ) {
    sprintf(dummy, "%d\n", *(pData+ii));
    fout.write(dummy, strlen(dummy));
  }
  
  
  fout.close();
  if(writeMessage)
    cout << "Spectrum " << specname << "(" << nChan << "*" << nSpec << ")" << endl;
}
