#ifdef ANCIL
#include "AgataAncillarySpider.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"


AgataAncillarySpider::AgataAncillarySpider(G4String path, G4String name )
{
    // dummy assignment needed for compatibility with other implementations of this class
    G4String iniPath    = path;
    
    dirName             = name;
    
    nam_vacuum          = "Vacuum";
    mat_vacuum          = NULL;
    
    nam_aluminium       = "Aluminum";
    mat_aluminium       = NULL;
    
    
    nam_silicon         = "Silicon";
    mat_silicon         = NULL;
    
    ancSD               = NULL;
    ancName             = G4String("SPIDER");
    ancOffset           = 21000;
    if(!getenv("G4GDMLPATH")){
        G4cerr << "Error! GDML Path is missing and I need it to construct Spider: please define G4GDMLPATH" << G4endl;
        std::exit(10);
    }
    gdml_path        = std::string(getenv("G4GDMLPATH"))+G4String("/GALILEO/Spider/");


    spider_rotation    = G4ThreeVector(0.,0.,0.);
    
    pSpiderSupport.clear();
    
    numAncSd            = 0;

    myMessenger = new AgataAncillarySpiderMessenger(this,name);


}

AgataAncillarySpider::~AgataAncillarySpider()
{}

G4Material* AgataAncillarySpider::FindMaterial (G4String Name){
    // search the material by its name
    G4Material* ptMaterial = G4Material::GetMaterial(Name);
    if (ptMaterial) {
        G4String nome = ptMaterial->GetName();
        G4cout << "----> Spider uses material " << nome << G4endl;
    }
    else {
        G4cout << " Could not find the material " << Name << G4endl;
        G4cout << " Could not build the ancillary Spider! " << G4endl;
        return NULL;
    }
    return ptMaterial;
}

G4int AgataAncillarySpider::FindMaterials()
{
    mat_vacuum      = FindMaterial(nam_vacuum);     if (!mat_vacuum)    return 1;
    mat_aluminium   = FindMaterial(nam_aluminium);  if (!mat_aluminium) return 1;
    mat_silicon     = FindMaterial(nam_silicon);    if (!mat_silicon)   return 1;
    return 0;
}

void AgataAncillarySpider::InitSensitiveDetector()
{
    G4int offset = ancOffset;
#ifndef FIXED_OFFSET
    offset = theDetector->GetAncillaryOffset();
#endif
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    
    if( !ancSD ) {
        ancSD = new AgataSensitiveDetector(dirName, "/anc/Spider", "SpiderCollection", offset,/* depth */ 0,/* menu */ false );
        SDman->AddNewDetector( ancSD );
        numAncSd++;
    }
}

G4bool AgataAncillarySpider::GetSegments()
{
    G4bool status = false;
    string gdml_file = "spider_segments.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open SPIDER segment GDML file: please check path: "
        << (gdml_path+gdml_file).c_str() << G4endl;
        return status;
        std::exit(10);
    }
    check_file.close();
    gdmlparser.Read((gdml_path+gdml_file));
    std::ostringstream el_name;
    std::ostringstream name;
    G4VisAttributes *pDetVTC;
    float Rcol[8]={0.8,0.0,0.6,0.0,0.4,0.0,0.2,0.0};
    float Gcol[8]={0.0,0.0,0.0,0.0,0.0,0.0,0.0,0.0};
    float Bcol[8]={0.0,0.8,0.0,0.6,0.0,0.4,0.0,0.2};
    int seg_order[8]={0,5,3,1,6,4,2,7};
    for(int j=0;j<8;++j){
        el_name.str("");name.str("");
        el_name << "spider_segments_" << seg_order[j] << "_vol";
        std::cout << el_name.str().c_str() << std::endl;
        name << "Spider_Seg_" << j ;
        if(pSpiderSegments.find(j)==pSpiderSegments.end()){
            pSpiderSegments[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),mat_silicon,"spiderSupport",0,0,0);
            pDetVTC = new G4VisAttributes( G4Colour(Rcol[j], Gcol[j], Bcol[j]) );
            pSpiderSegments[j]->SetVisAttributes(pDetVTC);
        }
    }
    return status;
}

G4bool AgataAncillarySpider::GetSupport()
{
    G4bool status = false;
    string gdml_file = "spider_support.gdml";
    std::ifstream check_file;check_file.open((gdml_path+gdml_file).c_str());
    if(!check_file.is_open()){
        G4cerr << "=== Cannot open SPIDER Support GDML file: please check path: "
        << (gdml_path+gdml_file).c_str() << G4endl;
        return status;
        std::exit(10);
    }
    check_file.close();
    gdmlparser.Read((gdml_path+gdml_file));
    std::ostringstream el_name;
    std::ostringstream name;
    G4VisAttributes *pDetVTC;
    float Rcol[8]={0.31,232,0.0,0.5,0.0,0.5,0.2,0.8};
    float Gcol[8]={0.36,213,0.0,0.5,0.5,0.0,0.2,0.8};
    float Bcol[8]={0.33, 93,1.0,0.0,0.5,0.5,0.2,0.8};
    for(int j=0;j<8;++j){
        el_name.str("");name.str("");
        el_name << "spider_support_" << j << "_vol";
        std::cout << el_name.str().c_str() << std::endl;
        name << "Spider_Support_" << j ;
        if(pSpiderSupport.find(j)==pSpiderSupport.end()){
            pSpiderSupport[j] = new G4LogicalVolume((gdmlparser.GetVolume(el_name.str().c_str()))->GetSolid(),mat_aluminium,"spiderSupport",0,0,0);
            pDetVTC = new G4VisAttributes( G4Colour(Rcol[j], Gcol[j], Bcol[j]) );
            pSpiderSupport[j]->SetVisAttributes(pDetVTC);
        }
    }
    return status;
}

void AgataAncillarySpider::GetDetectorConstruction()
{
    G4RunManager* runManager = G4RunManager::GetRunManager();
    theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillarySpider::Placement()
{
    ///////////////////////
    /// construct a shell
    ///////////////////////
    G4RunManager* runManager                = G4RunManager::GetRunManager();
    AgataDetectorConstruction* theTarget  =
    (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
    GetSupport();
    G4RotationMatrix rm ;
    rm = G4RotationMatrix::IDENTITY ;   // Reset rm rotation to IDENTITY
    rm.rotateX(-90*deg);
    rm.rotateZ(-108*deg);

// user-define rotation applied to support:
    rm.rotateX(spider_rotation[0]); // Added by Marc for AGATA+SPIDER at PRISMA with new chamber
    rm.rotateY(spider_rotation[1]); // Added by Marc for AGATA+SPIDER at PRISMA with new chamber
    rm.rotateZ(spider_rotation[2]); // Added by Marc for AGATA+SPIDER at PRISMA with new chamber

    //    G4VPhysicalVolume *phyvol; //removed do get rid of warning
    std::map<int,G4LogicalVolume*>::iterator it=pSpiderSupport.begin();
    for(;it!=pSpiderSupport.end();it++){
        /*phyvol =*/
        new G4PVPlacement(G4Transform3D(rm, rm(G4ThreeVector(0,0,1))),
                          G4String("toto"),it->second,(theTarget)->HallPhys(),
                          false,0);
    }
    GetSegments();
    it=pSpiderSegments.begin();
    for(;it!=pSpiderSegments.end();++it)
        it->second->SetSensitiveDetector(ancSD);
    
    
    rm = G4RotationMatrix::IDENTITY ;   // Reset rm rotation to IDENTITY
    rm.rotateX(-90*deg);
    rm.rotateZ(-108*deg);

    double rotatez[7]={51.5,51.5,51.5,51.5,51.5,51.5,51.5};
    //double rotatey[7]={spider_rotation[1],spider_rotation[1],spider_rotation[1],spider_rotation[1],spider_rotation[1],spider_rotation[1],spider_rotation[1]};

    std::cout << "Constructing Spider detectors:" << std::endl;
    for(int j=0;j<7;++j){
        it=pSpiderSegments.begin();
        rm.rotateZ(rotatez[j]*deg);
// user-defined Rotation (see messenger)
  	rm.rotateX(spider_rotation[0]); //Added by Marc for AGATA at PRISMA
	rm.rotateY(spider_rotation[1]); //Added by Marc for AGATA at PRISMA
  	rm.rotateZ(spider_rotation[2]); //Added by Marc for AGATA at PRISMA

        for(;it!=pSpiderSegments.end();it++){
           std::cout << "\t Detector=" << j << " segment=" << it->first << " id=" << it->first+j*10+ancOffset << std::endl;;
            //G4PVPlacement *phyvol =
            new G4PVPlacement(G4Transform3D(rm, rm(G4ThreeVector(0,0,0))),
                              G4String("toto"),it->second,
                              (theTarget)->HallPhys(), false,
                              it->first+j*10+ancOffset);
        }
// reverse user-defined rotation to build the next module:
  	rm.rotateZ(-1*spider_rotation[2]); // need to rotate back before changing module - Added by Marc for AGATA at PRISMA 
	rm.rotateY(-1*spider_rotation[1]); // need to rotate back before changing module - Added by Marc for AGATA at PRISMA 
 	rm.rotateX(-1*spider_rotation[0]); // need to rotate back before changing module - Added by Marc for AGATA at PRISMA 

    }

    std::cout << "Spider detectors Constructed" << std::endl;

    return;
}

void AgataAncillarySpider::ShowStatus()
{
    G4cout << " ANCILLARY Spider has been constructed." << G4endl;
    G4cout << "     Ancillary Vacuum      material is " << mat_vacuum->GetName() << G4endl;
    G4cout << "     Ancillary Detector    material is " << mat_silicon->GetName() << G4endl;
    G4cout << "     Ancillary Support     material is " << mat_aluminium->GetName() << G4endl;
}

void AgataAncillarySpider::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

void AgataAncillarySpider::SetRotation(G4ThreeVector rot)
{
  spider_rotation[0] = rot.x()*deg;
  spider_rotation[1] = rot.y()*deg;
  spider_rotation[2] = rot.z()*deg;

  cout << "Spider_rotation: " << spider_rotation[1] << endl;
}

/*void AgataAncillarySpider::SetGeometry(G4int geo)
{
  if(geo==0){
    G4cout << "  ---> Setting the geometry of SPIDER to AGATA+PRISMA configuration ..." << G4endl;
  }else{
    G4cout << " SPIDER configuration set to default " << G4endl;
    geo=0;
  }
  spider_geometry = geo;
}
*/


//////////////////////////////////
// The Messenger
/////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithABool.hh"
//#include "G4UIcmdWithADoubleAndUnit.hh"

AgataAncillarySpiderMessenger::AgataAncillarySpiderMessenger(AgataAncillarySpider* pTarget, G4String name)
  :myTarget(pTarget)
{ 

  G4String directoryName = name + "/detector/ancillary/SPIDER/";
  SpiderDir = new G4UIdirectory(directoryName);
  SpiderDir->SetGuidance("Control of SPIDER detector construction.");
  
  G4String commandName;
  const char *aLine;
  

  commandName = directoryName + "setRotation";
  aLine = commandName.c_str();
  setSpiderRotCmd = new G4UIcmdWith3Vector(aLine,this);
  setSpiderRotCmd -> SetGuidance("Set rotation of the SPIDER array with respect to x, y, z axis.");
  setSpiderRotCmd -> SetGuidance("The rotation is first apply w/r to x, then y and finally z.");
  setSpiderRotCmd -> SetGuidance("Required parameters: 3 doubles (angles in degrees)");
  setSpiderRotCmd -> SetDefaultValue(G4ThreeVector(0.,0.,0.));
  setSpiderRotCmd -> SetParameterName("Rotx", "Roty", "Rotz", true, true);
  setSpiderRotCmd -> AvailableForStates(G4State_PreInit,G4State_Idle);

/*
  commandName = directoryName + "setGeometry";
  aLine = commandName.c_str();
  setSpiderGeoCmd = new G4UIcmdWithAnInteger(aLine,this);
  setSpiderGeoCmd ->SetGuidance("Set SPIDER array geometry: 0 - standard configuration back angles");
  setSpiderGeoCmd ->SetGuidance("Set SPIDER array geometry: 1 - Configuration for AGATA+PRISMA");
  setSpiderGeoCmd ->SetDefaultValue(0);
  setSpiderGeoCmd ->AvailableForStates(G4State_PreInit,G4State_Idle);
*/

  
}
AgataAncillarySpiderMessenger::~AgataAncillarySpiderMessenger()
{

  delete setSpiderRotCmd;
  //delete setSpiderGeoCmd;
  delete SpiderDir;
}

void AgataAncillarySpiderMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{

  if(command==setSpiderRotCmd){
    myTarget->SetRotation(setSpiderRotCmd->GetNew3VectorValue(newValue));
  }
//  if(command==setSpiderGeoCmd){
//    myTarget->SetGeometry(setSpiderGeoCmd->GetNewIntValue(newValue));
//  }
  
}



#endif
