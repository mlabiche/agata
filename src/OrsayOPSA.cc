#include "OrsayOPSA.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

#include <sstream>

OrsayOPSA::OrsayOPSA(G4String path, G4String name)
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  dirName = name;
  
  OrsayOPSAZ=60*mm;
  matOrsayOPSA    = NULL;
  matName     = "LYSO";
  
  ancSD       = NULL;
  ancName     = G4String("OrsayOPSA");
  ancOffset   = 33000;
  
  numAncSd = 0;

  myMessenger = new OrsayOPSAMessenger(this,name);
}

OrsayOPSA::~OrsayOPSA()
{
  delete myMessenger;
}

G4int OrsayOPSA::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matOrsayOPSA = ptMaterial;
    G4String nome = matOrsayOPSA->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary shell! " << G4endl;
    return 1;
  }  
  return 0;
}

void OrsayOPSA::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    

  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, "/anc/orsayopsa", 
					"OrsayOPSAAncCollection", offset,
					depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }  
}


void OrsayOPSA::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void OrsayOPSA::Placement()
{
  ///////////////////////
  /// construct a crystal
  ///////////////////////
  G4Box *solidOrsayOPSA = new G4Box("LYSOSolid", 
				    LYSOSide/2.,
				    LYSOSide/2., 
				    LYSOThickness/2.);
  G4LogicalVolume *logicOrsayOPSA = 
    new G4LogicalVolume(solidOrsayOPSA, 
			matOrsayOPSA, 
			"OrsayOPSALogical", 0, 0, 0 );

  // Sensitive Detector
  logicOrsayOPSA->SetSensitiveDetector( ancSD );
  // Vis Attributes
  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(0.0, 1.0, .5) );
  pVA->SetVisibility(true);
  logicOrsayOPSA->SetVisAttributes( pVA );
  std::ofstream opsaangles("opsaangles.txt");
  //First the inner ring of 8
  for(int at=0; at<8; at++){
    double angle = (360./8.*at)*deg;
    G4RotationMatrix *rmDetA = new G4RotationMatrix;
    rmDetA->rotateZ(-angle);
    G4ThreeVector pos = G4ThreeVector(cos(angle)*R1,
				      sin(angle)*R1,
				      OrsayOPSAZ+LYSOThickness/2); 
    new G4PVPlacement(rmDetA,
		      pos,
		      ("OrsayOPSAPhysical"+std::to_string(at)).c_str(),
		      logicOrsayOPSA, 
		      theDetector->HallPhys(), false, at);
    opsaangles << std::setw(2) << at << " " << std::setprecision(4) << std::setw(6)
	       << pos.theta()/deg << " " << std::setprecision(4) << std::setw(6)
	       << (pos.phi()/deg>=0 ? pos.phi()/deg : 360+pos.phi()/deg) << "\n"; 
  }
  //then the outer ring of 16
  for(int at=0; at<16; at++){
    double angle = (360./16.*at)*deg;
    G4RotationMatrix *rmDetA = new G4RotationMatrix;
    rmDetA->rotateZ(-angle);
    G4ThreeVector pos = G4ThreeVector(cos(angle)*R2,
				      sin(angle)*R2,
				      OrsayOPSAZ+LYSOThickness/2); 
    new G4PVPlacement(rmDetA,pos, 
		      ("OrsayOPSAPhysical"+std::to_string(at+8)).c_str(),
		      logicOrsayOPSA, 
		      theDetector->HallPhys(), false, at);
    opsaangles << std::setw(2) << at+8 << " " << std::setprecision(4) << std::setw(6)
	       << pos.theta()/deg << " " << std::setprecision(4) << std::setw(6)
	       << (pos.phi()/deg>=0 ? pos.phi()/deg : 360+pos.phi()/deg) << "\n"; 
  }
  G4cout << "OrsayOPSA Z " << OrsayOPSAZ/mm << G4endl;
  G4cout << "OrsayOPSA material is " << matOrsayOPSA->GetName() << G4endl;

  return;
}

////////////////////////////////
/// methods for the messenger
////////////////////////////////



void OrsayOPSA::ShowStatus()
{
  G4cout << " Orsay OPSA has been constructed\n" << G4endl;
  G4cout << " Orsay OPSA  material is " << matOrsayOPSA->GetName() << G4endl;
}

void OrsayOPSA::WriteHeader(std::ofstream &outFileLMD, G4double)
{
  outFileLMD << "ORSAY-OPSA DET. 24 SEG OFFSET\n";
  outFileLMD << " coded as " << ancOffset << "+SegId\n";
  outFileLMD << "END ORSAY-OPSA\n";
}

///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"

OrsayOPSAMessenger::OrsayOPSAMessenger(OrsayOPSA* pTarget, G4String /*name*/)
:myTarget(pTarget)
{ 
  
  myDirectory = new G4UIdirectory("/Nuball2/detector/ancillary/OrsayOPSA/");
  myDirectory->SetGuidance("Control of Orsay OPSA detector construction.");
  SetOrsayOPSAZ =
    new G4UIcmdWithADouble("/Nuball2/detector/ancillary/OrsayOPSA/SetZ",
			   this);
  SetOrsayOPSAZ->
    SetGuidance("Sets position of LYSO crystals along beam axes");
  SetOrsayOPSAZ->AvailableForStates(G4State_PreInit,G4State_Idle);
  /*  SetOrsayOPSASizeCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/OrsayOPSA/ancillarySize", this);  
  SetOrsayOPSASizeCmd->SetGuidance("Define size of the ancillary shell.");
  SetOrsayOPSASizeCmd->SetGuidance("Required parameters: 2 double (Rmin, Rmax in mm).");
  SetOrsayOPSASizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  OrsayOPSAMatCmd = new G4UIcmdWithAString("/Agata/detector/ancillary/OrsayOPSA/ancillaryMaterial", this);
  OrsayOPSAMatCmd->SetGuidance("Select material of the ancillary.");
  OrsayOPSAMatCmd->SetGuidance("Required parameters: 1 string.");
  OrsayOPSAMatCmd->SetParameterName("choice",false);
  OrsayOPSAMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);*/
}

OrsayOPSAMessenger::~OrsayOPSAMessenger()
{
  delete myDirectory;
  delete SetOrsayOPSAZ;
}

void OrsayOPSAMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 
  if( command == SetOrsayOPSAZ ) {
    myTarget->SetOrsayOPSAZ(SetOrsayOPSAZ->GetNewDoubleValue(newValue));
  }
}    

G4int OrsayOPSA::GetSegmentNumber( G4int, G4int , G4ThreeVector )
{
  //G4cout<<i2<<"  "<<v1<<G4endl;
  return 1;
}
