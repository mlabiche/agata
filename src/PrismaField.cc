#include "PrismaField.hh"
#include "G4SystemOfUnits.hh"
#include "G4AutoLock.hh"
#include "G4ParticleDefinition.hh"

#include "G4Track.hh"
#include "G4Event.hh"
#include "AgataSteppingAction.hh"

#include <iostream>
#include <iomanip>
#include <istream>
#include <algorithm>
#include <fstream>
#include <vector>
#include <cmath>

#define quadrupole
// #define realDipole
#define simpleDipole

namespace
{
  G4Mutex PrismaFieldLock = G4MUTEX_INITIALIZER;
}

PrismaField::PrismaField() 
{
  // Reading the quadrupole field file

  // Lock the operation while reading input data so two threads don't try to
  // do it at the same time 
  G4AutoLock lock(&PrismaFieldLock);

  G4String fileName = "field/mapQuadrupole.dat";
  ifstream fMap;
  fMap.open(fileName, std::ios::out);

  if (fMap.fail())
  {
    G4ExceptionDescription ed;
    
    //ed << "Could not open field map file " << fMap << G4endl;
    ed << "Could not open field map file " << G4endl;
   
    G4Exception("PrismaField::PrismaField", "PRISMAFIELD001", FatalException,
     ed);
  }

  // WARNING: the PRISMA code uses a system of axis different than that of Geant4.
  // Following conversions (PRISMA -> AGATA) are necessary:
  // x -> +y, y -> -x and z -> +z 

  //G4int nbPointX_PRISMA = 96;
  //G4int nbPointY_PRISMA = 96;
  //G4int nbPointZ_PRISMA = 150;
  G4int nbPointX_PRISMA = 30;
  G4int nbPointY_PRISMA = 30;
  G4int nbPointZ_PRISMA = 120;

  nbX = nbPointY_PRISMA;
  nbY = nbPointX_PRISMA;
  nbZ = nbPointZ_PRISMA;

  fieldX.resize(nbY);
  fieldY.resize(nbY);
  fieldZ.resize(nbY);

  G4int iY, iX;

  for (iY = 0; iY < nbY; iY++)
  {
    fieldX[iY].resize(nbX);
    fieldY[iY].resize(nbX);
    fieldZ[iY].resize(nbX);

    for (iX = 0; iX < nbX; iX++)
    {
      fieldX[iY][iX].resize(nbZ);
      fieldY[iY][iX].resize(nbZ);
      fieldZ[iY][iX].resize(nbZ);
    }
  }

  string line;
  G4double y, x, z, bY, bX, bZ;
  G4double shiftZ = 75.*cm;

  // First loop to find the extend of the magnetic field along the x, y and z
  // axes
  while (std::getline(fMap, line))
  {
    istringstream iss(line);
    iss >> y;
    iss >> x;
    iss >> z;
    iss >> bY;
    iss >> bX;
    iss >> bZ;

    // Output coordinates from PRISMA (in mm) are with respect to the center of
    // the quadrupole located at (0., 0., 75.*cm) in the AGATA world
    x = -(x / 10.)*cm;
    y = (y / 10.)*cm;
    z = (z / 10.)*cm + shiftZ;
        
    posX.push_back(x);
    posY.push_back(y);
    posZ.push_back(z);

    if(fMap.eof()) break; 
  }

  minPosX = *std::min_element(posX.begin(), posX.end());
  maxPosX = *std::max_element(posX.begin(), posX.end());

  minPosY = *std::min_element(posY.begin(), posY.end());
  maxPosY = *std::max_element(posY.begin(), posY.end());

  minPosZ = *std::min_element(posZ.begin(), posZ.end());
  maxPosZ = *std::max_element(posZ.begin(), posZ.end());

  sizeX = maxPosX - minPosX;
  sizeY = maxPosY - minPosY;
  sizeZ = maxPosZ - minPosZ;

  G4cout << "Min X = " << minPosX/cm << " Max X = " << maxPosX/cm
  << " Min Y = " << minPosY/cm << " Max Y = " << maxPosY/cm
  << " Min Z = " << minPosZ/cm << " Max Z = " << maxPosZ/cm << G4endl;
     
  G4cout << "\n ---> Magnetic field extend x, y, z = " 
   << sizeX/cm << "*" << sizeY/cm << "*" << sizeZ/cm << "cm3"
   << G4endl;

  fMap.clear();
  fMap.seekg(0);

  G4int indY, indX, indZ;

  // Second loop to store the field components for each point
  for (indY = 0; indY < nbY; indY++)
  {
    for (indX = 0; indX < nbX; indX++)
    {
      for (indZ = 0; indZ < nbZ; indZ++)
      {
        std::getline(fMap, line);
        istringstream iss(line);
        
        iss >> y;
        iss >> x;
        iss >> z;
        iss >> bY;
        iss >> bX;
        iss >> bZ;
        
        fieldX[indY][indX][indZ] = -bX*tesla;
        fieldY[indY][indX][indZ] = bY*tesla;
        fieldZ[indY][indX][indZ] = bZ*tesla;
      }
    }
  }

  fMap.close();
  lock.unlock();

  // Dipole geometry
  curvature_outerRadius = 170.*cm; 
  curvature_innerRadius = 70.*cm; 
  dipole_halfHeight = 10.*cm;
}

PrismaField::~PrismaField()
{}

void PrismaField::GetFieldValue(const G4double point[3], G4double *Bfield) const
{
  Bfield[0] = 0.;
  Bfield[1] = 0.;
  Bfield[2] = 0.;

  G4double x = point[0];
  G4double y = point[1];
  G4double z = point[2];

#ifdef quadrupole
  G4double distToZAxis = sqrt(x*x + y*y);

  // Checks if the particle is inside the quadrupole
//  if (x >= minPosX && x <= maxPosX && y >= minPosY && y <= maxPosY
//   && z >= minPosZ && z <= maxPosZ)
//  {
   if (distToZAxis >= 0.*cm && distToZAxis <= 15.*cm && z >= 15.*cm
   && z <= 135.*cm)
  {   
    // Normalized position of the particle inside the quadrupole
    G4double xfraction = (-x - minPosX) / sizeX;
    G4double yfraction = (y - minPosY) / sizeY; 
    G4double zfraction = (z - minPosZ) / sizeZ;

    // Need addresses of these to pass to modf below.
    // modf uses its second argument as an OUTPUT argument.
    G4double fX, fY, fZ;
    
    // To get the indices (fX, fY, fZ) of the particle closest point in the
    // field map having LOWER x, y and z. 
    // xlocal, ylocal, zlocal are the coordinates of the particles
    // with respect to this point. These could be used for an interpolation of
    // the field.
    G4double xlocal = (std::modf(xfraction*(nbX-1), &fX));
    G4double ylocal = (std::modf(yfraction*(nbY-1), &fY));
    G4double zlocal = (std::modf(zfraction*(nbZ-1), &fZ));
    
    // Checks if the particle is closer to the next point in the field map along
    // x, y and z
    if (fX < nbX-1 && xlocal >= .5) fX++;
    if (fY < nbY-1 && ylocal >= .5) fY++;
    if (fZ < nbZ-1 && zlocal >= .5) fZ++;

    // The indices of the nearest tabulated point whose coordinates
    // are all less than those of the given point
    G4int indX = static_cast<G4int>(fX);
    G4int indY = static_cast<G4int>(fY);
    G4int indZ = static_cast<G4int>(fZ);

    Bfield[0] = fieldX[indY][indX][indZ];
    Bfield[1] = fieldY[indY][indX][indZ];
    Bfield[2] = fieldZ[indY][indX][indZ];


    G4cout << xlocal/mm << " " << ylocal/mm << " " << zlocal/mm
     << " Bx = " << Bfield[0]/tesla << " By = " << Bfield[1]/tesla
     << " Bz = " << Bfield[0]/tesla << G4endl;

  }
#endif
  
  /*********** REAL DIPOLE GEOMETRY ***********/

#ifdef realDipole
  // Coordinates of the centre of the section of cylindre defining the dipole in
  // the world frame
   G4double centreX = -cos(20.*deg) * 120.*cm;
   G4double centreZ = sin(20.*deg) * 120.*cm + 160.*cm;

   G4double xLocal = x - centreX;
   G4double zLocal = z - centreZ;

   G4double curvatureRadius = sqrt(xLocal*xLocal + zLocal*zLocal);
  
   // Checks if the particle is inside the dipole
   if (x > (centreZ - z) / tan(20.*deg) + centreX // Dipole entrance
    && x > (z - centreZ) / tan(55.*deg) + centreX // Dipole exit
    && y > -dipole_halfHeight && y < dipole_halfHeight
    && curvatureRadius > curvature_innerRadius
    && curvatureRadius < curvature_outerRadius)
   {
    // Intensity adapted to Zr-90 with an ionic charge = +40
     //G4double Bmax = .5295*tesla;
     // Intensity adapted to Zr-90 with an ionic charge = +30
    G4double Bmax = .699056*tesla;

     Bfield[1] = Bmax;
   }

#endif

  /*********** SIMPLE DIPOLE GEOMETRY FOR TEST ***********/

#ifdef simpleDipole

  G4double centreX = -120.*cm;
  G4double centreZ = 160.*cm;

  G4double xLocal = x - centreX;
  G4double zLocal = z - centreZ;

  G4double curvatureRadius = sqrt(xLocal*xLocal + zLocal*zLocal);
  
  // Checks if the particle is inside the dipole
  if (z >= 160.*cm // Dipole entrance
   && x >= ((z - centreZ) / tan(60.*deg)) + centreX  // Dipole exit
   && y >= -dipole_halfHeight && y <= dipole_halfHeight
   && curvatureRadius >= curvature_innerRadius
   && curvatureRadius <= curvature_outerRadius)
  {
    // Intensity adapted to Zr-90 with an ionic charge = +40
    //G4double Bmax = .5295*tesla; 
    // Intensity adapted to Zr-90 with an ionic charge = +30
    G4double Bmax = .699056*tesla; 

    Bfield[1] = Bmax;
  }
#endif

}
