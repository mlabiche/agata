#include "AgataAncillaryCluster.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"
#include "CConvexPolyhedron.hh"
#include "AgataRunAction.hh"

#include "AgataAncillaryHelper.hh"

#include "G4AssemblyVolume.hh"
#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4IntersectionSolid.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "globals.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryCluster::AgataAncillaryCluster( G4String path, G4String name, 
            G4String solid, G4String clu, G4String eul, G4String wal, G4String ancName, G4String matName )
{
  this->InitData( path, name, solid, clu, eul, wal, ancName, matName );
}

void AgataAncillaryCluster::InitData( G4String path, G4String name, 
            G4String solid, G4String clu, G4String eul, G4String wal, G4String ancName, G4String matName )
{
  if( path.find( "./", 0 ) != string::npos ) {
    G4int position = path.find( "./", 0 );
    if( position == 0 )
      path.erase( position, 2 );
  }
  iniPath = path;
  
  directoryName      = name;

  matCryst           = NULL;
  matWalls           = NULL;
  matHole            = NULL;
  
  matCrystName       = matName;
  matWallsName       = "Aluminium";
  matHoleName        = "Vacuum";
  
  nEuler             = 0;
  eulerFile          = iniPath + "Ancillary/" + eul;
  
  nPgons             = 0;
  solidFile          = iniPath + "Ancillary/" + solid;

  nWalls             = 0;
  wallsFile          = iniPath + "Ancillary/" + wal;

  nClAng             = 0;
  clustFile          = iniPath + "Ancillary/" + clu;
  
  clust.clear();
  pgons.clear();
  euler.clear();
  walls.clear();

  nDets              = 0;
  iCMin              = 0;
  iGMin              = 0;
  
  nPgons             =  0;
  nDets              =  0;
  nClus              =  0;
  
  thetaShift         = 0.;
  phiShift           = 0.;
  
  posShift           = G4ThreeVector();
  
  useCylinder        = true;
  
  maxSolids          = 0;
  
  myMessenger        = new AgataAncillaryClusterMessenger(this,name,ancName);
} 

AgataAncillaryCluster::~AgataAncillaryCluster()
{
  clust.clear();
  pgons.clear();
  euler.clear();
  walls.clear();

  delete  myMessenger;
}

G4int AgataAncillaryCluster::FindMaterials( G4String name )
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matCrystName);
  if (ptMaterial) {
    matCryst = ptMaterial;
    G4cout << "\n----> The crystals material of " << name << " is " << matCryst->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matCrystName << G4endl;
    G4cout << " Could not build " << name << "! " << G4endl;
    return 1;
  }  
  // search the material by its name
  ptMaterial = G4Material::GetMaterial(matWallsName);
  if (ptMaterial) {
    matWalls = ptMaterial;
    G4cout << "\n----> The capsules material of " << name << " is " << matWalls->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matWallsName << G4endl;
    G4cout << " Could not build " << name << "! " << G4endl;
  }
    
  ptMaterial = G4Material::GetMaterial(matHoleName);
  if (ptMaterial) {
    matHole = ptMaterial;
    G4cout << "\n----> The hole material of " << name << " is " << matHole->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matHoleName << G4endl;
    G4cout << " Could not build " << name << "! " << G4endl;
  }
  G4cout << " Completed!" << G4endl;
  return 0;  
}

void AgataAncillaryCluster::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
}

void AgataAncillaryCluster::Placement()
{
  G4int depth = 0;
  ancSD->SetDepth(depth);

  ReadSolidFile();
  ReadClustFile();
  ReadWallsFile();
  ReadEulerFile();
  ConstructGeCrystals();
  if(nWalls)
    ConstructTheWalls();

  ConstructTheClusters();
  PlaceTheClusters();
}


/////////////////////////////////////////////////////////////
///////////////// methods to read the files
/////////////////////////////////////////////////////////////
void AgataAncillaryCluster:: ReadSolidFile()
{
  FILE      *fp;
  char      line[256];
  G4int     lline, i1, i2, i3, nvdots, opgon;
  float     x, y, z, X, Y, Z;
  
  nPgons =  0;
  nDets  =  0;
  nClus  =  0;
  
  if( (fp = fopen(solidFile, "r")) == NULL) {
    G4cout << "\nError opening data file " << solidFile << G4endl;
    exit(-1);
  }

  G4cout << "\nReading description of crystals from file " << solidFile << " ..." << G4endl;

  pgons.clear();
  
  nvdots   =  0;
  opgon    = -1;
  maxPgons = -1;
  CpolyhPoints *pPg = NULL;

  while(fgets(line, 255, fp) != NULL) {
    lline = strlen(line);
    if(lline < 2) continue;
    if(line[0] == '#') continue;
    if(sscanf(line,"%d %d %d %f %f %f %f %f %f", &i1, &i2, &i3, &x, &y, &z, &X, &Y, &Z) != 9) {
      nPgons++;
      break;
    }
    if(opgon != i1) {
      nPgons++;
      opgon = i1;
      pgons.push_back( CpolyhPoints() );
      pPg = &pgons.back();
      pPg->whichGe  = i1;
      if( i1 > maxPgons )
        maxPgons = i1;
      pPg->npoints  = 2*i2;
      pPg->tubX       = -1.*mm;
      pPg->tubY       = -1.*mm;
      pPg->tubZ       = -1.*mm;
      pPg->tubr       = -1.*mm;
      pPg->tubR       = -1.*mm;
      pPg->tubL       = -1.*mm;
      pPg->capSpace   = -1.*mm;
      pPg->capThick   = -1.*mm;
      pPg->passThick1 = -1.*mm;
      pPg->passThick2 = -1.*mm;
      pPg->colx       =  0.;
      pPg->coly       =  0.;
      pPg->colz       =  0.;
      pPg->vertex.resize(pPg->npoints);
      pPg->cylinderMakesSense = true;
      pPg->makeCapsule        = true;
      pPg->isPlanar           = false;
      pPg->segSize_x  = -1.*mm;
      pPg->segSize_y  = -1.*mm;
      pPg->maxSize_x  = -1000.*m;
      pPg->maxSize_y  = -1000.*m;
      pPg->minSize_x  =  1000.*m;
      pPg->minSize_y  =  1000.*m;
      pPg->guardThick[0] = -1.*mm;
      pPg->guardThick[1] = -1.*mm;
      pPg->guardThick[2] = -1.*mm;
      pPg->guardThick[3] = -1.*mm;
      pPg->nSeg_x     =  1;
      pPg->nSeg_y     =  1;
    }
    if(i2==0 && i3==0) {
      pPg->tubr = ((G4double)x) * mm;
      pPg->tubR = ((G4double)y) * mm;
      pPg->tubL = ((G4double)z) * mm;
      pPg->tubX = ((G4double)X) * mm;
      pPg->tubY = ((G4double)Y) * mm;
      pPg->tubZ = ((G4double)Z) * mm;
    }
    else if(i2==0 && i3==1) {
      pPg->thick      = ((G4double)x) * mm;
    }
    else if(i2==0 && i3==2) {
      pPg->colx     = ((G4double)x);
      pPg->coly     = ((G4double)y);
      pPg->colz     = ((G4double)z);
    }
    else {
      pPg->vertex[i3   ] = G4Point3D( ((G4double)x), ((G4double)y),  ((G4double)z) ) * mm;
      pPg->vertex[i3+i2] = G4Point3D( ((G4double)X), ((G4double)Y),  ((G4double)Z) ) * mm;
      nvdots += 2;
    }
  }

  fclose(fp);
  G4cout << nPgons << " polyhedra for a total of " << nvdots << " vertex points read." << G4endl;
  
  G4int npt, npg, nn;
  G4double tolerance = 0.5*mm;
  for(npg = 0; npg < nPgons; npg++) {
    pPg = &pgons[npg];
    npt =  pPg->npoints;
    if(!npt) continue;

    // calculates z of the two faces of the original polyhedron
    pPg->centerFace1 = G4Point3D();
    pPg->centerFace2 = G4Point3D();
    for(nn=0; nn < npt/2; nn ++ ) {
      pPg->centerFace1 += pPg->vertex[nn      ];
      pPg->centerFace2 += pPg->vertex[nn+npt/2];
    }
    pPg->centerFace1 /= npt/2;
    pPg->centerFace2 /= npt/2;
    
    pPg->zFace1  = pPg->centerFace1.z();  
    pPg->zFace2  = pPg->centerFace2.z();
    pPg->zCenter = 0.5 * (pPg->zFace1 + pPg->zFace2);
    
    // calculates the minimum radius of a cylinder surrounding the polyhedron
    G4double minR2 = 0.;
    G4double theR2;
    for( nn=0; nn<npt; nn++ ) {
      theR2 = pow( pPg->vertex[nn].x(), 2. ) + pow( pPg->vertex[nn].y(), 2. );
      if(theR2 > minR2) minR2 = theR2;
    } 
    pPg->minR = sqrt(minR2) + 2.*tolerance; // safety margin!
    
    // check: to avoid tolerance problems, increase the cylinder length
    if( fabs((pPg->zFace2-pPg->zFace1) - pPg->tubL) < tolerance )
      pPg->tubL += 2.*tolerance;
    
    // check the validity of the cylinder
    if( pPg->cylinderMakesSense && (pPg->tubR < 0.) ) {
      pPg->cylinderMakesSense = false;
      G4cout << " Warning! Cylinder will not be built in solid " << pPg->whichGe << G4endl;
    }    
    if( pPg->cylinderMakesSense && (pPg->tubL < 0.) ) {
      pPg->cylinderMakesSense = false;  
      G4cout << " Warning! Cylinder will not be built in solid " << pPg->whichGe << G4endl;
    }    
    if( pPg->cylinderMakesSense && (pPg->tubr < 0.) ) {
      G4cout << " Warning! Setting inner cylinder radius to zero in solid " << pPg->whichGe << G4endl;
      pPg->tubr = 0.;
    }
    if( pPg->cylinderMakesSense && (pPg->tubr > pPg->tubR) ) {
      pPg->cylinderMakesSense = false;  
      G4cout << " Warning! Cylinder will not be built in solid " << pPg->whichGe << G4endl;
    }  
    // if the cylinder does not exceed the polyhedron, keep the cylinder coordinates!
    if( pPg->zCenter-pPg->tubL/2. > pPg->zFace1 )
      pPg->zFace1 = pPg->zCenter-pPg->tubL/2.; 
    if( pPg->zCenter+pPg->tubL/2. < pPg->zFace2 )
      pPg->zFace2 = pPg->zCenter+pPg->tubL/2.;
    // additional check: if crystal is not long enough, no hole!!!
    if( ( pPg->zFace1+pPg->thick ) > pPg->zFace2 ) {
      G4cout << " Warning! Setting inner cylinder radius to zero in solid " << pPg->whichGe << G4endl;
      pPg->tubr = 0.;
    }
    
    // passive areas
    // at the back of the detector
    
    if( pPg->makeCapsule && (pPg->capSpace <= 0.) ) {
      pPg->makeCapsule = false;
      G4cout << " Warning! Capsule will not be built for solid " << pPg->whichGe << G4endl;
    }  
    if( pPg->makeCapsule && (pPg->capThick <= 0.) ) {
      pPg->makeCapsule = false;
      G4cout << " Warning! Capsule will not be built for solid " << pPg->whichGe << G4endl;
    }
  }
}

void AgataAncillaryCluster:: ReadWallsFile()
{
  FILE      *fp;
  char      line[256];
  G4int     lline, i1, i2, i3, i4, i5, i6, nvdots, opgon;
  float     x, y, z, X, Y, Z;
  CpolyhPoints *pPg = NULL;

  nWalls = 0;
  nWlTot = 0;
  if(!wallsFile) return;

  if( (fp = fopen(wallsFile, "r")) == NULL) {
    G4cout << "\nError opening data file " << wallsFile << G4endl;
    G4cout << "No walls included." << G4endl;
    return;
  }

  G4cout << "\nReading description of walls from file " << wallsFile << " ..." << G4endl;

  walls.clear();
  
  nvdots = 0;
  opgon  = -1;

  while(fgets(line, 255, fp) != NULL) {
    lline = strlen(line);
    if(lline < 1) continue;
    if(line[0] == '#') continue;
    if(sscanf(line,"%d %d %d %d %d %d %f %f %f %f %f %f", 
         &i1, &i2, &i3, &i4, &i5, &i6, &x, &y, &z, &X, &Y, &Z) != 12)
      break;
    if(opgon != i3) {
      nWalls++;
      opgon = i3;
      walls.push_back( CpolyhPoints() );
      pPg = &walls.back();
      pPg->whichGe   = i1;
      pPg->whichCrystal = i2;
      pPg->whichWall = i3;
      pPg->npoints   = 2*i5;
      pPg->vertex.resize(pPg->npoints);
    }
    pPg->vertex[i6   ] = G4Point3D( ((G4double)x), ((G4double)y),  ((G4double)z) ) * mm;
    pPg->vertex[i6+i5] = G4Point3D( ((G4double)X), ((G4double)Y),  ((G4double)Z) ) * mm;
    nvdots += 2;
    
  }
  fclose(fp);
  G4cout << nWalls << " polyhedra for a total of " << nvdots << " vertex points read." << G4endl;
}

void AgataAncillaryCluster:: ReadClustFile()
{
  FILE      *fp;
  char      line[256];
  G4int     lline, i1, i2, i3, nsolids, oclust;
  float  psi, th, ph;
  float  x, y, z;
  
  nClAng    =  0;
  maxSolids = 0;
  
  if( (fp = fopen(clustFile, "r")) == NULL) {
    G4cout << "\nError opening data file " << clustFile << G4endl;
    exit(-1);
  }
  
  G4RotationMatrix rm;

  G4cout << "\nReading description of clusters from file " << clustFile << " ..." << G4endl;

  clust.clear();
  
  nsolids =  0;
  oclust  = -1;
  CclusterAngles *pPg = NULL;
  CeulerAngles   *pEa = NULL;

  while(fgets(line, 255, fp) != NULL) {
    lline = strlen(line);
    if(lline < 2) continue;
    if(line[0] == '#') continue;
    if(sscanf(line,"%d %d %d %f %f %f %f %f %f", &i1, &i2, &i3, &psi, &th, &ph, &x, &y, &z) != 9) {
      nClAng++;
      break;
    }
    if(oclust != i1) {
      nClAng++;
      oclust = i1;
      clust.push_back( CclusterAngles() );
      pPg = &clust.back();
      pPg->whichClus = i1;
      pPg->nsolids   = 0;
      pPg->solids.clear();
      pPg->nwalls   = 0;
      pPg->pAssV = new G4AssemblyVolume();
    }
    pPg->solids.push_back( CeulerAngles() );
    pEa = &pPg->solids.back();
    pEa->whichGe = i2;
    pEa->numPhys = i3;
    
    pEa->ps = ((G4double)psi) * deg;
    pEa->th = ((G4double)th) * deg;
    pEa->ph = ((G4double)ph) * deg;
    
    pEa->rotMat.set(0,0,0);
    pEa->rotMat.rotateZ(((G4double)psi) * deg);
    pEa->rotMat.rotateY(((G4double)th) * deg);
    pEa->rotMat.rotateZ(((G4double)ph) * deg);
    
    pEa->trasl = G4ThreeVector( ((G4double)x), ((G4double)y), ((G4double)z) ) * mm;

    pEa->pTransf = new G4Transform3D( pEa->rotMat, pEa->trasl );
    
    pPg->nsolids++;        
    nsolids++;    
  }

  fclose(fp);
  
  for( G4int ii=0; ii<nClAng; ii++ ) {
    pPg = &clust[ii];
    if( pPg->nsolids > maxSolids )
      maxSolids = pPg->nsolids;
  } 
  G4cout << " Read " << nClAng << " cluster description for a total of " << nsolids << " individual solids." << G4endl;
}

void AgataAncillaryCluster:: ReadEulerFile()
{
  FILE      *fp;
  char      line[256];
  G4int     lline, i1, i2;
  float     psi, th, ph, x, y, z;
  
  if( (fp = fopen(eulerFile, "r")) == NULL) {
    G4cout << "\nError opening data file " << eulerFile << G4endl;
    exit(-1);
  }

  euler.clear();

  G4cout << "\nReading Euler angles from file " << eulerFile << " ..." << G4endl;
  nEuler = 0;
  
  G4RotationMatrix  rm;
  CeulerAngles     *pEa = NULL;

  while(fgets(line, 255, fp) != NULL) {
    lline = strlen(line);
    if(lline < 2) continue;
    if(line[0] == '#') continue;
    if(sscanf(line,"%d %d %f %f %f %f %f %f", &i1, &i2, &psi, &th, &ph, &x, &y, &z) != 8)
      break;
    euler.push_back( CeulerAngles() );
    pEa = &euler[nEuler];
    
    
    pEa->numPhys = i1;
    pEa->whichGe = i2;

    pEa->rotMat.set( 0, 0, 0 );
    pEa->rotMat.rotateZ(((G4double)psi)*deg);
    pEa->rotMat.rotateY(((G4double)th)*deg);
    pEa->rotMat.rotateZ(((G4double)ph)*deg);
    
    pEa->ps      = ((G4double)psi)*deg;
    pEa->th      = ((G4double)th)*deg;
    pEa->ph      = ((G4double)ph)*deg;
    
    pEa->trasl   = G4ThreeVector( ((G4double)x), ((G4double)y), ((G4double)z) ) * mm;
    
    pEa->pTransf = new G4Transform3D( pEa->rotMat, pEa->trasl );
    
    nEuler++;
  }

  fclose(fp);
  G4cout << nEuler << " Euler angles read." << G4endl;
}


///////////////////////////////////////////////////////////////////////////////////////
///////////////// methods to construct and place the actual volumes
/////////////////////////////////////////////////////////////////////////////////////
void AgataAncillaryCluster::ConstructGeCrystals()
{
  
  if( !matCryst ) {
    G4cout << G4endl << "----> Missing material, cannot build the crystals!" << G4endl;
    return;
  }
  
  char sName[50];
  
  // identity matrix
  G4RotationMatrix rm;
  rm.set(0,0,0);

  G4int ngen, nPg, nGe, nPt;

  // data to construct the cylinders and the passive parts
  // they are generated in their final position to avoid problems in placement
  G4double *InnRadGe;
  G4double *OutRadGe;
  G4double *zSliceGe;
  
  G4cout << G4endl << "Generating crystals ... " << G4endl;
  ngen = 0;
  for(nPg = 0; nPg < nPgons; nPg++) {
    CpolyhPoints *pPg = &pgons[nPg];
    nGe = pPg->whichGe;
    if(nGe < 0) {
      G4cout << "DetectorConstruction::ConstructGeCrystals : crystal " << nPg
             << " skipped because nGe ( " << nGe << " ) out of range " << G4endl;
      continue;
    }
    nPt = pPg->npoints;
    if(nPt < 6) {
      G4cout << "DetectorConstruction::ConstructGeCrystals : crystal " << nPg
             << " skipped because of too few ( " << nPt << " ) points " << G4endl;
      continue;
    }

    sprintf(sName, "gePoly%2.2d", nGe);
#ifndef G4V10
    pPg->pPoly  = new CConvexPolyhedron(G4String(sName), pPg->vertex);
#else
    // maybe need equivalent here ??? 
#endif
    
    pPg->pDetVA  = new G4VisAttributes( G4Color(pPg->colx, pPg->coly, pPg->colz) );

    G4double zFace1 = pPg->zFace1;
    
    pPg->pDetL1 = NULL;
    pPg->pDetL2 = NULL;

    if ( useCylinder && pPg->cylinderMakesSense ) {
                
      // no coaxial hole!
      if(pPg->tubr == 0) {   
        zSliceGe = new G4double[2];
        zSliceGe[0] = pPg->zCenter-pPg->tubL/2.;
        zSliceGe[1] = pPg->zCenter+pPg->tubL/2.;

        InnRadGe = new G4double[2];
        InnRadGe[0] = pPg->tubr;
        InnRadGe[1] = pPg->tubr;

        OutRadGe = new G4double[2];
        OutRadGe[0] = pPg->tubR;
        OutRadGe[1] = pPg->tubR;

        sprintf(sName, "geTubs%2.2d", nGe);
        pPg->pCoax  = new G4Polycone(G4String(sName), 0.*deg, 360.*deg, 2, zSliceGe, InnRadGe, OutRadGe );
        sprintf(sName, "geCapsPolyTubs%2.2d", nGe);
        pPg->pCaps = new G4IntersectionSolid(G4String(sName), pPg->pPoly, pPg->pCoax, G4Transform3D( rm, G4ThreeVector() ) );

      }
      else {
        zSliceGe = new G4double[4];
        zSliceGe[0] = pPg->zCenter-pPg->tubL/2.;
        zSliceGe[1] = zFace1 + pPg->thick;
        zSliceGe[2] = zFace1 + pPg->thick;
        zSliceGe[3] = pPg->zCenter+pPg->tubL/2.;

        InnRadGe = new G4double[4];
        InnRadGe[0] = 0.;
        InnRadGe[1] = 0.;
        InnRadGe[2] = pPg->tubr;
        InnRadGe[3] = pPg->tubr;

        OutRadGe = new G4double[4];
        OutRadGe[0] = pPg->tubR;
        OutRadGe[1] = pPg->tubR;
        OutRadGe[2] = pPg->tubR;
        OutRadGe[3] = pPg->tubR;
        
        sprintf(sName, "gePcone%2.2d", nGe);
        pPg->pCoax  = new G4Polycone(G4String(sName), 0.*deg, 360.*deg, 4, zSliceGe, InnRadGe, OutRadGe );
        sprintf(sName, "geCapsPolyPcone%2.2d", nGe);
        pPg->pCaps = new G4IntersectionSolid(G4String(sName), pPg->pPoly, pPg->pCoax, G4Transform3D( rm, G4ThreeVector() ) );
        
      }
      sprintf(sName, "geDetCapsL%2.2d", nGe);
      pPg->pDetL  = new G4LogicalVolume( pPg->pCaps, matCryst, G4String(sName), 0, 0, 0 ); // intersezione di Poly e Tubs
    }
    else {
      sprintf(sName, "geDetPolyL%2.2d", nGe);
      pPg->pDetL  = new G4LogicalVolume( pPg->pPoly, matCryst, G4String(sName), 0, 0, 0 ); // solo i poliedri

    }

    pPg->pDetL->SetVisAttributes( pPg->pDetVA );
    pPg->pDetL->SetSensitiveDetector( ancSD );
    ngen++;
  }
  G4cout << "Number of generated crystals is " << ngen << G4endl;
}

void AgataAncillaryCluster::ConstructTheWalls()
{
  char sName[50];
  G4int ngen, nPg, nGe, nPt;
  
  if( !matWalls ) {
    G4cout << G4endl << "----> Missing material, cannot build the walls!" << G4endl;
    return;
  }
  
  G4cout << G4endl << "Generating walls ... " << G4endl;

  ngen = 0;
  for(nPg = 0; nPg < nWalls; nPg++) {
    CpolyhPoints *pPg = &walls[nPg];
    nGe = pPg->whichGe;
    if(nGe < 0 || nGe >= nPgons) continue;
    nPt = pPg->npoints;
    if(nPt >=6) {
      sprintf(sName, "wlPoly%2.2d", nGe);

#ifndef G4V10
      pPg->pPoly  = new CConvexPolyhedron(G4String(sName), pPg->vertex );
#else
      // maybe need something equivalent her ???
#endif

      sprintf(sName, "wlDetL%2.2d", nGe);
      pPg->pDetL  = new G4LogicalVolume( pPg->pPoly, matWalls, G4String(sName), 0, 0, 0 );

      pPg->pDetVA = new G4VisAttributes( G4Colour(0.5, 0.5, 0.5) );
      pPg->pDetVA->SetForceWireframe(false);
      pPg->pDetL->SetVisAttributes( pPg->pDetVA );

      ngen++;
    }
  }
  G4cout << "Number of generated walls is " << ngen << G4endl;
}

void AgataAncillaryCluster::ConstructTheClusters()
{
  
  G4int            nCa, nPg, nSo;
  CclusterAngles*  pCa;
  CpolyhPoints*    pPg;
  CeulerAngles*    pEa;
  G4RotationMatrix rm;
  G4ThreeVector    rotatedPos;
  G4Transform3D    transf;

  G4cout << G4endl << "Building the clusters ..." << G4endl;
  
  for( nCa=0; nCa<nClAng; nCa++ ) {
    G4cout << " Cluster #" << nCa << G4endl;
    pCa = &clust[nCa];
    for( nSo=0; nSo<pCa->nsolids; nSo++ ) {
      pEa = &pCa->solids[nSo];
      
      rm = pEa->rotMat;

      rotatedPos = pEa->trasl;
      
      // detectors
      for( nPg=0; nPg<nPgons; nPg++ ) {
        pPg = &pgons[nPg];  
        
        if( pPg->whichGe != pEa->whichGe )
          continue;
        if( !pPg->pDetL )
          continue;
          
        if( pCa->pAssV ) {
          transf = G4Transform3D( *(pEa->pTransf) );
          pCa->pAssV->AddPlacedVolume( pPg->pDetL, transf );
        }
        
        printf( "  Solid %4d      %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f\n", 
          pPg->whichGe, pEa->ps/deg, pEa->th/deg, pEa->ph/deg, rotatedPos.x()/cm, rotatedPos.y()/cm, rotatedPos.z()/cm );      
       
      }
    }
    // the walls
    rm.set(0, 0, 0);
    G4double psi = 0.;
    G4double th = 0.;
    G4double ph = 0.;
    rotatedPos = G4ThreeVector();
    // walls
    for( nPg=0; nPg<nWalls; nPg++ ) {
      pPg = &walls[nPg];

      if( pPg->whichGe != pCa->whichClus )
        continue;
      if( !pPg->pDetL )
        continue;

      if( pCa->pAssV ) {
        transf = G4Transform3D( rm, rotatedPos );
        pCa->pAssV->AddPlacedVolume( pPg->pDetL, transf );
        pCa->nwalls++;
      }  

      printf( "  Wall  %4d      %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f\n", pPg->whichWall,
       psi/deg, th/deg, ph/deg, rotatedPos.x()/cm, rotatedPos.y()/cm, rotatedPos.z()/cm );
    }
  }
}

void AgataAncillaryCluster::PlaceTheClusters()
{
  G4int    nGe, nCl, nEa, nCa, nPg, indexP;
  
  CclusterAngles* pCa = NULL;
  CeulerAngles*   pEc = NULL;

  G4RotationMatrix rm;
  G4RotationMatrix rm1;
  G4RotationMatrix radd;
  G4RotationMatrix rmP;    // PRISMA rotation
  G4ThreeVector rotatedPos;
  G4ThreeVector rotatedPos1;
  G4ThreeVector rotatedPos2;
  G4Transform3D transf;

  G4int  iClTot = 0;
  G4int  iClMin = -1;
  G4int  iClMax = -1;  
  
  nDets  = 0;
  nWlTot = 0;
  nClus  = 0;

  G4cout << G4endl << "Placing clusters ... " << G4endl;
  
  G4int  *iCl = new G4int[nClAng];
  memset(iCl, 0, nClAng*sizeof(G4int));

  rmP.set(0, 0, 0 );
  
  for(nEa = 0; nEa < nEuler; nEa++) {
    pEc = &euler[nEa];
    nCl = pEc->whichGe;
    if(nCl < 0) continue;
    
    nGe = pEc->numPhys;
    
    for(nCa = 0; nCa < nClAng; nCa++) {
      pCa = &clust[nCa];
      if(pCa->whichClus != nCl) continue;
      if(!pCa->pAssV) continue;
      
      rm = pEc->rotMat;

      rotatedPos = pEc->trasl + posShift;
      
      if( (thetaShift*phiShift!=0.) || (thetaShift+phiShift!=0.) ) {
        radd.set(0, 0, 0 );
        radd.rotateY( thetaShift );
        radd.rotateZ( phiShift );
        rotatedPos = radd( rotatedPos );
        rm = radd * rm;
      }
      
      indexP = 1000 * nGe + maxSolids * nGe;
      
      transf = G4Transform3D( rm, rotatedPos );

      pCa->pAssV->MakeImprint(theDetector->HallLog(), transf, indexP-1);
      
      nDets  += pCa->nsolids;
      nWlTot += pCa->nwalls;
      nClus++;
      
      if(iClMin < 0 || nGe < iClMin) iClMin = nGe;
      if(iClMax < 0 || nGe > iClMax) iClMax = nGe;
      
      printf("Placing Ancillary cluster: %4d %4d %4d %8d %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f\n",
             iClTot, nGe, nCl, indexP, pEc->ps/deg, pEc->th/deg, pEc->ph/deg,
                pEc->trasl.x()/cm, pEc->trasl.y()/cm, pEc->trasl.z()/cm );
      iCl[nCl]++;
      iClTot++;
    }
  }
  nClus = iClMax - iClMin + 1;
    
  iCMin = iClMin;
  iCMax = iClMax;
  iGMin = maxSolids*iClMin;
  iGMax = maxSolids*(iClMax+1)-1;
  
  G4cout << "Number of placed clusters is " << iClTot << "  [ ";
  for(nPg = 0; nPg < nClAng; nPg++) G4cout << iCl[nPg] << " ";
  G4cout << "]" << G4endl;
  G4cout << "Cluster  Index ranging from " << iClMin << " to " << iClMax << G4endl;
  G4cout << "Detector Index ranging from " << maxSolids*iClMin << " to " << maxSolids*(iClMax+1)-1 << G4endl << G4endl;
  G4cout << "Number of placed walls is " << nWlTot << G4endl << G4endl;
  delete [] iCl;
}

void AgataAncillaryCluster::ShowStatus( G4String name )
{
  G4cout << G4endl;
  G4int prec = G4cout.precision(3);
  G4cout.setf(ios::fixed);
  G4cout << " Array " << name << " composed of " << std::setw(3) << nDets  << " detectors" << G4endl;
  G4cout << "       arranged in " << std::setw(3) << nClus  << " clusters"  << G4endl;
  G4cout << " Array " << name << " composed of " << std::setw(3) << nWlTot << " walls"  << G4endl;
  G4cout << " Description of detectors                read from " << solidFile << G4endl;
  G4cout << " Description of dead materials (walls)   read from " << wallsFile << G4endl;
  G4cout << " Description of clusters                 read from " << clustFile << G4endl;
  G4cout << " Euler angles for clusters               read from " << eulerFile << G4endl;
  
  if( useCylinder )
    G4cout << " The intersection with a cylinder has been considered in generating the crystals." << G4endl;
  else
    G4cout << " The intersection with a cylinder has not been considered in generating the crystals." << G4endl;  
       
  G4cout << " The detectors   material is " << matCrystName << G4endl;
  G4cout << " The walls       material is " << matWallsName << G4endl;
  if( thetaShift || phiShift )
    G4cout << " The array is rotated by theta, phi = " << thetaShift/deg << ", " 
                                                       << phiShift/deg   << " degrees" << G4endl;
  if( posShift.mag2() )
    G4cout << " The array is shifted by " << posShift/mm << " mm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

void AgataAncillaryCluster::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

G4int AgataAncillaryCluster::GetSegmentNumber( G4int /*offset*/, G4int /*nGe*/, G4ThreeVector /*position*/ )
{
  return 0;
}

///////////////////////////////////////////////////////////////////
/// methods for the messenger
////////////////////////////////////////////////////////////////////
void AgataAncillaryCluster::SetSolidFile(G4String nome)
{
  if( nome(0) == '/' )
    solidFile = nome;
  else {
    if( nome.find( "./", 0 ) != string::npos ) {
      G4int position = nome.find( "./", 0 );
      if( position == 0 )
        nome.erase( position, 2 );
    }  
    solidFile = iniPath + "Ancillary/" + nome;
  }  
     
  G4cout << " ----> The solid vertexes are read from "
             << solidFile << G4endl;
}
  
void AgataAncillaryCluster::SetAngleFile(G4String nome)
{
  if( nome(0) == '/' )
    eulerFile = nome;
  else {
    if( nome.find( "./", 0 ) != string::npos ) {
      G4int position = nome.find( "./", 0 );
      if( position == 0 )
        nome.erase( position, 2 );
    }  
    eulerFile = iniPath + "Ancillary/" + nome;
  }  

  G4cout << " ----> The Euler angles are read from "
             << eulerFile << G4endl;
}
    
void AgataAncillaryCluster::SetWallsFile(G4String nome)
{
  if( nome(0) == '/' )
    wallsFile = nome;
  else {
    if( nome.find( "./", 0 ) != string::npos ) {
      G4int position = nome.find( "./", 0 );
      if( position == 0 )
        nome.erase( position, 2 );
    }  
    wallsFile = iniPath + "Ancillary/" + nome;
  }  
  
  G4cout << " ----> The walls vertexes are read from "
             << wallsFile << G4endl;
}

void AgataAncillaryCluster::SetClustFile(G4String nome)
{
  if( nome(0) == '/' )
    clustFile = nome;
  else {
    if( nome.find( "./", 0 ) != string::npos ) {
      G4int position = nome.find( "./", 0 );
      if( position == 0 )
        nome.erase( position, 2 );
    }  
    clustFile = iniPath + "Ancillary/" + nome;
  }  
  
  G4cout << " ----> The cluster descriptions are read from "
             << clustFile << G4endl;
}
  
void AgataAncillaryCluster::SetDetMate(G4String materialName)
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(materialName);
  if (ptMaterial) {
    matCrystName = materialName;
    G4cout << "\n ----> The detector material is "
          << materialName << G4endl;
  }
  else {
    G4cout << " Material not found! " << G4endl;
    G4cout << "Keeping previously set detector material ("
           << matCryst->GetName() << ")" << G4endl;
  }
}

void AgataAncillaryCluster::SetWallsMate(G4String materialName)
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(materialName);
  if (ptMaterial) {
    matWallsName = materialName;
    G4cout << "\n ----> The walls material is "
          << materialName << G4endl;
  }
  else {
    G4cout << " Material not found! " << G4endl;
    G4cout << " ----> Keeping previously set walls material ("
           << matWalls->GetName() << ")" << G4endl;
  }
}

void AgataAncillaryCluster::SetThetaShift( G4double angle )
{
  thetaShift = angle;
  G4cout << " ----> The positions are rotated by an angle theta = " << thetaShift/deg << " degrees" << G4endl;
}

void AgataAncillaryCluster::SetPhiShift( G4double angle )
{
  phiShift = angle;
  G4cout << " ----> The positions are rotated by an angle phi = " << phiShift/deg << " degrees" << G4endl;
}

void AgataAncillaryCluster::SetUseCylinder( G4bool value )
{
  useCylinder = value;
  if( useCylinder ) {
    G4cout << " ----> The intersection between a cylinder and a polyhedron will be performed." << G4endl;
  }
  else {  
    G4cout << " ----> The intersection between a cylinder and a polyhedron will not be performed." << G4endl;
  }  
}

void AgataAncillaryCluster::SetPosShift( G4ThreeVector shift )
{
  posShift = shift * mm;

  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  G4cout  << " --> Array will be shifted to ("
          << std::setw(8) << posShift.x()/mm
          << std::setw(8) << posShift.y()/mm
          << std::setw(8) << posShift.z()/mm << " ) mm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

  
///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"

AgataAncillaryClusterMessenger::AgataAncillaryClusterMessenger(AgataAncillaryCluster* pTarget,G4String name, G4String ancName)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/detector/Ancillary/" + ancName + "/";
  

  commandName = directoryName + "solidFile";
  aLine = commandName.c_str();
  SetSolidCmd = new G4UIcmdWithAString(aLine, this);
  SetSolidCmd->SetGuidance("Select file with the detector vertexes.");
  SetSolidCmd->SetGuidance("Required parameters: 1 string.");
  SetSolidCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
  commandName = directoryName + "clustFile";
  aLine = commandName.c_str();
  SetClustCmd = new G4UIcmdWithAString(aLine, this);
  SetClustCmd->SetGuidance("Select file with the cluster description.");
  SetClustCmd->SetGuidance("Required parameters: 1 string.");
  SetClustCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
  commandName = directoryName + "angleFile";
  aLine = commandName.c_str();
  SetAngleCmd = new G4UIcmdWithAString(aLine, this);
  SetAngleCmd->SetGuidance("Select file with the detector angles.");
  SetAngleCmd->SetGuidance("Required parameters: 1 string.");
  SetAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
  commandName = directoryName + "wallsFile";
  aLine = commandName.c_str();
  SetWallsCmd = new G4UIcmdWithAString(aLine, this);
  SetWallsCmd->SetGuidance("Select file with the walls vertexes.");
  SetWallsCmd->SetGuidance("Required parameters: 1 string.");
  SetWallsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "detectorMaterial";
  aLine = commandName.c_str();
  DetMatCmd = new G4UIcmdWithAString(aLine, this);
  DetMatCmd->SetGuidance("Select Material of the detector.");
  DetMatCmd->SetGuidance("Required parameters: 1 string.");
  DetMatCmd->SetParameterName("choice",false);
  DetMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "wallsMaterial";
  aLine = commandName.c_str();
  WalMatCmd = new G4UIcmdWithAString(aLine, this);
  WalMatCmd->SetGuidance("Select Material of the walls.");
  WalMatCmd->SetGuidance("Required parameters: 1 string.");
  WalMatCmd->SetParameterName("choice",false);
  WalMatCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "rotateArray";
  aLine = commandName.c_str();
  RotateArrayCmd = new G4UIcmdWithAString(aLine, this);
  RotateArrayCmd->SetGuidance("Select rotation of the array.");
  RotateArrayCmd->SetGuidance("Required parameters: 2 double (rotation angles in degrees).");
  RotateArrayCmd->SetParameterName("choice",false);
  RotateArrayCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "traslateArray";
  aLine = commandName.c_str();
  TraslateArrayCmd = new G4UIcmdWith3Vector(aLine, this);
  TraslateArrayCmd->SetGuidance("Select traslation of the array.");
  TraslateArrayCmd->SetGuidance("Required parameters: 3 double (x, y, z components in mm).");
  TraslateArrayCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "enableCyl";
  aLine = commandName.c_str();
  EnableCylCmd = new G4UIcmdWithABool(aLine, this);
  EnableCylCmd->SetGuidance("Consider the intersection between a cylinder and a polyhedron.");
  EnableCylCmd->SetGuidance("Required parameters: none.");
  EnableCylCmd->SetParameterName("useCylinder",true);
  EnableCylCmd->SetDefaultValue(true);
  EnableCylCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableCyl";
  aLine = commandName.c_str();
  DisableCylCmd = new G4UIcmdWithABool(aLine, this);
  DisableCylCmd->SetGuidance("Do not consider the intersection between a cylinder and a polyhedron.");
  DisableCylCmd->SetGuidance("Required parameters: none.");
  DisableCylCmd->SetParameterName("useCylinder",true);
  DisableCylCmd->SetDefaultValue(false);
  DisableCylCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataAncillaryClusterMessenger::~AgataAncillaryClusterMessenger()
{
  delete DetMatCmd;
  delete WalMatCmd;
  delete RotateArrayCmd;
  delete TraslateArrayCmd;
  delete SetSolidCmd;
  delete SetAngleCmd;
  delete SetWallsCmd;
  delete SetClustCmd;
  delete EnableCylCmd;
  delete DisableCylCmd;
}

void AgataAncillaryClusterMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetSolidCmd ) {
    myTarget->SetSolidFile(newValue);
  } 
  if( command == SetAngleCmd ) {
    myTarget->SetAngleFile(newValue);
  } 
  if( command == SetWallsCmd ) {
    myTarget->SetWallsFile(newValue);
  } 
  if( command == SetClustCmd ) {
    myTarget->SetClustFile(newValue);
  } 
  if( command == DetMatCmd ) {
    myTarget->SetDetMate(newValue);
  }
  if( command == WalMatCmd ) {
    myTarget->SetWallsMate(newValue);
  }
  if( command == RotateArrayCmd ) {
    float e1, e2;
//    sscanf( newValue, "%lf %lf", &e1, &e2);
    sscanf( newValue, "%f %f", &e1, &e2);
    myTarget->SetThetaShift( ((G4double)e1)*deg );
    myTarget->SetPhiShift  ( ((G4double)e2)*deg );
  }
  if( command == TraslateArrayCmd ) {
    myTarget->SetPosShift(TraslateArrayCmd->GetNew3VectorValue(newValue));
  }
  if( command == EnableCylCmd ) {
    myTarget->SetUseCylinder( EnableCylCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableCylCmd ) {
    myTarget->SetUseCylinder( DisableCylCmd->GetNewBoolValue(newValue) );
  }
}



