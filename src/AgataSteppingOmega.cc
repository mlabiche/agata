#include "AgataSteppingOmega.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"
#include "AgataEventAction.hh"
#include "G4RunManager.hh"

AgataSteppingOmega::AgataSteppingOmega()

{
  if(getenv("DeadCrystals")){
    std::istringstream deadcrystalss(getenv("DeadCrystals"));
    int acrystal;
    while(deadcrystalss>>acrystal){
      DeadDetectors.push_back(acrystal);
    }
    
  }
}

void AgataSteppingOmega::UserSteppingAction(const G4Step* theStep)
{
  // This "if" condition is needed, otherwise after the volume/solid angle
  // calculation it will be no more possible to track properly the other particles
  if( theStep->GetTrack()->GetDefinition()->GetParticleName() == "geantino" ) {
    G4RunManager * runManager = G4RunManager::GetRunManager();
    theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
    G4int evnb = ((AgataEventAction*)runManager->GetUserEventAction())->GetEventID();
    G4int depthSensitive = theDetector->GeSD()->GetDepth();
    G4int detCode;
#ifdef GASP
    if( theStep->GetPreStepPoint()->GetMaterial()->GetName() == "Tungsten" ||
        theStep->GetPreStepPoint()->GetMaterial()->GetName() == "BGO"      ||
        theStep->GetPreStepPoint()->GetMaterial()->GetName() == "Lead"        )
      theStep->GetTrack()->SetTrackStatus(fStopAndKill);  
#endif
    /// Material must be active germanium !!!
    if( theStep->GetPreStepPoint()->GetMaterial()->GetName() == "Germanium" &&
        theStep->GetPreStepPoint()->GetPhysicalVolume()->GetLogicalVolume()->GetSensitiveDetector() != NULL ) { 
      detCode = theStep->GetPreStepPoint()->GetTouchable()->GetReplicaNumber(depthSensitive);
      if(std::find(DeadDetectors.begin(),DeadDetectors.end(),detCode%1000)
	 ==DeadDetectors.end()){
	theDetector->IncrementHits( detCode%1000 );
	theDetector->IncrementThetaPhi(theStep->GetTrack()->GetPosition());
	theDetector->IncrementThetaCoin(theStep->GetTrack()->GetTrackID(),evnb
					,detCode%1000
					,theStep->GetTrack()->GetPosition());
      }
      theStep->GetTrack()->SetTrackStatus(fStopAndKill);
    }
  }
}
