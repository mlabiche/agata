#ifdef ANCIL
#include "AgataAncillaryFatima.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4Polyhedra.hh"
#include "G4Trap.hh"

#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4AssemblyVolume.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryFatima::AgataAncillaryFatima(G4String path, G4String name )
{
    // dummy assignment needed for compatibility with other implementations of this class
    G4String iniPath    = path;
    
    dirName             = name;
    
    nam_vacuum          = "Vacuum";
    mat_vacuum          = NULL;
    
    nam_aluminium       = "Aluminum";
    mat_aluminium       = NULL;
    
    nam_lead            = "Lead";
    mat_lead            = NULL;
    
    nam_labr3           = "LaBr3";
    mat_labr3           = NULL;
    
    nam_quartz          = "Quarz";
    mat_quartz          = NULL;

    nam_mumetal          = "Mumetal";
    mat_mumetal         = NULL;
    
    ancSD               = NULL;
    ancName             = G4String("FATIMA");
    ancOffset           = 18000;
    
    numAncSd            = 0;
}

AgataAncillaryFatima::~AgataAncillaryFatima()
{}

//G4Material* FindMaterialPrisma (G4String Name){ // since we added Prisma as ancillary
G4Material* FindMaterial (G4String Name){ 
    // search the material by its name
    G4Material* ptMaterial = G4Material::GetMaterial(Name);
    if (ptMaterial) {
        G4String nome = ptMaterial->GetName();
        G4cout << "----> FATIMA uses material " << nome << G4endl;
    }
    else {
        G4cout << " Could not find the material " << Name << G4endl;
        G4cout << " Could not build the ancillary Fatima! " << G4endl;
        return NULL;
    }
    return ptMaterial;
}

G4int AgataAncillaryFatima::FindMaterials()
{
    mat_vacuum      = FindMaterial(nam_vacuum);     if (!mat_vacuum)    return 1;  
    mat_aluminium   = FindMaterial(nam_aluminium);  if (!mat_aluminium) return 1;
    mat_lead        = FindMaterial(nam_lead);       if (!mat_lead)      return 1;
    mat_labr3       = FindMaterial(nam_labr3);      if (!mat_labr3)     return 1;
    mat_quartz      = FindMaterial(nam_quartz);     if (!mat_quartz)    return 1;
    mat_mumetal      = FindMaterial(nam_mumetal);     if (!mat_mumetal)    return 1;
//    mat_vacuum      = FindMaterialPrisma(nam_vacuum);     if (!mat_vacuum)    return 1;  // since we added Prisma as ancillary 
//    mat_aluminium   = FindMaterialPrisma(nam_aluminium);  if (!mat_aluminium) return 1;  // since we added Prisma as ancillary
//    mat_lead        = FindMaterialPrisma(nam_lead);       if (!mat_lead)      return 1;  // since we added Prisma as ancillary
//    mat_labr3       = FindMaterialPrisma(nam_labr3);      if (!mat_labr3)     return 1;  // since we added Prisma as ancillary
//    mat_quartz      = FindMaterialPrisma(nam_quartz);     if (!mat_quartz)    return 1;  // since we added Prisma as ancillary
//    mat_mumetal      = FindMaterialPrisma(nam_mumetal);     if (!mat_mumetal)    return 1;  // since we added Prisma as ancillary
    return 0;
}

void AgataAncillaryFatima::InitSensitiveDetector()
{
    G4int offset = ancOffset;
#ifndef FIXED_OFFSET
    offset = theDetector->GetAncillaryOffset();
#endif
    G4SDManager* SDman = G4SDManager::GetSDMpointer();
    
    if( !ancSD ) {
        ancSD = new AgataSensitiveDetector(dirName, "/anc/Fatima", "FatimaCollection", offset,/* depth */ 0,/* menu */ false );
        SDman->AddNewDetector( ancSD );
        numAncSd++;
    }
}


void AgataAncillaryFatima::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillaryFatima::Placement()
{
    ///////////////////////
    /// construct a shell
    ///////////////////////
    
    //double    Fatima_Radius = 15.6*cm;        //inner radius of Fatima (Default) = distance to target
    double    Fatima_Radius = 11.5*cm;        //inner radius of Fatima (Default) =distance to target
    const double    Labr_Length = 45.75*mm;         //length of the crystal (+5mm = 2inch)
    const double    Labr_Rad    = 19.0*mm;          //radius of the crystal (*2 = 1.5 inch)
    const double    Window_Length = 5.0*mm;         //quartz window length behind LaBr crystal
    const double    Pi  = 3.14159265358979323846;   //you realy have to read this?
    
    //geometries in G4: see
    // http://geant4.web.cern.ch/geant4/UserDocumentation/UsersGuides/ForApplicationDeveloper/html/ch04.html
    
    //Building the Aluminium case
    G4double zPlane1[16] = {0.0*mm  , 1.0*mm,   1.0*mm,   1.5*mm,   1.5*mm,   46.0*mm,  46.0*mm,  56.0*mm,  56.0*mm, 71.0*mm, 71.0*mm, 244.3*mm, 244.3*mm, 260.8*mm, 260.8*mm, 263.3*mm};
    G4double rInner1[16] = {19.0*mm , 19.0*mm,  0.0*mm,   0.0*mm,   Labr_Rad, Labr_Rad, Labr_Rad, Labr_Rad, 32.0*mm, 32.0*mm, 32.0*mm, 32.0*mm,  32.0*mm,  32.0*mm,  0.0*mm,   0.0*mm};
    G4double rOuter1[16] = {22.75*mm, 22.75*mm, 22.75*mm, 22.75*mm, 22.75*mm, 22.75*mm, 37.5*mm,  37.5*mm,  37.5*mm, 37.5*mm, 35.0*mm, 35.0*mm,  37.5*mm,  37.5*mm,  37.5*mm,  35.5*mm};
    G4Polycone *solid_PMT = new G4Polycone("PMT", 0.0, 2*Pi, 16, zPlane1 ,rInner1, rOuter1);
    G4LogicalVolume *logicPMT = new G4LogicalVolume( solid_PMT, mat_aluminium, "log_PMT", 0, 0, 0 );
    
    //Building the mu-metal shield
    //G4double Length=2*mm;
    //G4double Height=200*mm;
    //G4double BaseLarge=200*mm;
    //G4double BaseSmall=100*mm;
    //G4Trap *solidmumet = new G4Trap("MuMet", 
    //                                      Length/2, 0*deg, 0*deg, 
    //                                      Height/2, BaseLarge/2, BaseSmall/2, 0*deg, 
    //                                      Height/2, BaseLarge/2, BaseSmall/2, 0*deg);

    G4double zSlices[2]={0.*cm,8.*cm};
    G4double InnRad[2]={6.*cm,35.*cm};
    //G4double InnRad[2]={7.*cm,35.*cm};
    G4double OutRad[2]={7.*cm,36.*cm}; // 1cm thick
    //G4double OutRad[2]={7.*cm,37.*cm}; // 2cm thick
    //G4double OutRad[2]={7.2*cm,35.2*cm}; // 2mm thick
    //G4double OutRad[2]={8*cm,36*cm}; // 1cm thick
    G4Polyhedra *solidmumet = new G4Polyhedra("MuMet", 
					      0.*deg, 360.*deg, 
					      8, 2, zSlices, 
					      InnRad, OutRad);
    //G4LogicalVolume *logicmumet = new G4LogicalVolume(solidmumet, mat_lead, "MuMet", 0, 0, 0);
    G4LogicalVolume *logicmumet = new G4LogicalVolume(solidmumet, mat_mumetal, "MuMet", 0, 0, 0);
    //G4LogicalVolume *logicmumet = new G4LogicalVolume(solidmumet, mat_vacuum, "MuMet", 0, 0, 0);

    //Building the Lead shielding
    G4double zPlane2[5] = {0.0*mm  , 2.0*mm,   40.0*mm,  40.0*mm, 46.0*mm};
    G4double rInner2[5] = {22.8*mm , 22.8*mm,  22.8*mm,  22.8*mm, 22.8*mm};
    G4double rOuter2[5] = {25.75*mm, 27.75*mm, 27.75*mm, 37.5*mm, 37.5*mm};
    G4Polycone *solid_Shielding = new G4Polycone("Shielding", 0.0, 2*Pi, 5, zPlane2 ,rInner2, rOuter2);
    G4LogicalVolume *logicShielding = new G4LogicalVolume( solid_Shielding, mat_lead, "log_Shield", 0, 0, 0 );
    //G4LogicalVolume *logicShielding = new G4LogicalVolume( solid_Shielding, mat_vacuum, "log_Shield", 0, 0, 0 );
    
    //Building the LaBr3 crystal + window:
    G4Tubs *solid_Scintillator = new G4Tubs("Scintillator", 0.0*mm, Labr_Rad, Labr_Length/2 ,0, 2*Pi);
    G4LogicalVolume *logicScintillator = new G4LogicalVolume( solid_Scintillator, mat_labr3, "log_Scintillator", 0, 0, 0 );

    G4Tubs *solid_Window = new G4Tubs("Window", 0.0*mm, Labr_Rad, Window_Length/2 ,0, 2*Pi);
    G4LogicalVolume *logicWindow = new G4LogicalVolume( solid_Window, mat_quartz, "log_Window", 0, 0, 0 );


    //Assembly of FATIMA DETECTOR:
    G4RotationMatrix Rid = G4RotationMatrix::IDENTITY;

    G4AssemblyVolume* Fatima_Detector = new G4AssemblyVolume();
    G4Transform3D T1(Rid,G4ThreeVector(0.,0.,0.));                               Fatima_Detector->AddPlacedVolume(logicPMT,          T1);
    G4Transform3D T2(Rid,G4ThreeVector(0.,0.,0.));                               Fatima_Detector->AddPlacedVolume(logicShielding,    T2);
    G4Transform3D T3(Rid,G4ThreeVector(0.,0.,2*mm+Labr_Length/2));               Fatima_Detector->AddPlacedVolume(logicScintillator, T3);
    G4Transform3D T4(Rid,G4ThreeVector(0.,0.,2*mm+Labr_Length+Window_Length/2)); Fatima_Detector->AddPlacedVolume(logicWindow,       T4);
    

    //Assembly of mu-metal sheild
    G4AssemblyVolume* Fatima_MuMetShield = new G4AssemblyVolume();
    G4Transform3D Tmumet(Rid,G4ThreeVector(0.,0.,0.));  
    Fatima_MuMetShield->AddPlacedVolume(logicmumet, Tmumet);



    //Assembly of FATIMA CLUSTER:
    G4AssemblyVolume* Fatima_Cluster = new G4AssemblyVolume();
    /* For 4 det cluster
    G4Transform3D T5(Rid,G4ThreeVector( 37.5*mm,  37.5*mm,0.)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T5);
    G4Transform3D T6(Rid,G4ThreeVector( 37.5*mm, -37.5*mm,0.)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T6);
    G4Transform3D T7(Rid,G4ThreeVector(-37.5*mm,  37.5*mm,0.)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T7);
    G4Transform3D T8(Rid,G4ThreeVector(-37.5*mm, -37.5*mm,0.)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T8);
    */
    /* For 3 aligned det cluster */
    G4RotationMatrix Rid5Y, Rid7Y;
    Rid5Y.rotateY(22.50*deg);
    Rid7Y.rotateY(-22.50*deg);
    G4Transform3D T5(Rid5Y,G4ThreeVector( 56*mm,  0.*mm, -16.*mm)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T5);
    G4Transform3D T6(Rid,G4ThreeVector( 0*mm, 0*mm,0.)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T6);
    G4Transform3D T7(Rid7Y,G4ThreeVector( -56*mm,  0*mm, -16.*mm)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T7);

    //G4Transform3D T5(Rid5Y,G4ThreeVector( 53*mm,  0.*mm,0.)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T5);
    //G4Transform3D T6(Rid,G4ThreeVector( 0*mm, 0*mm,0.)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T6);
    //G4Transform3D T7(Rid7Y,G4ThreeVector( -53*mm,  0*mm,0.)); Fatima_Cluster->AddPlacedAssembly(Fatima_Detector, T7);


   
    //Placing the clusters:
    G4RotationMatrix Rz1,Rz2, Rz, Ry, Rtot;
    Ry.rotateX(90*deg);
    //Ry.rotateX(0*deg);

    /* For PARIS+Fatima: 
    Rz1.rotateZ(45*deg);
    for (int i=0; i<4; i++) {
      Rz2.rotateZ(i*90*deg);
        Rtot = Rz2*Rz1*Ry;
        G4Transform3D TF(Rtot, Rtot*G4ThreeVector(0., 0., Fatima_Radius));
        Fatima_Cluster->MakeImprint(theDetector->HallLog(),TF, 1000*(i+1));
    }
    */

    /* For Fatima alone */
    //Rz1.rotateZ(40*deg); // tilted 3 det cluster
    Rz1.rotateZ(37.2*deg); // tilted 3 det cluster
    Fatima_Radius = 11.5*cm;
      for (int i=0; i<8; i++) {
        Rz.rotateZ(i*45*deg);
        Rtot = Rz*Ry*Rz1;
        //Rtot = Rz*Ry;
        G4Transform3D TF(Rtot, Rtot*G4ThreeVector(0., 0., Fatima_Radius));
        Fatima_Cluster->MakeImprint(theDetector->HallLog(),TF, 1000*(i+1));


        //G4Transform3D TF2(Rz, Rz*G4ThreeVector(0.,Fatima_Radius, 200.*mm));
        //Fatima_MuMetShield->MakeImprint(theDetector->HallLog(),TF2, 1000*(i+1));

    }



      // Placing the mu-metal shield in lab.
        G4Transform3D TF2(Rid, Rid*G4ThreeVector(0.,0, 100.*mm));
        Fatima_MuMetShield->MakeImprint(theDetector->HallLog(),TF2, 2000);


    //Have a look at the CopyNumbers:
    G4cout << "\n \n Listing of FATIMA's sensitive volumes \n";
    std::vector<G4VPhysicalVolume*>::iterator VI = Fatima_Cluster->GetVolumesIterator();
    for (unsigned int i=0; i<Fatima_Cluster->TotalImprintedVolumes(); i++){
        int CopyNumber = (*VI)->GetCopyNo();
        if (CopyNumber %10 == 3) {// this selects the Scintillators...
            (*VI)->SetCopyNo(CopyNumber/100); //CopyNo coding "AB" with A = cluster numnber 1..8, B = detector number 0..3 (if cluster of 4 modules), 0..2 if cluster of 3 modules
            G4cout << (*VI)->GetCopyNo() << " " << (*VI)->GetName() << G4endl;
        }
        VI++;
    }
    
    //Scintillator is sensitive detector:
    logicScintillator -> SetSensitiveDetector(ancSD);
    
    // Vis Attributes
    G4VisAttributes *pVA1 = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) ); logicPMT->SetVisAttributes( pVA1 );
    G4VisAttributes *pVA2 = new G4VisAttributes( G4Colour(1.0, 1.0, 0.0) ); logicShielding->SetVisAttributes( pVA2 );
    G4VisAttributes *pVA3 = new G4VisAttributes( G4Colour(0.0, 1.0, 0.0) ); logicScintillator->SetVisAttributes( pVA3 );
    G4VisAttributes *pVA4 = new G4VisAttributes( G4Colour(0.0, 0.0, 1.0) ); logicWindow->SetVisAttributes( pVA4 );
     G4VisAttributes *pVA5 = new G4VisAttributes( G4Colour(1.0, 0.0, 1.0) ); logicmumet->SetVisAttributes( pVA5 );
    
    return;
}

void AgataAncillaryFatima::ShowStatus()
{
  G4cout << " ANCILLARY FATIMA has been constructed." << G4endl;
  G4cout << "     Ancillary Vacuum      material is " << mat_vacuum->GetName() << G4endl;
  G4cout << "     Ancillary Scintilator material is " << mat_labr3->GetName() << G4endl;
  G4cout << "     Ancillary Shielding   material is " << mat_lead->GetName() << G4endl;
  G4cout << "     Ancillary Window      material is " << mat_quartz->GetName() << G4endl;
  G4cout << "     Ancillary PMT         material is " << mat_aluminium->GetName() << G4endl;
}

void AgataAncillaryFatima::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

#endif
