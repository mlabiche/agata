#ifdef ANCIL
#include "AgataAncillaryGSIChambRing.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SubtractionSolid.hh"
#include "G4UnionSolid.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

AgataAncillaryGSIChambRing::AgataAncillaryGSIChambRing(G4String path, G4String name )
{
  // dummy assignment needed for compatibility with other implementations of this class
  G4String iniPath     = path;
  
  dirName     = name;
  
  matShell    = NULL;
  matName     = "Aluminum";
  
  ancSD       = NULL;
  ancName     = G4String("GSIChambRing");
  ancOffset   = 6000;
  
  numAncSd = 0;
}

AgataAncillaryGSIChambRing::~AgataAncillaryGSIChambRing()
{}

G4int AgataAncillaryGSIChambRing::FindMaterials()
{
  // search the material by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matName);
  if (ptMaterial) {
    matShell = ptMaterial;
    G4String nome = matShell->GetName();
    G4cout << "\n----> The ancillary material is "
          << nome << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matName << G4endl;
    G4cout << " Could not build the ancillary brick! " << G4endl;
    return 1;
  }  
  return 0;
}

void AgataAncillaryGSIChambRing::InitSensitiveDetector()
{}


void AgataAncillaryGSIChambRing::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*)(runManager->GetUserDetectorConstruction());
}


void AgataAncillaryGSIChambRing::Placement()
{
  ///////////////////////
  /// construct a shell
  ///////////////////////
  G4double Rmin = 16.5*cm;
  G4double Rmax = 19.*cm;
  G4double STheta = (90.-23.)*deg;
  G4double DTheta = (2.*23.)*deg;
  G4double SPhi = 0.*deg;
  G4double DPhi = 360.*deg;
  

  G4RotationMatrix* rm= new G4RotationMatrix();
  rm->rotateY(90.*deg);

  G4Sphere        *FullRing = new G4Sphere( "MotherRing", Rmin, Rmax, SPhi, DPhi, STheta, DTheta);

  //G4LogicalVolume *logicFullRing = new G4LogicalVolume( FullRing, matShell, "GSIChambRing", 0, 0, 0 );
  //new G4PVPlacement(rm, G4ThreeVector(0., 0., 0.), "MotherRing", logicFullRing, theDetector->HallPhys(), false, 0 );

  Rmin = (19.-1.5)*cm;
  Rmax = 20.*cm;
  STheta = (90.-20.)*deg;
  DTheta = (2.*20.)*deg;
  SPhi = 20.*deg;
  DPhi = 45.*deg;
  G4Sphere        *solidCut1 = new G4Sphere( "RingCut1", Rmin, Rmax, SPhi, DPhi, STheta, DTheta);
  G4SubtractionSolid *RingCut1=new G4SubtractionSolid("cut1",FullRing,solidCut1, 0, G4ThreeVector(0.0,0.0,0.0));

  SPhi = 295.*deg;
  G4Sphere        *solidCut2 = new G4Sphere( "RingCut2", Rmin, Rmax, SPhi, DPhi, STheta, DTheta);
  G4SubtractionSolid *RingCut2=new G4SubtractionSolid("cut2", RingCut1,solidCut2, 0, G4ThreeVector(0.0,0.0,0.0));


  // Holes
  Rmin=0.*cm;
  Rmax=4.2*cm;
  G4double Halflength=2.5*cm;
  G4Tubs        *solidCut3 = new G4Tubs( "RingCut3", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut3=new G4SubtractionSolid("cut3", RingCut2,solidCut3, rm, G4ThreeVector(17.5*cm,0.0,0.0));

  Rmax= 5.6*cm;
  G4Tubs        *solidCut4 = new G4Tubs( "RingCut4", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut4=new G4SubtractionSolid("cut4", RingCut3,solidCut4, rm, G4ThreeVector(-17.5*cm,0.0,0.0));

  G4RotationMatrix* rmCut5= new G4RotationMatrix();
  rmCut5->rotateY(90.*deg);
  rmCut5->rotateX(90.*deg);
  G4Tubs        *solidCut5 = new G4Tubs( "RingCut5", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut5=new G4SubtractionSolid("cut5", RingCut4,solidCut5, rmCut5, G4ThreeVector(0.0*cm,17.5*cm,0.0));

  G4Tubs        *solidCut6 = new G4Tubs( "RingCut6", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut6=new G4SubtractionSolid("cut6", RingCut5,solidCut6, rmCut5, G4ThreeVector(0.0*cm,-17.5*cm,0.0));

  Rmax= 5.*cm;
  G4RotationMatrix* rmCut7= new G4RotationMatrix();
  rmCut7->rotateY(90.*deg);
  rmCut7->rotateX(45.*deg);
  G4Tubs        *solidCut7 = new G4Tubs( "RingCut7", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut7=new G4SubtractionSolid("cut7", RingCut6,solidCut7, rmCut7, G4ThreeVector((-17.5*cos(45.*3.14159/180.))*cm,(17.5*sin(45.*3.14159/180.))*cm,0.0));

  G4RotationMatrix* rmCut8= new G4RotationMatrix();
  rmCut8->rotateY(90.*deg);
  rmCut8->rotateX(-45.*deg);
   G4Tubs        *solidCut8 = new G4Tubs( "RingCut8", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut8=new G4SubtractionSolid("cut8", RingCut7,solidCut8, rmCut8, G4ThreeVector((-17.5*cos(45.*3.14159/180.))*cm,(-17.5*sin(45.*3.14159/180.))*cm,0.0));


  // external cut
  Rmin=18.*cm;
  Rmax=19.5*cm;
  Halflength=6.5*cm;
  G4Tubs        *solidCut9 = new G4Tubs( "RingCut9", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut9=new G4SubtractionSolid("cut9", RingCut8,solidCut9, 0, G4ThreeVector(0.0, 0.0, 0.0));

  G4RotationMatrix* rmX= new G4RotationMatrix();
  rmX->rotateX(90.*deg);

  G4Box        *solidCut10 = new G4Box( "RingCut10", 7.8*cm, 7.8*cm, 1*cm );
  G4SubtractionSolid *RingCut10=new G4SubtractionSolid("cut10", RingCut9,solidCut10, rmX, G4ThreeVector(0.0, 17.5*cm, 0.0));
  G4SubtractionSolid *RingCut11=new G4SubtractionSolid("cut11", RingCut10,solidCut10, rmX, G4ThreeVector(0.0, -17.5*cm, 0.0));




  // internal cut
  // side tubes
  Rmin=16.*cm;
  Rmax=17.40*cm;
  Halflength=0.85*cm;
  G4Tubs        *solidCut12 = new G4Tubs( "RingCut12", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut12=new G4SubtractionSolid("cut12", RingCut11,solidCut12, 0, G4ThreeVector(0.0, 0.0, 7.*cm));
  G4Tubs        *solidCut13 = new G4Tubs( "RingCut13", Rmin, Rmax, Halflength, 0., 360*deg );
  G4SubtractionSolid *RingCut13=new G4SubtractionSolid("cut13", RingCut12,solidCut13, 0, G4ThreeVector(0.0, 0.0, -7.*cm));


  // additionnal top and bottom 4mm pipes 

  Rmin=5.6*cm;
  Rmax=6*cm;
  Halflength=6*cm;
  G4Tubs        *solidPipe = new G4Tubs( "Pipe", Rmin, Rmax, Halflength, 0., 360*deg );

  Rmin=5.6*cm;
  Rmax=8.6*cm;
  Halflength=1*cm;
  G4Tubs        *solidFlangeRing = new G4Tubs( "FlangeRing", Rmin, Rmax, Halflength, 0., 360*deg );

  G4RotationMatrix* rm_dummy= new G4RotationMatrix();

  G4UnionSolid *PipeTop=new G4UnionSolid("PipeTop", solidPipe, solidFlangeRing, rm_dummy, G4ThreeVector(0.0, 0.0, 6.*cm));
  G4UnionSolid *PipeBottom=new G4UnionSolid("PipeBot", solidPipe, solidFlangeRing, rm_dummy, G4ThreeVector(0.0, 0.0, -6.*cm));

  G4UnionSolid *GSIChRing1=new G4UnionSolid("GSIChRing1", RingCut13, PipeTop, rmX, G4ThreeVector(0.0, 22.5*cm, 0.));
  G4UnionSolid *GSIChRing2=new G4UnionSolid("GSIChRing2", GSIChRing1, PipeBottom, rmX, G4ThreeVector(0.0, -22.5*cm, 0.));

  // additionnal forward 3mm thick pipe 

  Rmin=4.2*cm;
  Rmax=4.5*cm;
  Halflength= 15.*cm;
  G4Tubs        *solidForwardpipe = new G4Tubs( "Forwardpipe", Rmin, Rmax, Halflength, 0., 360*deg );
  G4UnionSolid *GSIChambRing=new G4UnionSolid("GSIChambRing", GSIChRing2, solidForwardpipe, rm, G4ThreeVector((18.+15.)*cm, 0., 0.));


  G4LogicalVolume *logicGSIChambRing = new G4LogicalVolume( GSIChambRing, matShell, "GSIChambRingFinal", 0, 0, 0 );
  //new G4PVPlacement(rm, G4ThreeVector(0., 0., 0.), "MotherRing", logicGSIChambRing, theDetector->HallPhys(), false, 0 );
  new G4PVPlacement(rm, G4ThreeVector(0., 0., 0.), "MotherRing", logicGSIChambRing, theDetector->HallPhys(), false, 0 );



  // Vis Attributes
  G4VisAttributes *pVA = new G4VisAttributes( G4Colour(0.0, 1.0, 1.0) );
  logicGSIChambRing->SetVisAttributes( pVA );

  return;
}


void AgataAncillaryGSIChambRing::ShowStatus()
{
  G4cout << " GSI Chamber Ring support has been constructed." << G4endl;
  G4cout << " GSI Chamber Ring  material is " << matShell->GetName() << G4endl;
}

void AgataAncillaryGSIChambRing::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}

#endif
