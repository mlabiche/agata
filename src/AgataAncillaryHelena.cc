#ifdef ANCIL
#include "AgataAncillaryHelena.hh"
#include "AgataAncillaryCluster.hh"
#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4SDManager.hh"
#include "G4ios.hh"

AgataAncillaryHelena::AgataAncillaryHelena(G4String path, G4String name )
{
  G4String iniPath = path;
  
  dirName     = name;

  // files needed by AgataAncillaryCluster
  G4String solidName = G4String("hsolid");
  G4String clustName = G4String("hclust");
  G4String angleName = G4String("heuler");
  G4String wallsName = G4String("hwalls");
  G4String matName   = G4String("BaF2");
  
  // members inherited from AgataAncillaryScheme
  ancSD       = NULL;
  ancName     = G4String("HELENA");
  ancOffset   = 10000;
  
  numAncSd = 0;
  
  // finally, instance a "Cluster" object
  theCluster = new AgataAncillaryCluster( iniPath, name, solidName, clustName, angleName, wallsName, ancName, matName );
  
}

AgataAncillaryHelena::~AgataAncillaryHelena()
{}

G4int AgataAncillaryHelena::FindMaterials()
{
  return theCluster->FindMaterials(ancName);
  return 0;
}

void AgataAncillaryHelena::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif 

  G4String name   = G4String("/anc/Helena");
  G4String HCname = G4String("HelenaCollection");

  // Sensitive Detector
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
    ancSD = new AgataSensitiveDetector( dirName, name, HCname, offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
  }
  theCluster->SetSensitiveDetector(ancSD);
}

void AgataAncillaryHelena::GetDetectorConstruction()
{
  G4RunManager* runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theCluster->GetDetectorConstruction();
}


void AgataAncillaryHelena::Placement()
{
  theCluster->Placement();
  return;
}


void AgataAncillaryHelena::ShowStatus()
{
  theCluster->ShowStatus(ancName);
}

void AgataAncillaryHelena::WriteHeader(std::ofstream &outFileLMD, G4double unit)
{
  theCluster->WriteHeader(outFileLMD, unit);
}

#endif
