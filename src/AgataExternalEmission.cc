#include "AgataExternalEmission.hh"
#include "AgataEventAction.hh"
#include "AgataEmitted.hh"

#include "Randomize.hh"
#include "G4RunManager.hh"
#include "globals.hh"
#include "G4ios.hh"
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// For AGATA-PRISMA G4 10.5
#include "G4Transportation.hh"

AgataExternalEmission::AgataExternalEmission( G4String path, G4bool hadr, G4bool value, G4String name )
{
  if( path.find( "./", 0 ) != string::npos ) {
    G4int position = path.find( "./", 0 );
    if( position == 0 )
      path.erase( position, 2 );
  }
  iniPath = path;

  usePolar         = value;
  hadrons          = hadr;
  
  emitNuclei       = true;
  showStatus       = false;

  theExternalEmitter = new AgataExternalEmitter(name);
  theEmitter         = (AgataEmitter*)theExternalEmitter;
  
  theEmitted       = NULL;
  emittedLUT       = NULL;

  evFileName       = iniPath + "aevent";
  
  ///////////////////////////////////////////////////////////
  /// The following flags are always set to true, so that
  /// all of the information is always written out to the
  /// list-mode file
  ///////////////////////////////////////////////////////////
  theEmitter->SetRecoilDiffuse (true);
  theEmitter->SetSourceDiffuse (true);
  theEmitter->SetSourceLived   (true);
  theEmitter->SetMovingSource  (true);
  theEmitter->SetBetaDiffuse   (true);
  
  isBetaDecay      = false;
  
  verbose          = true;
  
  skipEvents       = -1;
  targetEvents     = 1000000000;
  
  directoryName    = name;
  
  myMessenger      = new AgataExternalEmissionMessenger(this, hadrons, name);
}

AgataExternalEmission::~AgataExternalEmission()
{
  delete myMessenger;
}

//////////////////////////////////////////////////////////////////////////////
/// Begin of run: header of event file is decoded, the AgataEmitted objects
/// allocated, the emitted cascades are reset
//////////////////////////////////////////////////////////////////////////////
void AgataExternalEmission::BeginOfRun()
{
  openedFile = false;
  abortRun   = false;

  G4cout << " >>> Opening event file " << evFileName << G4endl;
  if( (evFile = fopen( evFileName, "r" ))==NULL ) {
    abortRun = true;
    abortMessage = " Could not open input file, aborting run ...";
    return;
  }  
  else
    openedFile = true;   
  
  ReadFileHeader( evFile );
    
  if( abortRun )
    return;

  theExternalEmitter->BeginOfRun();
  
  cascadeDeltaTime = 0.;
  cascadeTime      = 0.;
  cascadeMult      = 0;
  cascadeOrder     = 0;
  startOfEvent     = true;
  endOfEvent       = false;
  endOfRun         = false;
  showStatus       = true;
  firstLine        = true;
  
  readEvents       = 0;
  processedEvents  = 0;
  if( skipEvents > 0 )
    targetEvents++;
  
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theEvent = (AgataEventAction *) runManager->GetUserEventAction();
  
}

void AgataExternalEmission::BeginOfEvent()
{}

//////////////////////////////////////////////////////////////////////////////////////////////////////
/// End of event: emitted cascade is reset, momentum conservation is applied, next line concerning
/// the source is decoded
/////////////////////////////////////////////////////////////////////////////////////////////////////
void AgataExternalEmission::EndOfEvent()
{
  if( startOfEvent ) {
    startOfEvent     = false;
  }

  if( endOfEvent ) {
    endOfEvent       = false;
    startOfEvent     = true;
    cascadeDeltaTime = 0.;
    cascadeTime      = 0.;
    cascadeMult      = 0;
    cascadeOrder     = 0;
    return;
  }
  
  // conservation of momentum
  if( !nucleusIsEmitted ) {
    if( (emittedAtomNum >  emitterAtomNum) || (emittedMassNum > emitterMassNum) ) {
      abortRun = true;
      abortMessage = " Emitted particle is not compatible with residual nucleus, aborting run ...";
      return;
    }
    else {
      if( isBetaDecay )
        theExternalEmitter->UpdateEmitter( emittedAtomNum, emittedMassNum ); 
      else
        theExternalEmitter->UpdateEmitter( emittedAtomNum, emittedMassNum, emittedMomentum ); 
      	 
      emitterAtomNum  = theExternalEmitter->GetEmitterAtomNum();
      emitterMassNum  = theExternalEmitter->GetEmitterMassNum();
    }
  }
  else if( decodeEmitter ) {
    DecodeEmitter( bufferLine );
    return;
  }  
}

/////////////////////////////////////////
/// End of run: input file is closed
////////////////////////////////////////
void AgataExternalEmission::EndOfRun()
{
  theExternalEmitter->EndOfRun();
  if( openedFile ) {
    G4cout << " Closing event file " << evFileName << G4endl;
    fclose( evFile );
  }
}

void AgataExternalEmission::ReadFileHeader( FILE *ifp )
{
  G4int   ii;
  char  dummy[256];
  
  G4int    zT =  0;
  G4int    aT =  0;
  G4int    nT = -1;
  G4int    zB =  0;
  G4int    aB =  0;
  G4int    nB = -1;
  G4double eB = -1.0;

  createdEmitted = false;

  if(fscanf(ifp, "%s", dummy) != 1) {
    abortRun = true;
    abortMessage = "Error reading input file, aborting run ...";
    return;
  }  
  
  // Emitter, emitted type
  if(!strcmp(dummy, "FORMAT")) {
    if(fscanf(ifp, "%d %d", &emitterType, &emittedType) == 2) {
      G4cout << " Emitter type is " << emitterType << G4endl;
      G4cout << " Emitted type is " << emittedType << G4endl;
    }
    else {
      G4cout << " Warning! Missing types, resetting to default values ..." << G4endl;
      emittedType = 0;
      emitterType = 0;
    }
  }

  while(1) {

    if(fscanf(ifp, "%s", dummy) != 1) {
      abortRun = true;
      abortMessage = "Error reading input file, aborting run ...";
      return;
    }  
    // comments
    if( dummy[0] == '#' ) { //skips rest of the line!!!
      fgets(dummy,128,ifp);
      continue;
    }
    
    // begin of events
    if(!strcmp(dummy, "$")) {
      firstLine = true;
      beginOfEvents = true;
      fgets( dummy, 256, ifp );
      break;
    }
    
    // other cases
    for(ii = 0; ii < (G4int)strlen(dummy); ii++) dummy[ii] = toupper(dummy[ii]);

    if(!strcmp(dummy, "REACTION")) {
      InitReaction(ifp);
      if( !abortRun )
        continue;
      else
        return;  
    }
    else if(!strcmp(dummy, "EMITTED")) {
      InitEmitted(ifp);
      if( !abortRun )
        continue;
      else
        return;  
    }
    else if(!strcmp(dummy, "ZBEAM")) {
      zB = InitZBeam(ifp);
      if( zB < 0 )
        return;
      if( nB++ > 0 ) {
        zBeam = zB;
        aBeam = aB;
        theExternalEmitter->SetBeamNucleus( zBeam, aBeam );
        G4cout << " >>> Values read from file: " << G4endl;
        G4cout << " Beam has Z = " << zBeam << ", A = " << aBeam << G4endl;
      }
      continue;      
    }
    else if(!strcmp(dummy, "ABEAM")) {
      aB = InitABeam(ifp);
      if( aB < 0 )
        return;
      if( nB++ > 0 ) {
        zBeam = zB;
        aBeam = aB;
        theExternalEmitter->SetBeamNucleus( zBeam, aBeam );
        G4cout << " >>> Values read from file: " << G4endl;
        G4cout << " Beam has Z = " << zBeam << ", A = " << aBeam << G4endl;
      }
      continue;      
    }
    else if(!strcmp(dummy, "ZTARG")) {
      zT = InitZTarg(ifp);
      if( zT < 0 )
        return;
      if( nT++ > 0 ) {
        zTarg = zT;
        aTarg = aT;
        theExternalEmitter->SetTargetNucleus( zTarg, aTarg );
        G4cout << " >>> Values read from file: " << G4endl;
        G4cout << " Target has Z = " << zTarg << ", A = " << aTarg << G4endl;
      }
      continue;      
    }
    else if(!strcmp(dummy, "ATARG")) {
      aT = InitATarg(ifp);
      if( aT < 0 )
        return;
      if( nT++ > 0 ) {
        zTarg = zT;
        aTarg = aT;
        theExternalEmitter->SetTargetNucleus( zTarg, aTarg );
        G4cout << " >>> Values read from file: " << G4endl;
        G4cout << " Target has Z = " << zTarg << ", A = " << aTarg << G4endl;
      }
      continue;      
    }
    else if(!strcmp(dummy, "EBEAM")) {
      eB = InitEBeam(ifp);
      if( eB < 0. )
        return;
      eBeam = eB * MeV;
      theExternalEmitter->SetBeamEnergy( eBeam/keV );  
      G4cout << " >>> Values read from file: " << G4endl;
      G4cout << " Beam has an energy of " << eBeam/MeV << " MeV." << G4endl;
      continue;      
    }
    else if(!strcmp(dummy, "RANGE")) {
      InitEmittedRange(ifp);
      continue;
    }
    else {
      abortRun = true;
      abortMessage = "Unrecognized keyword: ";
      abortMessage = abortMessage + G4String(dummy) + ", aborting run ..."; 
      return;
    }  
  }   // while
}

void AgataExternalEmission::InitReaction( FILE *ifp )
{
  if( fscanf(ifp, "%d %d %d %d %lf ", &zBeam, &aBeam, &zTarg, &aTarg, &eBeam) != 5 ) {
    abortRun = true;
    abortMessage = "Error reading reaction data, aborting run ...";
    return;
  }
  eBeam *= MeV;
   
  G4cout << " >>> Values read from file: " << G4endl;
  G4cout << "   Beam has Z = " << zBeam << ", A = " << aBeam << " at an energy of " << eBeam/MeV << " MeV" << G4endl;
  G4cout << " Target has Z = " << zTarg << ", A = " << aTarg << G4endl;
 
  theExternalEmitter->SetBeamNucleus   ( zBeam, aBeam );
  theExternalEmitter->SetBeamEnergy    ( eBeam/keV );
  theExternalEmitter->SetTargetNucleus ( zTarg, aTarg );
}

G4int AgataExternalEmission::InitZBeam( FILE *ifp )
{
  G4int zB = -1;
  if( fscanf(ifp, "%d", &zB) < 1 ) {
    abortRun = true;
    abortMessage = "Error reading zBeam, aborting run ...";
  }
  return zB;
}

G4int AgataExternalEmission::InitABeam( FILE *ifp )
{
  G4int aB = -1;
  if( fscanf(ifp, "%d", &aB) < 1 ) {
    abortRun = true;
    abortMessage = "Error reading aBeam, aborting run ...";
  }
  return aB;
}

G4double AgataExternalEmission::InitEBeam( FILE *ifp )
{
  G4double eB = -1.0;
  if( fscanf(ifp, "%lf", &eB) < 1 ) {
    abortRun = true;
    abortMessage = "Error reading eBeam, aborting run ...";
  }
  return eB;
}

G4int AgataExternalEmission::InitZTarg( FILE *ifp )
{
  G4int zT = -1;
  if( fscanf(ifp, "%d", &zT) < 1 ) {
    abortRun = true;
    abortMessage = "Error reading zTarg, aborting run ...";
  }
  return zT;
}

G4int AgataExternalEmission::InitATarg( FILE *ifp )
{
  G4int aT = -1;
  if( fscanf(ifp, "%d", &aT) < 1 ) {
    abortRun = true;
    abortMessage = "Error reading aTarg, aborting run ...";
  }
  return aT;
}

G4bool AgataExternalEmission::IsHadron( G4int type )
{
  if( (type>1) && (type<95) )
    return true;
  else
    return false;  
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////
/// InitEmitted allocates an array of AgataEmitted objects, each of them corresponding to a particle
/// type which can be emitted in the following run. If during run a particle not corresponding to one
/// of these objects is found, run is aborted! If hadron cross sections have not been loaded and a hadron
/// is declared, run is also aborted.
/////////////////////////////////////////////////////////////////////////////////////////////////////////////
void AgataExternalEmission::InitEmitted( FILE *ifp )
{
  G4String name;
  G4bool value;
  
  G4int dummy, nTypes;
  G4int ii, ij;
  
  maxIndex = -1;
  
  G4int *dummyTypes = NULL;
  
  nEmitted = 0;

  fscanf(ifp, "%d", &nTypes);
  
  dummyTypes = new G4int[nTypes];
  
  G4bool isGood = false;
  
  for( ii=0; ii<nTypes; ii++ ) {
    dummyTypes[ii] = -1;
    if( fscanf( ifp, "%d", &dummy ) != 1 ) {
      abortRun = true;
      abortMessage = "Error reading emitted data, aborting run ...";
    }
    else  if( ii == 0 ) {
      if( !hadrons && IsHadron(dummy) ) {
        G4cout << " Warning! Hadron emission not allowed, skipping emitted type #" << dummy << "..." << G4endl;
        break;
      }
      else {
        dummyTypes[nEmitted++] = dummy;
        if( dummy > maxIndex )
          maxIndex = dummy;
      }
    }  
    else {  // check for repeated particles!
      for( ij=0; ij<ii; ij++ ) {
        isGood = true; 
        if( dummy == dummyTypes[ij] ) {
          G4cout << " Warning! Skipping repeated emitted type #" << dummy << G4endl;
          isGood = false;
          break;
        }
        if( !hadrons && IsHadron(dummy) ) {
          G4cout << " Warning! Hadron emission not allowed, skipping emitted type #" << dummy << "..." << G4endl;
          isGood = false;
          break;
        }
      }
      if( isGood ) {
        dummyTypes[nEmitted++] = dummy;
        if( dummy > maxIndex )
          maxIndex = dummy;
      }
    }
  }
  
  if( !nEmitted ) {
    abortRun = true;
    abortMessage = " No emitted particles defined, aborting run ...\n";
    return;
  }
  
  theEmitted = new AgataEmitted*[nEmitted];
  emittedLUT = new G4int[++maxIndex];
  for( ii=0; ii<maxIndex; ii++ )
    emittedLUT[ii] = -1;
  
  for( ii=0; ii<nEmitted; ii++ ) {
    name = FetchEmittedName( dummyTypes[ii] );
    if( abortRun )
      return;
    if( name == "gamma" )
      value = usePolar;
    else
      value = false;  
    theEmitted[ii] = new AgataEmitted( directoryName, name, iniPath, value );
    emittedLUT[dummyTypes[ii]] = ii;
  }
  createdEmitted = true;
  
  for( ij=0; ij<nEmitted; ij++ ) {
    G4cout << " Emitted # " << ij << " is a " << theEmitted[ij]->GetEmittedName() << G4endl;
  }
  
  delete dummyTypes;

}

void AgataExternalEmission::InitEmittedRange( FILE *ifp )
{
  G4int emitted;
  G4double thetaMin, thetaMax, phiMin, phiMax;

  if( !createdEmitted ) {
    G4cout << " Warning! AgataEmitted objects have not been initialized, skipping the range procedure ..." << G4endl;
    fscanf(ifp, "%d %lf %lf %lf %lf", &emitted, &thetaMin, &thetaMax, &phiMin, &phiMax);
    return;
  }
    
  if( fscanf(ifp, "%d %lf %lf %lf %lf", &emitted, &thetaMin, &thetaMax, &phiMin, &phiMax) != 5 ) {
    G4cout << " Insufficient number of parameters, could not fix range for Emitted object #" << emitted << "..." << G4endl;
    return;
  }  
  
  if( emitted > maxIndex || emitted < 0 ) {
    G4cout << " Emitted value out of range, could could not fix range!" << G4endl;
    return;
  }
  if( emittedLUT[emitted] < 0 ) {
    G4cout << " Trying to fix range for non-existing Emitted object #" << emitted << "!" << G4endl;
    return;
  }
  theEmitted[emittedLUT[emitted]]->SetEmittedTh( thetaMin, thetaMax );
  theEmitted[emittedLUT[emitted]]->SetEmittedPh( phiMin, phiMax );
}


//////////////////////////////////////
/// FetchEmittedName: a lookup table
//////////////////////////////////////
G4String AgataExternalEmission::FetchEmittedName( G4int type )
{
  G4String name;
  
  switch(type)
  {
    case 1:
      name = "gamma";
      break;
    case 2:
      name = "neutron";
      break;
    case 3:
      name = "proton";
      break;
    case 4:
      name = "deuteron";
      break;
    case 5:
      name = "triton";
      break;
    case 6:
      name = "He3";
      break;
    case 7:
      name = "alpha";
      break;
    case 8:
      name = "GenericIon";
      break;
    case 95:
      name = "pi-";
      break;
    case 96:
      name = "mu-";
      break; 
    case 97:
      name = "e-";
      break;  
    case 98:
      name = "e+";
      break;  
    case 99:
      name = "geantino";
      break;  
    default:
      abortRun = true;
      name = "unknown particle";
      G4cout << " Undefined particle #" << type << ", run will be aborted ..." << G4endl;
      break;   
  }
  return name;
}

/////////////////////////////////////////////////////////////////////////////////////////////////////
/// Chooses next emitted particle and stores its name as emittedName, momentum as emittedMomentum
/////////////////////////////////////////////////////////////////////////////////////////////////////
void AgataExternalEmission::NextParticle()
{
  G4int type;
  G4int ii;
  
  char messaggio[256];

  if( endOfRun ) {
    abortRun = true;
    sprintf( messaggio, " >>>Regular end of run after %d processed events.", processedEvents );
    abortMessage = G4String(messaggio);
    return;
  }

  cascadeOrder = cascadeMult;
  
  while (1) {
    // if file ends, eventually emits recoil
    if( !fgets(bufferLine, 128, evFile) ) { //end of file
      endOfRun   = true;
      endOfEvent = true;
      if( hadrons && emitNuclei ) {
        nucleusIsEmitted  = true;
        decodeEmitter     = false;
        FetchEmitter();
        if( emittedAtomNum != 0 ) // if "residual nucleus" does not exist, should not emit it!
          return;
      }
      if( endOfRun ) {
        abortRun = true;
	      if( skipEvents > 0 )
	        sprintf( messaggio, " >>>End of file found. Regular end of run after %d processed events.", processedEvents );
	      else
	        sprintf( messaggio, " >>>End of file found. Regular end of run after %d processed events.", processedEvents+1 );
	      abortMessage = G4String(messaggio);
        return;
      }
    }

    // skip blank spaces at the beginning of the line
    ii = 0;
    while( bufferLine[ii++] == ' ' ) ;
    ii--;  
    
    // comment: another line
    if( bufferLine[ii] == '#' )
      continue;
        
    // begin-of-event tag: in case emit current nucleus
    if( bufferLine[ii] == '$' ) {
      readEvents++;
      if( readEvents < skipEvents ) continue;
      

      processedEvents++;
      if( processedEvents > targetEvents ) {
        if( hadrons && emitNuclei ) {
          endOfRun          = true;
          endOfEvent        = true;
          nucleusIsEmitted  = true;
          decodeEmitter     = false;
          FetchEmitter();
          if( emittedAtomNum != 0 ) // if "residual nucleus" does not exist, should not emit it!
            return;
        }

	abortRun = true;
	if( skipEvents > 0 )
	  sprintf( messaggio, " >>>Number of requested events (%d) reached. Regular end of run.", processedEvents-1 );
	else
	  sprintf( messaggio, " >>>Number of requested events (%d) reached. Regular end of run.", processedEvents );
	abortMessage = G4String(messaggio);
	return;
      }

      firstLine         = true;
      endOfEvent        = true;
      if( hadrons && emitNuclei ) {
        nucleusIsEmitted  = true;
        decodeEmitter     = false;
        FetchEmitter();
        if( emittedAtomNum != 0 ) {// if "residual nucleus" does not exist, should not emit it!
          cascadeMult++;
          return;
        }
      }
      else {
        endOfEvent = true;
        theEvent->FlushAnalysis();
        EndOfEvent();
        continue;  
      }
    }
    if( readEvents < skipEvents ) continue;

    // empty lines
    if( sscanf( bufferLine, "%d", &type ) < 1 )
      continue; 

    // recoil or something else?
    sscanf( bufferLine, "%d", &type );
    if( type < 0 ) {  // recoil
      if( !beginOfEvents && hadrons && emitNuclei && !firstLine ) {
        FetchEmitter();
        if( emittedAtomNum != 0 ) {// if "residual nucleus" does not exist, should not emit it!
          nucleusIsEmitted  = true;
          beginOfEvents     = false;
          decodeEmitter     = true;
          cascadeMult++;
          return;
        }
      }
      else {
        firstLine         = false;
        beginOfEvents     = false;
        decodeEmitter     = false; 
        DecodeEmitter( bufferLine );
        continue;
      }  
    }
    else {
      if( firstLine ) {
        abortRun = true;
	if( skipEvents > 0 )
	  sprintf( messaggio, " Missing emitter line, aborting run after %d processed events ... ", processedEvents-1 );
	else
	  sprintf( messaggio, " Missing emitter line, aborting run after %d processed events ... ", processedEvents );
	abortMessage = G4String(messaggio);
        return;
      }  
      nucleusIsEmitted  = false;
      decodeEmitter     = false;
      DecodeEmitted( bufferLine );
      cascadeMult++;
      return;
    }  
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////
/// DecodeEmitted: retrieves from input file the information on the next emitted particle
/////////////////////////////////////////////////////////////////////////////////////////////
void AgataExternalEmission::DecodeEmitted( char* line )
{
  G4int    dumType, index;
  G4double dumE, dumDx, dumDy, dumDz, dumSx, dumSy, dumSz;
  G4double dumT=0., dumP=0.;
  
  sscanf( line, "%d", &dumType );
  if( dumType >= maxIndex ) {
    abortRun = true;
    abortMessage = " Emitted type out of range, aborting run ..." ;
    return;
  }   
  
  index = emittedLUT[dumType];
  if( index < 0 ) {
    abortRun = true;
    abortMessage = " Undefined emitted type, aborting run ..." ;
    return;
  }
  
  sscanf( line, "%d %lf", &dumType, &dumE );
  if( dumE <= 0. ) {
    abortRun = true;
    abortMessage = " Invalid energy value, aborting run ..." ;
    return;
  }   
   
  ////////////////////////////////////////////////////////
  /// 03/11/2006 Making dumT, dumP optional parameters
  ////////////////////////////////////////////////////////
  switch( emittedType )
  {
    case 0:
      if( sscanf( line, "%d %lf %lf %lf %lf %lf %lf %lf %lf %lf", 
                 &dumType, &dumE, &dumDx, &dumDy, &dumDz, &dumSx, &dumSy, &dumSz, &dumT, &dumP ) < 8 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitted data, aborting run ...";
        return;
      }
      else {
        theExternalEmitter->SetEmitterPosition( G4ThreeVector( dumSx, dumSy, dumSz )*cm );
        theEmitted[index]->setVelocityLab( dumE*keV, G4ThreeVector( dumDx, dumDy, dumDz ), dumP*deg );
      }
      break;
    case 1:
      if( sscanf( line, "%d %lf %lf %lf %lf %lf %lf %lf %lf %lf", 
                 &dumType, &dumE, &dumDx, &dumDy, &dumDz, &dumSx, &dumSy, &dumSz, &dumT, &dumP ) < 8 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitted data, aborting run ...";
        return;
      }
      else {
        theExternalEmitter->SetEmitterPosition( G4ThreeVector( dumSx, dumSy, dumSz )*cm );
        theEmitted[index]->setVelocityCM( dumE*keV, G4ThreeVector( dumDx, dumDy, dumDz ),
                                 theEmitter->GetEmitterVelocity(), dumP*deg );
      }
      break;
    case 2:
      if( sscanf( line, "%d %lf %lf %lf %lf %lf %lf ", 
                 &dumType, &dumE, &dumDx, &dumDy, &dumDz, &dumT, &dumP ) < 5 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitted data, aborting run ...";
        return;
      }
      else {
        theEmitted[index]->setVelocityLab( dumE*keV, G4ThreeVector( dumDx, dumDy, dumDz ), dumP*deg );
      }
      break;
    case 3:
      if( sscanf( line, "%d %lf %lf %lf %lf %lf %lf", 
                 &dumType, &dumE, &dumDx, &dumDy, &dumDz, &dumT, &dumP ) < 5 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitted data, aborting run ...";
        return;
      }
      else {
        theEmitted[index]->setVelocityCM( dumE*keV, G4ThreeVector( dumDx, dumDy, dumDz ),
                                 theEmitter->GetEmitterVelocity(), dumP*deg );
      }
      break;
    case 4:
      if( sscanf( line, "%d %lf %lf %lf", &dumType, &dumE, &dumT, &dumP ) < 2 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitted data, aborting run ...";
        return;
      }
      else {
        theEmitted[index]->setVelocityCM( dumE*keV, 
	        theEmitter->GetEmitterVelocity(), dumP*deg );
      }
      break;


  }
  // update emission time and emitter position
  cascadeDeltaTime  = cascadeTime;
  cascadeTime       = dumT * ns;
  cascadeDeltaTime  = cascadeTime - cascadeDeltaTime;
  if( cascadeDeltaTime && (emittedType > 1) ) {
    theEmitter->TraslateEmitter( cascadeDeltaTime );
  }  
  
  // update emitted particle
  emittedName       = theEmitted[index]->GetEmittedName();
  emittedAtomNum    = theEmitted[index]->GetChargeNumber();
  emittedMassNum    = theEmitted[index]->GetMassNumber();
  emittedEnergy     = theEmitted[index]->GetEnergyLab();
  emittedDirection  = theEmitted[index]->GetDirLab();
  emittedMomentum   = theEmitted[index]->GetMomentumLab();
  
  // only polarized gammas
  if( usePolar && (emittedName == "gamma") ) {
    emittedPolarization = theEmitted[index]->GetPolarizationLab();
    polarizedEmitted = true;
  }
  else
    polarizedEmitted = false;  
}


/////////////////////////////////////////////////////////////////////////////
/// DecodeEmitter: retrieves from input file the information on the source
/////////////////////////////////////////////////////////////////////////////
void AgataExternalEmission::DecodeEmitter( char* line )
{
  G4double      dumE, dumDx, dumDy, dumDz, dumSx, dumSy, dumSz;
  G4int         dumZ, dumA, dumM;
// For AGATA-PRISMA
  G4int		dumI;
  
  G4ThreeVector dumD;
  G4ThreeVector dumS;
  
  switch( emitterType )
  {
    case 0:
      if( sscanf( line, "%d %d %d %lf %lf %lf %lf %lf %lf %lf", 
                 &dumM, &dumZ, &dumA, &dumE, &dumDx, &dumDy, &dumDz, &dumSx, &dumSy, &dumSz ) < 10 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitter data, aborting run ...";
        return;
      }
      else {
        dumD = G4ThreeVector( dumDx, dumDy, dumDz );
        if( dumD.mag2() ) {
          dumD = dumD.unit();
        } 
        else
          dumD = G4ThreeVector( 0., 0., 1. );
        dumS = G4ThreeVector( dumSx, dumSy, dumSz ) * cm;
        theExternalEmitter->RestartEmitter( dumZ, dumA, dumE * MeV, dumD, dumS );
      }
      break;
    case 1:
      if( sscanf( line, "%d %d %d %lf %lf %lf %lf", &dumM, &dumZ, &dumA, &dumE, &dumDx, &dumDy, &dumDz ) < 7 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitter data, aborting run ...";
        return;
      }
      else {
        dumD = G4ThreeVector( dumDx, dumDy, dumDz );
        if( dumD.mag2() )
          dumD = dumD.unit();
        else
          dumD = G4ThreeVector( 0., 0., 1. );
        theExternalEmitter->RestartEmitter( dumZ, dumA, dumE * MeV, dumD );
      }
      break;
    case 2:
      if( sscanf( line, "%d %d %d %lf", &dumM, &dumZ, &dumA, &dumE ) < 4 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitter data, aborting run ...";
        return;
      }
      else {
        theExternalEmitter->RestartEmitter( dumZ, dumA, dumE * MeV );
      }
      break;
    case 3:
      if( sscanf( line, "%d %d %d", &dumM, &dumZ, &dumA ) < 3 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitter data, aborting run ...";
        return;
      }
      else {
        theExternalEmitter->RestartEmitter( dumZ, dumA );
      }
      break;
    case 4:
      if( sscanf( line, "%d", &dumM ) < 1 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitter data, aborting run ...";
        return;
      }
      else {
        theExternalEmitter->RestartEmitter();
      }
      break;
    // For AGATA-PRISMA (copied from "case 1" is and added charge)
    case 5:
      if( sscanf( line, "%d %d %d %d %lf %lf %lf %lf", &dumM, &dumZ, &dumA, &dumI, &dumE, &dumDx, &dumDy, &dumDz ) < 8 ) {
        abortRun = true;
        abortMessage = " Warning! Wrong number of parameters reading emitter data, aborting run ...";
        return;
      }
      else {
        dumD = G4ThreeVector( dumDx, dumDy, dumDz );
        if( dumD.mag2() )
          dumD = dumD.unit();
        else
          dumD = G4ThreeVector( 0., 0., 1. );
        theExternalEmitter->RestartEmitter( dumZ, dumA, dumI, dumE * MeV, dumD );
      }
      break;

    default:
      abortRun = true;
      abortMessage = " Unsupported emitter format, aborting run ...";
      return;
  }

  emitterAtomNum  = theExternalEmitter->GetEmitterAtomNum();
  emitterMassNum  = theExternalEmitter->GetEmitterMassNum();
  // For AGATA-PRISMA
  emitterIonicCharge = theExternalEmitter->GetEmitterIonicCharge();
}

/////////////////////////////////////////////////////////////////////////
/// FetchEmitter: saves residual nucleus as the next emitted particle
/////////////////////////////////////////////////////////////////////////
void AgataExternalEmission::FetchEmitter( )
{
  emittedName = "GenericIon";
  emittedAtomNum    = theExternalEmitter->GetEmitterAtomNum();
  emittedMassNum    = theExternalEmitter->GetEmitterMassNum();
  emittedEnergy     = theExternalEmitter->GetEmitterEnergy();
  emittedDirection  = theEmitter->GetEmitterDirection();
  cascadeDeltaTime  = 0.; // same emission time as the last emitted particle!!!
  cascadeTime       = 0.; // resets cascade time (useful for binary reactions)!!!
}

void AgataExternalEmission::SetEventFile( G4String name )
{
  if( name(0) == '/' )
    evFileName = name;
  else {
    if( name.find( "./", 0 ) != string::npos ) {
      G4int position = name.find( "./", 0 );
      if( position == 0 )
        name.erase( position, 2 );
    }  
    evFileName = iniPath + name;
  }  
      
  G4cout << " ----> Events will be read from " << evFileName << G4endl;
}

void AgataExternalEmission::SetEmitNuclei( G4bool value )
{
  emitNuclei = value;
  if( emitNuclei && hadrons )
    G4cout << " ----> Emission of nuclei is now enabled. " << G4endl;
  else
    G4cout << " ----> Emission of nuclei is now disabled. " << G4endl;  
}

void AgataExternalEmission::GetStatus()
{
  if( showStatus ) {
    G4cout << " Emitted type: " << emittedType << G4endl;
    G4cout << " Emitted particles: ";
    for( G4int ii=0; ii<maxIndex; ii++ ) {
      if( emittedLUT[ii] > 0 )
        G4cout << FetchEmittedName( ii ) << " ";
    }
    G4cout << G4endl;

    G4cout << " Emitter type: " << emitterType << G4endl;
    theExternalEmitter->GetStatus( emitterType );
    
    if( verbose ) 
      G4cout << " Full information about the source to the output file." << G4endl;
    else  
      G4cout << " Reduced information about the source to the output file." << G4endl;
  }
}

void AgataExternalEmission::PrintToFile( std::ofstream &outFileLMD, G4double unitL, G4double unitE )
{
  outFileLMD << "GENERATOR 1" << G4endl;
  outFileLMD << "EVENTFILE " << evFileName << G4endl;
  if( emitNuclei && hadrons )
    outFileLMD << "EMITTED " << emittedType << " " << nEmitted+1;
  else  
    outFileLMD << "EMITTED " << emittedType << " " << nEmitted;
  for( G4int ii=0; ii<maxIndex; ii++ ) {
    if( emittedLUT[ii] >= 0 )
      outFileLMD << " " << ii;
  }
  if( emitNuclei && hadrons )
    outFileLMD << " 8" << G4endl;
  else  
    outFileLMD << G4endl;
  
  outFileLMD << "EMITTER " << emitterType << G4endl;
  theExternalEmitter->PrintToFile( emitterType, outFileLMD, unitL, unitE );
  
  if( verbose )
    outFileLMD << "VERBOSE 1" << G4endl;
  else  
    outFileLMD << "VERBOSE 0" << G4endl;
  
  outFileLMD << "ENDGENERATOR" << G4endl;

}

G4String AgataExternalEmission::GetEventHeader( G4double unitLength )
{
  char aLine[128];
  G4String eventHeader = G4String("");
  
  ////////////////////////////////////////////////////////////////////////////////////////////////
  /// 07/06/2006 chenge suggested by O.Stezowski: disable the writing out of "-101" and "-102" ///
  /// lines (optional, should be used with caution!)                                           ///
  ////////////////////////////////////////////////////////////////////////////////////////////////
  if( !verbose ) return eventHeader;
  
  if( this->IsStartOfEvent() ) {
  // Internal generation:
  // First writes out what is common to the cascade (recoil velocity and source position)
  // External generation: same thing
    // Internal generation:
    // writes out beta and direction only when they may change event-by-event
    // External generation: always write (flags set to true)

    if( this->GetEmitterVelocity().mag2() > 0. ) {
      G4ThreeVector recoildir  = this->GetEmitterVelocity().unit();
      sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, this->GetEmitterBeta(), recoildir.x(), recoildir.y(), recoildir.z() );
    }
    else
      sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, 0., 0., 0., 1. );
    eventHeader += G4String(aLine);

    G4ThreeVector position = this->GetEmitterPosition();
    sprintf(aLine, "%5d   %8.3f %8.3f %8.3f\n", -102, position.x()/unitLength, position.y()/unitLength, position.z()/unitLength );
    eventHeader += G4String(aLine);

    // writes out the time difference between the emission of two particles
    // in the cascade (or the time for the first particle) only when it is
    // non-zero
    if( (this->IsSourceLongLived()) && (this->GetCascadeDeltaTime() > 0.) ) {
      cascadeTime = this->GetCascadeTime();
      sprintf(aLine, "%5d  %9.3f\n", -103, cascadeTime/ns );
      eventHeader += G4String(aLine);
    }
  }
  else {  // following events
    // external event generation: always write out the recoil velocity line
    if( (this->GetCascadeOrder() > 0) ) {
      if( this->GetEmitterVelocity().mag2() > 0. ) {
        G4ThreeVector recoildir1  = this->GetEmitterVelocity().unit();
        sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, this->GetEmitterBeta(), recoildir1.x(), recoildir1.y(), recoildir1.z() );
      }
      else
        sprintf(aLine, "%5d  %9.5f %8.5f %8.5f %8.5f\n", -101, 0., 0., 0., 1. );
      eventHeader += G4String(aLine);
      // always write out source position 
      G4ThreeVector position2 = this->GetEmitterPosition();
      sprintf(aLine, "%5d   %8.3f %8.3f %8.3f\n", -102, position2.x()/unitLength, position2.y()/unitLength, position2.z()/unitLength );
      eventHeader += G4String(aLine);
    }
    // writes out the time difference between the emission of two particles
    // in the cascade (or the time for the first particle) only when it is
    // non-zero
    if( (this->IsSourceLongLived()) && (this->GetCascadeDeltaTime() > 0.) ) {
      cascadeTime = this->GetCascadeTime();
      sprintf(aLine, "%5d  %9.3f\n", -103, cascadeTime/ns );
      eventHeader += G4String(aLine);
    }
  }
  return eventHeader;  
}

G4String AgataExternalEmission::GetParticleHeader( const G4Event* evt, G4double /*unitLength*/, G4double unitEnergy )
{
  char aLine[128];  
  
  G4PrimaryVertex*   pVert     = evt->GetPrimaryVertex();
  G4PrimaryParticle* pPart     = pVert->GetPrimary();
  G4ThreeVector      momentum  = pPart->GetMomentum();
  G4double           mass      = pPart->GetG4code()->GetPDGMass(); // mass in units of equivalent energy.
  G4double           epart     = momentum.mag();
  G4ThreeVector      direction;
  if( epart )
    direction = momentum/epart;
  else
    direction = G4ThreeVector(0., 0., 1.);

  G4int    partType = 1;
  G4String nome = pPart->GetG4code()->GetParticleName();
  if( mass > 0. ) {                                           // kinetic energy (relativistic)
    epart  = sqrt( mass * mass + epart  * epart ) - mass;
    if( nome == "neutron" )  
      partType = 2;
    else if( nome == "proton" )  
      partType = 3;
    else if( nome == "deuteron" )  
      partType = 4;
    else if( nome == "triton" )  
      partType = 5;
    else if( nome == "He3" )  
      partType = 6;
    else if( nome == "alpha" )  
      partType = 7;
    else if( nome == "pi-" )
      partType = 95; 
    else if( nome == "mu-" )
      partType = 96; 
    else if( nome == "e-" )
      partType = 97;  
    else if( nome == "e+" )
      partType = 98;  
    else if( nome == "geantino" )
      partType = 99;  
    else                 // nucleus (GenericIon or similar)  
      partType = 8;
  }

  // Finally writes out the particles
  sprintf(aLine, "%5d %10.3f %8.5f %8.5f %8.5f %d\n", -partType, epart/unitEnergy, direction.x(), direction.y(), direction.z(), evt->GetEventID());
  return G4String(aLine);

}

void AgataExternalEmission::SetBetaDecay( G4bool value )
{
  isBetaDecay = value;
  if( isBetaDecay )
    G4cout << " ----> Beta-decay behaviour has been activated." << G4endl;
  else
    G4cout << " ----> Beta-decay behaviour has been deactivated." << G4endl;  
}

void AgataExternalEmission::SetSkipEvents( G4int value )
{
  if( value > 0 ) {
    skipEvents = value;
    G4cout << " ----> The first " << skipEvents << " events will be skipped." << G4endl;
  }
  else 
    G4cout << " ----> No events will be skipped." << G4endl;
}

void AgataExternalEmission::SetTargetEvents( G4int value )
{
  if( value > 0 ) {
    targetEvents = value;
    G4cout << " ----> Processing " << targetEvents << " events ..." << G4endl;
    targetEvents--;
    
    G4int numberOfEvents = 1000000000;
    
    G4RunManager * runManager = G4RunManager::GetRunManager();
    runManager->BeamOn(numberOfEvents);
    
  }
  targetEvents = 1000000000;
}

void AgataExternalEmission::SetVerbose( G4bool value )
{
  verbose = value;
    
  if( verbose ) 
    G4cout << " --->Full information about the source to the output file." << G4endl;
  else  
    G4cout << " --->Reduced information about the source to the output file." << G4endl;
}

G4String AgataExternalEmission::GetBeginOfEventTag()
{
  char dummy[64];
#ifdef WRITE_EVNUM
  sprintf( dummy, "-100\t%10d\n", processedEvents+1 );
#else  
  sprintf( dummy, "-100\n" );
#endif
  return G4String(dummy);
}

///////////////////
// The Messenger
///////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAnInteger.hh"

AgataExternalEmissionMessenger::AgataExternalEmissionMessenger(AgataExternalEmission* pTarget, G4bool hadr, G4String name)
:myTarget(pTarget)
{ 

  hadrons = hadr;

  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/generator/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of the event generation.");

  directoryName += "emitter/";
  
  commandName = directoryName + "eventFile";
  aLine = commandName.c_str();
  SetEventFileCmd = new G4UIcmdWithAString(aLine, this);  
  SetEventFileCmd->SetGuidance("Define input file with the events to be generated");
  SetEventFileCmd->SetGuidance("Required parameters: 1 string.");
  SetEventFileCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "enableBetaDecay";
  aLine = commandName.c_str();
  EnableBetaCmd = new G4UIcmdWithABool(aLine, this);  
  EnableBetaCmd->SetGuidance("Enables beta-decay behaviour");
  EnableBetaCmd->SetGuidance("Required parameters: none.");
  EnableBetaCmd->SetParameterName("isBetaDecay",true);
  EnableBetaCmd->SetDefaultValue(true);
  EnableBetaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "disableBetaDecay";
  aLine = commandName.c_str();
  DisableBetaCmd = new G4UIcmdWithABool(aLine, this);  
  DisableBetaCmd->SetGuidance("Disables beta-decay behaviour");
  DisableBetaCmd->SetGuidance("Required parameters: none.");
  DisableBetaCmd->SetParameterName("isBetaDecay",true);
  DisableBetaCmd->SetDefaultValue(false);
  DisableBetaCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  if( hadrons ) {
    commandName = directoryName + "enableNuclei";
    aLine = commandName.c_str();
    EnableNucleiCmd = new G4UIcmdWithABool(aLine, this);
    EnableNucleiCmd->SetGuidance("Activate the emission of nuclei");
    EnableNucleiCmd->SetGuidance("Required parameters: none.");
    EnableNucleiCmd->SetParameterName("emitNuclei",true);
    EnableNucleiCmd->SetDefaultValue(true);
    EnableNucleiCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

    commandName = directoryName + "disableNuclei";
    aLine = commandName.c_str();
    DisableNucleiCmd = new G4UIcmdWithABool(aLine, this);
    DisableNucleiCmd->SetGuidance("Deactivate the emission of nuclei");
    DisableNucleiCmd->SetGuidance("Required parameters: none.");
    DisableNucleiCmd->SetParameterName("emitNuclei",true);
    DisableNucleiCmd->SetDefaultValue(false);
    DisableNucleiCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  }
  

  directoryName = name + "/run/";
  
  commandName = directoryName + "skipEvents";
  aLine = commandName.c_str();
  SetSkipEventsCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetSkipEventsCmd->SetGuidance("Skip the first events in the input file.");
  SetSkipEventsCmd->SetGuidance("Required parameters: 1 integer.");
  SetSkipEventsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "beamOn";
  aLine = commandName.c_str();
  SetTargetEventsCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetTargetEventsCmd->SetGuidance("Process multiple events from the input file.");
  SetTargetEventsCmd->SetGuidance("Required parameters: 1 integer.");
  SetTargetEventsCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  

  directoryName = name + "/file/";
  
  commandName = directoryName + "fullOutput";
  aLine = commandName.c_str();
  VerboseOutputCmd = new G4UIcmdWithABool(aLine, this);
  VerboseOutputCmd->SetGuidance("Select full information on the source for output to file.");
  VerboseOutputCmd->SetGuidance("Required parameters: none.");
  VerboseOutputCmd->SetParameterName("verbose",true);
  VerboseOutputCmd->SetDefaultValue(true);
  VerboseOutputCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "reduceOutput";
  aLine = commandName.c_str();
  ReducedOutputCmd = new G4UIcmdWithABool(aLine, this);
  ReducedOutputCmd->SetGuidance("Select reduced information on the source for output to file.");
  ReducedOutputCmd->SetGuidance("Required parameters: none.");
  ReducedOutputCmd->SetParameterName("verbose",true);
  ReducedOutputCmd->SetDefaultValue(false);
  ReducedOutputCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataExternalEmissionMessenger::~AgataExternalEmissionMessenger()
{
  delete   myDirectory;
  delete   mySubDirectory;
  delete   SetEventFileCmd;
  delete   SetSkipEventsCmd;
  delete   SetTargetEventsCmd;
  delete   EnableBetaCmd;
  delete   DisableBetaCmd;
  delete   EnableNucleiCmd;
  delete   DisableNucleiCmd;
  delete   VerboseOutputCmd;
  delete   ReducedOutputCmd;
}

void AgataExternalEmissionMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{
  if( command == SetEventFileCmd ) {
    myTarget->SetEventFile(newValue);
  }
  if( command == SetSkipEventsCmd ) {
    myTarget->SetSkipEvents(SetSkipEventsCmd->GetNewIntValue(newValue));
  }
  if( command == SetTargetEventsCmd ) {
    myTarget->SetTargetEvents(SetTargetEventsCmd->GetNewIntValue(newValue));
  }
  if( command == EnableBetaCmd ) {
    myTarget->SetBetaDecay( EnableBetaCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableBetaCmd ) {
    myTarget->SetBetaDecay( DisableBetaCmd->GetNewBoolValue(newValue) );
  }
  if( hadrons ) {
    if( command == EnableNucleiCmd ) {
      myTarget->SetEmitNuclei( EnableNucleiCmd->GetNewBoolValue(newValue) );
    }
    if( command == DisableNucleiCmd ) {
      myTarget->SetEmitNuclei( DisableNucleiCmd->GetNewBoolValue(newValue) );
    }
  }
  if( command == VerboseOutputCmd ) {
    myTarget->SetVerbose( VerboseOutputCmd->GetNewBoolValue(newValue) );
  }
  if( command == ReducedOutputCmd ) {
    myTarget->SetVerbose( ReducedOutputCmd->GetNewBoolValue(newValue) );
  }
}

