#include "AgataRunAction.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataGeneratorAction.hh"
#include "AgataEventAction.hh"
#include "AgataAnalysis.hh"
#include "AgataSensitiveDetector.hh"
#include "AgataDistributedParameters.hh"

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UImanager.hh"
#include "G4VVisManager.hh"
#include "G4ios.hh"
#include <time.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include <sstream>

AgataRunAction::AgataRunAction(AgataAnalysis* pAnalysis, G4bool vol, G4String name)
  : theAnalysis(pAnalysis)
{
  volume = vol;

  runNumber       =  0;
  verboseLevel    =  0;
  
  workPath        = "";
  
  maxFileSize     = 2042626048; //> 95% of 2GB
  
  writeLMD        =  false;     //> By default, no output is provided!!!
  writeLMD_gammas = false;     //> By default no gamma hits
  writeTOFIFO     =  false;     //> Write to file, not FIFO
  firstLMDfile    =  true;
  myMessenger = new AgataRunActionMessenger(this, name);

}

AgataRunAction::~AgataRunAction()
{
  delete myMessenger;  
}

////////////////////////////////////////////////////////////////////
/// Begin of run: output file is opened, on-line analysis started
///////////////////////////////////////////////////////////////////
void AgataRunAction::BeginOfRunAction(const G4Run* aRun)
{
 G4cout << G4endl  << "### Run start." 
	 << G4endl << G4endl;


  runNumber  = aRun->GetRunID();
  fileNumber = 0;
  
  killedEvents = 0;

  G4cout << G4endl  << "### Run " << runNumber << " start." 
	 << G4endl << G4endl;
  
  G4RunManager * runManager = G4RunManager::GetRunManager();

  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
  theEvent     = (AgataEventAction*)          runManager->GetUserEventAction();
  
  if( !theDetector->CalcOmega() ) {
    theGenerator = (AgataGeneratorAction*)      runManager->GetUserPrimaryGeneratorAction();
    theGenerator->BeginOfRun();
  }
  
  theEvent->BeginOfRun();

  // Output file in list-mode
  if( writeLMD ) {
    if( OpenLMFile() ) {
      G4cout << " Could not open list-mode file, disabling output! " << G4endl;
      writeLMD = false;
    }
    else
      G4cout << outFileName << " opened" << G4endl;
  }
  
  if (G4VVisManager::GetConcreteInstance()) {
    G4UImanager* UI = G4UImanager::GetUIpointer();
    UI->ApplyCommand("/vis/scene/notifyHandlers");
  }
  theAnalysis->AnalysisStart(runNumber);
}

/////////////////////////////////////
/// End of run: closes what is open
/////////////////////////////////////
void AgataRunAction::EndOfRunAction(const G4Run* aRun)
{
  if( !aRun ) return;
  G4cout << "\n" << G4endl;
  if (G4VVisManager::GetConcreteInstance()) {
    G4UImanager::GetUIpointer()->ApplyCommand("/vis/viewer/update");
  }

  theAnalysis->AnalysisEnd(workPath);

    
  if( writeLMD ) {
    outFileLMD.close();
    if(writeLMD_gammas)
      outFileLMD_gammas.close();
    G4cout << outFileName << " file closed" << G4endl;
  }
  
  if( killedEvents )
    G4cout << "---> " << killedEvents << " have been killed by AgataSensitiveDetector." << G4endl;

  G4cout << "\n### Run " << runNumber << " stop." << G4endl;
}

void AgataRunAction::WriteHeader()
{
  time_t vTime;
  time( &vTime );
  
  G4double unitLength = theEvent->GetUnitLength();
  G4double unitEnergy = theEvent->GetUnitEnergy();
  
  outFileLMD << "AGATA " << FORMAT_VERSION << G4endl;
  
  theEvent->WriteHeader(outFileLMD);

  outFileLMD << "G4TRACKING " 
             << theDetector->GeSD()->GetMethod() << G4endl;

  outFileLMD << "DATE " << ctime(&vTime);  // date has already end-of-line character!

  theDetector->WriteHeader(outFileLMD,unitLength);
  if( !theDetector->CalcOmega() )  
    theGenerator->PrintToFile(outFileLMD,unitLength,unitEnergy);
  outFileLMD << "$" << G4endl;

  //Gammas
  if(writeLMD_gammas){
    outFileLMD_gammas << "AGATA " << FORMAT_VERSION << G4endl;
    theEvent->WriteHeader(outFileLMD_gammas);
    outFileLMD_gammas << "G4TRACKING " 
		      << theDetector->GeSD()->GetMethod() << G4endl;
    outFileLMD_gammas << "DATE " << ctime(&vTime);  // date has already end-of-line character!
    theDetector->WriteHeader(outFileLMD_gammas,unitLength);
    if( !theDetector->CalcOmega() )  
      theGenerator->PrintToFile(outFileLMD_gammas,unitLength,unitEnergy);
    outFileLMD_gammas << "$" << G4endl;
  }
}

G4int AgataRunAction::OpenLMFile()
{
  //char evname[32];
  char evname[100];
  std::cout << AgataDistributedParameters::resume_dir << "\n";
  if(!writeTOFIFO){
    if(AgataDistributedParameters::resume && firstLMDfile) {
      /*
	Here we have to find the correct file number
       */
      fileNumber=0;
      sprintf(evname, "GammaEvents.%4.4d", runNumber);
      //sprintf(evname, "/mnt/hgfs/Echanges/4Agata/GammaEvents.%4.4d", runNumber);
      struct stat buffer;
      G4String testname = workPath+G4String(evname);
      while(stat(testname.c_str(),&buffer)){
	fileNumber++;
      sprintf(evname, "GammaEvents.%4.4d.%2.2d", runNumber, fileNumber);
      //sprintf(evname, "/mnt/hgfs/Echanges/4Agata/GammaEvents.%4.4d.%2.2d", runNumber, fileNumber);
	testname = workPath+G4String(evname);
      }
    } else {
      if( fileNumber > 0 )
	sprintf(evname, "GammaEvents.%4.4d.%2.2d", runNumber, fileNumber);
	//sprintf(evname, "/mnt/hgfs/Echanges/4Agata/GammaEvents.%4.4d.%2.2d", runNumber, fileNumber);
      else  
	sprintf(evname, "GammaEvents.%4.4d", runNumber);
	//sprintf(evname, "/mnt/hgfs/Echanges/4Agata/GammaEvents.%4.4d", runNumber);
    }
  } else {
    if(!getenv("AGATAFIFONAME")) sprintf(evname, "GammaEvents.fifo.%4.4d",runNumber);
    else sprintf(evname, "%s.fifo",getenv("AGATAFIFONAME"));
  }
  outFileName = workPath + G4String(evname);
  outFileName_gammas = workPath + "Only"+G4String(evname);
  if(writeTOFIFO){
    std::ostringstream syscmd;
    syscmd << "rm -f " << outFileName;
    system(syscmd.str().c_str());
    syscmd.str("");
    syscmd << "mkfifo " << outFileName;
    system(syscmd.str().c_str());
    outFileLMD.open(outFileName,ios_base::app);
  } else {
    if((AgataDistributedParameters::resume && firstLMDfile)){
      //Here we have to open the file and find the correct position for next 
      //event
      std::ifstream datafile(outFileName);
      std::string oneline="";
      while(oneline!="$" && datafile.good()) 
	std::getline(datafile,oneline);
      streampos filepos=0;
      while(datafile.good()){
	filepos=datafile.tellg();
	std::getline(datafile,oneline);
	if(strncmp(oneline.c_str(),"-100",4)==0){
	  std::istringstream is(oneline);
	  int event;
	  is >> event >> event;
	  if(event==AgataDistributedParameters::resume_eventnb) break;
	}
      }
      outFileLMD.open(outFileName,ios::app);
      outFileLMD.seekp(filepos);
    } else  {
      outFileLMD.open(outFileName);
      if(writeLMD_gammas)
	outFileLMD_gammas.open(outFileName_gammas);
    }
  }
  
  if( !outFileLMD.is_open() )
    return 1;
  
  if(!(AgataDistributedParameters::resume && firstLMDfile)
     || writeTOFIFO) WriteHeader();
  fileNumber++;
  firstLMDfile=false;
  return 0;
}

void AgataRunAction::SplitLMFile()
{
  if( !writeLMD )
    return;
  
  outFileLMD.close();
  if(writeLMD_gammas)
    outFileLMD_gammas.close();
  OpenLMFile();
}


///////////////////////////////////
// Methods for the Messenger
///////////////////////////////////
void AgataRunAction::EnableWrite( G4bool enable )
{
  writeLMD = enable;

  if(writeLMD)
    G4cout << " ----> Event file will be written!" << G4endl; 
  else
    G4cout << " ----> Event file will not be written !" << G4endl; 
}

void AgataRunAction::EnableWriteGammas( G4bool enable )
{
  writeLMD_gammas = enable;

  if(writeLMD_gammas)
    G4cout << " ----> Gamma Event file will be written!" << G4endl; 
  else
    G4cout << " ----> Gamma Event file will not be written !" << G4endl; 
}

void AgataRunAction::WriteToFifo(G4bool val)

{
  writeTOFIFO = val;
  if(writeTOFIFO && writeLMD){
    G4cout << " ----> Event file will be written to FIFO!" << G4endl; 
  } else {
    G4cout << " ----> Event file will not be written to FIFO!" << G4endl; 
  }
}

void AgataRunAction::ShowStatus()
{
  G4cout << G4endl;
  if(writeLMD)
    G4cout << " ----> Event file will be written" << G4endl; 
  else
    G4cout << " ----> Event file will not be written !" << G4endl; 
}

////////////////////////////////////////////////////
/// This method changes the current run number.
/// An empty run is performed to ensure that the
/// Geant4 kernel is properly initialized.
////////////////////////////////////////////////////
void AgataRunAction::SetRunNumber( G4int number )
{
  G4bool doWrite    = false;
  G4bool doAnalysis = false;

  if( number < 0 ) {
    G4cout << " Invalid value, could not change run number! " << G4endl;
  }
  else {
    // save status
    doWrite = writeLMD;
    if( doWrite )
      writeLMD = false;
    doAnalysis = theAnalysis->IsEnabled();
    if( doAnalysis )
      theAnalysis->EnableAnalysis( false );  
  
    G4RunManager * runManager = G4RunManager::GetRunManager();
    runManager->BeamOn(0);  //> this is needed to initialize the run number
    runManager->SetRunIDCounter( number );
    G4cout << " ----> Run number has been set to " << number << G4endl;
    //> restore previous status
    if( doWrite )
      writeLMD = true;
    if( doAnalysis )
      theAnalysis->EnableAnalysis( true );  
  }
}

void AgataRunAction::SetVerboseLevel( G4int level )
{
  if( level >= 0 ) {
    verboseLevel = level;
    G4cout << " ----> Verbose level set to " << verboseLevel << G4endl;
  }
  else
    G4cout << " ----> Invalid value, keeping verbose level " << verboseLevel << G4endl;
}

void AgataRunAction::SetMaxFileSize( G4long size )
{
  if( size < 0 ) {
    G4cout << " ----> Invalid value, keeping previous limit ("
           << maxFileSize/1048576 << " MB)" << G4endl;
  }
  else if( size > 4096 ) {  // 4GB
    G4cout << " ----> Value out of range, resetting to maximum value." << G4endl;
    maxFileSize = 2042626048; // 95% of 2GB
  }
  else {
    maxFileSize = (G4long)( 0.95 * size * 1048576 );
    G4cout << " ----> Maximum file size has been set to " << maxFileSize << " MB." << G4endl;
  }
}

////////////////////////////////////////////////////////////
/// This method sets the directory where the output file
/// is written. It checks also that such directory
/// exists and that user can write to such directory.
///////////////////////////////////////////////////////////
void AgataRunAction::SetWorkingPath( G4String path )
{
  struct stat statbuf;
  
  if( stat(path, &statbuf) ) {
    G4cout << " Directory " << path << " must exist! Keeping previous working directory (" << workPath << ")" << G4endl;
    return;
  }
  if( !S_ISDIR(statbuf.st_mode) ) {
    G4cout << " " << path << " is not a directory! Keeping previous working directory (" << workPath << ")" << G4endl;
    return;
  }
  if( getuid() == statbuf.st_uid ) {
    if( !(statbuf.st_mode & S_IWUSR) ) {
      G4cout << " Directory " << path << " is not writable! Keeping previous working directory (" << workPath << ")" << G4endl;
      return;
    }
  }
  else if( getgid() == statbuf.st_gid ) {
    if( !(statbuf.st_mode & S_IWGRP) ) {
      G4cout << " Directory " << path << " is not writable! Keeping previous working directory (" << workPath << ")" << G4endl;
      return;
    }
  }
  else {
    if( !(statbuf.st_mode & S_IWOTH) ) {
      G4cout << " Directory " << path << " is not writable! Keeping previous working directory (" << workPath << ")" << G4endl;
      return;
    }
  }
  
  G4int length = strlen(path);
  
  if( path(length-1) == '/' )
    workPath = path;
  else
    workPath = path + "/";  

  if( workPath.find( "./", 0 ) != string::npos ) {
    G4int position = workPath.find( "./", 0 );
    if( position == 0 )
      workPath.erase( position, 2 );
  }
  
  G4cout << " ----> Working directory has been set to " << workPath << G4endl;
}

////////////////////////////
// The Messenger
////////////////////////////
#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"

AgataRunActionMessenger::AgataRunActionMessenger(AgataRunAction* pTarget, G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/run/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of run parameters.");

  commandName = directoryName + "status";
  aLine = commandName.c_str();
  StatusCmd = new G4UIcmdWithoutParameter(aLine, this);
  StatusCmd->SetGuidance("Print various parameters.");
  StatusCmd->SetGuidance("Required parameters: none.");
  StatusCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "runNumber";
  aLine = commandName.c_str();
  RunNumberCmd = new G4UIcmdWithAnInteger(aLine, this);
  RunNumberCmd->SetGuidance("Set the run number.");
  RunNumberCmd->SetGuidance("Required parameters: 1 integer.");
  RunNumberCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    

  directoryName = name + "/file/";

  fileDirectory = new G4UIdirectory(directoryName);
  fileDirectory->SetGuidance("Control of list-mode output.");
    
  commandName = directoryName + "enableLM";
  aLine = commandName.c_str();
  EnableWriteCmd = new G4UIcmdWithABool(aLine, this);
  EnableWriteCmd->SetGuidance("Activate the list-mode output.");
  EnableWriteCmd->SetGuidance("Required parameters: none.");
  EnableWriteCmd->SetParameterName("writeLMD",true);
  EnableWriteCmd->SetDefaultValue(true);
  EnableWriteCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableLM";
  aLine = commandName.c_str();
  DisableWriteCmd = new G4UIcmdWithABool(aLine, this);
  DisableWriteCmd->SetGuidance("Deactivate the list-mode output.");
  DisableWriteCmd->SetGuidance("Required parameters: none.");
  DisableWriteCmd->SetParameterName("writeLMD",true);
  DisableWriteCmd->SetDefaultValue(false);
  DisableWriteCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "enableGammaLM";
  aLine = commandName.c_str();
  EnableGammaWriteCmd = new G4UIcmdWithABool(aLine, this);
  EnableGammaWriteCmd->SetGuidance("Activate the gamma list-mode output.");
  EnableGammaWriteCmd->SetGuidance("Required parameters: none.");
  EnableGammaWriteCmd->SetParameterName("writeLMD_gammas",true);
  EnableGammaWriteCmd->SetDefaultValue(true);
  EnableGammaWriteCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disableGammaLM";
  aLine = commandName.c_str();
  DisableGammaWriteCmd = new G4UIcmdWithABool(aLine, this);
  DisableGammaWriteCmd->SetGuidance("Deactivate the list-mode output.");
  DisableGammaWriteCmd->SetGuidance("Required parameters: none.");
  DisableGammaWriteCmd->SetParameterName("writeLMD_gammas",true);
  DisableGammaWriteCmd->SetDefaultValue(false);
  DisableGammaWriteCmd->AvailableForStates(G4State_PreInit,G4State_Idle);


    
  commandName = directoryName + "WriteToFifo";
  aLine = commandName.c_str();
  WriteToFifoCmd = new G4UIcmdWithABool(aLine, this);
  WriteToFifoCmd->SetGuidance("Write list-mode to FIFO");
  WriteToFifoCmd->SetGuidance("Required parameters: bool");
  WriteToFifoCmd->SetParameterName("writeToFifo",true);
  WriteToFifoCmd->SetDefaultValue(false);
  WriteToFifoCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "verbose";
  aLine = commandName.c_str();
  VerboseLevelCmd = new G4UIcmdWithAnInteger(aLine, this);
  VerboseLevelCmd->SetGuidance("Set verbosity for list-mode file.");
  VerboseLevelCmd->SetGuidance("Required parameters: 1 integer.");
  VerboseLevelCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "fileSize";
  aLine = commandName.c_str();
  SetFileSizeCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetFileSizeCmd->SetGuidance("Set maximum size for the list-mode file.");
  SetFileSizeCmd->SetGuidance("Required parameters: 1 integer (file size in MB).");
  SetFileSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "workingPath";
  aLine = commandName.c_str();
  WorkPathCmd = new G4UIcmdWithAString(aLine, this);
  WorkPathCmd->SetGuidance("Set working directory for the list-mode file.");
  WorkPathCmd->SetGuidance("Required parameters: 1 string.");
  WorkPathCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}

AgataRunActionMessenger::~AgataRunActionMessenger()
{
  delete myDirectory;
  delete fileDirectory;
  delete EnableWriteCmd;
  delete DisableWriteCmd;
  delete RunNumberCmd;
  delete WorkPathCmd;
  delete VerboseLevelCmd;
  delete SetFileSizeCmd;
  delete StatusCmd;
}

void AgataRunActionMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{ 
  if( command == EnableWriteCmd ) {
    myTarget->EnableWrite( EnableWriteCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableWriteCmd ) {
    myTarget->EnableWrite( DisableWriteCmd->GetNewBoolValue(newValue) );
  }
  if( command == EnableGammaWriteCmd ) {
    myTarget->EnableWriteGammas( EnableWriteCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableGammaWriteCmd ) {
    myTarget->EnableWriteGammas( DisableWriteCmd->GetNewBoolValue(newValue) );
  }

  if( command == WriteToFifoCmd ) {
    myTarget->WriteToFifo( WriteToFifoCmd->GetNewBoolValue(newValue) );
  }
  if( command == StatusCmd ) {
    myTarget->ShowStatus( );
  }
  if( command == RunNumberCmd ) {
    myTarget->SetRunNumber( RunNumberCmd->GetNewIntValue(newValue) );
  }
  if( command == WorkPathCmd ) {
    myTarget->SetWorkingPath( newValue );
  }
  if( command == VerboseLevelCmd ) {
    myTarget->SetVerboseLevel( VerboseLevelCmd->GetNewIntValue(newValue) );
  }
  if( command == SetFileSizeCmd ) {
    myTarget->SetMaxFileSize( SetFileSizeCmd->GetNewIntValue(newValue) );
  }
}


