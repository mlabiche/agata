#include "AgataEmitter.hh"

#include "G4RunManager.hh"
#include <string>
#include <cmath>
#include "Randomize.hh"

#ifdef G4V10
using namespace CLHEP;
#endif

AgataEmitter::AgataEmitter()
{
  InitData();
}

AgataEmitter::~AgataEmitter()
{}

void AgataEmitter::InitData()
{
  sourceType              = 0;                 // default source position
  movingSource            = false;

  sourcePosition          = G4ThreeVector();   // source position from messenger = origin
  targetPosition          = G4ThreeVector();   // initial target position = origin
  targetSize              = G4ThreeVector();   // initial target size = 0
  targetSig               = G4ThreeVector();   // initial gaussian distribution
  initialPosition         = G4ThreeVector();
  
  emitterBetaAverage      = 0.;                 // emitter velocity of gamma rource
  emitterBetaSigma        = 0.;                 // sigma of the gaussian of emitter dispersion
  emitterBetaSigmaFrac    = 0.;                 // sigma of the gaussian of emitter dispersion
  emitterDirectionAverage = G4ThreeVector(0., 0., 1.);  // fixed emitter direction
  emitterOpAngleCos       = 1.;                 // opening angle for the emitter cone
  emitterOpAngle          = 0.;                 // opening angle for the emitter cone
  
  gaussAngle              = false;
  
  emitterRotation.set( 0, 0, 0 );
  
  emitterPosition         = G4ThreeVector();    // actual position resulting from flagSource
  emitterVelocity         = G4ThreeVector();    
}


void AgataEmitter::RestartEmitter()
{  

  emitterPosition  = G4ThreeVector();   // reset emitter position
  emitterVelocity  = G4ThreeVector();   // reset emitter velocity
  
  InitBeta();
  InitDirection();
  VelocityVector();
  emitterPosition = SourceDisplacement();
}

void AgataEmitter::InitBeta()
{
  emitterBeta  = emitterBetaAverage;
	  // allow for variable beta
  if( emitterBeta > 0. && emitterBetaSigmaFrac > 0. ) {
    emitterBeta  += G4RandGauss::shoot( 0., emitterBetaSigma );
    if( emitterBeta < 0. )
      emitterBeta = 0.00001;  // beta should not become zero to avoid divisions by zero
    if( emitterBeta >= 1. )
      emitterBeta = 0.99;   
  }
}

void AgataEmitter::InitDirection()
{    
  emitterDirection = emitterDirectionAverage;
  //  cout << " DEBUG InitDirection() " << endl;
  if( (emitterBeta > 0.) && (emitterOpAngle > 0.) ) {
  // if emitter is moving, determine velocity vector
  // calculate emitter direction
    G4double cosR;
    if( gaussAngle )
      {
	cosR = 1.0 - fabs( G4RandGauss::shoot( 0., emitterOpAngleSig ) );
	//	cout << " DEBUG InitDirection() uses gaussAngle " << endl;
      }
    else
      {
	cosR = emitterOpAngleCos + G4UniformRand() * ( 1. - emitterOpAngleCos );
	//	cout << " DEBUG InitDirection() uses UniformRand() " << endl;
      }
    G4double sinR = pow( (1.-cosR*cosR), 0.5 );
    G4double phiR = G4UniformRand() * 2.0 * M_PI;
    emitterDirection.set( sinR*cos(phiR), sinR*sin(phiR), cosR );

    //    cout << " DEBUG InitDirection("<<  sinR*cos(phiR) << "," << sinR*sin(phiR) << "," << cosR <<") " << endl;
    emitterDirection = emitterRotation( emitterDirection );
  }
}

void AgataEmitter::VelocityVector()
{
  emitterVelocity = emitterBeta * c_light * emitterDirection;  // emitter velocity vector
}

G4ThreeVector AgataEmitter::SourceDisplacement()
{
  initialPosition = G4ThreeVector();

  switch( sourceType )
  {
    case 0:                       // point source, position chosen with the messenger
      initialPosition = sourcePosition;
      break;
    case 1:                       // point source at target position
      initialPosition = targetPosition;
      break;
    case 2:                       // diffused source - uniformly spread across the target
      initialPosition.setX( (G4UniformRand() * 2.0 - 1.0) * targetSize.x() );
      initialPosition.setY( (G4UniformRand() * 2.0 - 1.0) * targetSize.y() );
      initialPosition.setZ( (G4UniformRand() * 2.0 - 1.0) * targetSize.z() );
      initialPosition += targetPosition;
      break;
    case 3:                       // diffused source - gaussian distribution inside the target
      G4double valx, valy, valz;
      while (true) {
        valx = G4RandGauss::shoot( 0., targetSig.x() ) ;
        if(abs(valx)>targetSize.x()) continue; 
        valy = G4RandGauss::shoot( 0., targetSig.y() ) ;
        if(abs(valy)>targetSize.y()) continue; 
        valz = G4RandGauss::shoot( 0., targetSig.z() ) ;
        if(abs(valz)>targetSize.z()) continue; 
        initialPosition.set( valx, valy, valz );
        break; 
      }
      initialPosition += targetPosition;
      break;
  case 4:                       // diffused source - gaussian x gaussian y
    G4double mvalx, mvaly, mvalz;
    while (true) {
      mvalx = G4RandGauss::shoot( 0., 60.0/2.35 ) ;// fwhm_x = 2.35*sigma_y = 6 cm
      if(abs(mvalx)>targetSize.x()) continue; 
      mvaly = G4RandGauss::shoot( 0., 40.0/2.35 ) ;// fwhm_y = 2.35*sigma_y = 4 cm
      if(abs(mvaly)>targetSize.y()) continue; 
      mvalz =(G4UniformRand() * 2.0 - 1.0) * targetSize.z()  ; // uniform along z (?)
      if(abs(mvalz)>targetSize.z()) continue; 
      initialPosition.set( mvalx, mvaly, mvalz );
      break; 
    }
    initialPosition += targetPosition;
    break;
  case 5:                       // diffused source - gaussian x gaussian y, constant along z
    G4double mvx, mvy, mvz;
    while (true) {
      mvx = G4RandGauss::shoot( 0., 60.0/2.35 ) ;// fwhm_x = 2.35*sigma_y = 6 cm
      if(abs(mvx)>targetSize.x()) continue; 
      mvy = G4RandGauss::shoot( 0., 40.0/2.35 ) ;// fwhm_y = 2.35*sigma_y = 4 cm
      if(abs(mvy)>targetSize.y()) continue; 
      mvz = (G4UniformRand() * 2.0 - 1.0) * targetSize.z();
      //      mvz =(RandExponential::shoot(57.14))  ; // exponential along z (?)
      if(abs(mvz)>targetSize.z()) continue; 
      initialPosition.set( mvx, mvy, mvz );
      break; 
    }
    initialPosition += targetPosition;
    break;    
  default:
    initialPosition  = sourcePosition;
    break;  
  }
  return initialPosition;
}

void AgataEmitter::RepositionEmitter( G4double tau )
{
  if( tau ) {     // update emitter position and time
    G4double emissionTime = -log(G4UniformRand())*tau;
    if( emitterBeta )
      emitterPosition += emitterVelocity * emissionTime;
  }  
}

void AgataEmitter::TraslateEmitter( G4double time )
{
  if( time && emitterBeta )
    emitterPosition += emitterVelocity * time;
}

#include "AgataDetectorConstruction.hh"
void AgataEmitter::BeginOfRun()
{
  G4RunManager * runManager = G4RunManager::GetRunManager();
  AgataDetectorConstruction* theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();

  targetPosition = theDetector->GetTargetPosition();
  targetSize     = theDetector->GetTargetSize()/2.;
  targetSig      = targetSize/3.;
  
  emitterBetaSigma = emitterBetaSigmaFrac * emitterBetaAverage;
  emitterBeta      = emitterBetaAverage;
  
  G4double theta = emitterDirectionAverage.theta();
  G4double phi   = emitterDirectionAverage.phi();
  if( theta > 0. ) {  // this rotation makes sense only if theta > 0.
    emitterRotation.rotateY( theta );
    emitterRotation.rotateZ( phi   );
  }
  else 
    emitterRotation.set( 0, 0, 0 );
    
  if( sourceType > 1 )
    diffusedSource = true;
  else 
    diffusedSource = false;

  if( emitterOpAngle > 0. )
    diffusedRecoil = true;
  else 
    diffusedRecoil = false;

  if( emitterBetaSigma > 0. )	
    diffusedBeta = true;
  else 
    diffusedBeta = false;
    
}

void AgataEmitter::SetEmitterOpAngle( G4double angle )
{
  if( angle > 0. && angle < 180. )
    emitterOpAngle = angle*deg;  // angle in degrees
  else
    emitterOpAngle = 0.;

  emitterOpAngleCos = cos( emitterOpAngle );
  emitterOpAngleSig = ( 1. - emitterOpAngleCos )/2.;
  G4cout << " ----> Opening angle for the emitters = " << emitterOpAngle/deg << " deg." << G4endl;   
}

void AgataEmitter::SetEmitterDirection( G4ThreeVector direction )
{
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  if( direction.mag() > 0. ) {
    emitterDirectionAverage = direction.unit();
    G4cout  << " ----> Emitter direction set to ("
            << std::setw(8) << emitterDirectionAverage.x()
            << std::setw(8) << emitterDirectionAverage.y()
            << std::setw(8) << emitterDirectionAverage.z() << " ) " << G4endl;
  }
  else {
    G4cout << " ----> Invalid value, keeping previous one (" 
           << std::setw(8) << emitterDirectionAverage.x()
           << std::setw(8) << emitterDirectionAverage.y()
           << std::setw(8) << emitterDirectionAverage.z() << ") " << G4endl;
  }
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

void AgataEmitter::SetGaussAngle( G4bool value )
{
  gaussAngle = value;
  if( gaussAngle )
    G4cout << " ---> Recoil opening angles will be treated with a GAUSSIAN distribution." << G4endl;
  else  
    G4cout << " ---> Recoil opening angles will be treated with a UNIFORM distribution." << G4endl;
}

void AgataEmitter::SetSourceType( G4int type )
{
  sourceType = type;
  G4cout << " ----> Source type " << sourceType << " chosen" << G4endl;
}

void AgataEmitter::SetSourcePosition( G4ThreeVector position )
{
  sourcePosition = position * mm;
  G4int prec = G4cout.precision(4);
  G4cout.setf(ios::fixed);
  G4cout << " ----> Initial source position set to ("
          << std::setw(10) << sourcePosition.x()/cm 
          << std::setw(10) << sourcePosition.y()/cm 
          << std::setw(10) << sourcePosition.z()/cm << ") cm" << G4endl;
  G4cout.unsetf(ios::fixed);
  G4cout.precision(prec);
}

