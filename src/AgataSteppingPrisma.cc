#include "AgataSteppingPrisma.hh"
#include "AgataDetectorConstruction.hh"
#include "G4RunManager.hh"
#include "G4Step.hh"
#include "G4VProcess.hh"

#include "G4TrackVector.hh"
#include "CSpec1D.hh"
#include <unistd.h>

AgataSteppingPrisma::AgataSteppingPrisma( G4String name )
{
  workPath        = "";
  maxFileSize     = 2042626048; //> 95% of 2GB
  
  writeFile       =  false;     //> By default, no output is provided!!!
  
  myMessenger = new AgataSteppingPrismaMessenger(this, name);
  
  theSpectrum = new  CSpec1D(8192,16);
  
  timeDiff = 0.;
  atoNum = -1;
  masNum = -1;    
// emitting_distr.dat, l:8:
//
// 0-> kin. en. moment of gamma emission
// 1-> depth of emission (along beam axis)
// 14 -> beta at the moment of the radioactive decay (emission of the gammas)
// 15 -> beta at the moment of the emission of the gammas (z component only)
//
}

AgataSteppingPrisma::~AgataSteppingPrisma()
{
  delete myMessenger;
}


void AgataSteppingPrisma::UserSteppingAction(const G4Step* aStep)
{

  

  G4int    atomNum, massNum;
  G4ThreeVector position_d;
  G4double energy_d, mass_d, beta_d, beta_z;
  G4TrackVector* theSecondary;
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theDetector  = 
    (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction(); 
  atomNum = aStep->GetTrack()->GetParticleDefinition()->GetAtomicNumber();
  massNum = aStep->GetTrack()->GetParticleDefinition()->GetAtomicMass();
  G4StepPoint *prestepoint = aStep->GetPreStepPoint();
  G4StepPoint *poststepoint = aStep->GetPostStepPoint();
  if(prestepoint && poststepoint){
    if(poststepoint->GetStepStatus()==fGeomBoundary){
      if(prestepoint->GetPhysicalVolume()->GetName()=="Sphere_Physical"){
	std::cerr << aStep->GetTrack()->GetParticleDefinition()->
	  GetParticleName() << " "
		  << aStep->GetTrack()->GetKineticEnergy() << " "
		  << aStep->GetPostStepPoint()->GetMomentumDirection() << " "
		  << aStep->GetTrack()->GetPosition() << "\n";
      }
      if(aStep->GetTrack() && aStep->GetTrack()->GetParticleDefinition()
	 && aStep->GetTrack()->GetParticleDefinition()->
	   GetParticleType()=="nucleus"
	 && !theDetector->GetBuildSpectrometer()){
	if(prestepoint->GetPhysicalVolume()->GetName()=="Target"){
	  energy_i   = aStep->GetTrack()->GetKineticEnergy();
	  momentum_i = aStep->GetPostStepPoint()->GetMomentumDirection();
	  atoNum = atomNum;
	  masNum = massNum;  
	  position = theDetector->GetTargetPosition();
	}
      }
    }
  }

  // only write out ions reaching outWorld
  // ions: heavier than alpha particle  

  if( (atomNum < 2) || (massNum < 4) )
    return;

  
  if( aStep->GetPostStepPoint() != NULL ) {
    if( aStep->GetPostStepPoint()->GetPhysicalVolume()){
      if( aStep->GetPostStepPoint()->GetPhysicalVolume()->GetName() 
	  == "specphys" ) {
	atoNum = atomNum;
	masNum = massNum;  
	//	position = aStep->GetTrack()->GetPosition();
	position = theDetector->GetTargetPosition();
	//G4cout << " posi " << position.z()/mm << G4endl;
	energy_i   = aStep->GetTrack()->GetKineticEnergy();
	momentum_i = aStep->GetPostStepPoint()->GetMomentumDirection();
	theSecondary = const_cast<G4TrackVector*>(aStep->GetSecondary());
	//      G4cout << "second. size " << theSecondary->size() << G4endl;

	for(unsigned int ii=0; ii < theSecondary->size(); ii++ ) {
	  /* G4cout << " Particle " << theSecondary->at(ii)->GetDefinition()->GetParticleName() << G4endl;
	     G4cout << " Kinetic energy (keV) " << theSecondary->at(ii)->GetKineticEnergy()/keV << G4endl;
	     G4cout << " Momentum " << theSecondary->at(ii)->GetMomentumDirection() << G4endl;
	     G4cout << " Position (nm)" << theSecondary->at(ii)->GetPosition()/(0.000001*mm) << G4endl;
	     G4cout << " GlobalTime (ps) " << theSecondary->at(ii)->GetGlobalTime()/(0.001*ns) << G4endl;
	     G4cout << " LocalTime (ps) " << theSecondary->at(ii)->GetLocalTime()/(0.001*ns) << G4endl;
	     G4cout << " ProperTime (ps) " << theSecondary->at(ii)->GetProperTime()/(0.001*ns) << G4endl;	
	     G4cout << " " << G4endl; */
	  if( ii == 0 )
	    timeDiff -= theSecondary->at(ii)->GetGlobalTime();
	  /* G4cout << " Time diff - " << timeDiff/(0.001*ns) << G4endl; */
	  
	} 
      
      
      }
    }  
  }

  if( aStep->GetPostStepPoint()->GetProcessDefinedStep() != NULL ) {
    if( aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName() == "Decay" ) {
      position_d = aStep->GetTrack()->GetPosition();
      energy_d   = aStep->GetTrack()->GetKineticEnergy();
      theSecondary = const_cast<G4TrackVector*>(aStep->GetSecondary());
      
//      G4cout << "second. size " << theSecondary->size() << G4endl;

      for( unsigned int ii=0; ii < theSecondary->size(); ii++ ) {
        if(theSecondary->at(ii)->GetDefinition()->GetAtomicNumber() > 4)
          theSpectrum->Incr((G4int)(theSecondary->at(ii)->GetKineticEnergy()/(0.1*MeV)),0);	  
//        G4cout << "second. 0 ener. " << theSecondary->at(ii)->GetKineticEnergy()/MeV << G4endl;
      }

      theSpectrum->Incr((G4int)(2000+position_d.z()/(0.001*0.001*mm)),1);
//      G4cout << " pos. decay " << position_d/mm << G4endl;
 
    }  
  }

  if( aStep->GetPostStepPoint()->GetProcessDefinedStep() != NULL ) {
    if( aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessName() == "RadioactiveDecay" ) {
      position_d = aStep->GetTrack()->GetPosition();
      energy_d   = aStep->GetTrack()->GetKineticEnergy();
      mass_d     = aStep->GetTrack()->GetDefinition()->GetPDGMass();
      
      beta_d = 10000.*sqrt( 2.* energy_d / mass_d );  // 100*beta[%]
      
//      G4cout << " beta_d  " << beta_d << G4endl;
      
      theSpectrum->Incr((G4int)(beta_d),14);
      
      beta_z = beta_d * aStep->GetTrack()->GetMomentum().unit().z();
      theSpectrum->Incr((G4int)(beta_z),15);      

      theSecondary = const_cast<G4TrackVector*>(aStep->GetSecondary());
      
//      G4cout << "second. size " << theSecondary->size() << G4endl;

      for(unsigned int ii=0; ii < theSecondary->size(); ii++ ) {
        /* G4cout << " Particle " << theSecondary->at(ii)->GetDefinition()->GetParticleName() << G4endl;
	G4cout << " Kinetic energy (keV) " << theSecondary->at(ii)->GetKineticEnergy()/keV << G4endl;
	G4cout << " Momentum " << theSecondary->at(ii)->GetMomentumDirection() << G4endl;
	G4cout << " Position (nm)" << theSecondary->at(ii)->GetPosition()/(0.000001*mm) << G4endl;
	G4cout << " GlobalTime (ps) " << theSecondary->at(ii)->GetGlobalTime()/(0.001*ns) << G4endl;
	G4cout << " LocalTime (ps) " << theSecondary->at(ii)->GetLocalTime()/(0.001*ns) << G4endl;
	G4cout << " ProperTime (ps) " << theSecondary->at(ii)->GetProperTime()/(0.001*ns) << G4endl;	
	G4cout << " " << G4endl; */
	if( ii == 0 )
	  timeDiff += theSecondary->at(ii)->GetGlobalTime();
        /* G4cout << " Time diff + " << timeDiff/(0.001*ns) << G4endl; */
      } 
    }  
  }

  if( !(aStep->GetPostStepPoint()->GetPhysicalVolume() ) ) {
    energy   = aStep->GetTrack()->GetKineticEnergy();
     
    //G4cout << " Energia " << energy/MeV << G4endl;
    
    momentum = aStep->GetPostStepPoint()->GetMomentumDirection();
    if( momentum.mag2() > 0. )
      momentum /= momentum.mag();
     //G4cout << " direction " << momentum << G4endl;    
  }
}

void AgataSteppingPrisma::BeginOfEventAction()
{  
  atoNum = -1;
  masNum = -1;
  energy =  0.*keV;
  momentum = G4ThreeVector(0., 1., 0.);
  position = G4ThreeVector(); 
  
  energy_i =  0.*keV;
  momentum_i = G4ThreeVector(0., 1., 0.);
  

  if( !writeFile ) 
    return;
  
  if( GetOutputFileSize() > maxFileSize )
    SplitOutputFile(); 
  
}

void AgataSteppingPrisma::EndOfEventAction()
{  
  /* G4cout << " Time diff " << timeDiff/(0.001*ns) << G4endl; */
  if( timeDiff < 0. ) timeDiff = 0.;
  
  theSpectrum->Incr((G4int)(timeDiff/(0.001*0.001*0.01*ns)),13);
  timeDiff = 0.;   

  if( !writeFile ) 
    return;
  
  char     line[128];

  sprintf( line, "$\n-101 %4d %4d %9.3f %9.5f %9.5f %9.5f %9.3f %9.3f %12.5f\n",
      atoNum, masNum, energy/MeV, momentum.x(), momentum.y(), momentum.z(),
      position.x()/mm, position.y()/mm, position.z()/mm );  
  outFile << line;
}

void AgataSteppingPrisma::BeginOfRunAction( G4int run )
{
  // in order to do something meaningful, should have writeout enabled
  // and target should be actually constructed  
  runNumber  = run;
  fileNumber = 0;
  
  if( !writeFile ) 
    return;
    
  G4RunManager * runManager = G4RunManager::GetRunManager();
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
    
  if( !theDetector->IsTarget() ) {
    G4cout << " --> Warning! No target constructed, disabling output from SteppingAction." << G4endl;
    writeFile = false;
    return;
  }  
    
  // open the output file!
  if( OpenOutputFile() ) {
    G4cout << " Could not open list-mode file, disabling output! " << G4endl;
    writeFile = false;
  }
  else
    G4cout << outFileName << " opened" << G4endl;
    
}

void AgataSteppingPrisma::EndOfRunAction()
{
  //  theSpectrum->write("emitting_distr.dat");
  
  if( !writeFile )
    return;
 
  CloseOutputFile();
  
}

void AgataSteppingPrisma::WriteHeader()
{}

G4int AgataSteppingPrisma::OpenOutputFile()
{
  char evname[32];
  
  if( fileNumber > 0 )
    sprintf(evname, "IonEvents.%4.4d.%2.2d", runNumber, fileNumber);
  else  
    sprintf(evname, "IonEvents.%4.4d", runNumber);
  
  outFileName = workPath + G4String(evname);
  
  outFile.open(outFileName);
  if( !outFile.is_open() )
    return 1;
  
  WriteHeader();
  fileNumber++;
  return 0;
}

void AgataSteppingPrisma::SplitOutputFile()
{
  if( !writeFile )
    return;
  
  CloseOutputFile();
  OpenOutputFile();
}

void AgataSteppingPrisma::CloseOutputFile()
{
  if( !writeFile )
    return;
  
  outFile.close();
  G4cout << outFileName << " file closed" << G4endl;
}


void AgataSteppingPrisma::SetMaxFileSize( G4int size )
{
  if( size < 0 ) {
    G4cout << " ----> Invalid value, keeping previous limit ("
           << maxFileSize/1048576 << " MB)" << G4endl;
  }
  else if( size > 2048 ) {  // 2GB
    G4cout << " ----> Value out of range, resetting to maximum value." << G4endl;
    maxFileSize = 2042626048; // 95% of 2GB
  }
  else {
    maxFileSize = (G4int)( 0.95 * size * 1048576 );
    G4cout << " ----> Maximum file size has been set to " << size << " MB." << G4endl;
  }
}

void AgataSteppingPrisma::EnableWrite( G4bool enable )
{
  writeFile = enable;
  
  G4cout << " WriteFile is " << writeFile << G4endl;

  if(writeFile)
    G4cout << " ----> PRISMA event file will be written!" << G4endl; 
  else
    G4cout << " ----> PRISMA event file will not be written!" << G4endl; 
}

////////////////////////////////////////////////////////////
/// This method sets the directory where the output file
/// is written. It checks also that such directory
/// exists and that user can write to such directory.
///////////////////////////////////////////////////////////
void AgataSteppingPrisma::SetWorkingPath( G4String path )
{
  struct stat statbuf;
  
  if( stat(path, &statbuf) ) {
    G4cout << " Directory " << path << " must exist! Keeping previous working directory (" << workPath << ")" << G4endl;
    return;
  }
  if( !S_ISDIR(statbuf.st_mode) ) {
    G4cout << " " << path << " is not a directory! Keeping previous working directory (" << workPath << ")" << G4endl;
    return;
  }
  if( getuid() == statbuf.st_uid ) {
    if( !(statbuf.st_mode & S_IWUSR) ) {
      G4cout << " Directory " << path << " is not writable! Keeping previous working directory (" << workPath << ")" << G4endl;
      return;
    }
  }
  else if( getgid() == statbuf.st_gid ) {
    if( !(statbuf.st_mode & S_IWGRP) ) {
      G4cout << " Directory " << path << " is not writable! Keeping previous working directory (" << workPath << ")" << G4endl;
      return;
    }
  }
  else {
    if( !(statbuf.st_mode & S_IWOTH) ) {
      G4cout << " Directory " << path << " is not writable! Keeping previous working directory (" << workPath << ")" << G4endl;
      return;
    }
  }
  
  G4int length = strlen(path);
  
  if( path(length-1) == '/' )
    workPath = path;
  else
    workPath = path + "/";  

  if( workPath.find( "./", 0 ) != string::npos ) {
    G4int pos = workPath.find( "./", 0 );
    if( pos == 0 )
      workPath.erase( pos, 2 );
  }
  
  G4cout << " ----> Working directory has been set to " << workPath << G4endl;
}

////////////////////////////
// The Messenger
////////////////////////////
#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"

AgataSteppingPrismaMessenger::AgataSteppingPrismaMessenger(AgataSteppingPrisma* pTarget, G4String name)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
  directoryName = name + "/stepping/";

  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of stepping action parameters.");

  commandName = directoryName + "enable";
  aLine = commandName.c_str();
  EnableWriteCmd = new G4UIcmdWithABool(aLine, this);
  EnableWriteCmd->SetGuidance("Activate the PRISMA event file output.");
  EnableWriteCmd->SetGuidance("Required parameters: none.");
  EnableWriteCmd->SetParameterName("writeFile",true);
  EnableWriteCmd->SetDefaultValue(true);
  EnableWriteCmd->AvailableForStates(G4State_PreInit,G4State_Idle);

  commandName = directoryName + "disable";
  aLine = commandName.c_str();
  DisableWriteCmd = new G4UIcmdWithABool(aLine, this);
  DisableWriteCmd->SetGuidance("Deactivate the PRISMA event file output.");
  DisableWriteCmd->SetGuidance("Required parameters: none.");
  DisableWriteCmd->SetParameterName("writeFile",true);
  DisableWriteCmd->SetDefaultValue(false);
  DisableWriteCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "fileSize";
  aLine = commandName.c_str();
  SetFileSizeCmd = new G4UIcmdWithAnInteger(aLine, this);
  SetFileSizeCmd->SetGuidance("Set maximum size for the PRISMA event file.");
  SetFileSizeCmd->SetGuidance("Required parameters: 1 integer (file size in MB).");
  SetFileSizeCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
  commandName = directoryName + "workingPath";
  aLine = commandName.c_str();
  WorkPathCmd = new G4UIcmdWithAString(aLine, this);
  WorkPathCmd->SetGuidance("Set working directory for the PRISMA event file.");
  WorkPathCmd->SetGuidance("Required parameters: 1 string.");
  WorkPathCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
  
}

AgataSteppingPrismaMessenger::~AgataSteppingPrismaMessenger()
{
  delete myDirectory;
  delete EnableWriteCmd;
  delete DisableWriteCmd;
  delete WorkPathCmd;
  delete SetFileSizeCmd;
}

void AgataSteppingPrismaMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{ 
  if( command == EnableWriteCmd ) {
    myTarget->EnableWrite( EnableWriteCmd->GetNewBoolValue(newValue) );
  }
  if( command == DisableWriteCmd ) {
    myTarget->EnableWrite( DisableWriteCmd->GetNewBoolValue(newValue) );
  }
  if( command == WorkPathCmd ) {
    myTarget->SetWorkingPath( newValue );
  }
  if( command == SetFileSizeCmd ) {
    myTarget->SetMaxFileSize( SetFileSizeCmd->GetNewIntValue(newValue) );
  }
}


