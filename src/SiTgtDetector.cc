/***************************************************************************
                          SiTgtDetector.cc  -  description
                             -------------------
    begin                : Thu Sep 29 2005
    copyright            : (C) 2005 by Mike Taylor
    email                : mjt502@york.ac.uk
    
Modified by Pavel Golubev (pavel.golubev@nuclear.lu.se) 
LYCCA0 configuration implemented
-----
Modified by MJT to incorporated messenger methods (6/11/07)
 ***************************************************************************/
// Adapted for Agata code, October 2009 by P. Joshi, The University of York.
#include "SiTgtDetector.h"

#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4ThreeVector.hh"
#include "G4SDManager.hh"
#include "G4Material.hh"

#include "globals.hh"
#include "G4ios.hh"

 SiTgtDetector::SiTgtDetector(G4double defTgtDetDist) :
 SiTgt_log(0), SiTgt_phys(0)
 {
   //ELOG292 (S377)
   detDist = defTgtDetDist;
   detDist = 1.7*cm;
 }

 SiTgtDetector::~SiTgtDetector()
 {
}

void SiTgtDetector::createDetector(G4VPhysicalVolume *lab_phy, G4Material* detMaterial)
{

  G4double SiTgt_z = 0.15*mm;    //   1/2 z length (thickness i.e. 300 um)
  G4double SiTgt_y = 3.0*cm;     //   1/2 y length
  G4double SiTgt_x = 3.0*cm;     //   1/2 x length
  //  G4double SupportDist1 = 1.0;
  //  G4double SupportDist2 = 2.0;

  G4Box* SiTgt_box = new G4Box("Si_box", SiTgt_x, SiTgt_y, SiTgt_z);

  SiTgt_log = new G4LogicalVolume(SiTgt_box, detMaterial, "SiTgt_log", 0,0,0);

  G4VisAttributes* SiTgtVisATT
                 = new G4VisAttributes(G4Colour(0.0,0.0,1.0));      // Set detector colour for visualisation

  SiTgtVisATT->SetForceSolid(true);     // Solid appearance NOT wireframe

  SiTgt_log->SetVisAttributes(SiTgtVisATT);

  placement[1] = G4ThreeVector( 0.,   0.,detDist);
  
  char phys_name[20];


      sprintf(phys_name, "SiTgt_Segment%d", 1);
        SiTgt_phys_cpy[1] = new  G4PVPlacement(0,
                 placement[1],
                 phys_name, SiTgt_log, lab_phy, false, 651 );
      G4cout << "Lycca Placement for SiTgt Segment#" << 651 <<"=" << placement[1] << G4endl;
 
}

