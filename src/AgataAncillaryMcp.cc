#include "AgataAncillaryMcp.hh"
//#include "AgataDetectorAncillary.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataSensitiveDetector.hh"

#include "G4Material.hh"
#include "G4AssemblyVolume.hh"
#include "CConvexPolyhedron.hh"
#include "G4LogicalVolume.hh"
#include "G4ThreeVector.hh"
#include "G4Transform3D.hh"
#include "G4RotationMatrix.hh"
#include "G4PVPlacement.hh"
#include "G4SDManager.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4RunManager.hh"
#include "G4ios.hh"

using namespace std;

AgataAncillaryMcp::AgataAncillaryMcp(G4String path, G4String name)
{
  // dummy assignment needed for compatibility with other implementations of this class
  iniPath = path;
  dirName = name;

  solidFile = iniPath + "Ancillary/DANTEsolid.list";
  eulerFile = iniPath + "Ancillary/DANTEeuler.list";
  
  matDet     = NULL;
  matDetName = G4String("Silicon");
  
  ancSD       = NULL;
  
  ancName   = G4String("DANTE");
  ancOffset = 3000;
  
  numAncSd = 0;

  myMessenger = new AgataAncillaryMcpMessenger(this,name);
}

AgataAncillaryMcp::~AgataAncillaryMcp()
{
  pgons.clear();
  clust.clear();
  euler.clear();
  delete myMessenger;
}

G4int AgataAncillaryMcp::FindMaterials()
{
  // search the materials by its name
  G4Material* ptMaterial = G4Material::GetMaterial(matDetName);
  if (ptMaterial) {
    matDet = ptMaterial;
    G4cout << "\n----> The Mcp detector material is "
          << matDet->GetName() << G4endl;
  }
  else {
    G4cout << " Could not find the material " << matDetName << G4endl;
    G4cout << " Could not build the Mcps! " << G4endl;
    return 1;
  }
  return 0;  
}

void AgataAncillaryMcp::InitSensitiveDetector()
{
  G4int offset = ancOffset;
#ifndef FIXED_OFFSET
  offset = theDetector->GetAncillaryOffset();
#endif    
  // Sensitive Detector
  G4SDManager* SDman = G4SDManager::GetSDMpointer();
  if( !ancSD ) {
    G4int depth  = 0;
    G4bool menu  = false;
//    ancSD = new AgataSensitiveDetector( dirName, "/anc/all", "AncCollection", offset, depth, menu );
    ancSD = new AgataSensitiveDetector( dirName, "/anc/Mcp", "AncCollection", offset, depth, menu );
    SDman->AddNewDetector( ancSD );
    numAncSd++;
    //cout << "Cava" << endl;

  }
}

void AgataAncillaryMcp::GetDetectorConstruction()
{
    //cout << "Cava4" << endl;
  G4RunManager* runManager = G4RunManager::GetRunManager();
    //cout << "Cava5" << endl;
  theDetector  = (AgataDetectorConstruction*) runManager->GetUserDetectorConstruction();
    //cout << "Cava6" << endl;
}


void AgataAncillaryMcp::ReadSolidFile()
{
  FILE      *fp;
  char      line[256];
  G4int     lline, i1/*, nvdots, opgon*/;
  G4double  x, y, z, X, Y, Z;
  
  nPgons =  0;
  nDets  =  0;
  nClus  =  0;
  
  if( (fp = fopen(solidFile, "r")) == NULL) {
    G4cout << "\nError opening data file " << solidFile << G4endl;
    exit(-1);
  }

  G4cout << "\nReading description of Mcp crystals from file " << solidFile << " ..." << G4endl;

  pgons.clear();
  clust.clear();
  
  //  nvdots   =  0;
  //  opgon    = -1;
  CmcpPoints *pPg = NULL;

  while(fgets(line, 255, fp) != NULL) {
    lline = strlen(line);
    if(lline < 2) continue;
    if(line[0] == '#') continue;
    if(sscanf(line,"%d %lf %lf %lf %lf %lf %lf", &i1, &x, &y, &z, &X, &Y, &Z) != 7) {
      nPgons++;
      break;
    }
    pgons.push_back( CmcpPoints() );
    pPg = &pgons.back();
    pPg->whichMcp = i1;
    pPg->lx   = x * mm;
    pPg->ly   = y * mm;
    pPg->lz   = z * mm;
    pPg->colx = X;
    pPg->coly = Y;
    pPg->colz = Z;
    pPg->npoints = 8;
    nPgons++;
  }
  clust.resize(nPgons);
  
  fclose(fp);
  G4cout << nPgons << " polyhedra read." << G4endl;
}

void AgataAncillaryMcp::ReadEulerFile()
{
  FILE      *fp;
  char      line[256];
  G4int     lline, i1, i2;
  G4double  psi, th, ph, dist;
  
  if( (fp = fopen(eulerFile, "r")) == NULL) {
    G4cout << "\nError opening data file " << eulerFile << G4endl;
    exit(-1);
  }

  euler.clear();

  G4cout << "\nReading Euler angles from file " << eulerFile << " ..." << G4endl;
  nEuler = 0;
  
  G4RotationMatrix  rm;
  CeulAngles     *pEa = NULL;

  while(fgets(line, 255, fp) != NULL) {
    lline = strlen(line);
    if(lline < 2) continue;
    if(line[0] == '#') continue;
    if(sscanf(line,"%d %d %lf %lf %lf %lf", &i1, &i2, &psi, &th, &ph, &dist) != 6)
      break;
    euler.push_back( CeulAngles() );
    pEa = &euler[nEuler];
    
    
    pEa->numPhys  = i1;
    pEa->whichMcp = i2;

    pEa->rotMat.set( 0, 0, 0 );
    pEa->rotMat.rotateZ(psi*deg);
    pEa->rotMat.rotateY(th*deg);
    pEa->rotMat.rotateZ(ph*deg);
    
    pEa->ps      = psi*deg;
    pEa->th      = th*deg;
    pEa->ph      = ph*deg;
    
    pEa->trasl   = pEa->rotMat( G4ThreeVector(0., 0., dist*mm ) );
    
    pEa->pTransf = new G4Transform3D( pEa->rotMat, pEa->trasl );
    
    nEuler++;
  }

  fclose(fp);
  G4cout << nEuler << " Euler angles read." << G4endl;
}

void AgataAncillaryMcp::Placement()
{
  ReadSolidFile();
  ReadEulerFile();
  ConstructTheMcps();
  PlaceTheMcps();
}

void AgataAncillaryMcp::ConstructTheMcps()
{
  char sName[50];
  G4int nPg;
  
  // identity matrix
  G4RotationMatrix rm;
  rm.set(0,0,0);
  
  G4Transform3D transf;
  
  CmcpPoints   *pPg = NULL;
  CmcpAngles *pCa = NULL;
  
  G4double lx, ly, lz;
  
  G4cout << G4endl << "Building the Mcps ..." << G4endl;

  for( nPg=0; nPg<nPgons; nPg++ ) {
    pPg = &pgons[nPg];
    pCa = &clust[nPg];
    
    // Each cluster contains ONLY one Mcp!
    pCa->whichMcp = pPg->whichMcp;

    lx = pPg->lx;
    ly = pPg->ly;
    lz = pPg->lz;
    
    pPg->vertex.resize(8);
    
    pPg->vertex[0].set(     0,     0, -lz );
    pPg->vertex[1].set( 2.*lx,     0, -lz );
    pPg->vertex[2].set( 2.*lx, 2.*ly, -lz );
    pPg->vertex[3].set(     0, 2.*ly, -lz );
    pPg->vertex[4].set(     0,     0,  lz );
    pPg->vertex[5].set( 2.*lx,     0,  lz );
    pPg->vertex[6].set( 2.*lx, 2.*ly,  lz );
    pPg->vertex[7].set(     0, 2.*ly,  lz );
    
    sprintf( sName, "mcpS%3.3d", pPg->whichMcp );
#ifndef G4V10
    pPg->pPoly = new CConvexPolyhedron( G4String(sName), pPg->vertex );
#endif
    
    sprintf( sName, "mcpL%3.3d", pPg->whichMcp );
    pPg->pDetL = new G4LogicalVolume( pPg->pPoly, matDet, G4String(sName), 0, 0, 0 );
    pPg->pDetVA  = new G4VisAttributes( G4Color(pPg->colx, pPg->coly, pPg->colz) );
    pPg->pDetL->SetVisAttributes( pPg->pDetVA );
    pPg->pDetL->SetSensitiveDetector( ancSD );
    
    pCa->pAssV = new G4AssemblyVolume();
    transf = G4Transform3D( rm, G4ThreeVector(-lx, -ly, 0.) ); 
    pCa->pAssV->AddPlacedVolume( pPg->pDetL, transf );  
  }
  G4cout << G4endl << "Mcps built successfully." << G4endl;
}

void AgataAncillaryMcp::PlaceTheMcps()
{
  G4int    nCa, nEa, nMcp;
  
  CmcpAngles* pCa = NULL;
  CeulAngles*   pEc = NULL;

  G4RotationMatrix rm;
  G4ThreeVector rotatedPos;
  G4Transform3D transf;

  G4int  iClTot = 0;
  G4int  iClMin = -1;
  G4int  iClMax = -1;  
  
  nDets  = 0;
  nClus  = 0;

  G4cout << G4endl << "Placing the Mcps ... " << G4endl;
  
  for(nEa = 0; nEa < nEuler; nEa++) {
    pEc = &euler[nEa];
    nMcp = pEc->numPhys;
    if(nMcp < 0) continue;
    
    nMcp = 1000 + pEc->numPhys;
    
    //G4cout << " nMcp is " << nMcp << G4endl;
    
    for(nCa = 0; nCa < nPgons; nCa++) {
      pCa = &clust[nCa];
      if(pCa->whichMcp != pEc->whichMcp) continue;
      if(!pCa->pAssV) continue;
      
      rm = pEc->rotMat;

      rotatedPos  = pEc->trasl;
      
      transf = G4Transform3D( rm, rotatedPos  );
      pCa->pAssV->MakeImprint(theDetector->HallLog(), transf, nMcp-1);
      
      printf(" %4d %4d %4d %8d %9.2f %9.2f %9.2f %9.2f %9.2f %9.2f\n",
             iClTot, pEc->whichMcp, pEc->numPhys, nMcp-1,pEc->ps/deg, pEc->th/deg, pEc->ph/deg,
                rotatedPos.x()/cm, rotatedPos.y()/cm, rotatedPos.z()/cm );
      
      nDets++;
      nClus++;
      iClTot++;
      
      if(iClMin < 0 || nMcp < iClMin) iClMin = nMcp-1000;
      if(iClMax < 0 || nMcp > iClMax) iClMax = nMcp-1000;
    }
  }
  
  iGMin = iClMin;
  
  G4cout << "Number of placed Mcps is " << iClTot << G4endl;
  G4cout << "Mcp detector index ranging from " << iClMin << " to " << iClMax << G4endl << G4endl;
}

///////////////////////////////////////////////////////////////////
/// methods for the messenger
////////////////////////////////////////////////////////////////////
void AgataAncillaryMcp::SetSolidFile(G4String nome)
{
  if( nome(0) == '/' )
    solidFile = nome;
  else {
    if( nome.find( "./", 0 ) != string::npos ) {
      G4int position = nome.find( "./", 0 );
      if( position == 0 )
        nome.erase( position, 2 );
    }  
    solidFile = iniPath + nome;
  }  
     
  G4cout << " ----> The Mcp sizes are read from "
             << solidFile << G4endl;
}
  
void AgataAncillaryMcp::SetAngleFile(G4String nome)
{
  if( nome(0) == '/' )
    eulerFile = nome;
  else {
    if( nome.find( "./", 0 ) != string::npos ) {
      G4int position = nome.find( "./", 0 );
      if( position == 0 )
        nome.erase( position, 2 );
    }  
    eulerFile = iniPath + nome;
  }  

  G4cout << " ----> The Euler angles are read from "
             << eulerFile << G4endl;
}

//////////////////////
// summary printout
//////////////////////
void AgataAncillaryMcp::ShowStatus()
{
  G4cout << " Placed " << nDets << " Mcp detector of DANTE." << G4endl;
  G4cout << " Size of the Mcp detectors read from " << solidFile << G4endl;
  G4cout << " Position of the Mcp detectors read from " << eulerFile << G4endl;
}

void AgataAncillaryMcp::WriteHeader(std::ofstream &/*outFileLMD*/, G4double /*unit*/)
{}
  
/////////////////////////////////////////////////////////////////////////
// The Messenger
/////////////////////////////////////////////////////////////////////////

#include "G4UIdirectory.hh"
#include "G4UIcmdWithAString.hh"

AgataAncillaryMcpMessenger::AgataAncillaryMcpMessenger(AgataAncillaryMcp* pTarget, G4String /*name*/)
:myTarget(pTarget)
{ 
  const char *aLine;
  G4String commandName;
  G4String directoryName;
  
#ifdef CLARA
  directoryName  = "/Clara";
#else  
  directoryName  = "/Agata";
#endif  
  directoryName += "/detector/ancillary/Dante/";
  
  myDirectory = new G4UIdirectory(directoryName);
  myDirectory->SetGuidance("Control of Dante detector construction.");
  
  commandName = directoryName + "solidFile";
  aLine = commandName.c_str();
  SetSolidCmd = new G4UIcmdWithAString(aLine,this);
  SetSolidCmd->SetGuidance("Select file with the Mcp detector size.");
  SetSolidCmd->SetGuidance("Required parameters: 1 string.");
  SetSolidCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
    
  commandName = directoryName + "angleFile";
  aLine = commandName.c_str();
  SetAngleCmd = new G4UIcmdWithAString(aLine,this);
  SetAngleCmd->SetGuidance("Select file with the Mcp detector angles.");
  SetAngleCmd->SetGuidance("Required parameters: 1 string.");
  SetAngleCmd->AvailableForStates(G4State_PreInit,G4State_Idle);
}

AgataAncillaryMcpMessenger::~AgataAncillaryMcpMessenger()
{
  delete myDirectory;
  delete SetSolidCmd;
  delete SetAngleCmd;
}

void AgataAncillaryMcpMessenger::SetNewValue(G4UIcommand* command,G4String newValue)
{ 

  if( command == SetSolidCmd ) {
    myTarget->SetSolidFile(newValue);
  } 
  if( command == SetAngleCmd ) {
    myTarget->SetAngleFile(newValue);
  } 
}
