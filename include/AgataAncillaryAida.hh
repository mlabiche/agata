#ifdef ANCIL
////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
/// simulation. The detector which is described here is merely an ideal shell of silicon.
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillaryAida_h
#define AgataAncillaryAida_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

// class AgataAncillaryCluster; // Doesn't work !!!!???
class G4Material;
class AgataSensitiveDetector;
//class AgataAncillaryAidaMessenger;

class AgataAncillaryAida : public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryAida(G4String,G4String);
    ~AgataAncillaryAida();

  private:
    G4String dirName;

  private:
  //  AgataAncillaryCluster* theCluster;  // Doesn't work !!!!???

  // AgataAncillaryAidaMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     nam_vacuum;
    G4Material  *mat_vacuum;
    
    G4String     nam_aluminium;
    G4Material  *mat_aluminium;
    
    G4String     nam_silicon;
    G4Material  *mat_silicon;

  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   ShowStatus              ();

  public:
    inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector ) { return 0;  };
    inline G4int GetCrystalType    ( G4int )			   { return -1; };

};

#endif

#endif
