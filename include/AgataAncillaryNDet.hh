#ifdef ANCIL
#ifndef NDetDetectorConstruction_h
#define NDetDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Transform3D.hh"
#include "G4Material.hh"
#include "G4ThreeVector.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

class AgataAncillaryNDetMessenger;

class AgataAncillaryNDet : public AgataAncillaryScheme
{
public:
  AgataAncillaryNDet(G4String,G4String);
  ~AgataAncillaryNDet();

private:
  G4bool useAlCans;
  
public:
  G4int FindMaterials();
  void  GetDetectorConstruction();
  void  InitSensitiveDetector();
  void  Placement();
  void  ShowStatus();
  void  WriteHeader (std::ofstream &outFileLMD, G4double=1.0*mm);
  
public:
  G4int GetSegmentNumber(G4int,G4int,G4ThreeVector);
  inline G4int GetCrystalType(G4int){return -1;};

public:
  void SetUseAlCans( G4bool );
  void SetDistanceToTheTarget( G4double );
  void SetCylinderDiameter   ( G4double );
  void SetCylinderHight      ( G4double );
  void SetAluminiumThickness ( G4double );
  

private:
  void PlaceCylinders();
  void PlaceAlCans();
  
  AgataAncillaryNDetMessenger* myMessenger;

private:
  G4String     nameAl;
  G4Material*  matAl;
  G4String     nameScint;
  G4Material*  Scint;
//  G4String     nameBC501A;
//  G4Material*  BC501A;
//  G4String     nameBC537;
//  G4Material*  BC537;
  
private:
  G4double distanceToTheTarget;
  G4double cylinderDiameter; 
  G4double cylinderHight;
  G4double aluminiumThickness;
  G4double theSmallDistance;
  G4RotationMatrix rmZero;
};

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
 
class AgataAncillaryNDetMessenger: public G4UImessenger
{
public:
  AgataAncillaryNDetMessenger(AgataAncillaryNDet*,G4String);
  ~AgataAncillaryNDetMessenger();
  
private:
  AgataAncillaryNDet*       myTarget;
  G4UIdirectory*            myDirectory;
  
  G4UIcmdWithABool*    enableAlCans;
  G4UIcmdWithABool*   disableAlCans;

  G4UIcmdWithADoubleAndUnit* DistanceToTheTarget;
  G4UIcmdWithADoubleAndUnit* CylinderDiameter; 
  G4UIcmdWithADoubleAndUnit* CylinderHight;
  G4UIcmdWithADoubleAndUnit* AluminiumThickness;

public:
  void SetNewValue(G4UIcommand * command,G4String newValues);
};

#endif
#endif
