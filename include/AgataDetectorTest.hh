////////////////////////////////////////////////////////
//  This class handles geometry to test attenuation, scattering and energy losses:
//  Added 09/2012 by Joa Ljungvall to make testing of the physics easier.
//  A target is positioned at 0,0,0 and encircled by a sphere. Energy loss in the
//  target is stored and the position and full energy of a particle when it hits the
//  sphere
////////////////////////////////////////////////////////

#ifndef AgataDetectorTest_h
#define AgataDetectorTest_h 1

#include "AgataDetectorConstruction.hh"

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4UserLimits.hh"

using namespace std;

#ifdef G4V10
using namespace CLHEP;
#endif

class G4Material;
class AgataDetectorTestMessenger;
class AgataDetectorAncillary;
class G4UserLimits;


class MyUserLimits : public G4UserLimits

{
public:
  MyUserLimits() : G4UserLimits() {;}
  ~MyUserLimits() {;}
  G4double GetMaxAllowedStep(const G4Track&);
};

class AgataDetectorTest : protected AgataDetectorConstructed
{
  public:
    AgataDetectorTest(G4String, G4String,G4String);
    ~AgataDetectorTest();

  private:
    G4double         targetThickness;
    G4double         targetSide;
    G4ThreeVector    targetposition;
    
  private:
    G4String         matTargetName;
    G4Material      *matTarget;

  private:
    G4String         matVacName;
    G4Material      *matVac;

  private:
  G4bool fScoreTestSphere;
  G4bool fScoreTarget;
  MyUserLimits *fUserLimits;
  AgataDetectorAncillary      *theAncillary;
  AgataDetectorTestMessenger*  myMessenger;
  
  private:
    G4int FindMaterials  ();
      
  public:
  void SetTargetThickness     ( G4double );
  void SetTargetSide     ( G4double );
  void SetTargetPosition   ( G4ThreeVector );
  void SetTargetMaterial(G4String);
  void SetScoreTestSphere(G4bool val){fScoreTestSphere=val;}
  void SetScoreTarget(G4bool val){fScoreTarget=val;}
  public:
    void Placement       ();
    void WriteHeader     ( std::ofstream &outFileLMD, G4double=1.*mm );
    void ShowStatus      ();
    
  public:
    inline G4int GetSegmentNumber ( G4int /*offset*/, G4int /*nGe*/, G4ThreeVector /*position*/ ) { return  0; };
    inline G4int GetCrystalType   ( G4int /*detNum*/ )                                            { return -1; };
};


#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;

class AgataDetectorTestMessenger: public G4UImessenger
{
  public:
    AgataDetectorTestMessenger(AgataDetectorTest*,G4String);
   ~AgataDetectorTestMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  AgataDetectorTest*       myTarget;
  
  G4UIdirectory*             myDirectory;
  G4UIcmdWithADouble*        SetThicknessCmd;
  G4UIcmdWithADouble*        SetSideCmd;
  G4UIcmdWith3Vector*        SetPositionCmd;
  G4UIcmdWithAString*        SetTargetMaterial;
  G4UIcmdWithABool*          SetScoreTestSphere;
  G4UIcmdWithABool*          SetScoreTarget;
};

#endif
