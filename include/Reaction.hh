#ifndef Reaction_h
#define Reaction_h 1

#include "G4ios.hh"
#include "globals.hh"
#include "G4VProcess.hh"
#include "G4VParticleChange.hh"
#include "G4ParticleChange.hh"
#include "G4Track.hh"
#include "G4Step.hh"
#include "G4UnitsTable.hh"
#include "G4UserLimits.hh"
#include "G4DynamicParticle.hh"
#include "G4ParticleTable.hh"
//#define   eps 0.00001
#ifdef G4V10
#include "G4IonTable.hh"
#endif

class Outgoing_Beam;
class Incoming_Beam;

class Reaction : public G4VProcess 
{
  public:     
  Reaction(Outgoing_Beam*, Incoming_Beam*,
	   const G4String& processName ="Reaction" );
     virtual ~Reaction();

  public:     
   G4bool reaction_here;

  private:
     G4double threshold;
  G4int ReactionEachNEvents;
  private:
    Outgoing_Beam* BeamOut;
    Incoming_Beam* BeamIn;

  public:     
    virtual 
    G4double PostStepGetPhysicalInteractionLength( const G4Track& track,
						   G4double previousStepSize,
						   G4ForceCondition* condition);

  public:     
    virtual G4VParticleChange* PostStepDoIt( const G4Track&, const G4Step& );
			    

  public:     
     //  no operation in  AtRestGPIL
    virtual 
    G4double AtRestGetPhysicalInteractionLength( const G4Track&,
						 G4ForceCondition* );
			    

  public:     
     //  no operation in  AtRestDoIt      
    virtual G4VParticleChange* AtRestDoIt( const G4Track&, const G4Step& );


  public:     
     //  no operation in  AlongStepGPIL
     virtual G4double AlongStepGetPhysicalInteractionLength(const G4Track&, 
							    G4double,G4double, 
							    G4double&, 
							    G4GPILSelection* );


  public:     
     //  no operation in  AlongStepDoIt
     virtual G4VParticleChange* AlongStepDoIt( const G4Track&, const G4Step& );

  void SetEventsPerReaction(G4int eventsperreaction)
  {ReactionEachNEvents=eventsperreaction;}
  private:
  // hide assignment operator as private 
    Reaction& operator=(const Reaction&){return *this;};
};

#endif

