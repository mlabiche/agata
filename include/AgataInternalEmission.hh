/////////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides an implementation of the abstract AgataEmission class. In this case
/// a cascade with fixed multiplicity is assumed, including neutrons, light charged particles
/// and gammas.
////////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataInternalEmission_h
#define AgataInternalEmission_h 1

#include "globals.hh"
#include <vector>

#include "AgataDefaultGenerator.hh"

using namespace std;

class AgataEmission;
class AgataInternalEmitter;
class AgataInternalEmissionMessenger;


class AgataInternalEmission : protected AgataEmission
{
  public:
    AgataInternalEmission( G4String, G4bool, G4bool, G4String );
    ~AgataInternalEmission();
    
  private:
    AgataInternalEmitter*             theInternalEmitter;
    AgataInternalEmissionMessenger*   myMessenger;

  private:
    G4String                          iniPath;    //> directory where the data files are stored
        
  private:
    G4int                             types;      //> number of possible emitted particles, depending whether
                                                  //> hadron emission is enabled or not
    std::vector <G4int>               mult;       //> multiplicity of each emitted particle
    std::vector <G4int>               multSum;    //> summed multiplicity along the cascade (sum over mult vector)
    std::vector <G4int>               index;      //> type of particle corresponding to a given position
                                                  //> within the cascade
    std::vector <G4int>               order;      //> order within a given kind of emitted particle
    
  ///////////////////////////////////////
  /// Methods required by AgataEmission
  ///////////////////////////////////////
  public:
    void BeginOfEvent ();
    void EndOfEvent   ();
    void BeginOfRun   ();
    void EndOfRun     ();
    void NextParticle ();
    
  public:
    void GetStatus();
    void PrintToFile( std::ofstream &outFileLMD, G4double=1.*mm, G4double=1.*keV );

  public:
    inline G4String GetBeginOfEventTag() { return G4String(""); };

  public:
    G4String GetEventHeader    ( G4double=1.*mm );
    G4String GetParticleHeader ( const G4Event*, G4double=1.*mm, G4double=1.*keV );
    
  public:
    void SetTargetEvents( G4int );     //> choses the number of events which should be processed
                                       //> in the following run
        
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAnInteger;

class AgataInternalEmissionMessenger: public G4UImessenger
{
  public:
    AgataInternalEmissionMessenger(AgataInternalEmission*, G4String);
   ~AgataInternalEmissionMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataInternalEmission*     myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithAnInteger*      SetTargetEventsCmd;

};

#endif
