#ifdef ANCIL
////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary detector for the AGATA
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillaryParis_h
#define AgataAncillaryParis_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillaryParisMessenger;

class AgataAncillaryParis : public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryParis(G4String,G4String);
    ~AgataAncillaryParis();

  private:
    AgataAncillaryParisMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     nam_vacuum;
    G4Material  *mat_vacuum;
    
    G4String     nam_aluminium;
    G4Material  *mat_aluminium;
    
    G4String     nam_labr3;
    G4Material  *mat_labr3;

    G4String	 nam_nai;
    G4Material  *mat_nai;
    
    G4String     nam_quartz;
    G4Material  *mat_quartz;
  
   static G4ThreeVector  GetDetPos(int nb);


  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   ShowStatus              ();

  public:
    inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector )  { return 0; };
    inline G4int GetCrystalType    ( G4int )                        { return -1; };

};

#endif

#endif
