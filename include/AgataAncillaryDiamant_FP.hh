#ifdef ANCIL
////////////////////////////////////////////////////////////////////////////////////////////
/// This class provides a very simple example of ancillary  defined in gdml for the 
///  AGATA simulation.
/// The geometry included is the Aluminium HoneyComb of Agata
////////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataAncillaryDiamant_FP_h
#define AgataAncillaryDiamant_FP_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

#include "G4GDMLParser.hh"


using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillaryDiamant_FPMessenger;

class AgataAncillaryDiamant_FP : public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryDiamant_FP(G4String,G4String);
    ~AgataAncillaryDiamant_FP();

  private:
    AgataAncillaryDiamant_FPMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     matName;
    G4Material  *matShell;

    G4LogicalVolume* m_LogicalDiamantFP;
 
	G4GDMLParser m_gdmlparser;

  
  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double=1.*mm);
    void   ShowStatus              ();

  public:
    inline G4int GetSegmentNumber  ( G4int, G4int, G4ThreeVector ) { return 0;  };
    inline G4int GetCrystalType    ( G4int )			   { return -1; };

};

#endif

#endif
