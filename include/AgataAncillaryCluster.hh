#ifndef AgataDetectorCluster_h
#define AgataDetectorCluster_h 1

#include "globals.hh"
#include "G4Point3D.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"
#include "G4Transform3D.hh"
#include <vector>

#include "AgataDetectorConstruction.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4Plane3D.hh"
#include "G4Normal3D.hh"

using namespace std;

class CConvexPolyhedron;
class G4Tubs;
class G4Polycone;
class G4IntersectionSolid;
class G4LogicalVolume;
class G4VisAttributes;
class G4VPhysicalVolume;

class CpolyhPoints;
class CclusterAngles;
class CeulerAngles;
class G4AssemblyVolume;

class G4Material;
class AgataAncillaryClusterMessenger;
class AgataDetectorAncillary;
class AgataDetectorConstruction;

/////////////////////////////////////////////////////////////////////
/// This class handles the construction of 
/// AGATA-style ancillary detectors
////////////////////////////////////////////////////////////////////
class AgataAncillaryCluster : protected AgataDetectorConstructed
{
  public:
    AgataAncillaryCluster(G4String, G4String, G4String, G4String, G4String, G4String, G4String, G4String);
    ~AgataAncillaryCluster();
    
  private:
    AgataAncillaryClusterMessenger *myMessenger;
    
  /////////////////////////////////////////////////
  /// Files from which actual geometry is read 
  ///////////////////////////////////////////////// 
  private:
    G4String                    iniPath;     //> directory where the files are located
    G4String                    eulerFile;   //> angles and positions to place the clusters into space
    G4String                    solidFile;   //> shape of the crystals
    G4String                    wallsFile;   //> cryostats
    G4String                    clustFile;   //> arrangement of the crystals and cryostats within a cluster
    
  private:
    G4String                    directoryName;  //> for the command line  
    
  /////////////////////////////////////
  /// materials (pointers and names)
  ///////////////////////////////////// 
  private:
    G4Material                  *matCryst;   //> crystals
    G4Material                  *matWalls;   //> cryostats and encapsulation
    G4Material                  *matHole;    //> vacuum within the cryostat
    
  private:
    G4String                    matCrystName; //> crystals
    G4String                    matWallsName; //> cryostats and encapsulation
    G4String                    matHoleName;  //> vacuum within the cryostat
     
  ///////////////////////////////////////////////////////////////////////
  /// structures needed to store geometry data during the construction  
  ///////////////////////////////////////////////////////////////////////
  private:
    std::vector<CeulerAngles>   euler;        //> angles and positions to place the clusters into space
    std::vector<CpolyhPoints>   pgons;        //> shape of the crystals
    std::vector<CpolyhPoints>   walls;        //> cryostats
    std::vector<CclusterAngles> clust;        //> arrangement of the crystals and cryostats within a cluster

  private:
    G4int                       nEuler;       //> number of clusters composing the array
    G4int                       nPgons;       //> number of different crystal shapes within the array
    G4int                       nClAng;       //> number of crystals composing a cluster
    G4int                       nWalls;       //> number of cryostat parts within a cluster
    G4int                       maxPgons;     //> maximum index of crystal shapes

  private:
    G4int                       nWlTot;       //> total number of cryostat parts within the array
    G4int                       maxSolids;    //> maximum number of solids within a cluster
    
    
  ///////////////////////////////////////////
  /// rotation applied to the whole array
  //////////////////////////////////////////
  private:
    G4double                    thetaShift;   //> theta
    G4double                    phiShift;     //> phi

  ///////////////////////////////////////////
  /// traslation applied to the whole array
  //////////////////////////////////////////
  private:
    G4ThreeVector               posShift;   
    
  /////////////////
  /// some flags 
  //////////////// 
  private:
    G4bool                      useCylinder;  //> true: the intersection with the cylinder will be considered

  private:
    AgataSensitiveDetector*     ancSD;        //> passed from "real" ancillary
    AgataDetectorConstruction*  theDetector;


  //////////////////////////////
  ///////// Methods  ///////////
  //////////////////////////////
  private:
    void     InitData(G4String, G4String, G4String, G4String, G4String, G4String, G4String, G4String);

  //////////////////////////
  /// read the input files
  /////////////////////////
  private:
    void      ReadEulerFile();
    void      ReadSolidFile();
    void      ReadWallsFile();
    void      ReadClustFile();
  
  //////////////////////////////////////////////////////////////
  /// look for the materials starting from the material names
  /////////////////////////////////////////////////////////////
  public:
    G4int     FindMaterials(G4String);
    
  /////////////////////////////////////////////////////////
  /// Construct the various elements composing the array
  /////////////////////////////////////////////////////////  
  private:
    void      ConstructGeCrystals  ();    
    void      ConstructTheClusters ();    
    void      ConstructTheWalls    ();
    
  ////////////////////////////////
  /// placement of the elements  
  ////////////////////////////////
  private:
    void      PlaceTheClusters     ();


  ////////////////
  /// placement
  ///////////////
  public:
    void Placement ();

  //////////////////////////////////////////////
  /// public interface to the private method!
  ////////////////////////////////////////////////
  public:
    G4int     GetSegmentNumber         ( G4int, G4int, G4ThreeVector );    
  
  //////////////////////////////////
  /// writes out the information
  /////////////////////////////////
  public:
    void WriteHeader                  ( std::ofstream &outFileLMD, G4double=1.*mm );

  public:
    void GetDetectorConstruction ();
    
  public:
    void SetSensitiveDetector ( AgataSensitiveDetector* theSD ) { ancSD = theSD; };
  //////////////////////////////////////////////////
  ///////////// public methods for the messenger
  ////////////////////////////////////////////////// 
  public:       
    void SetSolidFile           ( G4String );
    void SetWallsFile           ( G4String );
    void SetAngleFile           ( G4String );
    void SetClustFile           ( G4String );

  public:       
    void SetDetMate             ( G4String );
    void SetWallsMate           ( G4String );

  public:      
    void SetThetaShift          ( G4double );
    void SetPhiShift            ( G4double );
    void SetPosShift            ( G4ThreeVector );

  public:      
    void SetUseCylinder         ( G4bool );
    
  public:
    void ShowStatus             ( G4String );
    void ShowStatus () {};
    
  ///////////////////////////////////////////
  //////////////// inline "get" methods
  ///////////////////////////////////////////
  public:
    inline G4double              GetThetaShift      () { return thetaShift;  };
    inline G4double              GetPhiShift        () { return phiShift;    };
    
  public:
    inline G4String              GetEulerFile       () { return eulerFile;   };
    inline G4String              GetSolidFile       () { return solidFile;   };
    inline G4String              GetWallsFile       () { return wallsFile;   };
    
  public:
    inline G4int                 GetCrystalType ( G4int ) { return -1; };
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;
class G4UIcmdWithAnInteger;
class G4UIcmdWithABool;

class AgataAncillaryClusterMessenger: public G4UImessenger
{
  public:
    AgataAncillaryClusterMessenger(AgataAncillaryCluster*,G4String,G4String);
   ~AgataAncillaryClusterMessenger();
    
  private:
    AgataAncillaryCluster*        myTarget;
    
  private:
    G4UIcmdWithAString*        SetSolidCmd;
    G4UIcmdWithAString*        SetAngleCmd;
    G4UIcmdWithAString*        SetWallsCmd;
    G4UIcmdWithAString*        SetClustCmd;
    G4UIcmdWithAString*        DetMatCmd;
    G4UIcmdWithAString*        WalMatCmd;
    G4UIcmdWithAString*        RotateArrayCmd;
    G4UIcmdWith3Vector*        TraslateArrayCmd;
    G4UIcmdWithABool*          EnableCylCmd;
    G4UIcmdWithABool*          DisableCylCmd;
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
};


#endif
