
#ifndef AgataReactionPhysics_h
#define AgataReactionPhysics_h 1

#include "globals.hh"
#include "G4VPhysicsConstructor.hh"

#include "Reaction.hh"
#include "G4StepLimiter.hh"

class Outgoing_Beam;
class Incoming_Beam;

class AgataReactionPhysics : public G4VPhysicsConstructor
{
  public: 
    AgataReactionPhysics(const G4String& name = "reaction", G4int ver = 1);
    virtual ~AgataReactionPhysics();

  public: 
    // This method will be invoked in the Construct() method. 
    // each particle type will be instantiated
  virtual void ConstructParticle(){};  
  //We indeed use the one belonging to another PL ...
  
  // This method will be invoked in the Construct() method.
  // each physics process will be instantiated and
  // registered to the process manager of each particle type 
  virtual void ConstructProcess();
  
  void SetOutgoingBeam(Outgoing_Beam*);
  void SetIncomingBeam(Incoming_Beam*);
  Reaction* GetReactionProcess(){return fReactionProcess;}


private:
  Reaction* fReactionProcess;
  G4StepLimiter* fLimiterProcess;
  Outgoing_Beam* BeamOut;
  Incoming_Beam* BeamIn;
  G4int    verbose;
  G4bool   wasActivated;
};

#endif








