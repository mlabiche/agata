/////////////////////////////////////////////////////////////////////////////
/// This class handles the position and vector velocity of the emitting
/// source. It is never instantiated as it is, rather other classes inherit
/// the relevant members/methods.
////////////////////////////////////////////////////////////////////////////
#ifndef AgataEmitter_h
#define AgataEmitter_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "G4RotationMatrix.hh"

using namespace std;

class AgataEmitter
{
  public:
    AgataEmitter();
   ~AgataEmitter();
   
  protected:
    G4int               sourceType;       //> Type of source considered:             
                                          //> 0 --> point source, chosen position	  
                                          //> 1 --> point source, target position
                                          //> 2 --> uniformly diffused source in target
					  //> 3 --> gaussian distribution source in target

  ///////////////////////////////////////////
  /// Some characteristics of the source 
  ///////////////////////////////////////////
  protected:
    G4ThreeVector       sourcePosition;   //> sourceType = 0
    G4ThreeVector       targetPosition;   //> sourceType = 1
    G4ThreeVector       targetSize;       //> sourceType = 1,2 
    G4ThreeVector       targetSig;        //> sourceType = 2 
    G4ThreeVector       initialPosition;  //> resulting from sourceType
    
  protected:
    G4int               flagRecoil;       //> 0 --> recoil along fixed direction               
                                          //> 1 --> opening angle along z
    
  protected:
    G4bool              movingSource;     //> true: source moves
    G4bool              longLivedSource;  //> true: decay times should be considered
    G4bool              diffusedSource;   //> true: source is not point-like
    G4bool              diffusedRecoil;   //> true: source velocity is not fixed
    G4bool              diffusedBeta;     //> true: source velocity module is not fixed

  /////////////////////////
  /// Velocity module
  ////////////////////////
  protected:
    G4double            emitterBeta;             //> event-by-event
    G4double            emitterBetaAverage;      //> average
    G4double		emitterBetaSigma;        //> dispersion (absolute value)
    G4double		emitterBetaSigmaFrac;    //> dispersion (fraction of emitterBetaAverage)

  /////////////////////////
  /// Velocity direction
  ////////////////////////
  protected:
    G4ThreeVector       emitterDirection;         //> event-by-event
    G4ThreeVector       emitterDirectionAverage;  //> average
    G4double            emitterOpAngle;    	  //> opening of the recoil cone
    G4double            emitterOpAngleCos;	  //> cosine of emitterOpAngle
    G4RotationMatrix    emitterRotation;          //> rotation from z axis (in case the AGATA
                                                  //> array is rotated)

  protected:
    G4bool              gaussAngle;
    G4double            emitterOpAngleSig;	  //> cosine of emitterOpAngle (gaussian distrib.)

  /////////////////////////////////////////////////////////////////
  /// Velocity and position of the source (event-by-event values)
  //////////////////////////////////////////////////////////////////
  protected:
    G4ThreeVector       emitterPosition;            
    G4ThreeVector       emitterVelocity;  
      
  // For AGATA-PRISMA
    G4int emitterIonicCharge;      
  
  ///////////////////////////////////////////////////////////////////////////////
  /// Methods called each time a new event (considering multiplicity) is fired
  ///////////////////////////////////////////////////////////////////////////////
  protected:
    void InitData         ();
    void InitBeta         ();
    void InitDirection    ();
    
  protected:
    void VelocityVector();           //> calculates vector velocity from module and direction
    
  ////////////////////////////////////////////////////////////////////////
  /// This method returns the initial position of the source when a new 
  /// event (considering multiplicity) is fired
  ////////////////////////////////////////////////////////////////////////
  protected:
    G4ThreeVector SourceDisplacement();
    
  ////////////////////////
  /// Public methods
  ////////////////////////  
  
  /////////////////////////////////////////////////////////////////////////
  /// Calculates initial position and velocity of the source when a new
  /// event (considering multiplicity) is fired
  ////////////////////////////////////////////////////////////////////////
  public:
    void   RestartEmitter();     
    
  /////////////////////////////////////////////////////////////////////////
  /// Recalculates the position of the source when a new particle is fired
  /// (with a non-zero lifetime)
  ////////////////////////////////////////////////////////////////////////
  public:
    void   RepositionEmitter( G4double );
    void   TraslateEmitter( G4double );
    
  public:
    void   BeginOfRun(); 

  public:
    void SetEmitterOpAngle    ( G4double );
    void SetEmitterDirection  ( G4ThreeVector );
    void SetSourceType        ( G4int );
    void SetGaussAngle        ( G4bool );
    void SetSourcePosition    ( G4ThreeVector );

  ////////////////////////
  /// Inline methods
  ////////////////////////  
    
  public:
    inline G4ThreeVector GetEmitterVelocity()    { return emitterVelocity;   };  
    inline G4ThreeVector GetEmitterPosition()    { return emitterPosition;   };  
    inline G4ThreeVector GetEmitterDirection()   { return emitterDirection;  };  
    inline G4int         GetSourceType()         { return sourceType;        };  
    inline G4ThreeVector GetSourcePosition()     { return sourcePosition;    };  
    inline G4double      GetEmitterBeta()        { return emitterBeta;       };  
    inline G4double      GetEmitterBetaAverage() { return emitterBetaAverage;};  
    inline G4double      GetEmitterBetaSigma()   { return emitterBetaSigma;  };  
    inline G4double      GetEmitterOpAngle()     { return emitterOpAngle;    };

    // For AGATA-PRISMA
    inline G4int         GetEmitterIonicCharge() { return emitterIonicCharge;};  
      
  public:
    inline G4bool        IsRecoilDiffuse()       { return diffusedRecoil;    };
    inline G4bool        IsSourceDiffuse()       { return diffusedSource;    };
    inline G4bool        IsSourceLongLived()     { return longLivedSource;   };
    inline G4bool        IsSourceMoving()        { return movingSource;      };
    inline G4bool        IsBetaDiffuse()         { return diffusedBeta;      };
      
  public:
    inline void   SetRecoilDiffuse ( G4bool value ) { diffusedRecoil  = value;    };
    inline void   SetSourceDiffuse ( G4bool value ) { diffusedSource  = value;    };
    inline void   SetSourceLived   ( G4bool value ) { longLivedSource = value;    };
    inline void   SetMovingSource  ( G4bool value ) { movingSource    = value;    };
    inline void   SetBetaDiffuse   ( G4bool value ) { diffusedBeta    = value;    };

        
};

#endif

