/***************************************************************************
                          CsIDetector.h  -  description
                             -------------------
    begin                : Mon Oct 3 2005
    copyright            : (C) 2005 by Mike Taylor
    email                : mjt502@york.ac.uk
    
Modified by Pavel Golubev (pavel.golubev@nuclear.lu.se) 
LYCCA0 configuration implemented
 ***************************************************************************/

#ifndef CsIDetector_h
#define CsIDetector_h 1


#if defined G4V10
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#endif

#include "G4ThreeVector.hh"

class G4PVPlacement;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4Material;

class CsIDetector
{
  public:

    CsIDetector(G4double);
    virtual ~CsIDetector();

    void createDetector(G4VPhysicalVolume *,  G4Material *);
    
    G4double detDist;

    G4PVPlacement *CsI_phys_cpy[109];
    G4ThreeVector placement[109];
    G4LogicalVolume *CsI_log;
    G4LogicalVolume *Cate_CsI_log;

  private:

//    void defineMaterial();
//  void setAsSensitiveDetector();

    G4Material *detectorMaterial;
    G4VPhysicalVolume *CsI_phys;
    G4VPhysicalVolume *Cate_CsI_phys;

    G4bool constructed;

    
};

#endif
