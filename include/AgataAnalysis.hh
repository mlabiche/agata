///////////////////////////////////////////////////////////////////////////////
/////// Simulation of Agata with Geant 4
/////// by Enrico Farnea
/////// version 2.0 19/01/2003
///////////////////////////////////////////////////////////////////////////////
/////// AgataAnalysis class
///////////////////////////////////////////////////////////////////////////////

#ifndef AgataAnalysis_h
#define AgataAnalysis_h 1

#include "G4UserRunAction.hh"
#include <fstream>
#include <vector>

#include "G4ThreeVector.hh"
#include "globals.hh"

using namespace std;


class G4Run;
class AgataDetectorConstruction;
class AgataGeneratorAction;
class AgataEventAction;
class AgataPhysicsList;
class AgataHitDetector;
class AgataAnalysisMessenger;
class CSpec1D;
class CSpec2D;
class CGaspBuffer;

class AgataAnalysis : public G4UserRunAction
{
  public:
    AgataAnalysis(G4String);
   ~AgataAnalysis();

  ////////////////////////////////////////
  ////////// Members
  ////////////////////////////////////////
  
  private:
    AgataAnalysisMessenger*    myMessenger;

  public:
    G4double      Beta;

  private:
    AgataDetectorConstruction* theDetector;
    AgataGeneratorAction*      theGenerator;
    AgataEventAction*          theEvent;

  private:
    G4int      runNumber;

  private:
    G4int      nDetectorsAncilIndex; // added by Marc 
    G4int      nDetectors;  
    G4int      nClusters;   
    G4int      nInstances;  
    G4int      nAncil;	   
    G4int*     EXOGAM_AC_hit; // added by Marc    
    G4int*     EXOGAM_Clover_hit; // added by Marc    

  // status flags
  private:
    G4bool     enabledAnalysis;
    G4bool     packEvent;
    G4bool     enabledResolution;
    G4int      ancillaryOnly;
    G4bool     makeSpectra;
    G4bool     makeBuffer;
    G4bool     asciiSpectra;     //> true: spectra will be written out in ASCII format (rather than binary)

  // energy resolution parameters (for Ge detectors only) 
  private:
    G4double   FWHMa;
    G4double   FWHMb;
    G4double   FWHMk;

  private:
    std::vector<G4int> ancLut;
    std::vector<G4int> parLut;
    std::vector<G4int> minLut;

  // energy buffers for the various kind of detector
  private:
    G4int      minGeAccu, maxGeAccu;
    G4double*  geAccu;
    G4double*  gtAccu;
    G4double*  cluAccu;
    
  private:
    G4int      minAnAccu, maxAnAccu;
    G4double*  anAccu;
    G4double* *anAccuSeg;  // For exogam
  //G4double   anAccuSeg[16][4];  // For exogam
    
  private:
    G4int      minAgAccu, maxAgAccu;
    G4int*     agAccu;
    
  // buffer to write out in GASP format
  private:
    CGaspBuffer*  gaspBuffer;
    G4int         position;

  private:
   char*       aLine;
    
  // single event in GASP format
  private:
    unsigned short int* gaspEvent;
    
  // maximum file size (files are split when size reaches the limit value)
  private:
    G4int      maxFileSize;  
    G4int      fileNumber;   
  
  //////////////////////////////////////////////////////
  // energy/time calibrations for the reduced Gasp file
  ///////////////////////////////////////////////////////
  private:
    G4double   gf1;
    G4double   gf2;
    G4double   gf3;
    G4double   fThreshold;

  //////////////////////////////////////////////
  /// Offset and gain for the on-line spectra
  //////////////////////////////////////////////
  private:
    G4double  offset;
    G4double  gain;

  //////////////////////////////////////////////
  /// On-line spectra (and matrices) defined as
  /// a vector of CSpec1D (CSpec2D) objects
  //////////////////////////////////////////////
  private:
    std::vector<CSpec1D*> spec1;
    // uncomment if matrices are needed!
    //std::vector<CSpec2D*> spec2;

  private:
    G4int      specLength;       //> length (channels) of each spectrum

  private:
    G4int*     det2clust;        //> lookup table to convert detector number into cluster number
    
  private:
    G4double  scaleEtot;            //> scale factor for the total energy (sum over the detectors)

  /////////////////////////////////////////////////////////////
  /////////////////// Methods
  //////////////////////////////////////////////////////////////
  
  public:
    void    AnalysisStart        (G4int);
    void    AnalysisEnd          (G4String);
    void    EndOfEvent           ();

  public:
    void    ShowStatus();
  
  public:
    void    AddHit               ( AgataHitDetector* );

  private:
    void    AddEnergy            (G4double, G4int, G4int);
  void    AddEnergy            (G4double, G4int, G4int, G4int); // added an extra G4int to deal with EXOGAM segments
    void    AddGermaniumEnergy   (G4double, G4int, G4int);
    void    AddAncillaryEnergy   (G4double, G4int);
    void    AddAncillaryEnergy   (G4double, G4int, G4int);  // added an extra G4int to deal with EXOGAM segments
    void    AddTime              (G4double, G4double, G4int);
    void    AddGermaniumTime     (G4double, G4double, G4int);

  private:
    void    WriteGaspEvent       ();

  private:
    void    FlushSpectra         (G4String);
    void    IncrementSpectra     ();

  private:
    void    ResetBuffers         ();
    
  public:
    void EnableAnalysis          (G4bool);
    void SetAncillaryOnly        (G4bool);

  public:
    void EnableResolution        (G4bool);
    void CalculateResolution     (G4double, G4double, G4double, G4double, G4bool = true);

  public:
    void EnableSpectra           (G4bool);
    void EnableBuffer            (G4bool);
  
    
  public:
    void    SetEnerGain           ( G4double );
    void    SetTimeGain           ( G4double );
    void    SetFThreshold         ( G4double );

  public:
    void    SetPackEvent          (G4bool);

  private:
    G4double EnergyResolution    (G4double);
    void     ApplyResolution     ();

  public:
    void    SetMaxFileSize       (G4int);
     
  public:
    void    SetSpecLength       ( G4int );
     
  public:
    void    SetAsciiSpectra     ( G4bool );
    
  public:
    void    SetOffset           ( G4double );
    void    SetGain             ( G4double );
  
  //   Inline methods
  public:
    inline G4int  GetMaxFileSize ()                 { return maxFileSize;        };                   
    inline G4bool IsEnabled      ()                 { return enabledAnalysis;    };
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWithAnInteger;
class G4UIcmdWithoutParameter;


class AgataAnalysisMessenger: public G4UImessenger
{
  public:
    AgataAnalysisMessenger(AgataAnalysis*,G4String);
   ~AgataAnalysisMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);

    
  private:
    AgataAnalysis*             myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithABool*          EnableAnalysisCmd;
    G4UIcmdWithABool*          DisableAnalysisCmd;
    G4UIcmdWithABool*          EnableSpectraCmd;
    G4UIcmdWithABool*          DisableSpectraCmd;
    G4UIcmdWithABool*          EnableBufferCmd;
    G4UIcmdWithABool*          DisableBufferCmd;
    G4UIcmdWithABool*          AncillaryOnlyCmd;
    G4UIcmdWithABool*          AncillaryCoincCmd;
    G4UIcmdWithABool*          EnableResolutionCmd;
    G4UIcmdWithABool*          DisableResolutionCmd;
    G4UIcmdWithAString*        CalculateResolutionCmd;
    G4UIcmdWithADouble*        SetEnerGainCmd;
    G4UIcmdWithADouble*        SetTimeGainCmd;
    G4UIcmdWithADouble*        SetFThresholdCmd;
    G4UIcmdWithABool*          PackEventCmd;
    G4UIcmdWithABool*          UnpackEventCmd;
    G4UIcmdWithAnInteger*      SetFileSizeCmd;
    G4UIcmdWithoutParameter*   StatusCmd;
    G4UIcmdWithABool*          SetAsciiSpectraCmd;
    G4UIcmdWithABool*          UnsetAsciiSpectraCmd;
    G4UIcmdWithAnInteger*      SetSpecLengthCmd;
    G4UIcmdWithADouble*        SetOffsetCmd;
    G4UIcmdWithADouble*        SetGainCmd;
};

#endif
