#ifdef MINIBALL
#ifndef Miniball_h
#define Miniball_h 1

#include <iostream>
#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4VPhysicalVolume.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Box.hh"
#include "G4Trd.hh"
#include "G4Trap.hh"
#include "G4Tubs.hh"
#include "G4Cons.hh"
#include "G4Sphere.hh"
#include "G4RotationMatrix.hh"
#include "G4UnionSolid.hh" 
#include "G4IntersectionSolid.hh"
#include "G4SubtractionSolid.hh"
#include "G4AssemblyVolume.hh"
#include "G4RunManager.hh"
#include "G4ThreeVector.hh"
#include "G4Polyhedra.hh"
#include "G4Assembly.hh"
#include "G4PlacementVector.hh"
#include "G4VisAttributes.hh"
#include "G4Colour.hh"
#include "G4Transform3D.hh"
#include "G4PlacedSolid.hh"
#include "G4BREPSolidPolyhedra.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4VisAttributes;
class MiniballMessenger;
class AgataSensitiveDetector;
class AgataDetectorAncillary;

#include "globals.hh"
#include "G4VUserDetectorConstruction.hh"
#include "G4ThreeVector.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

class MiniBallTripleDetector;

struct MiniBallClusterPos {
  G4double r,theta,phi,psi,eta,gamma; /*mm and deg*/
};


class Miniball : public AgataAncillaryScheme 
{
public:
  Miniball();
  ~Miniball();
  virtual G4int FindMaterials();
  virtual void GetDetectorConstruction();
  virtual void WriteHeader(std::ofstream &outFileLMD, G4double unit=1.*mm);
  virtual void ShowStatus(){;}
  virtual void InitSensitiveDetector();
  virtual void Placement();
  virtual G4int GetSegmentNumber( G4int, G4int, G4ThreeVector );
  inline G4int GetCrystalType( G4int /*detnum*/ ) { return -1; };
  void SetAddBGO(bool val){AddBGO=val;};
  void ClearListOfClusters();
  void AddACluster(G4String par);
private:
  vector<MiniBallTripleDetector*> Clusters;
  vector<MiniBallClusterPos> PosOfClusters;
  G4Material  *matAntiCompton;
  G4Material *matCol;
  G4LogicalVolume *logBGO;
  G4LogicalVolume *logCol;
  MiniballMessenger *myMessenger;
  bool AddBGO;
  //BGO ancSD
  AgataSensitiveDetector *bgoancSD;
};



#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithAnInteger;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;

class MiniballMessenger: public G4UImessenger
{
  public:
    MiniballMessenger(Miniball*,G4String);
   ~MiniballMessenger();
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  Miniball* MiniballDetector;
  G4UIdirectory* MiniballDetDir;
  G4UIcmdWithAString *MiniballAddDet;
  G4UIcmdWithAString *MiniballAddDetToMBFrame;
  G4UIcmdWithoutParameter* MiniballClearDets;
  G4UIcmdWithABool *UseBGO;

};


#endif
#endif
