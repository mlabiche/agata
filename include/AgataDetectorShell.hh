///////////////////////////////////////////////////////////
/// This class handles the ideal germanium shell
/// used normally to benchmark tracking algorithms
///////////////////////////////////////////////////////////

#ifndef AgataDetectorShell_h
#define AgataDetectorShell_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "AgataDetectorConstruction.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataDetectorShellMessenger;

class AgataDetectorShell : protected AgataDetectorConstructed
{
  public:
    AgataDetectorShell(G4String);
    ~AgataDetectorShell();

  private:
    G4double     shellRmin;
    G4double     shellRmax;
    
  private:
    G4String     matName;
    G4Material  *matShell;

  private:
    AgataDetectorShellMessenger* myMessenger;
    
  public:
    void SetShellRmin  ( G4double );
    void SetShellRmax  ( G4double );
    void SetShellMate  ( G4String ); 
    
  public:
    inline G4double GetShellRmin()   { return shellRmin;  };
    inline G4double GetShellRmax()   { return shellRmax;  }; 
    
  public:
    inline G4int GetSegmentNumber ( G4int /*offset*/, G4int /*nGe*/, G4ThreeVector /*position*/ ) { return  0; };
    inline G4int GetCrystalType   ( G4int /*detNum*/ )                                            { return -1; };


  public:
    void Placement   ();
    void WriteHeader ( std::ofstream &outFileLMD, G4double=1.*mm );
    void ShowStatus  ();

  private:
    G4int FindMaterials();

};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;

class AgataDetectorShellMessenger: public G4UImessenger
{
  public:
    AgataDetectorShellMessenger(AgataDetectorShell*,G4String);
   ~AgataDetectorShellMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataDetectorShell*        myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithAString*        ShellMatCmd;
    G4UIcmdWithAString*        SetShellSizeCmd;
            
};

#endif
