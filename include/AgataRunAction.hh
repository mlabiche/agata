//////////////////////////////////////////////////////////////////////////////////////////
/// This class provides the implementation to the abstact class G4UserRunAction.
/// It also handles the output to the list-mode file.
/// The name for the output file is GammaEvents.XXXX, where XXXX is the current run number;
/// if the file size exceeds a given limit, the list-mode file is split and new files
/// are created (GammaEvents.XXXX.01, GammaEvents.XXXX.02, ...)
//////////////////////////////////////////////////////////////////////////////////////////

#ifndef AgataRunAction_h
#define AgataRunAction_h 1

#include <fstream>

#include "globals.hh"
#include "G4UserRunAction.hh"

/////////////////////////////////////////
/// The format version is defined here
/////////////////////////////////////////
#define  FORMAT_VERSION "7.3.0"

using namespace std;

class G4Run;
class AgataDetectorConstruction;
class AgataGeneratorAction;
class AgataEventAction;
class AgataAnalysis;
class AgataRunActionMessenger;

class AgataRunAction : public G4UserRunAction
{
  public:
    AgataRunAction(AgataAnalysis*, G4bool, G4String);
   ~AgataRunAction();
    
  private:
    std::ofstream outFileLMD;  //> List-mode output file
  std::ofstream outFileLMD_gammas; //> List-mode output file gamma int pos
  /////////////////////////////////////////////////////
  /// Pointers to other classes
  //////////////////////////////////////////////////////
  private:
    AgataAnalysis             *theAnalysis;
    AgataDetectorConstruction *theDetector;
    AgataEventAction          *theEvent;
    AgataGeneratorAction      *theGenerator;
    
  private:
    AgataRunActionMessenger   *myMessenger;  //> Messenger class
    
  private:
    G4int    runNumber;                        //> Current run number
    G4int    fileNumber;                       //> Current file number (within a given run)

  private:
    G4bool   writeLMD;                         //> true: output to the list-mode file is enabled
  G4bool   writeLMD_gammas;                         //> true: output to gamma hits to the list-mode file is enabled
  G4bool writeTOFIFO;
    G4String outFileName;                      //> Name of the list-mode file
  G4String outFileName_gammas;                      //> Name of the list-mode file
  G4bool firstLMDfile;                         //Keeps track of if this first
  //call to openLMDFile, this is related to resume option
  private:
    G4String workPath;                         //> Directory where the list-mode file is written

  private:
    G4int    killedEvents;
    
  private:
    G4long  maxFileSize;                        //> Maximum size of the list-mode file
    
  private:
    G4bool volume;                             //> True: volume and solid angle calculation
                                               //> have been enabled in the geometry part
    
  private:
    G4int  verboseLevel;                       //> Level of geometrical information contained in the list-mode file
                                               //>    (only for the case of the AGATA geometry). At a given level,
					       //>    the information is added to the information from the previous
					       //>    levels:
                                               //> 0 --> only the summary is written out
					       //> 1 --> the files used to generate the geometry are copied to the output
					       //> 2 --> the positions (and rotations) of the clusters, crystals and segments
					       //>       are written out
					       //> 3 --> the transformations to place the individual crystals are written out

    
  private:
    void  WriteHeader ();                      //> Writes the header of the LM file (calling other classes to obtain the relevant information)
    G4int OpenLMFile  ();                      //> Opens the LM file 

  ////////////////////////////////////////////////////
  /// These methods are needed by G4UserRunAction
  ////////////////////////////////////////////////////
  public:
    void BeginOfRunAction (const G4Run*);
    void EndOfRunAction   (const G4Run*);

  public:
    void EnableWrite      ( G4bool );      //> Enables/disables the output to the LM file
  void EnableWriteGammas      ( G4bool );      //> Enables/disables the output to the Gammas LM file
  void WriteToFifo      (G4bool);          //> Writes to fifo, not file
    void ShowStatus       ();              //> Prints out the present status to screen 
    void SetRunNumber     ( G4int );       //> Sets the current run number to a given value
    void SetVerboseLevel  ( G4int );       //> Sets the level of verbosity
    void SetMaxFileSize   ( G4long );       //> Sets the maximum size of the LM file
    void SplitLMFile      ();              //> Splits the LM file (closes the old one and opens a new one)
    void SetWorkingPath   ( G4String );    //> Chooses the directory where the LM file is written
    
  /////////////////////////////////////////////////
  /// These methods send strings to the LM file
  ////////////////////////////////////////////////
  public:
    inline void  LMwrite(const G4String &theString) { outFileLMD << theString;   };
    inline void  LMwrite(const char     *theChars)  { outFileLMD << theChars;    };
  inline void  LMwrite_gammas(const G4String &theString) { if(writeLMD_gammas) outFileLMD_gammas << theString;   };
  inline void  LMwrite_gammas(const char     *theChars)  { if(writeLMD_gammas) outFileLMD_gammas << theChars;    };
    
  public:
    inline G4bool DoWrite()                         { return writeLMD;           };
    inline G4int  GetRunNumber()                    { return runNumber;          };
    inline G4int  GetVerbose()                      { return verboseLevel;       };
    
  public:
    inline G4long  GetMaxFileSize()                  { return maxFileSize;        };  
    inline G4long  GetLMFileSize()                   { return outFileLMD.tellp(); };
    //////////////////////////////////////////////////////////////////////
    /// NB: outFileLMD.seekp(0, ios::end) --> goes to the end of the file
    //////////////////////////////////////////////////////////////////////
    
  public:
    inline G4String GetWorkPath ()                  { return workPath;           };   
  ///////////////////////////////////////////////////////////////////////////////////
  /// This method is needed to access the analysis class from the other classes.
  /// Since there is no G4UserAnalysis prototype, it is not possible to access it
  /// via G4RunManager as for the other UserActions
  ///////////////////////////////////////////////////////////////////////////////////
  public:
    inline AgataAnalysis* GetTheAnalysis()          { return theAnalysis;        };
    
  public:
    inline void IncrementErrors ()                  { killedEvents++; return;    };
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithABool;
class G4UIcmdWithAString;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAnInteger;

class AgataRunActionMessenger: public G4UImessenger
{
  public:
    AgataRunActionMessenger(AgataRunAction*, G4String);
   ~AgataRunActionMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataRunAction*            myTarget;
    G4UIdirectory*             myDirectory;
    G4UIdirectory*             fileDirectory;
    G4UIcmdWithABool*          EnableWriteCmd;
    G4UIcmdWithABool*          DisableWriteCmd;
  G4UIcmdWithABool*          EnableGammaWriteCmd;
  G4UIcmdWithABool*          DisableGammaWriteCmd;
  G4UIcmdWithABool*          WriteToFifoCmd;
    G4UIcmdWithoutParameter*   StatusCmd;
    G4UIcmdWithAString*        WorkPathCmd;
    G4UIcmdWithAnInteger*      RunNumberCmd;
    G4UIcmdWithAnInteger*      VerboseLevelCmd;
    G4UIcmdWithAnInteger*      SetFileSizeCmd;
};

#endif





