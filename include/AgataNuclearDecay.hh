#ifndef _AgataNuclearDecay_
#define _AgataNuclearDecay_

#include <vector>
#include <map>
#include <sstream>

#ifdef G4V10
#include "G4SystemOfUnits.hh"
#else
#include <CLHEP/Units/SystemOfUnits.h>
#endif

#include "G4ios.hh"
#include "globals.hh"
#include "G4VRestDiscreteProcess.hh"
#include "G4ParticleChangeForRadDecay.hh"
#include "G4RadioactiveDecaymessenger.hh"  

#include "G4NucleusLimits.hh"

#ifndef G4V10_5
#include "G4RadioactiveDecayRate.hh"
#include "G4RadioactiveDecayRateVector.hh"
#else
#include "G4RadioactiveDecayRatesToDaughter.hh"
#include "G4RadioactiveDecayChainsFromParent.hh"
#endif
#include "G4Ions.hh"

#ifndef G4V10_3
#include "G4RIsotopeTable.hh"
#endif


#include "G4RadioactivityTable.hh"
#include "G4ThreeVector.hh"


class AgataGamma 

{
public:
  AgataGamma(G4double Ei, G4double Egamma, G4double prob, G4int dJ, 
	     G4double tau, G4int Ji, G4double tot_ic, G4double icK, G4double icL1, 
	     G4double icL2, G4double icL3, G4double icM1, G4double icM2, G4double icM3, 
	     G4double icM4, G4double icM5, G4double icOS, 
	     G4double Mul_mixing) {
    fEi=Ei; fEgamma=Egamma; fprob=prob; dJ=dJ;ftau=tau;fJi=Ji;ftot_ic=tot_ic;ficK=icK;
    ficL1=icL1;ficL2=icL2;ficL3=icL3;ficM1=icM1;ficM2=icM2;ficM3=icM3;ficM4=icM4;ficM5=icM5;
    ficOS=icOS;fMul_mixing=Mul_mixing;
  }
  ~AgataGamma(){;}
  G4double fEi;
  G4double fEgamma;
  G4double fprob;
  G4int fdJ;
  G4double ftau;
  G4int fJi;
  G4double ftot_ic;
  G4double ficK;
  G4double ficL1;
  G4double ficL2;
  G4double ficL3;
  G4double ficM1;
  G4double ficM2;
  G4double ficM3;
  G4double ficM4;
  G4double ficM5;
  G4double ficOS;
  G4double fMul_mixing;
};

class AgataNuclearDecay : public G4VRestDiscreteProcess 

{
public:
  AgataNuclearDecay(const G4String& processName="AgataNuclearDecay");
  ~AgataNuclearDecay();
  void SetGamma(G4int A, G4int Z, G4double Ei, G4double Egamma, G4double prob, G4int dJ, 
		G4double tau, G4int Ji, G4double tot_ic, G4double icK, G4double icL1, 
		G4double icL2, G4double icL3, G4double icM1, G4double icM2, G4double icM3, 
		G4double icM4, G4double icM5, G4double icOS, 
		G4double Mmixing);
protected:
  virtual G4double GetMeanFreePath(const G4Track& aTrack,G4double previousStepSize,
				   G4ForceCondition* condition);
  //  Calculates from the macroscopic cross section a mean
  //  free path, the value is returned in units of distance.

  virtual G4double GetMeanLifeTime(const G4Track& aTrack,G4ForceCondition* condition);
  //  Calculates the mean life-time (i.e. for decays) of the
  //  particle at rest due to the occurence of the given process,
  //  or converts the probability of interaction (i.e. for
  //  annihilation) into the life-time of the particle for the
  //  occurence of the given process.
private:
  G4double AtRestGetPhysicalInteractionLength(const G4Track& track,G4ForceCondition* condition);
  G4VParticleChange* AtRestDoIt(const G4Track& theTrack,const G4Step& theStep);
  G4VParticleChange* PostStepDoIt(const G4Track& theTrack,const G4Step& theStep);
  bool LoadGammaDecays(G4int Z,G4int A);
  std::map<G4int,std::vector<AgataGamma> > fGammas;
  G4String dirName;
};
#endif
