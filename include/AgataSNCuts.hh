


#ifndef AgataSNCuts_h
#define AgataSNCuts_h 1

#include "G4ios.hh"
#include "globals.hh"
#include "G4VProcess.hh"
#include "AgataDetectorConstruction.hh"
#include "G4RunManager.hh"

class AgataSNCutsMessenger;



class AgataSNCuts : public G4VProcess 
{
  public:     

     AgataSNCuts(const G4String& processName ="AgataSNCut" );

     virtual ~AgataSNCuts();

     virtual G4double PostStepGetPhysicalInteractionLength(
                             const G4Track& track,
			     G4double   previousStepSize,
			     G4ForceCondition* condition
			    );

     virtual G4VParticleChange* PostStepDoIt(
			     const G4Track& ,
			     const G4Step& 
			    );
			    
     //  no operation in  AtRestGPIL
     virtual G4double AtRestGetPhysicalInteractionLength(
                             const G4Track& ,
			     G4ForceCondition* 
			    ){ return -1.0; };
			    
     //  no operation in  AtRestDoIt      
     virtual G4VParticleChange* AtRestDoIt(
			     const G4Track& ,
			     const G4Step&
			    ){return NULL;};

     //  no operation in  AlongStepGPIL
     virtual G4double AlongStepGetPhysicalInteractionLength(
                             const G4Track&,
			     G4double  ,
			     G4double  ,
			     G4double& ,
                             G4GPILSelection*
			    ){ return -1.0; };

     //  no operation in  AlongStepDoIt
     virtual G4VParticleChange* AlongStepDoIt(
			     const G4Track& ,
			     const G4Step& 
			    ) {return NULL;};

  void SetEnergyCutCreatedScreenedElastic(G4double val){fEnergyLimitCSN=val;}
  void SetEnergyCutHeavyIon(G4double val){fEnergyLimitHI=val;}
  void SetEnergyCutHeavyIonSensitive(G4double val){fEnergyLimitHIS=val;}
  void SetKillGammasBefore(bool before){
    KillBeforeDeg=before;
    G4cout << "Set KillBeforeDeg to " << KillBeforeDeg << "\n";
  }
  void SetKillGammasAfter(bool after){
    KillAfterDeg=after;
    G4cout << "Set KillAfterDeg to " << KillAfterDeg << "\n";
  }
private:
  
  // hide assignment operator as private 
     AgataSNCuts& operator=(const AgataSNCuts&){return *this;};
  static G4double fEnergyLimitCSN,fEnergyLimitHI,fEnergyLimitHIS;
  static AgataSNCutsMessenger*    myMessenger;
  AgataDetectorConstruction* theDetector;
  static bool KillBeforeDeg,KillAfterDeg;
  std::ofstream scorefile;
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADoubleAndUnit;
class G4UIcmdWithABool;

class AgataSNCutsMessenger: public G4UImessenger
{
  public:
    AgataSNCutsMessenger(AgataSNCuts*, G4String);
   ~AgataSNCutsMessenger();
    
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
  AgataSNCuts*     myCuts;
  G4UIdirectory*         myDirectory;
  G4UIcmdWithADoubleAndUnit *cmdSetCSN;
  G4UIcmdWithADoubleAndUnit *cmdSetHI;
  G4UIcmdWithADoubleAndUnit *cmdSetHIS;
  G4UIcmdWithABool *cmdkillbeforedegrader;
  G4UIcmdWithABool *cmdkillafterdegrader;

};


#endif



