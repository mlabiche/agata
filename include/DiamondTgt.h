/***************************************************************************
                          DiamondTgt.h  -  description
                             -------------------
    begin                : Thu Jul 13 2006
    copyright            : (C) 2006 by Mike Taylor
    email                : mjt502@york.ac.uk
 ***************************************************************************/

#ifndef DiamondTgt_h
#define DiamondTgt_h 1

#if defined G4V10
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#endif

#include "G4ThreeVector.hh"

class G4LogicalVolume;
class G4VPhysicalVolume;
class G4PVPlacement;
class G4Material;

class DiamondTgt
{
  public:

    DiamondTgt();
    virtual ~DiamondTgt();

    void createDetector(G4VPhysicalVolume *, G4Material *);

   G4PVPlacement *Diamond_phys_cpy[10];
   G4ThreeVector placement[10];
   G4LogicalVolume *DiamondTgt_log;

  private:

//    void defineMaterial();
//    void setAsSensitiveDetector();
    
    G4Material *detectorMaterial;
    G4VPhysicalVolume *DiamondTgt_phys;

};                                          

#endif
