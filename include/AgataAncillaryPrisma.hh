#ifdef ANCIL
#ifndef AgataAncillaryPrisma_h
#define AgataAncillaryPrisma_h 1


#include "globals.hh"

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"

#include "PrismaField.hh"
#include "G4EqMagElectricField.hh"
#include "G4PropagatorInField.hh"
#include "G4TransportationManager.hh"
#include "G4ChordFinder.hh"
#include "G4ClassicalRK4.hh"

using namespace std;

class G4Material;
class AgataSensitiveDetector;
class AgataAncillaryPrismaMessenger;

class AgataAncillaryPrisma : public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryPrisma(G4String, G4String);
    ~AgataAncillaryPrisma();

  private:
    AgataAncillaryPrismaMessenger* myMessenger;

  /////////////////////////////
  /// Material and its name
  ////////////////////////////
  private:
    G4String     nam_vacuum;
    G4Material  *mat_vacuum;
    
    G4String     nam_aluminium;
    G4Material  *mat_aluminium;

    // static G4ThreadLocal EMField * fField;

  ///////////////////////////////////////////
  /// Methods required by AncillaryScheme
  /////////////////////////////////////////// 
  public:
    G4int  FindMaterials           ();
    void   GetDetectorConstruction ();
    void   InitSensitiveDetector   ();
    void   Placement               ();

  public:
    void   WriteHeader             (std::ofstream &outFileLMD, G4double = 1.*mm);
    void   ShowStatus              ();

  public:
    inline G4int GetSegmentNumber  (G4int, G4int, G4ThreeVector)  {return 0;};
    inline G4int GetCrystalType    (G4int)                        {return -1;};

   //G4FieldManager* fFieldMgr;

};

#endif

#endif
