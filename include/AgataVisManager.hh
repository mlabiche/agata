////////////////////////////////////////////////////////////////
/// This class handles the visualization. It is fully copied
/// from one of the novice examples.
////////////////////////////////////////////////////////////////
#ifndef AgataVisManager_h
#define AgataVisManager_h 1

#if defined(G4VIS_USE_OI) || defined(G4VIS_USE_OIQT) || defined(G4VIS_USE_OPENGL) || defined(G4VIS_USE_OPENGLQT) || defined(G4VIS_USE_OPENGLX) || defined(G4VIS_USE_OPENGLXM) || defined(G4VIS_USE_RAYTRACERX) || defined(G4VIS_USE)

#include "G4VisManager.hh"

class AgataVisManager: public G4VisManager {

  public:
    AgataVisManager ();

  private:
    void RegisterGraphicsSystems ();

};

#endif

#endif
