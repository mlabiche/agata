////////////////////////////////////////////////////////
/// This class handles the Galileo array 
////////////////////////////////////////////////////////

#ifndef GalileoDetector_h
#define GalileoDetector_h 1

#include "AgataDetectorConstruction.hh"
#include "AgataDetectorArray.hh"
#include "AgataAncillaryCluster.hh"
#include "AgataSensitiveDetector.hh"
#include "AgataDetectorConstruction.hh"
#include "AgataDetectorAncillary.hh"
#include "G4GDMLParser.hh"

#include "globals.hh"
#include <fstream>
#include <iostream>
#include "G4ThreeVector.hh"
#include <set>

using namespace std;

class G4Material;
class GalileoDetectorMessenger;
class AgataDetectorAncillary;
class AgataAncillaryCluster;
class AgataDetectorArray;

struct PlacementOfAGASP {
  G4double x,y,z,phi,theta,psi;
  G4int id;
};

class GalileoDetector : protected AgataDetectorConstructed/*, public AgataAncillaryScheme*/
{
public:
  GalileoDetector(G4String,G4String, G4String);
  ~GalileoDetector();

  void  Placement       ();
  void  WriteHeader      ( std::ofstream &outFileLMD, G4double = 1.*mm );
  void  ShowStatus       ()                                              {;}
  G4int GetSegmentNumber ( G4int offset, G4int nGe, G4ThreeVector position );
  inline G4int GetCrystalType   ( G4int detNum )  {
    return -1*(detNum+1)/(detNum+1); };
  void  AddGalileoDetectors(G4String detectors);
  void  SetMakeLeadCollimator(G4bool val) {fMakeLeadCollimator=val;
   G4cout << "-----> Set Make Lead Collimator to : " << fMakeLeadCollimator << G4endl;}
  void SetMakeChamber(G4bool val){fMakeChamber=val;
   G4cout<< "-----> Set Make Chamber to : " << fMakeChamber << G4endl;}
  G4String GetAnc(){return ancType;}
  std::map<int,G4ThreeVector> GetAllDetAngles(){return fAllDetAngles;}
  void SetArrayRadius (G4double val) {
//     if(val<0 && (192.5+val<150.9)){
//      val=0.*mm;
//      G4cout << " -----> Warning collision between lead collimator and detector fronting!" << G4endl;
//      G4cout << " ---------> We will keep the default Radius (192.5 mm)" << G4endl;            
//     }
     fUserDefinedRadius=val;}
private:
  AgataDetectorAncillary* theAncillary;
  G4String                directoryName;  //> for the command line  
  G4String                iniPath;     //> directory where the files are located
  G4String                ancType;
  G4String                gdml_path;


  // Detectors Volume and construction
  
  
  G4LogicalVolume *GetGalileoFronting();
  G4LogicalVolume *GetGalileoACCapsule();
  G4LogicalVolume *GetGalileoACCrystal();
  G4LogicalVolume *GetGalileoGeCapsule();
  G4LogicalVolume *GetGalileoGeCrystal();
  G4bool GetGalileoTripleClusterGe();
  G4bool GetGalileoTripleClusterAC();
  G4bool GetGalileoTripleClusterGeHousing();
  G4bool GetGalileoTripleClusterACHousing();
  //G4LogicalVolume *GetGalileoGeTripleCapsule();
  void GetGalileoTripleCluster();
  AgataDetectorArray *galileoTC;
  AgataDetectorConstructed* theConstructed;

  G4LogicalVolume *pGalileoFronting;
  G4LogicalVolume *pGalileoACCapsule;
  G4LogicalVolume *pGalileoACCrystal;
  G4LogicalVolume *pGalileoGeCapsule;
  G4LogicalVolume *pGalileoGeCrystal;



  std::map<int,G4LogicalVolume *> pGalileoTripleClusterGe;
  std::map<int,G4LogicalVolume *> pGalileoTripleClusterAC;
  std::map<int,G4LogicalVolume *> pGalileoTripleClusterGeHousing;
  std::map<int,G4LogicalVolume *> pGalileoTripleClusterACHousing;
  G4GDMLParser gdmlparser;

  
  G4bool      fTripleCluster;
  G4String    matFrontingName;
  G4String    matCapsuleName;
  G4String    matACCrystalName;
  G4String    matGeCrystalName;

  G4Material  *matFronting;
  G4Material  *matCapsule;
  G4Material  *matGeCrystal;
  G4Material  *matACCrystal;
  G4Material  *matVacuum;
  
  // Lead collimator attached to the structure
  G4String    matLeadCollimatorName;
  G4Material  *matLeadCollimator; 
   
  G4LogicalVolume *pGalileoCollimator;
  G4LogicalVolume *pGalileoTCCollimator;
  G4LogicalVolume *pGalileoCollimatorBackward;
  G4LogicalVolume *pGalileoCollimatorCentral;
  G4LogicalVolume *pGalileoCollimatorForward1;
  G4LogicalVolume *pGalileoCollimatorForward2;
  
  G4LogicalVolume *GetGalileoCollimator();
  G4LogicalVolume *GetGalileoTCCollimator();
  G4LogicalVolume *GetGalileoCollimatorHL(G4int j,bool tc);
  
  AgataSensitiveDetector    *bgoSD;
  GalileoDetectorMessenger  *myMessenger;
  G4int FindMaterials  ();
        

  // Galileo related

  G4double fUserDefinedRadius ; 
  std::set<int> fUsedGalileoDetectors;
  std::vector<PlacementOfAGASP> fExtraPhaseIs;
  //To keep track of detectorangles for header
  std::map<int,G4ThreeVector> fDetAngles;
  std::map<int,int> fDetType;
  std::map<int,G4ThreeVector> fAllDetAngles;
  std::set<int> fDontMakeThisFrame;
  G4bool fMakeChamber;
  G4bool fMakeLeadCollimator;
};



#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWithoutParameter;
class G4UIcmdWithABool;
class G4UIcmdWithADoubleAndUnit;

class GalileoDetectorMessenger: public G4UImessenger
{
public:
  GalileoDetectorMessenger(GalileoDetector*);
  ~GalileoDetectorMessenger();
    
  void SetNewValue(G4UIcommand*, G4String);
    
private:
  GalileoDetector*       myTarget;
    
  G4UIdirectory*             myDirectory;
  G4UIcmdWithAString*        SetUsedGePositionCmd;
  G4UIcmdWithABool*          SetMakeLeadCollimatorCmd;
  G4UIcmdWithABool*          SetMakeChamberCmd;
  G4UIcmdWithADoubleAndUnit* SetArrayRadiusCmd;
}; 

#endif
