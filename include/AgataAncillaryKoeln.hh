///////////////////////////////////////////////////////////////////////////
/// This class describes the S2 detector from Micron, used during the
/// first in-beam test of the AGATA triple cluster
///////////////////////////////////////////////////////////////////////////
#ifndef AgataAncillaryKoeln_h
#define AgataAncillaryKoeln_h 1

#include "globals.hh"
#include "G4ThreeVector.hh"
#include "AgataDetectorAncillary.hh"

using namespace std;

class G4Material;
class AgataAncillaryKoelnMessenger;
class AgataSensitiveDetector;

class AgataAncillaryKoeln :  public AgataAncillaryScheme
{
  
  public:
    AgataAncillaryKoeln(G4String,G4String);
    ~AgataAncillaryKoeln();

  private:
    AgataAncillaryKoelnMessenger* myMessenger;

  private:
    G4String iniPath;
    
  ////////////////////////////
  /// annular detector size
  ////////////////////////////
  private:
    G4double innRad;
    G4double outRad;
    G4double passTh;
    G4double thickn;

  //////////////////////
  /// absorber size
  /////////////////////
  private:
    G4double innRaA;
    G4double outRaA;
    G4double thickA;

  ///////////////////
  /// target size
  ///////////////////
  private:
    G4ThreeVector sizeT;
  
  /////////////////////
  /// segmentation
  ///////////////////
  private:
    G4int nSect;
    G4int nRing;

  private:
    G4double dPhi;
    G4double dRad;
    
  /////////////////
  /// positions
  ////////////////
  private:
    G4ThreeVector positionD;  
    G4ThreeVector positionA;  
    G4ThreeVector positionT;
    
  /////////////////////
  /// target rotation
  ////////////////////
  private:
    G4double psi;
    G4double theta;
    G4double phi;    
  
  /////////////////////////
  /// materials and names
  //////////////////////////
  private:
    G4String     matDNam;
    G4Material  *matAnnul;
    G4String     matANam;
    G4Material  *matAbso;
    G4String     matTNam;
    G4Material  *matTarg;

  private:
    G4bool       writeSegments;  //> true: writes out angles of the segments in GSORT format
    G4String     numbeFile;      //> file where angles of the segments in GSORT format are written out

  private:
    void PlaceDetector ();
    void PlaceAbsorber ();
    void PlaceTarget   ();

  public:
    G4int FindMaterials();
    void GetDetectorConstruction();
    void InitSensitiveDetector();
    void Placement();

  public:
    void WriteHeader(std::ofstream &outFileLMD, G4double=1.*mm);
    void ShowStatus();

  public:
    G4int GetSegmentNumber ( G4int, G4int, G4ThreeVector );

  public:
    inline G4int GetCrystalType ( G4int ) { return -1; };

//  public:
//    inline G4String GetName () { return G4String("KOELN"); };
    
  public:
    void SetNSect         ( G4int );
    void SetNRing         ( G4int );
    void SetAnnulRmin     ( G4double ); 
    void SetAnnulRmax     ( G4double ); 
    void SetPassTh        ( G4double );
    void SetThickn        ( G4double );
    void SetPositionD     ( G4ThreeVector ); 
    
  public:
    void SetAbsoMate      ( G4String ); 
    void SetAbsoRmin      ( G4double ); 
    void SetAbsoRmax      ( G4double ); 
    void SetThickA        ( G4double );
    void SetPositionA     ( G4ThreeVector ); 
    
  public:
    void SetTargMate      ( G4String ); 
    void SetTargXSize     ( G4double ); 
    void SetTargYSize     ( G4double ); 
    void SetThickT        ( G4double );
    void SetRotationT     ( G4ThreeVector ); 
    void SetPositionT     ( G4ThreeVector ); 
    
  public:
    void SetWriteSegments ( G4bool );
};

#include "G4UImessenger.hh"

class G4UIdirectory;
class G4UIcmdWithAString;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;

class AgataAncillaryKoelnMessenger: public G4UImessenger
{
  public:
    AgataAncillaryKoelnMessenger(AgataAncillaryKoeln*,G4String);
   ~AgataAncillaryKoelnMessenger();
    
  public:
    void SetNewValue(G4UIcommand*, G4String);
    
  private:
    AgataAncillaryKoeln*       myTarget;
    G4UIdirectory*             myDirectory;
    G4UIcmdWithAString*        SetAnnulSizeCmd;
    G4UIcmdWithAString*        SetSegmentsCmd;
    G4UIcmdWithADouble*        SetThicknCmd;
    G4UIcmdWithADouble*        SetPassThCmd;
    G4UIcmdWith3Vector*        SetPositionDCmd;
    G4UIcmdWithAString*        SetAbsoMateCmd;
    G4UIcmdWithAString*        SetAbsoSizeCmd;
    G4UIcmdWithADouble*        SetThickACmd;
    G4UIcmdWith3Vector*        SetPositionACmd;
    G4UIcmdWithAString*        SetTargMateCmd;
    G4UIcmdWithAString*        SetTargSizeCmd;
    G4UIcmdWithADouble*        SetThickTCmd;
    G4UIcmdWith3Vector*        SetRotationTCmd;
    G4UIcmdWith3Vector*        SetPositionTCmd;
    G4UIcmdWithABool*          EnableSegmentsCmd;
    G4UIcmdWithABool*          DisableSegmentsCmd;            
            
};

#endif
