#ifndef AgataDetectorAncillary_h
#define AgataDetectorAncillary_h 1

#include "AgataDetectorConstruction.hh"
#include "G4RunManager.hh"
#include "G4ThreeVector.hh"

//////////////////////////////////////////////////////////////////////////////
/// This class provides the interface to define specific ancillary detectors
//////////////////////////////////////////////////////////////////////////////
class AgataAncillaryScheme : public AgataDetectorConstructed
{
  public:
    AgataAncillaryScheme(G4String) {};
    AgataAncillaryScheme()         {};
    ~AgataAncillaryScheme()        {};

  ///////////////////////////////////////////////////
  /// pointer to the Sensitive Detector class
  /// for the ancillary detectors
  ///////////////////////////////////////////////////
  protected:
    AgataSensitiveDetector*     ancSD;

  ///////////////////////////////////////////////////
  /// pointer to the user Detector Construction class
  /// needed to retrieve materials and place volumes
  ///////////////////////////////////////////////////
  protected:
    AgataDetectorConstruction*  theDetector;

  ///////////////////////////////////////////////////
  /// Number of instances of AgataSensitiveDetector
  /// effectively used for each ancillary
  //////////////////////////////////////////////////
  protected:
    G4int                       numAncSd;

  //////////////////////////////////////////////////
  /// Name of the ancillary detector
  //////////////////////////////////////////////////
  protected:
    G4String                    ancName;

  //////////////////////////////////////////////////
  /// Name of the command directory
  //////////////////////////////////////////////////
  protected:
    G4String                    dirName;

  //////////////////////////////////////////////////
  /// Offset for the ancillary detector SD
  //////////////////////////////////////////////////
  protected:
    G4int                       ancOffset;

  //////////////////////////////////////////////////
  /// Retrievals
  //////////////////////////////////////////////////
  public:
    inline G4int      GetNumAncSd () { return numAncSd;  };
    inline G4int      GetAncOffset() { return ancOffset; };
    inline G4String   GetAncName  () { return ancName;   };
  
  ///////////////////////////////////////////////////
  /// Pure virtual methods which should be implemented
  /// by the user
  ////////////////////////////////////////////////////
  public:
    virtual G4int    FindMaterials	     () = 0;
    virtual void     GetDetectorConstruction () = 0;
    virtual void     InitSensitiveDetector   () = 0;
    //virtual G4String GetName                 () = 0;

};

class AgataAncillaryADCA;

#ifdef GASP
class AgataAncillaryEuclides;
#endif
#ifdef CLARA
class AgataAncillaryMcp;
#endif
#ifdef EUCLIDES
class AgataAncillaryEuclides;
#endif
class AgataDetectorAncillaryMessenger;
//////////////////////////////////////////////////////////////////////////////
/// This class handles the placement of ancillary detectors within the
/// Agata geometry
//////////////////////////////////////////////////////////////////////////////
class AgataDetectorAncillary
{
  public:
    //////////////////////////////////////////////////////
    /// Agata actually uses only the second constructor
    /// The first one is left for compatibility with
    /// other programs (Gasp, Clara, Polar)
    /////////////////////////////////////////////////////
    AgataDetectorAncillary(G4int,    G4String, G4String);
    AgataDetectorAncillary(G4String, G4String, G4String);
    ~AgataDetectorAncillary();

  ////////////////////////////////////////////////////////////
  ///// The Placement() method calls (in the proper sequence)
  ///// the methods of the ancillary detector class which has
  ///// been instantiated in the constructor
  /////////////////////////////////////////////////////////////
  public:
    void Placement();
  static void AddAdditionalStrucutre(std::string libname);
  private:
    //////////////////////////////////////////////////////////
    /// numAnc is the number of ancillary detector objects
    /// instantiated here
    /////////////////////////////////////////////////////////
    G4int numAnc;

  private:
    ///////////////////////////////////////////////////////////
    /// ancLut tells which of the ancillaries uses a specific
    /// offset for AgataSensitiveDetector
    ///////////////////////////////////////////////////////////
    std::vector<G4int>  ancLut;
    G4int               minOffset;

  //////////////////////////////////////////////////////////////
  /// The pointer to the ancillary detector class which has
  /// been instantiated is cast to these pointers.
  //////////////////////////////////////////////////////////////
  protected:
  std::vector<AgataAncillaryScheme*>      theAncillary;
  std::vector<AgataDetectorConstructed*>  theConstructed;
  std::vector<G4int>                      whichAnc;

  AgataAncillaryADCA*          theADCA; // the Agata Double Cluster Array (GSI)

#ifdef GASP
    AgataAncillaryEuclides*    theEuclides;
#endif

#ifdef CLARA
    AgataAncillaryMcp*         theMcp;
#endif

#ifdef EUCLIDES
    AgataAncillaryEuclides*    theEuclides;
#endif

public:

  ////////
  /// list of add on libs
  ///////
  static std::vector<void*> AddOns;


  //////////////////////////////////////////////////////////////////////////
  /// Prepares a lookup table linking the offset of AgataSensitiveDetector
  /// and the ancillary detector used
  /////////////////////////////////////////////////////////////////////////
  private:
    void FillAncLut();
       
  public:
    ///////////////////////////////////////////////////////////
    /// Prints out useful information (on screen)
    //////////////////////////////////////////////////////////
    void ShowStatus   ();
    //////////////////////////////////////////////////////////
    /// Writes useful information to the output file
    /////////////////////////////////////////////////////////
    void WriteHeader  ( std::ofstream &outFileLMD, G4double=1.*mm );
    
  public:
    ////////////////////////////////////////////////////////
    /// Calculates the segment number from offset, detector
    /// number and position (relative to the detector)
    ///////////////////////////////////////////////////////
    G4int GetSegmentNumber( G4int, G4int, G4ThreeVector );

  /////////////////////////////////////////////////////////////
  /// Methods to access the members of theConstructed object
  ////////////////////////////////////////////////////////////
  public:
    G4int  GetNumberOfDetectors ();
    G4int  GetMaxDetectorIndex  ();
    G4bool GetReadOut	        ();

#ifdef GASP
  public:
    void SetGeometry      ( G4int );
    void ResetNumberOfSi  ();
    void ResetMaxSiIndex  ();
#endif
};

#endif
