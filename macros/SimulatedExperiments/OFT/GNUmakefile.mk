PROCS_NUMBER=$(shell grep processor /proc/cpuinfo | wc -l)

AGATA_INTERFACE_FLAG=no

CC=gcc
CPP=g++
AR=ar
FLAGS=-pedantic -Wall

#COMPILE_PATH=/misc/agatasimschool2013/OFT
COMPILE_PATH=/home/local1/geant4/agata/branches/GANIL/trunk/macros/SimulatedExperiments/OFT
OFT_LIB_PATH=$(COMPILE_PATH)/OFT

.SUFFIXES: .c .o .cpp

.c.o:
	$(CC) $(CFLAGS) $(DFLAGS) $(INCFLAGS) -o $@ -c $< -static

.cpp.o:
	$(CPP) $(CFLAGS) $(DFLAGS) $(INCFLAGS) -o $@ -c $<
