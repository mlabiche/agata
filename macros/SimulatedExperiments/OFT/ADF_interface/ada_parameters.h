#ifndef _PARAMETERS_H_
#define _PARAMETERS_H_

#ifdef __cplusplus
extern "C" {
#endif
  // 
  // item format is "parameter_name;param_type;mode;default_value"
  extern void ada_new_parameter(char *item,
                                int *error_code);
  // paramters are given as char array
  extern void ada_set_parameter(char *parameter_name,
                                char *parameter_value,
                                int *error_code);
  // buffer should have sufficent length to store parameter
  extern void ada_get_parameter(char *parameter_name,
                                char *buffer,
                                int buffer_length,
                                int *error_code);
#ifdef __cplusplus
};
#endif

#endif //  _PARAMETERS_H_
