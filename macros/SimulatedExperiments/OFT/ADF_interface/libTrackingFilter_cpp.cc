#include "TrackingFilter.hh"
#include "TrackingFilterOFT.h"

/**
\mainpage PSA-actors for AGATA and NARVAL
The basic thought behind what is done for the preprocessing and PSA is that
for each new actor one should only have to reimplement 
\code
virtual Int_t PreprocessingFilter::InitPreprocess(){return 0;}
virtual Int_t PreprocessingFilter::ResetPreprocess(){return 0;}
virtual Int_t PreprocessingFilter::Process();
\endcode
for the preprocessing and
\code
virtual Int_t TrackingFilter::InitDataContainer(){return 0;}
virtual Int_t TrackingFilter::ResetDataContainer(){return 0;}
virtual Int_t TrackingFilter::Process();
\endcode
for the PSA.

For the preprocessing a very stupid example of a Process method could be
\include process_example.cpp


The Init methods are called last in the process_initialise method. For the
TrackingFilter there is a void pointer to hold any structure needed for pulse
shape-data bases etc. Check out TrackingFilterSimpleGrid and GridSearchData to see 
how this could work.
\code
void *fDataContainer; 
\endcode


In the PSA class the resulting positions and energies are passed on by a call
to
\code
void TrackingFilter::AddHit(ADF::PSAHit *ahit);
\endcode
on a appropriate position within the 
\code virtual Int_t TrackingFilter::Process(); \endcode method.
The hits list is reseted at each event in 
\code
void TrackingFilter::process_block 
\endcode
method. Check also out the Related Pages link.
*/

extern "C" {
  TrackingFilter *process_register (UInt_t *error_code)
  { 
    //if(TrackingFilter::fWhichTrackingFilter=="Basic") {
    //  std::cout << "\nTrackingFilter::fWhichTrackingFilter == \"BaseClass\"\n";
    //  return new TrackingFilter(); 
    //}
    if (TrackingFilter::fWhichTrackingFilter == "OFT") {
      std::cout << "\nTrackingFilter::fWhichTrackingFilter == \"OFT\"\n";
      return new TrackingFilterOFT();
    }
    else {
      std::cout << "\nTrackingFilter::fWhichTrackingFilter Undefined\n";
      return NULL;
    }
  }
  void process_config (const char *directory_path, UInt_t *error_code)
  {
    TrackingFilter::process_config (directory_path, error_code);
  }
  void process_block( TrackingFilter *algo_data,
                      void *input_buffer,  UInt_t size_of_input_buffer,
                      void *output_buffer, UInt_t size_of_output_buffer,
                      UInt_t *used_size_of_output_buffer,
                      UInt_t *error_code)
  {
    algo_data->process_block( input_buffer, size_of_input_buffer,
                              output_buffer, size_of_output_buffer,
                              used_size_of_output_buffer, error_code);
  }
  void process_initialise(TrackingFilter *algo_data, UInt_t *error_code)
  {
    algo_data->process_initialise(error_code);
    //printf("-------------------------------------------------------EXIT LIB TRACK INITIALISE\n");
  }
  void process_reset(TrackingFilter *algo_data,UInt_t *error_code)
  {
    algo_data->process_reset(error_code);
  }
  void process_start(TrackingFilter *algo_data, UInt_t *error_code)
  {
    algo_data->process_start(error_code);
  }
  void process_stop(TrackingFilter *algo_data,UInt_t *error_code)
  {
    algo_data->process_stop(error_code);
  }
  void process_pause(TrackingFilter *algo_data,UInt_t *error_code)
  {
    algo_data->process_pause(error_code);
  }
  void process_resume(TrackingFilter *algo_data, UInt_t *error_code)
  {
    algo_data->process_resume(error_code);
  }
};

