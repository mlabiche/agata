#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
//#include <unistd.h>



#include "tracking_define.h"


/* Diffusion compton */
double sig_compt(double E)
{

  /* sigma = 2 pi r_0^2 *[((1+gamma)/gamma^2 *(2(1+gamma)/1+2gamma - ln(1+2gamma)/gamma)) + ln(1+2gamma)/2gamma - 1+3gamma/(1+2gamma)^2] */
  /* fits geant data very well    */
  double temp;
  double temp0;
  double temp1;
  double temp2;
  double temp3;
  double gamma;
  gamma = E / mec2;

  temp0=1e4 * 2 * PI * SQ(r_0) * Z_ge;  /* in cm2/atom */
  temp1 = 1 + gamma;
  temp2 = 1 + (2 * gamma);
  temp3 = 1 + (3 * gamma);
  temp = (temp1 / SQ(gamma)) * (((2 * temp1) / temp2) - (log(temp2) / gamma));
  temp = temp + (log(temp2) / (2 * gamma) - (temp3 / SQ(temp2)));
  temp = temp * temp0;
  /*    printf("in:%20e    out:%20e\n", E, temp);*/
  return temp;


}



/* Effet photo electrique */
double sig_abs(double E)
{

  /* sigma = 4 alpha^4*sqrt(2)*Z^5 phi_0*(E/mec2)^(-7/2),phi_0=8/3 pi r_0^2 */
  /* sigma abs K+L = 9/8* sigma_Kshell */
  double temp;
  double gamma;
  double hnu_k;
  hnu_k = SQ(Z_ge - 0.03) * mec2 * SQ(fine_alpha) / 2;
  gamma = CB(E / mec2) * CB(E / mec2) * E / mec2;
  gamma = sqrt(gamma);
  temp = 4 * CB(fine_alpha) * fine_alpha * 1.4142 * 6.651e-25 * CB(Z_ge) * SQ(Z_ge);
  temp = sqrt(E / mec2) * temp / gamma;       /* en cm2/atom */
  /* not well suited for energies below 20 keV 
     removed the 1.125 factor and added sqrt(E/mec2) to fit data */
  if (E < 0.025) {
    temp = 2.2 * pow((hnu_k / E), 2.6666) * 6.3e-18 / SQ(Z_ge);
    if (E < 0.0111)
      temp = temp / 8.5;
  }
  return temp;

}



/* Creation de pair electron-positron
 * Energy > 1021 KeV
 */
double sig_pair(double E)
{

  /* fitted in 1e-24cm2 units for Ge from atomic data tables 1970 7 page 590 */
  double temp;

 temp = 0.792189 * log(E + 0.948261 - 1.1332 * E + 0.15567*SQ(E));

 if(E<1.022)temp = 0;
  
  if((E < 1.15) & (E >= 1.022))
    temp=(1-((1.15-E)/0.129))*7.55e-28;
  
  
  return temp*1e-24;

 
}

/* Calcul le parcours moyen a partir des sections efficaces */
double range_process(double sig)
{

  /* SIGMA MACRO = 1/lambda = 6.022e23 * rho_ge/A_ge * sigma (cm2/atom) */
  double temp;
  temp = (sig * N_av * rho_ge) / A_ge;
  temp = 1 / (temp);		/* in cm */
  return temp;

}


/* Probabilite de parcourir distance etant donne le parcours moyen range */
double proba(double range, double distance)
{
  double temp;
  double nlambda;
  nlambda = distance / range;
  temp = exp(-nlambda);
  return temp;
}

