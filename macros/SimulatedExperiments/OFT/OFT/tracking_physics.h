#ifndef PHYSICS_H
#define PHYSICS_H

double  sig_compt(double E);
double  sig_abs(double E);
double  sig_pair(double E);
double  range_process(double sig);
void    range_process_init();
double  proba(double range, double distance);

#endif
