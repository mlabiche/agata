#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
//#include <unistd.h>

void swap(double v[], int m, int l)
{
  register double temp;
  temp = v[m];
  v[m] = v[l];
  v[l] = temp;
} 
void swapi(int v[], int m, int l)
{
  register int temp;
  temp = v[m];
  v[m] = v[l];
  v[l] = temp;
}
