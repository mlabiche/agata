#ifndef DEFINE_H
#define DEFINE_H

#include <stdlib.h>
#include <stdio.h>

typedef struct my_struct {

  int nb_int;       
  int mult;         

  double *etot;
  double *xpos;
  double *ypos;
  double *zpos;
  double *probtot;
  double *xfirst;
  double *yfirst;
  double *zfirst;
  double *xsecond;
  double *ysecond;
  double *zsecond;

  double *source;
  double *vel;
  double  inner_r;
  double  outer_r;

  int    *seg;
  int    *det;
  double *e;
  double *et;
  int    *nbtot;

  double *r;            /*[intmax][intmax]*/
  double *r_ge;         /*[intmax][intmax]*/
  int    *sn;           /*[intmax][kmax]*/
  int    *interaction;  /*[intmax][kmax]*/ 
  double *angtheta;
  double *angphi;
  double *angn;    /*peut etre aussi*/

  int    *numn;
  int    *flagu;

} CntlStruct;

#define TRUE 1
#define FALSE 0

#define SQ(x) ((x)*(x))
#define CB(x) ((x)*(x)*(x))

/* useful definitions */

#define PI 3.1415927
#define voverc 0. /* velocity of source */
#define betafactor sqrt(1-SQ(voverc))

/* tracking parameters */

#define kmax 7  /* max number of interactions for a given incident gamma-ray
                 * Si >7, pbme de performance dans evaluation des clusters Compton */
#define min_opening_angle     0.15 /* minimum value of the cluster search angle */
#define step_in_opening_angle 0.10 /* step in angle for cluster serach */
#define eres          3e-3         /* energy resolution */
#define eresSQ        9e-6         /* SQ(energy resolution) */
#define photodistmax  4            /* min distance in cm between 2 points to process single int. point */
#define minprobtrack  0.05         /* threshold for track acceptance */
#define minprobsing   0.15         /* threshold for single int acceptance */
#define intmax        800          /* max of interaction points per event */

/* physical parameters */

#define rho_ge 5.32           /* g/cm3 */
#define Z_ge   32
#define A_ge   74
#define N_av   6.022e23
#define r_0    2.8179e-15
#define mec2   0.511          /* electron mass MeV */
#define fine_alpha 0.0072993  /* fine structure constant 1/137 */

/* geometrical parameters */

#define resolution 0.5        /* distance below which one cannot distinguish the presence of more than 1 interaction */
#define resolution_carre 0.25 /* its square */
#define uncertainty 0.1       /* uncertainty - sigma - in the determination of position */ 
#define threshold 0.005       /* energy threshold */
#define sigma_thet 0.25       /* position uncertainty in mm for costheta */ 

#endif
