#include <stdio.h>
#include <stdlib.h>
#include <fcntl.h>
#include <math.h>
#include <string.h>
#include <sys/types.h>
#include <sys/stat.h>
//#include <unistd.h>

#include "tracking_define.h"
#include "tracking_utilitaires.h"

/*
* Calcule la distance entre deux interractions
*/
void distances(double r[intmax*intmax],	/*Distance physique dans le detecteur */
               double r_ge[intmax*intmax],	  /*Distance efgfective dans le Ge */
               int number,
               double posx[], double posy[], double posz[],
               double radius, double radius_out,
               double source[3])
{
  int i, j, k, l;

  double ri, rj, Ax, Ay, Az, Bx, By, Bz;
  double radiusSQ;
  double costest, costest1, vectproduct, r_center, r_vacuum, r_vacuumi, ri_orig;
  double xsource, ysource, zsource;

  xsource = source[0];
  ysource = source[1];
  zsource = source[2];

  radiusSQ = SQ(radius);
  for (i = 0; i < number; i++) {
    r[i*intmax+i] = sqrt(SQ(posx[i] - xsource) + SQ(posy[i] - ysource) + SQ(posz[i] - zsource));

    ri_orig=sqrt(SQ(posx[i])+SQ(posy[i])+SQ(posz[i]));

    /* distance from geometrical point to i */

    Ax = -posx[i]; 
    Ay = -posy[i];
    Az = -posz[i];

    /* distance from  i to source */

    Bx=xsource-posx[i];
    By=ysource-posy[i];
    Bz=zsource-posz[i];

    vectproduct = sqrt(SQ((Ay*Bz)-(Az*By))+SQ((Az*Bx)-(Ax*Bz))+SQ((Ax*By)-(Ay*Bx)));

    /* distance from center to line joining points : rcenter = sintheta * distance center-i */

    r_center = vectproduct/r[i*intmax+i]; 

    r_ge[i*intmax+i]=r[i*intmax+i];

    if(r_center < radius) {
      r_vacuum=sqrt(SQ(radius)-SQ(r_center)); 

      /* distance from perpendicular intersection to i-source
      from origin to inner surface of shell */

      r_vacuumi=sqrt(SQ(ri_orig)-SQ(r_center)); 

      /* distance from perpendicular intersction to i-source
      from origin to i */

      r_ge[i*intmax+i]=r_vacuumi-r_vacuum;

    }	  

    if(r_ge[i*intmax+i] < 0)r_ge[i*intmax+i]=uncertainty;

    for (j = i + 1; j < number; j++) {
      r[i*intmax+j] = sqrt(SQ(posx[i] - posx[j]) + SQ(posy[i] - posy[j]) + SQ(posz[i] - posz[j]));
      r[j*intmax+i] = r[i*intmax+j];
      ri = ri_orig;
      rj = sqrt(SQ(posx[j]) + SQ(posy[j]) + SQ(posz[j]));;
      if (ri > rj) {
        k = j;
        l = i;
      } else {
        k = i;
        l = j;
      }

      /* vector (interaction point i -center) */
      Ax = - posx[k];
      Ay = - posy[k];
      Az = - posz[k];

      /* vector (interaction point i - interaction point j  */
      Bx = posx[l] - posx[k];
      By = posy[l] - posy[k];
      Bz = posz[l] - posz[k];
      costest = Ax * Bx + Ay * By + Az * Bz;
      costest1 = sqrt(SQ(Ax) + SQ(Ay) + SQ(Az));
      costest = costest / (costest1 * r[i*intmax+j]);

      /* norm of vector product = sintheta * distance center-i * distance i-j */
      vectproduct = sqrt(SQ((Ay * Bz) - (Az * By)) + SQ((Az * Bx) - (Ax * Bz)) + SQ((Ax * By) - (Ay * Bx)));

      /* distance from center to line joining points : rcenter = sintheta * distance center-i */
      r_center = vectproduct / r[i*intmax+j];
      r_ge[i*intmax+j] = r[i*intmax+j];
      if ((r_center < radius) && (costest > 0)) {
        /* if < radius and angle between i-center and i-j is */
        /*less than 90 degrees : the photon does not go through */
        /* Ge all the way from j to i */
        /* right triangle:  r_vacuum^2 + rcenter^2 = radius^2 */
        /* 2*r_vacuum = distance to take away from rmin */
        r_vacuum = sqrt(radiusSQ - SQ(r_center));
        r_ge[i*intmax+j] = r[i*intmax+j] - 2 * r_vacuum;
      }
      if (r_ge[i*intmax+j] < 0)
        r_ge[i*intmax+j] = resolution;
      r_ge[j*intmax+i] = r_ge[i*intmax+j];
    }
  }
}


/*
* Calcul de l erreur sur cosinus ABC a partir des incertitudes des positions
*/
double err_cos (double xa,
                double xb,
                double xc,
                double ya,
                double yb,
                double yc,
                double za,
                double zb,
                double zc,
                double rab,
                double rbc)
{
  /* error on AB.BC/AB x BC */

  double prod;
  double dcosdxa, dcosdxb, dcosdxc;
  double dcosdya, dcosdyb, dcosdyc;
  double dcosdza, dcosdzb, dcosdzc;
  double prodSrbcFrabCB;
  double prodSrabFrbcCB;
  double rabFrbc, inv_rabFrbc;


#ifdef OPTIM10
  long _xa,_xb,_xc,_ya,_yb,_yc,_za,_zb,_zc,_rab,_rbc; 
  long _rabFrbc, _prod;
  _xa = xa * 1000;
  _xb = xb * 1000;
  _xc = xc * 1000;
  _ya = ya * 1000;
  _yb = yb * 1000;
  _yc = yc * 1000;
  _za = za * 1000;
  _zb = zb * 1000;
  _zc = zc * 1000;
  _rab = rab * 1000;
  _rbc = rbc * 1000;

  _rabFrbc = (_rab * _rbc);
  inv_rabFrbc = 1000000./ _rabFrbc;
  _prod = (_xa - _xb) * (_xb - _xc) + (_ya - _yb) * (_yb - _yc) + (_za - _zb) * (_zb - _zc);
  prodSrbcFrabCB = _prod / ( _rabFrbc * _rab * _rab * 1000000);
  prodSrabFrbcCB = _prod / ( _rabFrbc * _rbc * _rbc * 1000000);

#else

  rabFrbc = (rab * rbc);

  inv_rabFrbc = 1./ rabFrbc;

  prod = (xa - xb) * (xb - xc) + (ya - yb) * (yb - yc) + (za - zb) * (zb - zc);

  prodSrbcFrabCB = prod / ( rabFrbc * rab * rab );
  prodSrabFrbcCB = prod / ( rabFrbc * rbc * rbc );
#endif

  dcosdxa = -(xc - xb) * inv_rabFrbc
    - (xa - xb) * prodSrbcFrabCB;

  dcosdxb = (xa - (xb + xb) + xc) * inv_rabFrbc 
    - (xb - xa) * prodSrbcFrabCB
    + (xc - xb) * prodSrabFrbcCB;

  dcosdxc = (xb - xa) * inv_rabFrbc 
    - (xc - xb) * prodSrabFrbcCB;

  dcosdya = -(yc - yb) * inv_rabFrbc
    -  (ya - yb) * prodSrbcFrabCB;

  dcosdyb = (ya - (yb + yb) + yc) * inv_rabFrbc
    - (yb - ya) * prodSrbcFrabCB
    + (yc - yb) * prodSrabFrbcCB;

  dcosdyc = (yb - ya) * inv_rabFrbc
    - (yc - yb) * prodSrabFrbcCB;

  dcosdza = -(zc - zb) * inv_rabFrbc
    - (za - zb) * prodSrbcFrabCB;

  dcosdzb = (za - (zb + zb) + zc) * inv_rabFrbc
    - (zb - za) * prodSrbcFrabCB
    + (zc - zb) * prodSrabFrbcCB;

  dcosdzc = (zb - za) * inv_rabFrbc
    - (zc - zb) * prodSrabFrbcCB;


  return ( sigma_thet * sqrt ( SQ(dcosdxa) + 
    SQ(dcosdxb) + 
    SQ(dcosdxc) + 
    SQ(dcosdya) + 
    SQ(dcosdyb) + 
    SQ(dcosdyc) + 
    SQ(dcosdza) + 
    SQ(dcosdzb) + 
    SQ(dcosdzc) ) );
}


